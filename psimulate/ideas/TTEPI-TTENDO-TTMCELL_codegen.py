import sympy as sym
from sympy import cos,sin,exp,log,sqrt,acos
import sympy.printing.ccode as ccode
import collections

show = "rates" # options: "constants", "rates" 
model = "ttepi"  # options: "ttepi" "ttendo" ""ttmcell"
const_repr = "vector" # options: "vector", "names", "values"
inter_repr = "vec" # options: "names", "vec"
state_repr = "vec" #options: names", "vec"

if not show in ("constants", "rates", "initial", "constvalues"):
	raise ValueError("unrecognised show option: " + show)
if not model in ( "ttepi", "ttendo", "ttmcell"):
	raise ValueError("unrecognised model option: " + model)
if not const_repr in ("vector", "names", "values"):
	raise ValueError("unrecognised const_repr option: " + const_repr)

print("// model: " + model)

N_var = 17
state_names = ["Voltage","Ca_i_total","Ca_SR_total","Na_i","K_i","m_gate","h_gate","j_gate",\
		"X_r1_gate", "X_r2_gate","X_s_gate","r_gate","s_gate","d_gate","f_gate","F_Ca","g_gate"]
assert(N_var == len(state_names))
if state_repr == "names":
	x = [sym.Symbol(s) for s in state_names]
if state_repr == "vec":
	x = [sym.Symbol("x["+str(i)+"]") for i in range(N_var)]
Voltage = x[0]
Ca_i_total = x[1]
Ca_SR_total = x[2]
Na_i = x[3]
K_i = x[4]
m_gate = x[5]
h_gate = x[6]
j_gate = x[7]
X_r1_gate = x[8]
X_r2_gate = x[9]
X_s_gate = x[10]
r_gate = x[11]
s_gate = x[12]
d_gate = x[13]
f_gate = x[14]
F_Ca = x[15]
g_gate = x[16]

#Constants
dict_constants = collections.OrderedDict([
	("Ko" , 5.4),
	("Cao" , 2.0),
	("Nao" , 140.0),
	("Vc" , 0.016404),
	("Vsr" , 0.001094),
	("Bufc" , 0.15),
	("Kbufc" , 0.001),
	("Bufsr" , 10.),
	("Kbufsr" , 0.3),
	("taufca" , 2.),
	("taug" , 2.),
	("Vmaxup" , 0.000425),
	("Kup" , 0.00025),
	("R" , 8314.472),
	("F" , 96485.3415),
	("T" , 310.0),
	("RTONF" , 0),# initialized later
	("CAPACITANCE" , 0.185),
	("Gkr" , 0.096),
	("pKNa" , 0.03),
	("Gks" , 0),# initialized later
	("GK1" , 5.405),
	("Gto" , 0.294),# initialized later
	("GNa" , 14.838),
	("GbNa" , 0.00029),
	("KmK" , 1.0),
	("KmNa" , 40.0),
	("knak" , 1.362),
	("GCaL" , 0.000175),
	("GbCa" , 0.000592),
	("knaca" , 1000),
	("KmNai" , 87.5),
	("KmCa" , 1.38),
	("ksat" , 0.1),
	("n" , 0.35),
	("GpCa" , 0.825),
	("KpCa" , 0.0005),
	("GpK" , 0.0146),
	("FONRT" , 0),# initialized later
	("inverseVcF2" , 0),
	("inverseVcF" , 0),
	("Kupsquare", 0) ])

dict_constants["RTONF"] = dict_constants["R"]*dict_constants["T"]/dict_constants["F"] 
if model == "ttepi":
	dict_constants["Gks"] = 0.245
	dict_constants["Gto"] = 0.294
elif model == "ttendo":
	dict_constants["Gks"] = 0.245
	dict_constants["Gto"] = 0.073
elif model == "ttmcell":
	dict_constants["Gks"] = 0.062
	dict_constants["Gto"] = 0.294
dict_constants["FONRT"] = dict_constants["F"] / (dict_constants["R"]*dict_constants["T"])
dict_constants["inverseVcF2"] = 1. / (2*dict_constants["Vc"]*dict_constants["F"])
dict_constants["inverseVcF"] = 1. / (dict_constants["Vc"]*dict_constants["F"])
dict_constants["Kupsquare"] = dict_constants["Kup"] * dict_constants["Kup"]

constant_values = dict_constants.values()
constant_names = [sym.Symbol(s) for s in dict_constants.keys()]
constant_vec = [sym.Symbol("constants["+str(i)+"]") for i in range(len(dict_constants))]
if const_repr == "values":
	constants_repr = constant_values
elif const_repr == "names":
	constants_repr = constant_names
elif const_repr == "vector":
	constants_repr = constant_vec
# for turning dict_constants into the following list
# kyyjpk0xxEd$Jde$h+

Ko = constants_repr[0]
Cao = constants_repr[1]
Nao = constants_repr[2]
Vc = constants_repr[3]
Vsr = constants_repr[4]
Bufc = constants_repr[5]
Kbufc = constants_repr[6]
Bufsr = constants_repr[7]
Kbufsr = constants_repr[8]
taufca = constants_repr[9]
taug = constants_repr[10]
Vmaxup = constants_repr[11]
Kup = constants_repr[12]
R = constants_repr[13]
F = constants_repr[14]
T = constants_repr[15]
RTONF = constants_repr[16]
CAPACITANCE = constants_repr[17]
Gkr = constants_repr[18]
pKNa = constants_repr[19]
Gks = constants_repr[20]
GK1 = constants_repr[21]
Gto = constants_repr[22]
GNa = constants_repr[23]
GbNa = constants_repr[24]
KmK = constants_repr[25]
KmNa = constants_repr[26]
knak = constants_repr[27]
GCaL = constants_repr[28]
GbCa = constants_repr[29]
knaca = constants_repr[30]
KmNai = constants_repr[31]
KmCa = constants_repr[32]
ksat = constants_repr[33]
n = constants_repr[34]
GpCa = constants_repr[35]
KpCa = constants_repr[36]
GpK = constants_repr[37]
FONRT = constants_repr[38]
inverseVcF2 = constants_repr[39]
inverseVcF = constants_repr[40]
Kupsquare = constants_repr[41]

#Help Variables/Functions
I_stim = sym.Symbol("I_stim") #THIS HAS TO BE A SYMBOL!

intermediate_names = ["bc", "Ca_i_free", "Ca_SR_free", "bjsr", "Ek", "Ena", "Ak1"]
intermediate_symbols = [sym.Symbol(i) for i in intermediate_names]
if (inter_repr=="names"):
	intermediate_repr = [sym.Symbol(i) for i in intermediate_names]
elif (inter_repr=="vec"):
	intermediate_repr = [sym.Symbol("intermediates["+str(i)+"]") for i in range(len(intermediate_names))]
# transforms "bc", "bjsr", ... to the following
#    ^M
# xEa  keld$kyyjpkJde$h+
bc = intermediate_repr[0]
Ca_i_free = intermediate_repr[1]
Ca_SR_free = intermediate_repr[2]
bjsr = intermediate_repr[3]
Ek = intermediate_repr[4]
Ena = intermediate_repr[5]
Ak1 = intermediate_repr[6]

###############################################################################
#  Equations
###############################################################################

bc_t = Bufc - Ca_i_total + Kbufc
cc = Kbufc * Ca_i_total
Ca_i_free_t = ( sqrt(bc*bc+4*cc) - bc) / 2
bjsr_t = Bufsr - Ca_SR_total + Kbufsr
cjsr = Kbufsr * Ca_SR_total
Ca_SR_free_t = ( sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2

Ek_t = RTONF * log( Ko / K_i )
Ena_t = RTONF * log( Nao / Na_i )
Eks = RTONF * log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i))
Eca = 0.5 * RTONF * log( Cao / Ca_i_free )
Ak1_t = 0.1 / ( 1.0 + exp( 0.06*(Voltage-Ek-200) ) )
Bk1 = ( 3. * exp( 0.0002*(Voltage-Ek+100) ) + exp(0.1*(Voltage-Ek-10)))  /  (1. + exp(-0.5*Voltage-Ek))
rec_iK1 = Ak1 / ( Ak1+Bk1 )
rec_iNaK = 1./( 1.+0.1245*exp(-0.1*Voltage*FONRT)+0.0353*exp(-Voltage*FONRT) )
rec_ipK = 1. / (1.+exp( (25-Voltage)/5.98) )
 
# steady state values and time constants ####################
#--\__
step_Ina = 1.0/(1+exp(sym.Mul(sym.Add(Voltage,40,evaluate=False),-41.66666666666666,evaluate=False),evaluate=False))

AM=1./(1.+exp((-60.-Voltage)/5.))
BM=0.1/(1.+exp((Voltage+35.)/5.))+0.10/(1.+exp((Voltage-50.)/200.))
TAU_M=AM*BM
M_INF=1./((1.+exp((-56.86-Voltage)/9.03))*(1.+exp((-56.86-Voltage)/9.03)))

#if (Voltage>=-40.)
AH_1 = 0
BH_1 = (0.77/(0.13*(1.+exp(-(Voltage+10.66)/11.1))))
TAU_H_1 = 1.0/(AH_1+BH_1)
#else
AH_2 = (0.057*exp(-(Voltage+80.)/6.8))
BH_2=(2.7*exp(0.079*Voltage)+(3.1e5)*exp(0.3485*Voltage))
TAU_H_2=1.0/(AH_2+BH_2)
#fi
TAU_H = (1-step_Ina) * TAU_H_1 + step_Ina * TAU_H_2
H_INF=1./((1.+exp((Voltage+71.55)/7.43))*(1.+exp((Voltage+71.55)/7.43)))

#if(Voltage>=-40.)
AJ_1 = 0.
BJ_1 = (0.6*exp((0.057)*Voltage)/(1.+exp(-0.1*(Voltage+32.))))
TAU_J_1 = 1.0/(AJ_1+BJ_1)
#else
AJ_2 = (((-2.5428e4)*exp(0.2444*Voltage)-(6.948e-6)* exp(-0.04391*Voltage))*(Voltage+37.78)/ (1.+exp(0.311*(Voltage+79.23))))
BJ_2 = (0.02424*exp(-0.01052*Voltage)/(1.+exp(-0.1378*(Voltage+40.14))))
TAU_J_2 = 1.0/(AJ_2+BJ_2)
#fi
TAU_J = (1-step_Ina) * TAU_J_1 + step_Ina * TAU_J_2
J_INF = H_INF

Xr1_INF = 1./(1.+exp((-26.-Voltage)/7.))
axr1 = 450./(1.+exp((-45.-Voltage)/10.))
bxr1 = 6./(1.+exp((Voltage-(-30.))/11.5))
TAU_Xr1 = axr1*bxr1
Xr2_INF = 1./(1.+exp((Voltage-(-88.))/24.))
axr2 = 3./(1.+exp((-60.-Voltage)/20.))
bxr2 = 1.12/(1.+exp((Voltage-60.)/20.))
TAU_Xr2 = axr2*bxr2

Xs_INF = 1./(1.+exp((-5.-Voltage)/14.))
Axs = 1100./(sqrt(1.+exp((-10.-Voltage)/6)))
Bxs = 1./(1.+exp((Voltage-60.)/20.))
TAU_Xs = Axs*Bxs
if model == "ttepi":
	R_INF=1./(1.+exp((20-Voltage)/6.))
	S_INF=1./(1.+exp((Voltage+20)/5.))
	TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8
	TAU_S=85.*exp(-(Voltage+45.)*(Voltage+45.)/320.)+5./(1.+exp((Voltage-20.)/5.))+3.
elif model == "ttendo":
	R_INF=1./(1.+exp((20-Voltage)/6.))
	S_INF=1./(1.+exp((Voltage+28)/5.))
	TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8
	TAU_S=1000.*exp(-(Voltage+67)*(Voltage+67)/1000.)+8.
elif model == "ttmcell":
	R_INF=1./(1.+exp((20-Voltage)/6.))
	S_INF=1./(1.+exp((Voltage+20)/5.))
	TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8
	TAU_S=85.*exp(-(Voltage+45.)*(Voltage+45.)/320.)+5./(1.+exp((Voltage-20.)/5.))+3.
D_INF = 1./(1.+exp((-5-Voltage)/7.5))
Ad = 1.4/(1.+exp((-35-Voltage)/13))+0.25
Bd = 1.4/(1.+exp((Voltage+5)/5))
Cd = 1./(1.+exp((50-Voltage)/20))
TAU_D = Ad*Bd+Cd
F_INF = 1./(1.+exp((Voltage+20)/7))
TAU_F = 1125*exp(-(Voltage+27)*(Voltage+27)/300)+80+165/(1.+exp((25-Voltage)/10))
     
FCa_INF = (1./(1.+(Ca_i_free/0.000325)**8)+ 0.1/(1.+exp((Ca_i_free-0.0005)/0.0001))+ 0.20/(1.+exp((Ca_i_free-0.00075)/0.0008))+ 0.23 )/1.46
#--\__
step_Irel = 1.0/(1+exp(sym.Mul(sym.Add(Ca_i_free,-0.00035,evaluate=False),-41.66666666666666,evaluate=False),evaluate=False))
# if(Ca_i_free<0.00035)
G_INF_1 = 1./(1.+pow((Ca_i_free/0.00035),6))
# else
G_INF_2 = 1./(1.+pow((Ca_i_free/0.00035),16))
#fi
G_INF = step_Irel*G_INF_1 + (1-step_Irel)*G_INF_2

# currents ################################################# 
INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena)
ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( exp(2*Voltage*FONRT)-1.)
Ito = Gto*r_gate*s_gate*(Voltage-Ek)
IKr = Gkr*sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek)
IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks)
IK1 = GK1*rec_iK1*(Voltage-Ek)
INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*exp((n-1)*Voltage*FONRT)))*(exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5)
INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK
IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free)
IpK = GpK*rec_ipK*(Voltage-Ek)
IbNa = GbNa*(Voltage-Ena)
IbCa = GbCa*(Voltage-Eca)

sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK + I_stim

Ileak = 0.00008 * (Ca_SR_free - Ca_i_free)
SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) )
CaSRsquare = Ca_SR_free * Ca_SR_free
A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232
Irel = A * d_gate * g_gate

# voltage rate ################################################
Voltage_rate = - sItot

# concentration rates #########################################
CaCurrent = - ( ICaL + IbCa + IpCa - 2*INaCa ) * inverseVcF2 * CAPACITANCE
CaSRCurrent = SERCA - Irel - Ileak
Ca_i_total_rate = CaCurrent - CaSRCurrent
Ca_SR_total_rate = Vc/Vsr * CaSRCurrent
Na_i_rate = - ( INa + IbNa + 3*INaK + 3*INaCa ) * inverseVcF * CAPACITANCE
K_i_rate = - ( I_stim + IK1 + Ito + IKr + IKs - 2*INaK + IpK) * inverseVcF * CAPACITANCE 

# gating rates ################################################
m_gate_rate = ( M_INF - m_gate ) / TAU_M
h_gate_rate = ( H_INF - h_gate ) / TAU_H
j_gate_rate = ( J_INF - j_gate ) / TAU_J
X_r1_gate_rate = ( Xr1_INF - X_r1_gate ) / TAU_Xr1
X_r2_gate_rate = ( Xr2_INF - X_r2_gate ) / TAU_Xr2
X_s_gate_rate = ( Xs_INF - X_s_gate ) / TAU_Xs
r_gate_rate = ( R_INF - r_gate ) / TAU_R
s_gate_rate = ( S_INF - s_gate ) / TAU_S
d_gate_rate = ( D_INF - d_gate ) / TAU_D
f_gate_rate = ( F_INF - f_gate ) / TAU_F
step_FCa = 1.0 / (1+exp(sym.Mul(sym.Add(Voltage,60,evaluate=False),41.66666666666666\
		,evaluate=False),evaluate=False)) * \
		1.0 / (1+exp(sym.Mul(sym.Add(FCa_INF,-F_Ca,evaluate=False),5000\
		,evaluate=False),evaluate=False))
F_Ca_rate = step_FCa * ( FCa_INF - F_Ca ) / taufca
step_g_gate = 1.0 / (1+exp(sym.Mul(sym.Add(Voltage,60,evaluate=False),41.66666666666666\
		,evaluate=False),evaluate=False)) * \
		1.0 / (1+exp(sym.Mul(sym.Add(G_INF,-g_gate,evaluate=False),5000\
		,evaluate=False),evaluate=False))
g_gate_rate = step_g_gate * ( G_INF - g_gate ) / taug
    
###############################################################################
#Printing
###############################################################################

intermediates_t = [bc_t, Ca_i_free_t, Ca_SR_free_t, bjsr_t, Ek_t, Ena_t, Ak1_t]
rates = [Voltage_rate, Ca_i_total_rate, Ca_SR_total_rate, Na_i_rate, K_i_rate,\
		m_gate_rate, h_gate_rate, j_gate_rate, X_r1_gate_rate, X_r2_gate_rate,\
		X_s_gate_rate, r_gate_rate, s_gate_rate, d_gate_rate, f_gate_rate, F_Ca_rate, g_gate_rate]

#Rates:
if show == "rates":
	print("// intermediate results:")
	for i in range(len(intermediates_t)):
		print(str(intermediate_repr[i]) + " = " + str(ccode(intermediates_t[i])) + ";")
	print("\n// rates:")
	for i in range(len(rates)):
		print("dxdt["+str(i)+"] = " + str(ccode(rates[i])) + ";")

#Constants
elif show == "constants":
	# print("// " + str(len(dict_constants)) + "constants in dictionary:")
	for i in range(len(constant_values)):
		#print("constants[" + str(i) + "] = " + str(constant_values[i]) + "; // " + str(constant_names[i]))
		print(str(constant_values[i]) + ", // " + str(constant_names[i]) + " [" + str(i)+"]")

else:
	print("Unrecognised option: " + show)

