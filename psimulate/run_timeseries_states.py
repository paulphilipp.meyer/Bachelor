'''
runs bin/tim_sn with the parameters specified further down
can also be submitted to the cluster/sun grid engin/sun grid enginee via
	qsub psimulate/run_timeseries_states.py
'''
# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime
init_state_str = "not defined"

################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "tims_TT-BrS"

model = "xiaepi_bs"
cl = 1000
N_beats = 5000
skip_beats = 0
debug = 2
#init_state= "None" # use default simulation state, other states can be found in data/states

# xiaepibs1000_2 last from cl[20] 1000.0ms from beef_xiaepi_bs_23 -> chaotic?
init_state_str = "xiaepibs1000_2"
init_state = "-86.9563237497, 0.00689531576495, 3.76820890771, 8.81668966859, 140.623155845, 0.00118740467179, 0.789083222525, 0.789080375802, 0.000165224869653, 0.489130073138, 0.00286013303638, 1.81235620739e-08, 0.999998471561, 1.79570510621e-05, 0.999929603593, 1.00698194016, 0.999993481902, 0.00111462957863, 0.317307829043"


dt = 0.001
abserr = 1e-8
relerr = 1e-8


#################################################################################

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("running psimulate/run_timeseries_states.py")
doc("ID %d" % id)
if (id == 0):
	doc("Running on %s in window %s" % (os.getenv("HOSTNAME"), os.getenv("WINDOW")))
else:
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))


doc("\ncalculating timeseries\n")
doc("saving to %s" % directory)
doc("model:          %s" % model)
doc("cl:             %f" % cl)
doc("N_beats:        %d" % N_beats)
doc("skip_beats:     %d" % skip_beats)
doc("init_state_str: " + init_state_str)
doc("dt:             %f" % dt)
doc("abserr:         %e" % abserr)
doc("relerr:         %e" % relerr)

# executing program
t0 = datetime.datetime.now()
doc("started: %s\n" % str(t0))
doc_file.flush()
sys.stdout.flush()
#call(["/home/pmeyer/brugada_code/bin/test",
#call(["/home/pmeyer/brugada_code/bin/tim_s",
#call(["/home/pmeyer/brugada_code/bin/tim_errcontrol",
call(["/home/pmeyer/brugada_code/bin/tim_sn",
#call(["/home/pmeyer/brugada_code/bin/tim_dopri",
    model,
	str(cl),
    str(N_beats),
    str(skip_beats),
    directory,
	str(debug),
	init_state,
	str(dt),
	str(abserr),
	str(relerr)
    ])
t1 = datetime.datetime.now()
doc("\nfinished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
doc("saved to %s" % directory)
