# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 30-51
#$ -tc 30

import os
import sys
from subprocess import call
import datetime
import numpy as np
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))
if id==0:
	id = 3
################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "threshold_wt"

model = "wt"
T = 300
debug = 3
dt = 0.001
I_stims = np.linspace(60, 70, 21)
I_stim = I_stims[id-30]

#init_state= "None" # use default simulation state, other states can be found in data/states
# wtss_1
init_state_str = "wtss_1"
init_state = "-89.8847, 0.994945, 0.000681152, 0.996451, 0, 0.99983, 0.000109665, 0.0113366, 12.5963, 143.355, 7.19183, 1.19728, 0.00418464, 0.000887366, 0.995211, 0.00418464, 0"

#################################################################################

# determining directory and job variables
directory = dir_data + "/" + folder + "_" + str(id)
try:
	os.makedirs(directory)
except OSError:
	sys.exit("couldn't make directory " % directory)


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("ID %d" % id)
doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
doc("This job was submitted from %s, it is currently running on %s"
      % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\ncalculating timeseries\n")
doc("saving to %s" % directory)
doc("model: %s" % model)
doc("T: %f" % T)
doc("init_state: " + init_state)
doc("dt: %f" % dt)
doc("I_stim: %f" % I_stim)

# executing program
t0 = datetime.datetime.now()
doc("started: %s\n" % str(t0))
call(["/home/pmeyer/brugada_code/bin/threshold",
    model,
	str(T),
    directory,
	str(debug),
	init_state,
	str(dt),
	str(I_stim)
    ])
t1 = datetime.datetime.now()
doc("\nfinished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
doc("saved to %s" % directory)
