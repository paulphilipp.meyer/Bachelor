# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime

################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "test"

model = "xiaepi_bs"
T = 300
debug = 3
#init_state= "None" # use default simulation state, other states can be found in data/states
# xiaepi1500_1 last from einschwingen_xiaepi_3
init_state_str = "xiaepi1500_1"
init_state = "-86.8000386749, 0.0116035358227, 7.47672044253, 8.6928423543, 140.242955923, 0.00122774821588, 0.785347690026, 0.785329635023, 0.000168965104923, 0.487502918113, 0.00289215128742, 1.86019550161e-08, 0.999998423004, 1.83351691162e-05, 0.999927859934, 1.00539453724, 0.999819646744, 0.00114418898201, 0.309501831329"
dt = 0.001
abserr = 1e-8
relerr = 1e-8

#################################################################################

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("ID %d" % id)
doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
doc("This job was submitted from %s, it is currently running on %s"
      % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\ncalculating timeseries\n")
doc("saving to %s" % directory)
doc("model: %s" % model)
doc("T: %f" % T)
doc("init_state: " + init_state)
doc("dt: %f" % dt)
doc("abserr: %e" % abserr)
doc("relerr: %e" % relerr)

# executing program
t0 = datetime.datetime.now()
doc("started: %s\n" % str(t0))
call(["/home/pmeyer/brugada_code/bin/tim_simple",
    model,
	str(T),
    directory,
	str(debug),
	init_state,
	str(dt),
	str(abserr),
	str(relerr)
    ])
t1 = datetime.datetime.now()
doc("\nfinished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
doc("saved to %s" % directory)
