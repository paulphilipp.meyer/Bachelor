# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime

################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "tims_exp_TT"

debug = 2
#init_state= "None" # use default simulation state, other states can be found in data/states

severity = 0

# xiaepi1000_1 last from einschwingen_xiaepi_1
init_state_str = "xiaepi1000_1"
init_state = "-86.5258234323, 0.0153547151776, 8.97095781061, 10.0383584239, 138.690665024, 0.00130181111298, 0.778667374989, 0.778569759578, 0.000175821160157, 0.484648599904, 0.00294919614454, 1.94720530113e-08, 0.999998334051, 1.90179506221e-05, 0.999783005391, 1.00381326619, 0.998861785669, 0.00119792709109, 0.268965710337"

dt = 0.001
abserr = 1e-8
relerr = 1e-8

#################################################################################

#shape = 4
#samples = np.random.gamma(shape, scale=1/shape, size = 100000)

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("running psimulate/run_tim_2.py")
doc("ID %d" % id)
if (id == 0):
	doc("Running on %s in window %s" % (os.getenv("HOSTNAME"), os.getenv("WINDOW")))
else:
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\ncalculating timeseries\n")
doc("saving to %s" % directory)
doc("init_state: " + init_state)
doc("dt:        %f" % dt)
doc("abserr:    %e" % abserr)
doc("relerr:    %e" % relerr)
doc("severity:  %f" % severity)

# executing program
t0 = datetime.datetime.now()
doc("started: %s\n" % str(t0))
call(["/home/pmeyer/brugada_code/bin/tim_2",
    directory,
	str(debug),
	init_state,
	str(dt),
	str(abserr),
	str(relerr),
	str(severity)
    ])
t1 = datetime.datetime.now()
doc("\nfinished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
doc("saved to %s" % directory)
