# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1-6
#$ -tc 20
icls = (0, 500, 1500, 3500, 5500,8500)

import os
import sys
from subprocess import call
import datetime
import numpy as np


init_state_str = "not defined"
#id = 1
id = int(os.getenv("SGE_TASK_ID", 0))

################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"

folder = "spot_LR-WTM"

model = "wtm"
N_beats = 20
skip_beats = 0
debug = 2
save_buffer_size = 100
save_voltages = 1
dt = 0.001

fromfile = "bif_wtm_0"

################################################################################
# pick startstate and cl
i_cl = icls[id-1]
directory = dir_data + "/" + folder + '_' + str(id)
fromdirectory = dir_data + "/" + fromfile + "/"
try:
	os.makedirs(directory)
except OSError:
	print("error creating directory"+directory)

doc_file = open(directory+"/info.txt", "w")
def doc(string):
	print(string)
	doc_file.write(string+"\n")

cl = np.load(fromdirectory + 'cl.npy', mmap_mode='r')[i_cl]
last_state = np.load(fromdirectory+'last_states.npy', mmap_mode='r')[i_cl]
pick_lstate_str = ""
for value in last_state:
	exponent = np.floor(np.log10(np.abs(value)))
	if np.abs(exponent) > 300:
		if exponent < -300:
			value = 1e-300
		else:
			doc("value bigger than 1e300 encountered")
	pick_lstate_str += str(value)
	pick_lstate_str += ", "
init_state = pick_lstate_str[:-2]


# determining directory and job variables


first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))
screen_window = int(os.getenv("WINDOW", 0))


# documenting
doc("running psimulate/run_timm_from_bif.py")
doc("ID %d" % id)
if (id == 0):
	doc("Running on %s in window %s" % (os.getenv("HOSTNAME"), os.getenv("WINDOW")))
else:
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))


 
doc("\nsaving to %s\n" % directory)
doc("fromfile:         %s" % fromfile)
doc("model:            %s" % model)
doc("cl:               %d" % cl)
doc("N_beats:          %d" % N_beats)
doc("skip_beats:_______%d" % skip_beats)
doc("save_buffer_size: %d" % save_buffer_size)
doc("save_voltages:    %d" % save_voltages)
doc("dt:               %f" % dt)
doc("i_cl:             %d" % i_cl)
doc("init_state:\n%s" % init_state)

args = [model,
		str(cl),
		str(N_beats),
		str(skip_beats),
		directory,
		str(debug),
		init_state,
		str(dt),
		str(save_buffer_size),
		str(save_voltages)]

# executing program
t0 = datetime.datetime.now()
doc("\nstarted:    %s" % str(t0))
doc_file.flush()
sys.stdout.flush()
call(["/home/pmeyer/brugada_code/bin/tim_meas",] + args)
t1 = datetime.datetime.now()
doc("finished:   %s" % str(t1))
doc("runtime:    %s" % str(t1-t0))
print("saved to %s" % directory)

