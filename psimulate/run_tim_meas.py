# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime
init_state_str = "not defined"


################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "timm_TT-BrS"


model = "xiaepi_bs"
cl = 1000
N_beats = 500
skip_beats = 0
debug = 2
save_buffer_size = 10
save_voltages = 0
dt = 0.001
init_state = "None"


# xiaepibs1250_1 last from ein_xiaepi_bs_10
#init_state_str = "xiaepibs1250_1"
#init_state = "-87.0349021494, 0.00669685921779, 3.59079926748, 8.25664077072, 141.20725574, 0.00116761836383, 0.790940952996, 0.790936939049, 0.000163381026897, 0.489948241589, 0.00284417024371, 1.78877533128e-08, 0.999998495394, 1.77698981093e-05, 0.999915817363, 1.00704348668, 0.999994573536, 0.00110005243883, 0.300509788019"

## xiaepibs400_3 last from cl[124] 400.446428571ms from beef_xiaepi_bs_1
#init_state_str = "xiaepibs400_3"
#init_state = "-86.5840132415, 0.00921503154887, 4.2681015648, 11.5728464173, 137.795726448, 0.00128573669792, 0.780106473333, 0.771280141194, 0.000212544603314, 0.485254766747, 0.0029369690944, 1.9283213746e-08, 0.999998353535, 1.88708945851e-05, 0.999229072367, 1.00622609121, 0.999958478679, 0.00118631999989, 0.269347901228"

## xiaepibs400_2 last from ein_xiaepi_bs_1
#init_state_str = "xiaepibs400_2"
#init_state = "-86.5805930485, 0.00927264042272, 4.36425698005, 11.5818510911, 137.773781544, 0.0012866769155, 0.780022311795, 0.772217638518, 0.00020313209331, 0.485219144222, 0.00293768598379, 1.92942513946e-08, 0.999998352398, 1.88795055478e-05, 0.999347354999, 1.00620688241, 0.999956808032, 0.00118699915426, 0.259156223354"

################################################################################

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)


id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))
screen_window = int(os.getenv("WINDOW", 0))

# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("running psimulate/run_tim_meas")
doc("ID %d" % id)
if (id == 0):
	doc("Running on %s in window %s" % (os.getenv("HOSTNAME"), os.getenv("WINDOW")))
else:
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))


 

doc("\nsaving to %s\n" % directory)
doc("model:            %s" % model)
doc("cl:               %d" % cl)
doc("N_beats:          %d" % N_beats)
doc("skip_beats:_______%d" % skip_beats)
doc("init_state_str:   %s" % init_state_str)
doc("save_buffer_size: %d" % save_buffer_size)
doc("save_voltages:    %d" % save_voltages)
doc("dt:               %f" % dt)

args = [model,
		str(cl),
		str(N_beats),
		str(skip_beats),
		directory,
		str(debug),
		init_state,
		str(dt),
		str(save_buffer_size),
		str(save_voltages)]

# executing program
t0 = datetime.datetime.now()
doc("\nstarted:    %s" % str(t0))
doc_file.flush()
sys.stdout.flush()
call(["/home/pmeyer/brugada_code/bin/tim_meas",] + args)
t1 = datetime.datetime.now()
doc("finished:   %s" % str(t1))
doc("runtime:    %s" % str(t1-t0))
print("saved to %s" % directory)

