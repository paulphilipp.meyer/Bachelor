'''
runs bin/bifn with the parameters specified further down
can also be submitted to the cluster/sun grid engin/sun grid enginee via
	qsub psimulate/run_bif_diag.py
'''
# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime


################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "test"

model = "wtm"
cl_start = 150
cl_stop = 1000
cl_steps = 851
N_beats = 200
skip_beats = 0
init_skip_beats = 0
init_each = 0
debug = 2
#init_state = "None"

# wtm150_3 
init_state_str = "wtm150_3"
init_state = "-88.9030664287, 1.96576284944e-89, 0.870629604266, 0.0297690784836, 0.0425016756689, 20.2487359177, 135.855122222, 7.01633525949, 3.46879348192, 0.0348860575208, 0.00104152412251, 0.591880820551, 0.0411982645423, 2.91388943243e-10, 0.669847477637, 0.0115843399963, 0.26240629128, 0.00354044490874, 0.000170238176482, 0.079873248875, 0.0410112745927, 0.605260049672, 0.00770125221643, 3.715965326e-05, 4.07663139974e-08"


dt = 0.001

################################################################################

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)

id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("running psimulate/run_bif_diag.py")
doc("ID %d" % id)
if (id == 0):
	doc("Running on %s in window %s" % (os.getenv("HOSTNAME"), os.getenv("WINDOW")))
else:
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\nsaving to %s\n" % directory)
doc("model:           %s" % model)
doc("cl_start:        %f" % cl_start)
doc("cl_stop:         %f" % cl_stop)
doc("cl_steps:________%f" % cl_steps)
doc("N_beats:         %d" % N_beats)
doc("skip_beats:      %d" % skip_beats)
doc("init_skip_beats:_%d" % init_skip_beats)
doc("init_state_str:  " + init_state_str)
doc("dt:              %f" % dt)


args = [model,
		str(cl_start),
		str(cl_stop),
		str(cl_steps),
		str(N_beats),
		str(skip_beats),
		str(init_skip_beats),
		str(init_each),
		directory,
		str(debug),
		init_state,
		str(dt)]

# executing program
t0 = datetime.datetime.now()
doc("started: %s" % str(t0))
doc_file.flush()
sys.stdout.flush()
#call(["/home/pmeyer/brugada_code/bin/bif",] + args)
call(["/home/pmeyer/brugada_code/bin/bifn",] + args)
t1 = datetime.datetime.now()
doc("finished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
print("saved to %s" % directory)

