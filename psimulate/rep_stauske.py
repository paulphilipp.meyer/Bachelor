'''
runs bin/tim_sn with the parameters specified further down
can also be submitted to the cluster/sun grid engin/sun grid enginee via
	qsub psimulate/run_timeseries_states.py
'''
# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 10
#$ -tc 10

import os
import sys
from subprocess import call
import datetime
import random
random.seed()
init_state_str = "not defined"

################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "ein_xiaepi_bs"

model = "xiaepi_bs"
cl = 1148
N_beats = 50
skip_beats = 300
debug = 3
#init_state= "None" # use default simulation state, other states can be found in data/states

# xiaepibs1000|1 last from einschwingen_xiaepi_bs_4
init_state_str = "xiaepibs1000|1"
init_state = [-86.9565643404, 0.00690981195686, 3.7854028918, 8.81207663639, 140.625446486, 0.00118734356685, 0.789088927834, 0.789086014932, 0.000165219235213, 0.489132577855, 0.00286008403876, 1.81228357478e-08, 0.999998471634, 1.7956475074e-05, 0.999929615228, 1.00697742882, 0.999993395322, 0.00111458465788, 0.317221891232]

dt = 0.001
abserr = 1e-8
relerr = 1e-8

shape = 20
rand_modifier = np.random.gamma(shape, scale=1/shape, size = len(init_state))
for i in range(len(init_state)):
	rand_modifier.append(random.gauss(0, 0.05))


#################################################################################
return
# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("ID %d" % id)
doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
doc("This job was submitted from %s, it is currently running on %s"
      % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\ncalculating timeseries\n")
doc("saving to %s" % directory)
doc("model: %s" % model)
doc("cl: %f" % cl)
doc("N_beats: %d" % N_beats)
doc("skip_beats: %d" % skip_beats)
doc("init_state_str: " + init_state_str)
doc("init_state: " + init_state)
doc("dt: %f" % dt)
doc("abserr: %e" % abserr)
doc("relerr: %e" % relerr)

# executing program
t0 = datetime.datetime.now()
doc("started: %s\n" % str(t0))
doc_file.flush()
#call(["/home/pmeyer/brugada_code/bin/test",
#call(["/home/pmeyer/brugada_code/bin/tim_s",
#call(["/home/pmeyer/brugada_code/bin/tim_errcontrol",
call(["/home/pmeyer/brugada_code/bin/tim_sn",
#call(["/home/pmeyer/brugada_code/bin/tim_dopri",
    model,
	str(cl),
    str(N_beats),
    str(skip_beats),
    directory,
	str(debug),
	init_state,
	str(dt),
	str(abserr),
	str(relerr)
    ])
t1 = datetime.datetime.now()
doc("\nfinished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
doc("saved to %s" % directory)
