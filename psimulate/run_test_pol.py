# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime

################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "pol"

mu = 10
T = 100
init_state = "2, 0"
dt = 0.001
abserr = 1e-8
relerr = 1e-8

debug = 3


#################################################################################

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("ID %d" % id)
doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
doc("This job was submitted from %s, it is currently running on %s"
      % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("running psimulate/run_test_pol.py")
doc("saving to %s" % directory)
doc("mu: %f" % mu)
doc("T: %f" % T)
doc("init_state: " + init_state)
doc("dt: %f" % dt)
doc("abserr: %e" % abserr)
doc("relerr: %e" % relerr)

# executing program
t0 = datetime.datetime.now()
doc("started: %s\n" % str(t0))

doc("args: " +" "+ str(mu) +" "+ str(T) +" "+ dir_data +" "+ str(debug) +" "+ init_state +" "+ str(dt) +" "+ str(abserr) +" "+ str(relerr))

call(["/home/pmeyer/brugada_code/bin/test_pol",
	str(mu),
	str(T),
	directory,
	str(debug),
	init_state,
	str(dt),
	str(abserr),
	str(relerr)
	])

t1 = datetime.datetime.now()
doc("\nfinished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
doc("saved to %s" % directory)
