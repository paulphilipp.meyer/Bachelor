import sympy as sym
from sympy import cos,sin,exp,log,sqrt,acos
import sympy.printing.ccode as ccode
import sympy.printing.cxxcode as cxxcode

show = "rates" # options: "constants", "rates", "constvalues"
model = "wt"  # options: "bs", "wt", "wtm"
const_repr = "values" # options: "vector", "names", "values"

if not show in ("constants", "rates", "initial", "constvalues"):
	raise ValueError("unrecognised show option: " + show)
if not model in ( "bs", "wt", "wtm"):
	raise ValueError("unrecognised model option: " + model)
if not const_repr in ("vector", "names", "values"):
	raise ValueError("unrecognised const_repr option: " + const_repr)

print("// model: " + model)

if model == "wt":

	N_var = 17


	s = ["Voltage","h_gate","m_gate","j_gate","d_gate","f_gate","X_r","Ca_i","Na_i","K_i","Ca_JSR","Ca_NSR","X_s","b_gate","g_gate","X_s2","I_rel"]
	# x = [sym.Symbol(i) for i in s]
	x = [sym.Symbol("x[" + str(i) + "]") for i in range(N_var)]
	Voltage = x[0]
	h_gate = x[1]
	m_gate = x[2]
	j_gate = x[3]
	d_gate = x[4]
	f_gate = x[5]
	X_r = x[6]
	Ca_i = x[7]
	Na_i = x[8]
	K_i = x[9]
	Ca_JSR = x[10]
	Ca_NSR = x[11]
	X_s = x[12]
	b_gate = x[13]
	g_gate = x[14]
	X_s2 = x[15]
	I_rel = x[16]

	x_rate = [sym.Symbol(i + "_rate") for i in s]

	Voltage_rate =x_rate[0]
	h_gate_rate = x_rate[1]
	m_gate_rate = x_rate[2]
	j_gate_rate = x_rate[3]
	d_gate_rate = x_rate[4]
	f_gate_rate = x_rate[5]
	X_r_rate = x_rate[6]
	Ca_i_rate = x_rate[7]
	Na_i_rate = x_rate[8]
	K_i_rate = x_rate[9]
	Ca_JSR_rate = x_rate[10]
	Ca_NSR_rate = x_rate[11]
	X_s_rate = x_rate[12]
	b_gate_rate = x_rate[13]
	g_gate_rate = x_rate[14]
	X_s2_rate = x_rate[15]
	I_rel_rate = x_rate[16]


elif model == "bs" or model == "wtm":

	N_var = 16

	inital_conditions = [-89.9863526453,0,0.9998361223,0.00010819,0.0113541899,12.6287781918,143.9032192933,7.1976768273,1.1992257705,0.0041593655,0.000879064,0.9952963692,0.0041593655,7.9050503334599447068e-323,0.9999848527,0.0108622692,0.2647239905,0.0029986711,1.26611666411852E-05,3.10466004246949E-06,4.35027112113673E-08,0.7239492459,0.0082005628,3.46249012981194E-05,2.33634992229991E-08,7.62051838475029E-05,8.63217135543962E-07,3.64472645555614E-09,2.45931569494852E-12]

	s = ["Voltage","d_gate","f_gate","X_r","Ca_i","Na_i","K_i","Ca_JSR","Ca_NSR","X_s","b_gate","g_gate","X_s2","I_rel","y_gate","z_gate"]
	# x = [sym.Symbol(i) for i in s]
	x = [sym.Symbol("x[" + str(i) + "]") for i in range(N_var)]
	Voltage = x[0]
	d_gate = x[1]
	f_gate = x[2]
	X_r = x[3]
	Ca_i = x[4]
	Na_i = x[5]
	K_i = x[6]
	Ca_JSR = x[7]
	Ca_NSR = x[8]
	X_s = x[9]
	b_gate = x[10]
	g_gate = x[11]
	X_s2 = x[12]
	I_rel = x[13]
	y_gate = x[14]
	z_gate = x[15]

	x_rate = [sym.Symbol(i + "_rate") for i in s]

	Voltage_rate = x_rate[0]
	d_gate_rate = x_rate[1]
	f_gate_rate = x_rate[2]
	X_r_rate = x_rate[3]
	Ca_i_rate = x_rate[4]
	Na_i_rate = x_rate[5]
	K_i_rate = x_rate[6]
	Ca_JSR_rate = x_rate[7]
	Ca_NSR_rate = x_rate[8]
	X_s_rate = x_rate[9]
	b_gate_rate = x_rate[10]
	g_gate_rate = x_rate[11]
	X_s2_rate = x_rate[12]
	I_rel_rate = x_rate[13]
	y_gate_rate = x_rate[14]
	z_gate_rate = x_rate[15]
	


#Help Variables/Functions
I_stim = sym.Symbol("I_stim") #THIS HAS TO BE A SYMBOL!
b_myo = sym.Function("b_myo")
c_myo = sym.Function("c_myo")
d_myo = sym.Function("d_myo")
Ca_i_free = sym.Function("Ca_i_free")
b_JSR = sym.Function("b_JSR")
c_JSR = sym.Function("c_JSR")
Ca_JSR_free = sym.Function("Ca_JSR_free")
tau_d = sym.Function("tau_d")
d_inf = sym.Function("d_inf")
f_inf = sym.Function("f_inf")
tau_f = sym.Function("tau_f")
f_Ca = sym.Function("f_Ca")
I_LBar_Ca = sym.Function("I_LBar_Ca")
I_LBar_Na = sym.Function("I_LBar_Na")
I_LBar_K = sym.Function("I_LBar_K")
I_L_Ca = sym.Function("I_L_Ca")
I_L_Na = sym.Function("I_L_Na")
I_L_K = sym.Function("I_L_K")
E_Na = sym.Function("E_Na")
step_function_Na = sym.Function("step_function_Na")
aux = sym.Function("aux")
sNa = sym.Function("sNa")
alpha_h = sym.Function("alpha_h")
beta_h = sym.Function("beta_h")
alpha_j = sym.Function("alpha_j")
beta_j = sym.Function("beta_j")
alpha_m = sym.Function("alpha_m")
beta_m = sym.Function("beta_m")
I_Na = sym.Function("I_Na")
I_Na_b = sym.Function("I_Na_b")
step_function_CaT = sym.Function("step_function_CaT")
aux = sym.Function("aux")
b_inf = sym.Function("b_inf")
tau_b = sym.Function("tau_b")
g_inf = sym.Function("g_inf")
tau_g = sym.Function("tau_g")
E_Ca = sym.Function("E_Ca")
I_CaT = sym.Function("I_CaT")
G_K1 = sym.Function("G_K1")
E_K = sym.Function("E_K")
alpha_K1 = sym.Function("alpha_K1")
beta_K1 = sym.Function("beta_K1")
I_K1 = sym.Function("I_K1")
I_Kp = sym.Function("I_Kp")
G_Ks = sym.Function("G_Ks")
E_Ks = sym.Function("E_Ks")
Xs_inf = sym.Function("Xs_inf")
tau_Xs = sym.Function("tau_Xs")
I_Ks = sym.Function("I_Ks")
R_gate = sym.Function("R_gate")
Xr_inf = sym.Function("Xr_inf")
tau_X_r = sym.Function("tau_X_r")
I_Kr = sym.Function("I_Kr")
I_pCa = sym.Function("I_pCa")
I_Ca_b = sym.Function("I_Ca_b")
I_leak = sym.Function("I_leak")
I_up = sym.Function("I_up")
I_tr = sym.Function("I_tr")
I_NaCa = sym.Function("I_NaCa")
sigma_NaK = sym.Function("sigma_NaK")
f_NaK = sym.Function("f_NaK")
I_NaK = sym.Function("I_NaK")
I_rel_inf = sym.Function("I_rel_inf")
tau_rel = sym.Function("tau_rel")
I_Ca_tot = sym.Function("I_Ca_tot")
I_Na_tot = sym.Function("I_Na_tot")
I_K_tot = sym.Function("I_K_tot")

I_to = sym.Function("I_to")
alpha_z = sym.Function("alpha_z")
beta_z = sym.Function("beta_z")
alpha_y = sym.Function("alpha_y")
beta_y = sym.Function("beta_y")
R_to  = sym.Function("R_to")

#Constants


#This is the opposite of elegant, sorry....You might wanna change that...
#	constant_values = [140,4.5,1.8,96485,0.037435883507802616,0.00054,1,0.341,2,1.93E-07,0.75,0.75,1,6.75E-07,0.75,0.75,1,0.0006,0.004,16,0.05,0.75,0.00552,0.433,0.01833,0.02614,1.15,0.0005,0.003016,0.00875,15,0.00092,120,2.25,10,1.5,0.59375,9,4.75,1,0.05,0.07,10,0.00238,0.0005,0.8,1.59025118102644E-09,2.58490243537385E-05,2.09833256518583E-06,1.82463701320507E-07,0.00025,0.0001,0.15,1.1,23.5]
constant_values = [140,4.5,1.8,96485,0.037435883507802616,0.00054,1,0.341,2,1.93E-07,0.75,0.75,1,6.75E-07,0.75,0.75,1,0.0006,0.004,16,0.05,0.75,0.00552,0.433,0.01833,0.02614,1.15,0.0005,0.003016,0.00875,15,0.00092,120,2.25,10,1.5,0.59375,9,4.75,1,0.05,0.07,10,0.00238,0.0005,0.8,1.59025118102644E-09,2.58490243537385E-05,2.09833256518583E-06,1.82463701320507E-07,0.00025,0.0001,0.15,1.1,23.5]

constant_names = [ sym.Symbol("Na_o"),sym.Symbol("K_o"),sym.Symbol("Ca_o"),sym.Symbol("Faraday"),sym.Symbol("FRT"),sym.Symbol("P_Ca"),sym.Symbol("gamma_Cai"),sym.Symbol("gamma_Cao"),sym.Symbol("z_Ca"),sym.Symbol("P_K"),sym.Symbol("gamma_Ki"),sym.Symbol("gamma_Ko"),sym.Symbol("z_K"),sym.Symbol("P_Na"),sym.Symbol("gamma_Nai"),sym.Symbol("gamma_Nao"),sym.Symbol("z_Na"),sym.Symbol("K_M_Ca"),sym.Symbol("G_Na_b"),sym.Symbol("G_Na"),sym.Symbol("g_CaT"),sym.Symbol("G_K1max"),sym.Symbol("G_Kp"),sym.Symbol("G_Ks_max"),sym.Symbol("PrNaK"),sym.Symbol("G_Kr_max"),sym.Symbol("I_bar_pCa"),sym.Symbol("KMpCa"),sym.Symbol("G_Ca_b"),sym.Symbol("I_up_bar"),sym.Symbol("NSR_bar"),sym.Symbol("K_M_up"),sym.Symbol("tau_tr"),sym.Symbol("I_NaK_bar"),sym.Symbol("K_m_Nai"),sym.Symbol("K_m_Ko"),sym.Symbol("alpha_rel"),sym.Symbol("qn_rel"),sym.Symbol("tau_rel_const"),sym.Symbol("K_rel_inf"),sym.Symbol("CMDN_bar"),sym.Symbol("TRPN_bar"),sym.Symbol("CSQN_bar"),sym.Symbol("KM_CMDN"),sym.Symbol("KM_TRPN"),sym.Symbol("KM_CSQN"),sym.Symbol("AF"),sym.Symbol("Volume_myo"),sym.Symbol("Volume_NSR"),sym.Symbol("Volume_JSR"),sym.Symbol("C1_const"),sym.Symbol("C2_const"),sym.Symbol("gamma_NaCa"), sym.Symbol("G_to"),sym.Symbol("G_Na_markov")]
#
constants_vec = [sym.Symbol("constants[" + str(i) + "]") for i in range(len(constant_values))]

if const_repr == "values":
	constants_repr = constant_values
elif const_repr == "names":
	constants_repr = constant_names
elif const_repr == "vector":
	constants_repr = constants_vec

Na_o = constants_repr[0]
K_o = constants_repr[1]
Ca_o = constants_repr[2]
Faraday = constants_repr[3]
FRT = constants_repr[4]
P_Ca = constants_repr[5]
gamma_Cai = constants_repr[6]
gamma_Cao = constants_repr[7]
z_Ca = constants_repr[8]
P_K = constants_repr[9]
gamma_Ki = constants_repr[10]
gamma_Ko = constants_repr[11]
z_K= constants_repr[12]
P_Na = constants_repr[13]
gamma_Nai = constants_repr[14]
gamma_Nao = constants_repr[15]
z_Na = constants_repr[16]
K_M_Ca = constants_repr[17]
G_Na_b = constants_repr[18]
G_Na = constants_repr[19]
g_CaT = constants_repr[20]
G_K1max = constants_repr[21]
G_Kp = constants_repr[22]
G_Ks_max = constants_repr[23]
PrNaK = constants_repr[24]
G_Kr_max = constants_repr[25]
I_bar_pCa = constants_repr[26]
KMpCa = constants_repr[27]
G_Ca_b = constants_repr[28]
I_up_bar = constants_repr[29]
NSR_bar = constants_repr[30]
K_M_up = constants_repr[31]
tau_tr = constants_repr[32]
I_NaK_bar = constants_repr[33]
K_m_Nai = constants_repr[34]
K_m_Ko = constants_repr[35]
alpha_rel = constants_repr[36]
qn_rel = constants_repr[37]
tau_rel_const = constants_repr[38]
K_rel_inf = constants_repr[39]
CMDN_bar = constants_repr[40]
TRPN_bar = constants_repr[41]
CSQN_bar = constants_repr[42]
KM_CMDN = constants_repr[43]
KM_TRPN = constants_repr[44]
KM_CSQN = constants_repr[45]
AF = constants_repr[46]
Volume_myo = constants_repr[47]
Volume_NSR = constants_repr[48]
Volume_JSR = constants_repr[49]
C1_const = constants_repr[50]
C2_const = constants_repr[51]
gamma_NaCa = constants_repr[52]
G_to = constants_repr[53]
G_Na_markov = constants_repr[54]

###############################################################################
#  Equations
###############################################################################


#Calcium buffering calculations. The total calcium (Ca_i and Ca_JSR) will be buffered in TRPN, CMDN and CSQN. Resulting values are Ca_i_free and Ca_JSR_free
b_myo = CMDN_bar+TRPN_bar-Ca_i+KM_TRPN+KM_CMDN
c_myo = KM_CMDN*KM_TRPN -Ca_i*(KM_TRPN+KM_CMDN)+TRPN_bar*KM_CMDN+CMDN_bar*KM_TRPN
d_myo = -KM_TRPN*KM_CMDN*Ca_i
Ca_i_free = (2*pow(b_myo*b_myo-3*c_myo,0.5)/3)*cos(acos((9*b_myo*c_myo-2*b_myo*b_myo*b_myo-27*d_myo)/(2*pow(b_myo*b_myo-3*c_myo,1.5)))/3)-(b_myo/3)

b_JSR=CSQN_bar+KM_CSQN-Ca_JSR
c_JSR=Ca_JSR*KM_CSQN
Ca_JSR_free = -b_JSR/2+sqrt(b_JSR*b_JSR+4*c_JSR)/2


#------------------------------------------------------------------------------
#I_L
tau_d=1.0/(1.0+exp(-(Voltage+10)/6.24))*(1-exp(-(Voltage+10)/6.24))/(0.035*(Voltage+10))

d_inf = 1.0/(1.0+exp(-(Voltage+10)/6.24))*1/(1+exp(-(Voltage+60)/0.024,evaluate=False))

f_inf = 1/(1+exp((Voltage+32)/8))+(0.6)/(1+exp((50-Voltage)/20))

tau_f=1/(0.0197*exp(-pow(0.0337*(Voltage+10),2.0))+0.02)


f_Ca = 1/(1+(Ca_i_free/K_M_Ca))



I_LBar_Ca = P_Ca*(z_Ca**2)*Voltage*Faraday*FRT*((gamma_Cai*Ca_i_free*exp(z_Ca*Voltage*FRT)-gamma_Cao*Ca_o)/(exp(z_Ca*Voltage*FRT)-1))

I_LBar_Na = P_Na*(z_Na**2)*Voltage*Faraday*FRT*((gamma_Nai*Na_i*exp(z_Na*Voltage*FRT)-gamma_Nao*Na_o)/(exp(z_Na*Voltage*FRT)-1))

I_LBar_K = P_K*(z_K**2)*Voltage*Faraday*FRT*((gamma_Ki*K_i*exp(z_K*Voltage*FRT)-gamma_Ko*K_o)/(exp(z_K*Voltage*FRT)-1))


if model == "wt":
	I_L_Ca_factor = 1
if model == "bs" or model == "wtm":
	I_L_Ca_factor = 0.5

I_L_Ca = d_gate*f_gate*f_Ca*I_LBar_Ca * I_L_Ca_factor

I_L_Na = d_gate*f_gate*f_Ca*I_LBar_Na * I_L_Ca_factor

I_L_K = d_gate*f_gate*f_Ca*I_LBar_K * I_L_Ca_factor


E_K = log(K_o/K_i)/FRT


E_Na = log(Na_o/Na_i)/FRT

if model == "wt":

	#------------------------------------------------------------------------
	# I_Na	
	step_function_Na = 1.0-(1.0/(1+exp(sym.Mul(sym.Add(Voltage,40,evaluate=False),-41.66666666666666,evaluate=False),evaluate=False))) #Dont change variables!!

	# test TODO entfernen!!!
	print("test")
	step_function_Na =  1.0 - (1.0 / ( 1+exp( sym.Add(Voltage,40, evaluate=False) *-41.66666666666666) ) )


	#step_function_Na = Heaviside(-Voltage-40)

	aux = step_function_Na

	alpha_h = aux*0.135*exp((80+Voltage)/(-6.8))


	beta_h = (1.0-aux)/(0.13*(1+exp((Voltage+10.66)/(-11.1)))) +(aux)*(3.56*exp(0.079*Voltage)+3.1*1e5*exp(0.35*Voltage))


	alpha_j =  aux*(-1.2714e5*exp(0.2444*Voltage)-3.474e-5*exp(-0.04391*Voltage))*(Voltage+37.78)/(1+exp(0.311*(Voltage+79.23)))

	beta_j = (1-aux)*(0.3*exp(-2.535e-7*Voltage)/(1+exp(-0.1*(Voltage+32))))+(aux)*(0.1212*exp(-0.01052*Voltage)/(1+exp(-0.1378*(Voltage+40.14))))

	alpha_m = 0.32*(Voltage+47.13)/(1-exp(-0.1*(Voltage+47.13)))
	beta_m = 0.08*exp(-Voltage/11)

	I_Na = G_Na * m_gate**3*h_gate*j_gate*(Voltage-E_Na)
	I_to = 0.0
	
elif model == "bs":
	#------------------------------------------------------------------------
	# I_Na
	l = ["alpha11","alpha12","alpha13","beta11","beta12","beta13","alpha3","beta3","alpha2","beta2","alpha4","beta4","alpha5","beta5","mUL","mLU"]

	N_markov = 13
	N_var_nomarkov = 16
	arg_markov= [sym.Symbol(i) for i in l]

	alpha11 = arg_markov[0]
	alpha12 = arg_markov[1]
	alpha13 = arg_markov[2]
	beta11 = arg_markov[3]
	beta12 = arg_markov[4]
	beta13 = arg_markov[5]
	alpha3 = arg_markov[6]
	beta3 = arg_markov[7]
	alpha2 = arg_markov[8]
	beta2 = arg_markov[9]
	alpha4 = arg_markov[10]
	beta4 = arg_markov[11]
	alpha5 = arg_markov[12]
	beta5 = arg_markov[13]
	mUL = arg_markov[14]
	mLU = arg_markov[15]

	alpha11=3.802/(0.1027*exp(-Voltage/17.0)+0.20*exp(-Voltage/150.0))#xC3
	alpha12=3.802/(0.1027*exp(-Voltage/15.0)+0.23*exp(-Voltage/150.0))#xC2
	alpha13=3.802/(0.1027*exp(-Voltage/12.0)+0.25*exp(-Voltage/150.0))#xC1

	beta11=0.1917*exp(-Voltage/20.3)#xC2
	beta12=0.20*exp(-(Voltage-5.0)/20.3)#xC1
	beta13=0.22*exp(-(Voltage-10.0)/20.3)#xO

	alpha3=((3.7933e-7)*exp(-Voltage/7.7))/2.5#UIF

	beta3=(0.0084+0.00002*Voltage)#x[26]

	alpha2=(9.178*exp(Voltage/29.68))#x[27]
	beta2=(alpha13*alpha2*alpha3)/(beta13*beta3)#UIF

	alpha4=alpha2/100.0#UIF
	beta4=alpha3#UIM
	alpha5=alpha2/(3.5e4)#UIM
	beta5=alpha3/20.0#x[23]

	mUL=1.0e-7#backgroundtoburst(U
	mLU=9.5e-4#bursttobackground(L

	mat = [[0 for i in range(N_markov)] for i in range(N_markov)]

	#UIC3 - x[16]
	mat[0][0] = -alpha11 - alpha3
	mat[0][1] = beta11 #x[20]
	mat[0][5] = beta3 #x[24]

	#UIC2 - x[17]
	mat[1][1] = -beta11 -alpha12 -alpha3
	mat[1][0] = alpha11 #x[19]
	mat[1][2] = beta12 #UIF
	mat[1][6] = beta3 #x[25]

	#UIF - x[18]
	mat[2][2] = -beta12 - beta2 - alpha3 - alpha4
	mat[2][1] = alpha12 #x[20]
	mat[2][3] = beta4 #x[22]
	mat[2][7] = beta3 #x[26]
	mat[2][8] = alpha2 #x[27]

	#UIM1 - x[19]
	mat[3][3] = -beta4 - alpha5
	mat[3][2] = alpha4 #UIF
	mat[3][4] = beta5 #x[23]

	#UIM2 - x[20]
	mat[4][4] = -beta5
	mat[4][3] = alpha5

	#UC3 - x[21]
	mat[5][5] = -mUL - alpha11 - beta3
	mat[5][0] = alpha3 #x[19]
	mat[5][6] = beta11 #x[25]
	mat[5][9] = mLU #x[28]

	#UC2 - x[22]
	mat[6][6] = -mUL - alpha12 - beta11 - beta3
	mat[6][5] = alpha11 #x[24]
	mat[6][7] = beta12 #x[26]
	mat[6][1] = alpha3 #x[20]
	mat[6][10] = mLU #x[29]

	#UC1 - x[23]
	mat[7][7] = -mUL - alpha13 - beta12 - beta3
	mat[7][6] = alpha12 #x[25]
	mat[7][8] = beta13 #x[27]
	mat[7][2] = alpha3 #UIF
	mat[7][11] = mLU #x[30]

	#UO - x[24]
	mat[8][8] = -mUL - alpha2 - beta13
	mat[8][7] = alpha13 #x[26]
	mat[8][2] = beta2 #UIF
	mat[8][12] = mLU #x[31]

	#LC3 - x[25]
	mat[9][9] = -mLU -alpha11
	mat[9][10] = beta11 #x[29]
	mat[9][5] = mUL #x[24]

	#LC2 - x[26]
	mat[10][10] = -mLU - beta11 - alpha12
	mat[10][9] = alpha11 #x[28]
	mat[10][11] = beta12 #x[30]
	mat[10][6] = mUL #x[25]

	#LC1 - x[27]
	mat[11][11] = -mLU - alpha13 - beta12
	mat[11][10] =  alpha12#x[30]
	mat[11][12] = beta13 #x[31]
	mat[11][7] = mUL #x[26]

	#LO - x[28]
	mat[12][12] = -mLU - beta13
	mat[12][11] = alpha13 #x[30]
	mat[12][8] = mUL #x[27]

	x_markov = [sym.Symbol("x[" + str(N_var_nomarkov  + i) + "]") for i in range(N_markov)]
	x_rate_markov = [0 for i in range(N_markov)]
	for i in range(N_markov):
		for j in range(N_markov):
			x_rate_markov[i] += mat[i][j] * x_markov[j]

	I_Na = G_Na_markov * (x_markov[8]+x_markov[12]) * (Voltage-E_Na)

	#------------------------------------------------------------------------
	# I_to	
	R_to = exp(Voltage/100.0)

	alpha_z = 10.0*exp((Voltage-40.0)/25.0)/(1.0 + exp((Voltage-40.0)/25.0))
	beta_z =  10.0*exp(-(Voltage+90.0)/25.0)/(1.0 + exp(-(Voltage+90.0)/25.0))

	alpha_y = 0.015/(1.0+exp((Voltage+60.0)/5.0))
	beta_y =  0.1*exp((Voltage+25.0)/5.0)/(1.0 + exp((Voltage+25.0)/5.0))



	I_to = G_to * z_gate * z_gate*z_gate*y_gate*R_to*(Voltage-E_K)

elif model == "wtm":
	
	N_var_nomarkov = 16
	
	#------------------------------------------------------------------------
	# I_Na
	N_markov = 9
	
	alpha11 = 3.802/(0.1027*exp(Voltage/-17.0)+0.2*exp(Voltage/-150.0))
	alpha12 = 3.802/(0.1027*exp(Voltage/-15.0)+0.23*exp(Voltage/-150.0))
	alpha13 = 3.802/(0.1027*exp(Voltage/-12.0)+0.25*exp(-Voltage/150.0))
	beta11 = 0.1917*exp(Voltage/-20.3)
	beta12 = 0.20*exp((Voltage-5.0)/-20.3)
	beta13 = 0.22*exp((Voltage-10.0)/-20.3)
	alpha3 = ((3.7933e-7)*exp(Voltage/-7.7))
	beta3 = (0.0084+0.00002*Voltage)
	alpha2 = (9.178*exp(Voltage/29.68))
	beta2 = (alpha13*alpha2*alpha3)/(beta13*beta3)
	alpha4 = alpha2/100.0
	beta4 = alpha3
	alpha5 = alpha2/(9.5e4)
	beta5 = alpha3/50.0

	mat = [[0 for i in range(N_markov)] for i in range(N_markov)]

	# IC3 - x[16]
	mat[0][0] = -alpha11 - alpha3
	mat[0][1] = beta11 # IC2 - x[17]
	mat[0][5] = beta3 # C3 - x[21]

	# IC2 - x[17]
	mat[1][1] = -beta11 -alpha12 -alpha3
	mat[1][0] = alpha11 # IC3 - x[16]
	mat[1][2] = beta12 # IF - x[18]
	mat[1][6] = beta3 # C2 - x[22]

	# IF - x[18]
	mat[2][2] = -beta12 - beta2 - alpha3 - alpha4
	mat[2][1] = alpha12 # IC2
	mat[2][3] = beta4 # IM1
	mat[2][7] = beta3 # C1
	mat[2][8] = alpha2 # O

	# IM1 - x[19]
	mat[3][3] = -beta4 - alpha5
	mat[3][2] = alpha4 # IF
	mat[3][4] = beta5 # IM2

	# IM2 - x[20]
	mat[4][4] = -beta5
	mat[4][3] = alpha5 # IM1

	# C3 - x[21]
	mat[5][5] = - alpha11 - beta3
	mat[5][0] = alpha3 # IC3
	mat[5][6] = beta11 # C2

	# C2 - x[22]
	mat[6][6] = - alpha12 - beta11 - beta3
	mat[6][5] = alpha11 # C3
	mat[6][7] = beta12 # C1
	mat[6][1] = alpha3 # IC2

	# C1 - x[23]
	mat[7][7] = - alpha13 - beta12 - beta3
	mat[7][6] = alpha12 # C2
	mat[7][8] = beta13 # O
	mat[7][2] = alpha3 # IF

	# O - x[24]
	mat[8][8] = - alpha2 - beta13
	mat[8][7] = alpha13 # C1
	mat[8][2] = beta2 # IF


	x_markov = [sym.Symbol("x[" + str(N_var_nomarkov  + i) + "]") for i in range(N_markov)]
	x_rate_markov = [0 for i in range(N_markov)]
	for i in range(N_markov):
		for j in range(N_markov):
			x_rate_markov[i] += mat[i][j] * x_markov[j]

	I_Na = G_Na_markov * x_markov[8] * (Voltage-E_Na)

	#------------------------------------------------------------------------
	# I_to
	R_to = exp(Voltage/100.0)

	alpha_z = 10.0*exp((Voltage-40.0)/25.0)/(1.0 + exp((Voltage-40.0)/25.0))
	beta_z =  10.0*exp(-(Voltage+90.0)/25.0)/(1.0 + exp(-(Voltage+90.0)/25.0))

	alpha_y = 0.015/(1.0+exp((Voltage+60.0)/5.0))
	beta_y =  0.1*exp((Voltage+25.0)/5.0)/(1.0 + exp((Voltage+25.0)/5.0))

	I_to = G_to * z_gate * z_gate*z_gate*y_gate*R_to*(Voltage-E_K)


#End wtm Na&I_to if


I_Na_b = G_Na_b * (Voltage - E_Na)


E_Ca = log(Ca_o/Ca_i_free)/(2*FRT)
step_function_CaT = 1.0-(1.0/(1+exp(sym.Mul(sym.Add(Voltage,00,evaluate=False),-41.66666666666666,evaluate=False),evaluate=False))) #Dont change variables!!
aux = step_function_CaT
b_inf = 1/(1+exp(-(Voltage+14.0)/10.8))
tau_b= 3.7+6.1/(1+exp((Voltage+25.0)/4.5))
g_inf = 1/(1+exp((Voltage+60.0)/5.6))
tau_g = aux*(-0.875*Voltage+12.0)+12.0*(1-aux)
I_CaT = g_CaT*b_gate*b_gate*g_gate*(Voltage-E_Ca)




G_K1 = G_K1max*sqrt(K_o/5.4)
alpha_K1 = 1.02/(1+exp(0.2385*(Voltage-E_K-59.215)))
beta_K1 = (0.49124*exp(0.08032*(Voltage-E_K+5.476))+exp(0.06175*(Voltage-E_K-594.31)))/(1+exp(-0.5143*(Voltage-E_K+4.753)))
I_K1 = G_K1 * (alpha_K1)/(alpha_K1+beta_K1)*(Voltage-E_K)

I_Kp = G_Kp*(Voltage-E_K)/(1+exp((7.488-Voltage)/5.98))
G_Ks = G_Ks_max*(1+0.6/(1+pow(3.8e-5/Ca_i_free,1.4)))
E_Ks = log((K_o+PrNaK*Na_o)/(K_i+PrNaK*Na_i))/FRT
Xs_inf = 1/(1+exp(-(Voltage-1.5)/16.7))
tau_Xs = 1/(0.0000719*(Voltage+30)/(1-exp(-0.148*(Voltage+30)))+0.000131*(Voltage+30)/(exp(0.0687*(Voltage+30))-1))
I_Ks = G_Ks * X_s * X_s2 * (Voltage-E_Ks)


R_gate = 1.0/(1.0+exp((Voltage+9.0)/22.4))
Xr_inf = 1/(1+exp(-(Voltage+21.5)/7.5))
tau_X_r = 1/(0.00138*(Voltage+14.2)/(1-exp(-0.123*(Voltage+14.2)))+0.00061*(Voltage+38.9)/(exp(0.145*(Voltage+38.9))-1))
I_Kr = G_Kr_max*sqrt(K_o/5.4)*X_r*R_gate*(Voltage-E_K)



I_pCa = I_bar_pCa*Ca_i_free/(KMpCa + Ca_i_free)

I_Ca_b = G_Ca_b*(Voltage - E_Ca)

I_leak = (I_up_bar/NSR_bar) * Ca_NSR

I_up = I_up_bar*Ca_i_free/(Ca_i_free+K_M_up)

I_tr = (Ca_NSR-Ca_JSR_free)/tau_tr

I_NaCa = C1_const *exp(( gamma_NaCa-1)*Voltage* FRT)*((exp(Voltage* FRT)*pow(Na_i,3)*Ca_o- pow(Na_o,3)*Ca_i_free)/(1+ C2_const*exp(( gamma_NaCa-1)*Voltage* FRT)*(exp(Voltage* FRT)*pow(Na_i,3)*Ca_o+ pow(Na_o,3)*Ca_i_free)))

sigma_NaK = (exp(Na_o/67.3)-1)/7
f_NaK = 1/(1+0.1245*exp((-0.1*Voltage*FRT)) + 0.0365*sigma_NaK*exp(-Voltage*FRT))
I_NaK = I_NaK_bar*f_NaK/(1+pow(K_m_Nai/Na_i,2))/(1+K_m_Ko/K_o)


I_rel_inf = I_L_Ca*alpha_rel/(1+pow(K_rel_inf/Ca_JSR_free,qn_rel))
tau_rel=tau_rel_const/(1+0.0123/Ca_JSR_free)

I_Ca_tot = I_L_Ca + I_Ca_b  + I_pCa - 2*I_NaCa + I_CaT

I_Na_tot = I_Na + I_Na_b + 3*I_NaCa + I_L_Na + 3*I_NaK

I_K_tot = I_Kr + I_Ks + I_K1 + I_Kp + I_L_K - 2*I_NaK + I_to - I_stim

d_gate_rate=(d_inf-d_gate)/tau_d
f_gate_rate=(f_inf-f_gate)/tau_f

X_r_rate=(Xr_inf-X_r)/tau_X_r

X_s_rate=(Xs_inf-X_s)/tau_Xs
X_s2_rate=(Xs_inf-X_s2)/(tau_Xs*4.0)

b_gate_rate=(b_inf-b_gate)/tau_b
g_gate_rate=(g_inf-g_gate)/tau_g

I_rel_rate = -(I_rel_inf + I_rel)/tau_rel

Na_i_rate=-I_Na_tot*AF/(Volume_myo)
K_i_rate=-I_K_tot*AF/(Volume_myo)
Ca_i_rate =-I_Ca_tot*AF/(Volume_myo*2)+(I_leak-I_up)*Volume_NSR/Volume_myo+(I_rel)*Volume_JSR/Volume_myo

Ca_NSR_rate = I_up-I_tr*Volume_JSR/Volume_NSR-I_leak
Ca_JSR_rate = I_tr-(I_rel)

Voltage_rate = -(I_Na_tot + I_K_tot + I_Ca_tot)

if model == "wt":
	h_gate_rate = alpha_h*(1.0-h_gate)-beta_h*h_gate
	m_gate_rate = alpha_m*(1.0-m_gate)-beta_m*m_gate
	j_gate_rate = alpha_j*(1.0-j_gate)-beta_j*j_gate
	x_rate = [Voltage_rate,h_gate_rate,m_gate_rate,j_gate_rate,d_gate_rate,f_gate_rate,X_r_rate,Ca_i_rate,Na_i_rate,K_i_rate,Ca_JSR_rate,Ca_NSR_rate,X_s_rate,b_gate_rate,g_gate_rate,X_s2_rate,I_rel_rate]

	x = [Voltage,h_gate,m_gate,j_gate,d_gate,f_gate,X_r,Ca_i,Na_i,K_i,Ca_JSR,Ca_NSR,X_s,b_gate,g_gate,X_s2,I_rel]

elif model == "bs" or model == "wtm":
	y_gate_rate = alpha_y*(1.0-y_gate)-beta_y*y_gate
	z_gate_rate = alpha_z*(1.0-z_gate)-beta_z*z_gate
	x_rate = [Voltage_rate,d_gate_rate,f_gate_rate,X_r_rate,Ca_i_rate,Na_i_rate,K_i_rate,Ca_JSR_rate,Ca_NSR_rate,X_s_rate,b_gate_rate,g_gate_rate,X_s2_rate,I_rel_rate,y_gate_rate,z_gate_rate]
	x_rate = x_rate + x_rate_markov

	x = [Voltage,d_gate,f_gate,X_r,Ca_i,Na_i,K_i,Ca_JSR,Ca_NSR,X_s,b_gate,g_gate,X_s2,I_rel,y_gate,z_gate]
	x = x + x_markov

###############################################################################
#Printing
###############################################################################

#Rates:
if show == "rates":
	for i in range(len(x)):
		print("dxdt[" + str(i) + "] = " + str(cxxcode(x_rate[i])) + ";")

#Constants
elif show == "constants":
	for i in range(len(constant_values)):
		print(str(constant_names[i]) + ": constants[" + str(i) + "] = " + str(constant_values[i]) + ";")

#Constants name = value
elif show == "constvalues":
	for i in range(len(constant_values)):
		print( str(constant_names[i]) + " = " + str(constant_values[i]))

else:
	print("Unrecognised option: " + show)

