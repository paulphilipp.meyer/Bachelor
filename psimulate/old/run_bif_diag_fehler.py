# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime


# user options
queue = False

dir_data = "/scratch16/pmeyer/brugada"
folder = "beef_bs"

model = "bs"
cl_start = 150
cl_stop = 350
cl_steps = 2000
N_beats = 100
skip_beats = 0
init_skip_beats = 0
init_each = 0
debug = 2
#init_state = "None"
init_state = "-88.5222865987, 2.88453538815e-117, 0.964332985158, 0.00413709550324, 0.0412990131707, 17.2407367196, 139.052784824, 2.896094779, 2.66954285043, 0.0188813428877, 0.0010190446789, 0.757583996696, 0.0306656358173, 1.62096030176e-21, 0.853822150442, 0.0118504897772, 0.379369395658, 0.00519859470133, 0.000155909515386, 0.174501891887, 0.0782370764846, 0.35775752862, 0.00474137318823, 2.38067954357e-05, 2.38610015799e-08, 1.39840063944e-05, 1.85318632873e-07, 9.26454555899e-10, 7.58957333513e-13"

# determining directory and job variables
for i in range(100):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 100 versions of %s" % folder)

if queue:
	id = int(os.getenv("SGE_TASK_ID", 0))
	first = int(os.getenv("SGE_TASK_FIRST", 0))
	last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
if queue:
	doc("ID %d" % id)
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("saving to %s\n" % directory)
doc("model: %s" % model)
doc("cl_start: %f" % cl_start)
doc("cl_stop: %f" % cl_stop)
doc("cl_steps: %f" % cl_steps)
doc("N_beats: %d" % N_beats)
doc("skip_beats: %d" % skip_beats)
doc("init_skip_beats: %d" % init_skip_beats)
doc("init_state:\n" + init_state +"\n")

args = [model,
		str(cl_start),
		str(cl_stop),
		str(cl_steps),
		str(N_beats),
		str(skip_beats),
		str(init_skip_beats),
		str(init_each),
		directory,
		str(debug),
		init_state]

# executing program
t0 = datetime.datetime.now()
doc("started: %s" % str(t0))
#	call(["/home/pmeyer/brugada_code/bin/bifurcation_wt", "--help"])
call(["/home/pmeyer/brugada_code/bin/bif",] + args)
t1 = datetime.datetime.now()
doc("finished: %s" % str(t1))
doc("runtime: %s" % str(t1-t0))
print("saved to %s" % directory)

