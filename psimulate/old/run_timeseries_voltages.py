# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime


# set user options
dir_data = "/scratch16/pmeyer/brugada"
folder = "tim_v_bs_chaos"

model = "bs"
cl = 113.5
N_beats = 5000
skip_beats = 0


# determining directory and job variables
for i in range(100):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 100 versions of %s" % folder)
	
id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))


# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# document
doc("ID %d" % id)
doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
doc("This job was submitted from %s, it is currently running on %s"
      % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\ncalculating timeseries\n")
doc("saving to %s" % directory)
doc("model: %s" % model)
doc("cl: %f" % cl)
doc("N_beats: %d" % N_beats)
doc("skip_beats: %d" % skip_beats)


# execute program
t0 = datetime.datetime.now()
call(["/home/pmeyer/brugada_code/bin/timeseries_v_"+model,
    "--cl", str(cl),
    "--N_beats", str(N_beats),
    "--skip_beats", str(skip_beats),
    "--dir_data", directory
    ])
t1 = datetime.datetime.now()
doc("started: %s" % str(t0))
doc("runtime: %s" % str(t1-t0))
