'''
runs bin/bifn with the parameters specified further down
can also be submitted to the cluster/sun grid engin/sun grid enginee via
	qsub psimulate/run_bif_diag.py
'''
# Use python as shell
#$ -S /usr/bmp/python2.7.6/bin/python

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line): "testname-py"
#$ -N Leap-Window-py

# Path for the output files
#$ -o /home/pmeyer/tmp/q-out/

# Parallel environment: request at least N cores: -pe threaded_* 2
# may be threaded_all, threaded_blades or threaded_frontends
# -pe -v GE_PARALLEL_ENVIRONMENT 1

# Project
# default is "BMP", use "WholeClusterBMP" if you want to use the whole cluster
# (both groups)
#$ -q grannus.q

# Array job size
#$ -t 1
#$ -tc 1

import os
import sys
from subprocess import call
import datetime


################################################################################
# user options

dir_data = "/scratch16/pmeyer/brugada"
folder = "bif_sev"

sev_start = 0
sev_stop = 1
sev_steps = 501
N_beats = 100
skip_beats = 0
init_skip_beats = 0
init_each = 0
debug = 2
#init_state = "None"

# xiaepi1000_1 last from einschwingen_xiaepi_1
init_state_str = "xiaepi1000_1"
init_state = "-86.5258234323, 0.0153547151776, 8.97095781061, 10.0383584239, 138.690665024, 0.00130181111298, 0.778667374989, 0.778569759578, 0.000175821160157, 0.484648599904, 0.00294919614454, 1.94720530113e-08, 0.999998334051, 1.90179506221e-05, 0.999783005391, 1.00381326619, 0.998861785669, 0.00119792709109, 0.268965710337"


dt = 0.001
cl = 1000
save_voltages = 1
################################################################################

# determining directory and job variables
for i in range(1000):
	directory = dir_data + "/" + folder + "_" + str(i)
	try:
		os.makedirs(directory)
		break
	except OSError:
		continue
else:
	sys.exit("there already exist 1000 versions of %s" % folder)

try:
	os.makedirs(directory+'/timeseries')
except OSError:
	sys.exit("could not create directory "+ directory+'/timeseries')

id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))
screen_window = int(os.getenv("WINDOW", 0))

# prepare information output
doc_file = open(directory+"/info.txt", "w")

def doc(string):
	print(string)
	doc_file.write(string+"\n")


# documenting
doc("running psimulate/run_bif_severity")
doc("ID %d" % id)
if (id == 0):
	doc("Running on %s in window %s" % (os.getenv("HOSTNAME"), os.getenv("WINDOW")))
else:
	doc("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
	doc("This job was submitted from %s, it is currently running on %s"
		  % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
	doc("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))

doc("\nsaving to %s\n" % directory)
doc("model:           xiaepi_bs")
doc("sev_start:       %f" % sev_start)
doc("sev_stop:        %f" % sev_stop)
doc("sev_steps:_______%f" % sev_steps)
doc("N_beats:         %d" % N_beats)
doc("skip_beats:      %d" % skip_beats)
doc("init_skip_beats:_%d" % init_skip_beats)
doc("init_state_str:  %s" % init_state_str)
doc("dt:              %f" % dt)
doc("cl:              %f" % cl)
doc("save_voltages:   %i" % save_voltages)

args = [str(cl),
		str(sev_start),
		str(sev_stop),
		str(sev_steps),
		str(N_beats),
		str(skip_beats),
		str(init_skip_beats),
		str(init_each),
		directory,
		str(debug),
		init_state,
		str(dt),
		str(save_voltages)]

# executing program
t0 = datetime.datetime.now()
doc("\nstarted:    %s" % str(t0))
doc_file.flush()
sys.stdout.flush()
call(["/home/pmeyer/brugada_code/bin/bif_severity",] + args)
t1 = datetime.datetime.now()
doc("finished:   %s" % str(t1))
doc("runtime:    %s" % str(t1-t0))
print("saved to %s" % directory)

