#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap


#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
#dir_data = "/scratch16/pmeyer/brugada"
dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

name = "test_13"
prefix = "sevn0_"
#directory = dir_data + "/" + name + "/"
directory = dir_data + "\\" + name + "\\"

#  from voltages.npy, times.npy
v = np.load(directory + prefix +'voltages.npy')
t = np.load(directory + prefix +'times.npy')


# # from states.dat
#t, v = np.loadtxt(directory + '/states.dat', usecols = (0, 1), unpack = True) # states


skip_first = 9000
#skip_first = 100000

ut = np.copy(t)
unwrap(ut)
ut /= 113.500000

fig = plt.figure()
ax = fig.add_subplot(111)
#ax.plot(ut)
ax.plot(ut[skip_first:], v[skip_first:])
#plt.title("cl: "+prefix)
plt.xlabel("time [ms]")
plt.ylabel("membrane potential [mV]")
plt.show()
