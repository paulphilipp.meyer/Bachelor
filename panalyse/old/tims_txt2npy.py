#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from numpy import loadtxt, save

assert len(sys.argv) > 1

files = sys.argv[1:]

for f in files:
    data = loadtxt(f).T
    fn = f.replace('.dat', '')
    save(fn, data)
