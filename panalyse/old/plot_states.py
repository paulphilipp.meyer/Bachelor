#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, ind2name, normalized

dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
#dir_data = "/scratch16/pmeyer/brugada"

prefix = "test_tim_s_wt_1"

directory = dir_data + "/" + prefix
data = np.loadtxt(directory +'/states.dat', unpack = True) # states

#data = data[:,28800:]


t = data[0]
v = data[1]

#skip_first = 1000000
skip_first = 0

ut = np.copy(t)
unwrap(ut)
#%%
fig = plt.figure()
ax = fig.add_subplot(111)
#ax.plot(ut)
#ax.plot(ut[skip_first:], (v[skip_first:]), label = "voltage")
for i in range(0, data.shape[0]-1):
    ax.plot(ut[skip_first:], (((data[i+1, skip_first:]))), label = ind2name(i, model = "bs"))

#for i, x in enumerate(data[:,.1]):
#    plt.plot(x, y)
#    plt.text(x[-1], y[-1], 'sample {i}'.format(i=i))
#plt.title("cl: "+prefix)
plt.xlabel("time [ms]")
plt.ylabel("membrane potential [mV]")
plt.legend()
plt.show()
