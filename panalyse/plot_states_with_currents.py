#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

name = "test_na_da_1"
model = "xiaepi"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states


t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')

skip_first = 0
till = -1
every = 50
t = t[skip_first:till:every]
states = states[skip_first:till:every]

#skip_first = 1000000


ut = np.copy(t)
unwrap(ut)
#%%

run timeseries2currents

#%%

fig = plt.figure()
ax = fig.add_subplot(111)
#ax.plot(ut)
#ax.plot(ut[skip_first:], (v[skip_first:]), label = "voltage")
#for i in range(0, states.shape[1]):
##for i in black:
#for i in (0,5, 6, 7):
for i in (0,4):
    ax.plot(ut, (((states[:, i]))), lw=3, label = ind2name(i, model = model))
#for i in range(len(currents)):
for i in (0,):
    ax.plot(ut, (((currents[i]))), label = currents_names[i])

#ax.plot(ut[1:], np.diff(ut), label="dt")
#ax.plot(ut, Ak1*100, label="Ak1")
#ax.plot(ut, Bk1*100, label="Bk1")
#ax.plot(ut, rec_iK1, label="rec_iK1")
#ENa = 27 * np.log(140/states[:,3])
#INa = 15*states[:,5]**3*states[:,6]*states[:,7]* (states[:,0]-ENa)
#D_INF = 1./(1.+np.exp((-5-states[:,0])/7.5))
#ax.plot(ut, ENa, label="ENa") 
#ax.plot(ut, INa, label="INa") 
#ax.plot(ut, D_INF, label="D_INF") 
#for i, x in enumerate(data[:,.1]):
#    plt.plot(x, y)
#    plt.text(x[-1], y[-1], 'sample {i}'.format(i=i))
plt.title(name)
#plt.xlim((-10,20))
#plt.ylim((-10,20))
plt.xlabel("time [ms]")
plt.ylabel("membrane potential [mV]")
plt.legend()
#plt.show()
