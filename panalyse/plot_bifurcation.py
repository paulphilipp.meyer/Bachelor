#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, normalize


prefix = 'test_52'
bif_param = "cl"


#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

skip_beats = 0


directory = dir_data + "/" + prefix + "/"
#directory = dir_data + "\\" + prefix + "\\"

if bif_param == "cl":
    param = np.load(directory + 'cl.npy')
elif bif_param == "sev":
    param = np.load(directory + 'sev.npy')
v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]


param = param[:v_s.shape[0]]
#for darray in (v_max, v_min, v_s, apd90):
#    normalize(darray)

#%%


fig = plt.figure()
ax = fig.add_subplot(111)



markersize = 2
#for row in v_max.T: ax.scatter(param, row, color = "b", label = "$v_{max}$", s=markersize)
#for row in v_min.T: ax.scatter(param, row, color = "orange", label = "$v_{min}$", s=markersize)
#for row in v_s.T: ax.scatter(param, row, color = "g", label = "$v_s$", s=markersize)
#for row in apd90.T[::]: ax.scatter(param, row, color = "r", label = "$APD90$", s=markersize)

num_points = apd90.shape[1]
colors = plt.get_cmap('jet')(np.linspace(1,0,num_points))
i=0
for row in apd90.T[::]:
    ax.scatter(param, row, color = colors[i], label = "$APD90$", s=markersize)
    i+=1

# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc='upper center')

plt.title(prefix)
ax.set_ylim((-100,200))
plt.ylabel("Action potential measure")
if bif_param == "cl":
    plt.xlabel("Cycle length [ms]")
elif bif_param == "sev":
    plt.xlabel("Severity")
plt.show()

#from subprocess import call
#filename = "tmp.png"
#
#plt.savefig(filename)
#call(["eog", filename])
