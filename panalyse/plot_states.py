#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = "/data.bmp/pmeyer/brugada"

#ids = np.arange()

name = "tims_TT-BrS_0"
model = "wt"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states


till = 100000
t = np.load(directory+'/times.npy', mmap_mode='r')[:till]
states = np.load(directory+'/states.npy', mmap_mode='r')[:till]



skip = 0
ut = np.copy(t)
unwrap(ut)


every = 1
t = t[skip::every]
states = states[skip::every]
ut = ut[skip::every]

#dt = np.diff(ut)

#%%

fig = plt.figure()
ax = fig.add_subplot(111)

ii = 0
lines = []
labels = []

# values ###################################################################

#for i in range(0, states.shape[1]):
for i in (0,):
#for i in (3, 18):
#    labels.append(ind2name(i, model = model))
    labels.append(str(i))
    
    # normal
    lines.append(ax.plot(ut, (((states[:, i]))),visible=True, label = labels[-1], lw=1)[0])
    
    # normalized
#    lines.append(ax.plot(t, normalized(states[:, i]), '.',  visible=True, label = labels[-1]+ "(renormalized)", lw=5)[0])

    # to zero
#    lines.append(ax.plot(ut, states[:, i]-states[-1,i] ,visible=True, label = labels[-1], lw=1)[0])

# differentials ############################################################
#for i in range(0, states.shape[1]):
#for i in (0,1,2,3,4,5):
#for i in (6,7,8,9,10):
#for i in (11,12,13,14,15):
#for i in (16,17,18):
#    labels.append(ind2name(i, model = model))
#    lines.append(ax.plot(ut[1:], np.abs(np.diff(states[:, i])/dt), visible=True, label = "d/dt "+labels[-1], lw=1)[0])
#    lines.append(ax.plot(ut[1:], np.abs(np.diff(states[:, i])/dt/states[1:,i]), visible=True, label = "(d/dt "+labels[-1]+")/"+labels[-1], lw=1)[0])
    
#ax.plot(ut[1:], dt, label = "dt")
#ax.plot(ut, t/1000)
#abschanges = np.zeros(states.shape[1])
#relchanges = np.zeros(states.shape[1])
#for i in range(0, states.shape[1]):
    



#plt.title(name)
plt.xlabel("time [ms]")
plt.ylabel("time step [ms]")
#plt.xlim((35000,40000))
#plt.ylim((-0.01, 0.01))
plt.legend()


#plt.show(block=True)

