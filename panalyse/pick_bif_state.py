import numpy as np

prefix = 'bif_wtm_0'
bif_param = "cl"

dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"
#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"

directory = dir_data + "/" + prefix + "/"
#directory = dir_data + "\\" + prefix + "\\"

if bif_param == "cl":
    cl = np.load(directory + 'cl.npy')
elif bif_param == "sev":
    sev = np.load(directory + 'sev.npy')
last_states = np.load(directory+'last_states.npy')
first_states = np.load(directory+'first_states.npy')

spot = 1000
epsilon = 0.001
i, = np.where(abs(cl-spot)<epsilon)
print('cl '+str(cl[i])+': '+str(i))
#i, = np.where(sev > 0.5)
#%%
init_state_str = "xiaepibs1250_2"
pick = i[0]
pick_lstate = last_states[pick]
#pick_state.resize((1, len(pick_state)))
pick_lstate_str = ""
for value in pick_lstate:
    pick_lstate_str += str(value)
    pick_lstate_str += ", "
pick_lstate_str = pick_lstate_str[:-2]
if bif_param == "cl":
    print("# "+init_state_str+" last from cl["+str(pick)+"] "+str(cl[pick])+"ms from "+prefix)
elif bif_param == "sev":
    print("# "+init_state_str+" last from sev["+str(pick)+"] "+str(sev[pick])+" from "+prefix)
print('init_state_str = "'+init_state_str+'"')
print('init_state = "'+ pick_lstate_str+'"')
print()
pick_fstate = first_states[pick]
#pick_state.resize((1, len(pick_state)))
pick_fstate_str = ""
for value in pick_fstate:
    pick_fstate_str += str(value)
    pick_fstate_str += ", "
pick_fstate_str = pick_fstate_str[:-2]
if bif_param == "cl":
    print("first state for cl#"+str(pick)+" "+str(cl[pick])+"ms from "+prefix)
elif bif_param == "sev":
    print("first state for sev#"+str(pick)+" "+str(sev[pick])+" from "+prefix)
print('init_state_str = "'+init_state_str+'"')
print('init_state = "'+ pick_fstate_str + '"')

#%%
