#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, normalize

fig = plt.figure()
ax = fig.add_subplot(111)
plt.ylabel("APD90")
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

plt.ylabel("Membrane potential (t=50ms) [mV]")
plt.xlabel("Excitation cycle length [ms]")



#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
#sep = "\\"
markersize = 20
lw = 1
i = 0

# 100 - 400
prefix = 'beef_xiaepi_16'
skip_beats = 40
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_max, v_min, v_s, apd90):
#    normalize(darray)
marker = "."
markersize =  20
#for row in v_max.T: ax.scatter(cl, row, color = "r", label = "$v_{max}$", marker = marker, s=markersize)
#for row in v_min.T: ax.scatter(cl, row, color = "g", label = "$v_{min}$", marker = marker, s=markersize)
for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix, marker = marker, s=markersize)
for row in apd90.T[::]: ax.scatter(cl, row, color = "r",marker = marker, s=markersize)

# 100 - 400
prefix = 'beef_xiaepi_17'
skip_beats = 20
color = "b"
marker = "3"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# 100 - 400
prefix = 'bif_xiaepi_1'
skip_beats = 20
color = "g"
marker = "3"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# 100 - 400
prefix = 'bif_xiaepi_2'
skip_beats = 20
color = "orange"
marker = "3"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)


# 500 - 800
prefix = 'beef_xiaepi_8'
skip_beats = 10
color = "orange"
marker = "x"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# 600 - 900
prefix = 'beef_xiaepi_12'
skip_beats = 10
color = "g"
marker = "3"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# 800 - 1100
prefix = 'beef_xiaepi_9'
skip_beats = 10
color = "orange"
marker = "+"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# 900 - 1000
prefix = 'beef_xiaepi_14'
skip_beats = 10
color = "b"
marker = "1"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# 1000 - 1300
prefix = 'beef_xiaepi_15'
skip_beats = 10
color = "b"
marker = "1"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)


prefix = 'bif_xiaepi_7'
skip_beats = 10
color = "magenta"
marker = "1"
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize, lw = lw)

# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc='lower right')
plt.title("Xia -> WT")


#from subprocess import call
#filename = "tmp.png"
#plt.savefig(filename)
#call(["eog", filename])
