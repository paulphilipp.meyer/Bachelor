#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = "/data.bmp/pmeyer/brugada"
fig = plt.figure()
ax = fig.add_subplot(111)

ids = np.arange(30, 52)

skip = 1
till = -1
every = 10
model = "wt"

for num in ids:
    name = "threshold_wt_"+str(num)
    
    directory = dir_data + "/" + name
    t = np.load(directory+'/times.npy')
    states = np.load(directory+'/states.npy')
    ut = np.copy(t)
    unwrap(ut)
    
    t = t[skip:till:every]
    states = states[skip:till:every]
    ut = ut[skip:till:every]
    
    ii = 0
    lines = []
    labels = []
    
    # values ###################################################################
    
    #for i in range(0, states.shape[1]):
    for i in (0,):
    #for i in (0,1,3,4):
        labels.append(ind2name(i, model = model))
        
        # normal
        lines.append(ax.plot(ut, (((states[:, i]))),visible=True, label = str(num), lw=1)[0])
        
        # normalized
    #    lines.append(ax.plot(t, normalized(states[:, i]), ',',  visible=True, label = labels[-1]+ "(renormalized)", lw=1.5)[0])
    
        # to zero
    #    lines.append(ax.plot(ut, states[:, i]-states[-1,i] ,visible=True, label = labels[-1], lw=1)[0])


plt.title(name)
plt.xlabel("time [ms]")
plt.ylabel("timestep [ms]")
#plt.xlim((35000,40000))
#plt.ylim((-0.01, 0.01))
plt.legend()


#plt.show(block=True)

