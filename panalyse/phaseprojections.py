#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import numpy as np
from helper import unwrap, ind2name, normalized
import subprocess

interactive = False
filename = 'tmp.png'

if not interactive:
    plt.ioff()
else:
    plt.ion()

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = "/data.bmp/pmeyer/brugada"

#ids = np.arange()

name = "tims_TT-BrS_0"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states


t = np.load(directory+'/times.npy', mmap_mode='r')
states = np.load(directory+'/states.npy', mmap_mode='r')

#ut = np.copy(t)
#unwrap(ut)

skip = 100000000
till = 111110000
every = 100
#t = t[skip:till:every]
#states = states[skip:till:every]
#ut = ut[skip:till:every]

#dt = np.diff(ut)

#%%
for i in range(1):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #fig2 = plt.figure()
    #ax2 = fig2.add_subplot(111)
    ii = 0
    lines = []
    labels = []
    
    first = np.random.choice(np.arange(states.shape[1]))
    second = np.random.choice(np.arange(states.shape[1]))
    while(second == first):
        second = np.random.choice(np.arange(states.shape[1]))
#    first = 4
#    second = 2
	first = 3
	second = 4
    
    # values ###################################################################
#    ax.scatter(states[skip:till:every,first], states[skip:till:every, second], marker='.', s=5, c=t[skip:till:every])
    ax.scatter(states[skip:till:every,first], states[skip:till:every, second], marker='.', s=5, c=np.arange(len(states[skip:till:every, second])), cmap='hot')
#    ax.scatter(t[skip:till:every], states[skip:till:every, 0], marker='.', s=1, c=t[skip:till:every])
    #ax2.scatter(states[:,0], states[:, 2], marker='.', s=1)
    #ax2.plot(t, states[:, 0])
    
    #plt.title(name)
    plt.xlabel(str(first))
    plt.ylabel(str(second))
#plt.xlim((35000,40000))
#plt.ylim((-0.01, 0.01))


#plt.show(block=True)

# save / display plot
plt.savefig(filename)
#os.system("exo-open "+filename+" &")
if not interactive:
	#subprocess.Popen(["exo-open "+filename+" &"],shell=True)
    subprocess.Popen(["eog "+filename+" &"],shell=True)

