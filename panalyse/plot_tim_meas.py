#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import normalized, unwrap
from subprocess import call
#helper.initLatex()

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'

sep = "/"

name = "test_7850"


directory = data_dir + sep + name + sep

apd50 = np.load(directory + "apd50.npy")
apd90 = np.load(directory + "apd90.npy")
v_max = np.load(directory + "v_max.npy")
v_min = np.load(directory + "v_min.npy")
v_10 = np.load(directory + "v_10.npy")
v_50 = np.load(directory + "v_50.npy")
v_100 = np.load(directory + "v_100.npy")
v_200 = np.load(directory + "v_200.npy")
beats = np.arange(len(v_max))


meas =   [ apd50 ,  apd90 ,  v_max ,  v_min ,  v_10 ,  v_50 ,  v_100 ,  v_200 ]
labels = ["apd50", "apd90", "v_max", "v_min", "v_10", "v_50", "v_100", "v_200"]

fig = plt.figure()
ax = fig.add_subplot(111)


marker = "."
for i in (0,1,2,3,4,5,6,7):
#for i in (2,):
    measure = meas[i]
    ax.plot(beats, measure, marker = marker, label=labels[i])
#        ax.plot(beats, normalized(measure), marker = marker, label=labels[i])
plt.title(name)
plt.legend()
#%%

tload = np.load(directory + "times.npy", mmap_mode='r')
vload = np.load(directory + "voltages.npy", mmap_mode='r')
every = 10
till = 10000000
t = tload[:till:every]
voltages = vload[:till:every]

#ut = t.copy()
#unwrap(ut)

fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(t, voltages)