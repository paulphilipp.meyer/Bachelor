#%%
import matplotlib.pyplot as plt
import numpy as np
from helper import get_state_names

#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

prefix = "test_ttepi_72"


directory = dir_data + "/" + prefix

till = 5000
t = np.load(directory+'/times.npy')[:till]
states = np.load(directory+'/states.npy')[:till]

#%%
from helper import get_state_names
#model = "xiaepi"

state_names = get_state_names(model)
assert len(state_names) == states.shape[1]

d = dict(zip(state_names, states))

if model == "ttepi":
    assert state_names == ['Voltage', 'Ca_i_total', 'Ca_SR_total', 'Na_i', 'K_i', 'm_gate', 'h_gate', 'j_gate', 'X_r1_gate',
                           'X_r2_gate', 'X_s_gate', 'r_gate', 's_gate', 'd_gate', 'f_gate', 'F_Ca', 'g_gate']
    Voltage     = states[:,0]
    Ca_i_total  = states[:,1]
    Ca_SR_total = states[:,2]
    Na_i        = states[:,3]

    K_i         = states[:,4]
    m_gate      = states[:,5]
    h_gate      = states[:,6]
    j_gate      = states[:,7]
    X_r1_gate   = states[:,8]
    X_r2_gate   = states[:,9]
    X_s_gate    = states[:,10]
    r_gate      = states[:,11]
    s_gate      = states[:,12]
    d_gate      = states[:,13]
    f_gate      = states[:,14]
    F_Ca        = states[:,15]
    g_gate      = states[:,16]
    
    constants_names = ["Ko", "Cao", "Nao", "Vc", "Vsr", "Bufc", "Kbufc", "Bufsr", "Kbufsr", "taufca", "taug", "Vmaxup", "Kup", "R", "F", "T", "RTONF", "CAPACITANCE", "Gkr", "pKNa", "Gks", "GK1", "Gto", "GNa", "GbNa", "KmK", "KmNa", "knak", "GCaL", "GbCa", "knaca", "KmNai", "KmCa", "ksat", "n", "GpCa", "KpCa", "GpK", "FONRT", "inverseVcF2", "inverseVcF", "Kupsquare" ]
    constants_values = [5.4, 2.0, 140.0, 0.016404, 0.001094, 0.15, 0.001, 10.0, 0.3, 2.0, 2.0, 0.000425, 0.00025, 8314.472, 96485.3415, 310.0, 26.7137606597, 0.185, 0.096, 0.03, 0.245, 5.405, 0.294, 14.838, 0.00029, 1.0, 40.0, 1.362, 0.000175, 0.000592, 1000, 87.5, 1.38, 0.1, 0.35, 0.825, 0.0005, 0.0146, 0.0374338908227, 0.000315906749849, 0.000631813499697, 6.25e-08 ]
    Ko          = constants_values[0]
    Cao         = constants_values[1]
    Nao         = constants_values[2]
    Vc          = constants_values[3]
    Vsr         = constants_values[4]
    Bufc        = constants_values[5]
    Kbufc       = constants_values[6]
    Bufsr       = constants_values[7]
    Kbufsr      = constants_values[8]
    taufca      = constants_values[9]
    taug        = constants_values[10]
    Vmaxup      = constants_values[11]
    Kup         = constants_values[12]
    R           = constants_values[13]
    F           = constants_values[14]
    T           = constants_values[15]
    RTONF       = constants_values[16]
    CAPACITANCE = constants_values[17]
    Gkr         = constants_values[18]
    pKNa        = constants_values[20]
    Gks         = constants_values[20]
    GK1         = constants_values[21]
    Gto         = constants_values[22]
    GNa         = constants_values[23]
    GbNa        = constants_values[24]
    KmK         = constants_values[25]
    KmNa        = constants_values[26]
    knak        = constants_values[27]
    GCaL        = constants_values[28]
    GbCa        = constants_values[29]
    knaca       = constants_values[30]
    KmNai       = constants_values[31]
    KmCa        = constants_values[32]
    ksat        = constants_values[33]
    n           = constants_values[34]
    GpCa        = constants_values[35]
    KpCa        = constants_values[36]
    GpK         = constants_values[37]
    FONRT       = constants_values[38]
    inverseVcF2 = constants_values[39]
    inverseVcF  = constants_values[40]
    Kupsquare   = constants_values[41]   
    
    bc = Bufc - Ca_i_total + Kbufc
    cc = Kbufc * Ca_i_total
    Ca_i_free = ( np.sqrt(bc*bc+4*cc) - bc) / 2
    bjsr = Bufsr - Ca_SR_total + Kbufsr
    cjsr = Kbufsr * Ca_SR_total
    Ca_SR_free = ( np.sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2                                                               

    Ek = RTONF * np.log( Ko / K_i )
    Ena = RTONF * np.log( Nao / Na_i )
    Eks = RTONF * np.log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i))
    Eca = 0.5 * RTONF * np.log( Cao / Ca_i_free )
    Ak1 = 0.1 / ( 1.0 + np.exp( 0.06*(Voltage-Ek-200) ) )
    Bk1 = ( 3. * np.exp( 0.0002*(Voltage-Ek+100) ) + np.exp(0.1*(Voltage-Ek-10)))  /  (1. + np.exp(-0.5*(Voltage-Ek)))
    rec_iK1 = Ak1 / ( Ak1+Bk1 )
    rec_iNaK = 1./( 1.+0.1245*np.exp(-0.1*Voltage*FONRT)+0.0353*np.exp(-Voltage*FONRT) )
    rec_ipK = 1. / (1.+np.exp( (25-Voltage)/5.98) )

    # currents ################################################# 
    INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena)
    ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( np.exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( np.exp(2*Voltage*FONRT)-1.)
    Ito = Gto*r_gate*s_gate*(Voltage-Ek)
    IKr = Gkr*np.sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek)
    IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks)
    IK1 = GK1*rec_iK1*(Voltage-Ek)
    INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*np.exp((n-1)*Voltage*FONRT)))*(np.exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-np.exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5)
    INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK
    IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free)
    IpK = GpK*rec_ipK*(Voltage-Ek)
    IbNa = GbNa*(Voltage-Ena)
    IbCa = GbCa*(Voltage-Eca)

    sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK #+ I_stim

    Ileak = 0.00008 * (Ca_SR_free - Ca_i_free)
    SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) )
    CaSRsquare = Ca_SR_free * Ca_SR_free
    A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232
    Irel = A * d_gate * g_gate 
    
    currents =       [ INa ,  ICaL ,  Ito ,  IKr ,  IKs ,  IK1 ,  INaCa ,  INaK ,  IpCa ,  IpK ,  IbNa ,  IbCa]
    currents_names = ["INa", "ICaL", "Ito", "IKr", "IKs", "IK1", "INaCa", "INaK", "IpCa", "IpK", "IbNa", "IbCa"]
elif model == "xiaepi":

    assert state_names == ["Voltage", "Ca_i_total", "Ca_SR_total", "Na_i", "K_i", "m_gate", "h_gate", "j_gate", "X_r1_gate", "X_r2_gate", "X_s_gate", "r_gate", "s_gate", "d_gate", "f_gate", "F_Ca", "g_gate", "mL_gate", "hL_gate"]
    I_stim = 0
    Ko          = 5.4
    Cao         = 2.0
    Nao         = 140.0
    Vc          = 0.016404
    Vsr         = 0.001094
    Bufc        = 0.15
    Kbufc       = 0.001
    Bufsr       = 10.0
    Kbufsr      = 0.3
    taufca      = 2.0
    taug        = 2.0
    Vmaxup      = 0.000425
    Kup         = 0.00025
    R           = 8314.472
    F           = 96485.3415
    T           = 310.0
    RTONF       = 26.7137606597
    CAPACITANCE = 0.185
    Gkr         = 0.096
    pKNa        = 0.03
    GK1         = 5.405
    GNa         = 14.838
    GbNa        = 0.00029
    KmK         = 1.0
    KmNa        = 40.0
    knak        = 1.362
    GCaL        = 0.000175
    GbCa        = 0.000592
    knaca       = 1000
    KmNai       = 12.3 ## 87.5 <- tentusscher
    KmCa        = 1.38
    ksat        = 0.27 ## 0.1 <- tentusscher
    n           = 0.35
    GpCa        = 0.825
    KpCa        = 0.0005
    GpK         = 0.0146
    FONRT       = 0.0374338908227
    inverseVcF2 = 0.000315906749849
    inverseVcF  = 0.000631813499697
    Kupsquare   = 6.25e-08
    eta         = 0.35
    KmNao       = 87.5
    KmCai       = 0.0036
    KmCao       = 1.3
    KmCaact     = 1.25e-4
    if model=="xiaepi":
        	GNaL    = 0.0065
        	numax   = 5.0
        	Gto     = 0.294
        	Gks     = 0.245
    elif model=="xiaendo":
        	GNaL    = 0.0065
        	numax   = 3.9
        	Gto     = 0.053
        	Gks     = 0.225
    elif model=="xiamcell":
        	GNaL    = 0.0095
        	numax   = 4.3
        	Gks     = 0.062
        	Gto     = 0.238
    Nao_cubed   = Nao*Nao*Nao
    KmNao_cubed = KmNao*KmNao*KmNao
    KmNai_cubed = KmNai*KmNai*KmNai

    Voltage     = states[:,0]
    Ca_i_total  = states[:,1]
    Ca_SR_total = states[:,2]
    Na_i        = states[:,3]
    K_i         = states[:,4]
    m_gate      = states[:,5]
    h_gate      = states[:,6]
    j_gate      = states[:,7]
    X_r1_gate   = states[:,8]
    X_r2_gate   = states[:,9]
    X_s_gate    = states[:,10]
    r_gate      = states[:,11]
    s_gate      = states[:,12]
    d_gate      = states[:,13]
    f_gate      = states[:,14]
    F_Ca        = states[:,15]
    g_gate      = states[:,16]
    mL_gate     = states[:,17]
    hL_gate     = states[:,18]

    bc = Bufc - Ca_i_total + Kbufc
    cc = Kbufc * Ca_i_total
    Ca_i_free = ( np.sqrt(bc*bc+4*cc) - bc) / 2
    bjsr = Bufsr - Ca_SR_total + Kbufsr
    cjsr = Kbufsr * Ca_SR_total
    Ca_SR_free = ( np.sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2
    Ek = RTONF * np.log( Ko / K_i )
    Ena = RTONF * np.log( Nao / Na_i )
    Eks = RTONF * np.log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i))
    Eca = 0.5 * RTONF * np.log( Cao / Ca_i_free )
    Ak1 = 0.1 / ( 1.0 + np.exp( 0.06*(Voltage-Ek-200) ) )
    Bk1 = ( 3. * np.exp( 0.0002*(Voltage-Ek+100) ) + np.exp(0.1*(Voltage-Ek-10)))  /  (1. + np.exp(-0.5*(Voltage-Ek)))
    rec_iK1 = Ak1 / ( Ak1+Bk1 )
    rec_iNaK = 1./( 1.+0.1245*np.exp(-0.1*Voltage*FONRT)+0.0353*np.exp(-Voltage*FONRT) )
    rec_ipK = 1. / (1.+np.exp( (25-Voltage)/5.98) )
    
    # currents ################################################# 
    INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena)
    ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( np.exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( np.exp(2*Voltage*FONRT)-1.)
    Ito = Gto*r_gate*s_gate*(Voltage-Ek)
    IKr = Gkr*np.sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek)
    IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks)
    IK1 = GK1*rec_iK1*(Voltage-Ek)
    # INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*np.exp((n-1)*Voltage*FONRT)))*(np.exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-np.exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5)
    INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK
    IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free)
    IpK = GpK*rec_ipK*(Voltage-Ek)
    IbNa = GbNa * (Voltage-Ena)
    IbCa = GbCa * (Voltage-Eca)
    INaL = GNaL * mL_gate*mL_gate*mL_gate * hL_gate * ( Voltage-Ena )
    Na_i_cubed = Na_i*Na_i*Na_i
    INaCa = 1 / ( 1 + KmCaact*KmCaact/(2.25*Ca_i_free*Ca_i_free)) *        \
    		numax*( Na_i_cubed*Cao*np.exp(eta*Voltage*FONRT) - 1.5*Nao_cubed*Ca_i_free*np.exp((eta-1)*Voltage*FONRT )) / ( \
    			( 1+ksat*np.exp((eta-1)*Voltage*FONRT) ) * (            \
    				KmCao*Na_i_cubed +                               \
    				1.5*KmNao_cubed*Ca_i_free +                      \
    				KmNai_cubed*Cao*(1+1.5*Ca_i_free/KmCai) +        \
    				KmCai*Nao_cubed*(1+Na_i_cubed/KmNai_cubed) +     \
    				Na_i_cubed*Cao +                                 \
    				1.5*Nao_cubed*Ca_i_free                          \
    			)                                                    \
    		)
    
    sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK + INaL - I_stim
    Ileak = 0.00008 * (Ca_SR_free - Ca_i_free)
    SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) )
    CaSRsquare = Ca_SR_free * Ca_SR_free
    A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232
    Irel = A * d_gate * g_gate
    
    currents =       [ INa ,  INaL,   ICaL ,  Ito ,  IKr ,  IKs ,  IK1 ,  INaCa ,  INaK ,  IpCa ,  IpK ,  IbNa ,  IbCa]
    currents_names = ["INa", "INaL", "ICaL", "Ito", "IKr", "IKs", "IK1", "INaCa", "INaK", "IpCa", "IpK", "IbNa", "IbCa"]

elif model == "xiaepi_bs":
    severity = 1
    assert state_names == ["Voltage", "Ca_i_total", "Ca_SR_total", "Na_i", "K_i", "m_gate", "h_gate", "j_gate", "X_r1_gate", "X_r2_gate", "X_s_gate", "r_gate", "s_gate", "d_gate", "f_gate", "F_Ca", "g_gate", "mL_gate", "hL_gate"]
    I_stim = 0
    Ko          = 5.4
    Cao         = 2.0
    Nao         = 140.0
    Vc          = 0.016404
    Vsr         = 0.001094
    Bufc        = 0.15
    Kbufc       = 0.001
    Bufsr       = 10.0
    Kbufsr      = 0.3
    taufca      = 2.0
    taug        = 2.0
    Vmaxup      = 0.000425
    Kup         = 0.00025
    R           = 8314.472
    F           = 96485.3415
    T           = 310.0
    RTONF       = 26.7137606597
    CAPACITANCE = 0.185
    Gkr         = 0.096
    pKNa        = 0.03
    GK1         = 5.405
    GNa         = 14.838
    GbNa        = 0.00029
    KmK         = 1.0
    KmNa        = 40.0
    knak        = 1.362
    GCaL        = 0.000175 * (1 - 0.5*severity)
    GbCa        = 0.000592
    knaca       = 1000
    KmNai       = 12.3 ## 87.5 <- tentusscher
    KmCa        = 1.38
    ksat        = 0.27 ## 0.1 <- tentusscher
    n           = 0.35
    GpCa        = 0.825
    KpCa        = 0.0005
    GpK         = 0.0146
    FONRT       = 0.0374338908227
    inverseVcF2 = 0.000315906749849
    inverseVcF  = 0.000631813499697
    Kupsquare   = 6.25e-08
    eta         = 0.35
    KmNao       = 87.5
    KmCai       = 0.0036
    KmCao       = 1.3
    KmCaact     = 1.25e-4
    if model=="xiaepi":
        	GNaL    = 0.0065
        	numax   = 5.0
        	Gto     = 0.294 * (1+5*severity)
        	Gks     = 0.245
    elif model=="xiaendo":
        	GNaL    = 0.0065
        	numax   = 3.9
        	Gto     = 0.053
        	Gks     = 0.225
    elif model=="xiamcell":
        	GNaL    = 0.0095
        	numax   = 4.3
        	Gks     = 0.062
        	Gto     = 0.238
    Nao_cubed   = Nao*Nao*Nao
    KmNao_cubed = KmNao*KmNao*KmNao
    KmNai_cubed = KmNai*KmNai*KmNai

    Voltage     = states[:,0]
    Ca_i_total  = states[:,1]
    Ca_SR_total = states[:,2]
    Na_i        = states[:,3]
    K_i         = states[:,4]
    m_gate      = states[:,5]
    h_gate      = states[:,6]
    j_gate      = states[:,7]
    X_r1_gate   = states[:,8]
    X_r2_gate   = states[:,9]
    X_s_gate    = states[:,10]
    r_gate      = states[:,11]
    s_gate      = states[:,12]
    d_gate      = states[:,13]
    f_gate      = states[:,14]
    F_Ca        = states[:,15]
    g_gate      = states[:,16]
    mL_gate     = states[:,17]
    hL_gate     = states[:,18]

    bc = Bufc - Ca_i_total + Kbufc
    cc = Kbufc * Ca_i_total
    Ca_i_free = ( np.sqrt(bc*bc+4*cc) - bc) / 2
    bjsr = Bufsr - Ca_SR_total + Kbufsr
    cjsr = Kbufsr * Ca_SR_total
    Ca_SR_free = ( np.sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2
    Ek = RTONF * np.log( Ko / K_i )
    Ena = RTONF * np.log( Nao / Na_i )
    Eks = RTONF * np.log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i))
    Eca = 0.5 * RTONF * np.log( Cao / Ca_i_free )
    Ak1 = 0.1 / ( 1.0 + np.exp( 0.06*(Voltage-Ek-200) ) )
    Bk1 = ( 3. * np.exp( 0.0002*(Voltage-Ek+100) ) + np.exp(0.1*(Voltage-Ek-10)))  /  (1. + np.exp(-0.5*(Voltage-Ek)))
    rec_iK1 = Ak1 / ( Ak1+Bk1 )
    rec_iNaK = 1./( 1.+0.1245*np.exp(-0.1*Voltage*FONRT)+0.0353*np.exp(-Voltage*FONRT) )
    rec_ipK = 1. / (1.+np.exp( (25-Voltage)/5.98) )
    
    # currents ################################################# 
    INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena)
    ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( np.exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( np.exp(2*Voltage*FONRT)-1.)
    Ito = Gto*r_gate*s_gate*(Voltage-Ek)
    IKr = Gkr*np.sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek)
    IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks)
    IK1 = GK1*rec_iK1*(Voltage-Ek)
    # INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*np.exp((n-1)*Voltage*FONRT)))*(np.exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-np.exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5)
    INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK
    IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free)
    IpK = GpK*rec_ipK*(Voltage-Ek)
    IbNa = GbNa * (Voltage-Ena)
    IbCa = GbCa * (Voltage-Eca)
    INaL = GNaL * mL_gate*mL_gate*mL_gate * hL_gate * ( Voltage-Ena )
    Na_i_cubed = Na_i*Na_i*Na_i
    INaCa = 1 / ( 1 + KmCaact*KmCaact/(2.25*Ca_i_free*Ca_i_free)) *        \
    		numax*( Na_i_cubed*Cao*np.exp(eta*Voltage*FONRT) - 1.5*Nao_cubed*Ca_i_free*np.exp((eta-1)*Voltage*FONRT )) / ( \
    			( 1+ksat*np.exp((eta-1)*Voltage*FONRT) ) * (            \
    				KmCao*Na_i_cubed +                               \
    				1.5*KmNao_cubed*Ca_i_free +                      \
    				KmNai_cubed*Cao*(1+1.5*Ca_i_free/KmCai) +        \
    				KmCai*Nao_cubed*(1+Na_i_cubed/KmNai_cubed) +     \
    				Na_i_cubed*Cao +                                 \
    				1.5*Nao_cubed*Ca_i_free                          \
    			)                                                    \
    		)
    
    sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK + INaL - I_stim
    Ileak = 0.00008 * (Ca_SR_free - Ca_i_free)
    SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) )
    CaSRsquare = Ca_SR_free * Ca_SR_free
    A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232
    Irel = A * d_gate * g_gate
    
    currents =       [ INa ,  INaL,   ICaL ,  Ito ,  IKr ,  IKs ,  IK1 ,  INaCa ,  INaK ,  IpCa ,  IpK ,  IbNa ,  IbCa]
    currents_names = ["INa", "INaL", "ICaL", "Ito", "IKr", "IKs", "IK1", "INaCa", "INaK", "IpCa", "IpK", "IbNa", "IbCa"]
