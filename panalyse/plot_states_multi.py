#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import numpy as np
from helper import unwrap, ind2name, normalized



#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = "/data.bmp/pmeyer/brugada"
fig = plt.figure()
ax = fig.add_subplot(111)


model = "xiaepi"

name = "tmp_tim_3"
directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states
t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')
ut = np.copy(t)
unwrap(ut)
dt = np.diff(ut)

skip = 1
till = -1
every = 10
t = t[skip:till:every]
states1 = states[skip:till:every]
ut1 = ut[skip:till:every]

name = "tmp_tim_5"
directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states
t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')
ut = np.copy(t)
unwrap(ut)
dt = np.diff(ut)

skip = 1
till = -1
every = 10
t = t[skip:till:every]
states2 = states[skip:till:every]
ut2 = ut[skip:till:every]


plt.plot(ut1, states1[:,0])
plt.plot(ut2, states2[:,0])


# values ###################################################################
#%%
#for i in range(0, states.shape[1]):
for i in (0,):
#for i in (0,11,12):
    labels.append(ind2name(i, model = model))
    
    # normal
    lines.append(ax.plot(ut, (((states[:, i]))),visible=True, label = labels[-1], lw=1)[0])
    
    # normalized
#    lines.append(ax.plot(t, normalized(states[:, i]), ',',  visible=True, label = labels[-1]+ "(renormalized)", lw=1.5)[0])

    # to zero
#    lines.append(ax.plot(ut, states[:, i]-states[-1,i] ,visible=True, label = labels[-1], lw=1)[0])

# differentials ############################################################
#for i in range(0, states.shape[1]):
#for i in (0,1,2,3,4,5):
#for i in (6,7,8,9,10):
#for i in (11,12,13,14,15):
#for i in (16,17,18):
#    labels.append(ind2name(i, model = model))
#    lines.append(ax.plot(ut[1:], np.abs(np.diff(states[:, i])/dt), visible=True, label = "d/dt "+labels[-1], lw=1)[0])
#    lines.append(ax.plot(ut[1:], np.abs(np.diff(states[:, i])/dt/states[1:,i]), visible=True, label = "(d/dt "+labels[-1]+")/"+labels[-1], lw=1)[0])
    
#ax.plot(ut[1:], dt, label = "dt")
#ax.plot(ut, t/1000)
#abschanges = np.zeros(states.shape[1])
#relchanges = np.zeros(states.shape[1])
#for i in range(0, states.shape[1]):
    



plt.title(name)
plt.xlabel("time [ms]")
plt.ylabel("timestep [ms]")
#plt.xlim((35000,40000))
#plt.ylim((-0.01, 0.01))
plt.legend()


#plt.show(block=True)

