import numpy as np
import argparse

parser = argparse.ArgumentParser(description='Print given npy file.')
parser.add_argument('filename')
args = parser.parse_args()

data = np.load(args.filename)
print(data)
