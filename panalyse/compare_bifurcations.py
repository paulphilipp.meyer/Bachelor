#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, normalize

fig = plt.figure()
ax = fig.add_subplot(211)
plt.ylabel("APD90")
ax2 = fig.add_subplot(212)
plt.ylabel("V_s")
plt.xlabel("Cycle length [ms]")

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
#sep = "\\"

prefix = 'beef_xiaepi_bs_20'
skip_beats = 20
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_max, v_min, v_s, apd90):
#    normalize(darray)
marker = "+"
markersize = 30
#for row in v_max.T: ax.scatter(cl, row, color = "r", label = "$v_{max}$", marker = marker, s=markersize)
#for row in v_min.T: ax.scatter(cl, row, color = "g", label = "$v_{min}$", marker = marker, s=markersize)
for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix , marker = marker, s=markersize)
for row in apd90.T[::]: ax.scatter(cl, row, color = "r", marker = marker, s=markersize)

prefix = 'beef_xiaepi_bs_21'
color = "b"
skip_beats = 20
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_max, v_min, v_s, apd90):
#    normalize(darray)
marker = "+"
markersize = 30
#for row in v_max.T: ax.scatter(cl, row, color = "r", label = "$v_{max}$", marker = marker, s=markersize)
#for row in v_min.T: ax.scatter(cl, row, color = "g", label = "$v_{min}$", marker = marker, s=markersize)
for row in v_s.T: ax2.scatter(cl, row, color = color, label = prefix , marker = marker, s=markersize)
for row in apd90.T[::]: ax.scatter(cl, row, color = color, marker = marker, s=markersize)



# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc='lower right')



#from subprocess import call
#filename = "tmp.png"
#plt.savefig(filename)
#call(["eog", filename])
