#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, normalize
mpl.rc('text', usetex=False)

fig = plt.figure()
ax = fig.add_subplot(111)
plt.ylabel("APD90")
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
plt.ylabel("V_s")
plt.xlabel("relative severity of the disease")

plt.title("Xia linearer Parameterübergang WT->Brugada \@ cl1000 ms")



#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
lw = "0"

skip_beats = 50
markersize = 30
marker = '.'
# ueberblick cl1000: 'bif_sev_4' 'bif_sev_2'
# ueberblick cl1148: 'bif_sev_6' 'bif_sev_7'

name = 'bif_sev_15'
color = "g"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_16'
color = "orange"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_17'
color = "g"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_18'
color = "orange"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_19'
color = "g"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_20'
color = "orange"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_2'
color = "r"
directory = dir_data + sep + name  + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)
#
#
name = 'bif_sev_3'
color = "magenta"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)
#
name = 'bif_sev_12'
color = "b"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)

name = 'bif_sev_14'
color = "r"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,label = name, s = markersize, lw=lw)




# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc='lower left')

#plt.title(prefix)

plt.show()

#from subprocess import call
#filename = "tmp.png"
#
#plt.savefig(filename)
#call(["eog", filename])
