#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

base_colors = ['#007f0a', '#0a007f', '#7f0a00']

def initLatex():
#    mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})		# These two lines
    mpl.rc('font', **{'family': 'serif', 'serif': ['Latin Modern']})		# These two lines
    mpl.rc('text', usetex=True)							# are needed for LaTeX

    latex_kevin = {
        'text.latex.preamble': [
            r'\usepackage{txfonts}',
            r'\usepackage{lmodern}',
            r'\usepackage{gensymb}',
            r'\usepackage{siunitx}',
            r'\usepackage{verbatim}'
            ],
        'text.latex.unicode': True,             # This one lets you use Unicode strings.
        'font.size': 12                        # You can set the general font size here.
    }
    mpl.rcParams.update(latex_kevin)


#    mpl.rc('text', usetex=True)
#    pgf_with_pdflatex = {
#        "pgf.texsystem": "pdflatex",
#        "pgf.preamble": [
#             r"\usepackage[utf8x]{inputenc}",
#             r"\usepackage[T1]{fontenc}",
#             r"\usepackage{cmbright}",
#             ]
#    }
#    mpl.rcParams.update(pgf_with_pdflatex)


def unwrap(data):
    cuts = []
    corrections = [0]
    for i in range(1, len(data)):
        if (data[i] < data[i-1]):
            cuts += [i]
            corrections += [corrections[-1] + data[i-1],]
    cuts += [len(data),]
    corrections.pop(0)
    #print(cuts)
    #print(corrections)
    beatnumbers = np.zeros_like(data)
    i_beat = 1
    for i in range(len(cuts)-1):
        data[cuts[i]:cuts[i+1]] += corrections[i]
        beatnumbers[cuts[i]:cuts[i+1]] += i_beat
        i_beat += 1
    return beatnumbers

def sep_beats(times, voltages):
    cuts = [0]
    for i in range(1, len(times)):
        if (times[i] < times[i-1]):
            cuts += [i]
    cuts += [len(times)]
    numpoints = np.diff(cuts)
    numbeats = len(cuts)-1
    data = np.empty((2, numbeats, max(numpoints)))
    data.fill(np.nan)
    for i in range(len(cuts)-1):
        data[0, i, 0:numpoints[i]] = times[cuts[i]:cuts[i+1]]
        data[1, i, 0:numpoints[i]] = voltages[cuts[i]:cuts[i+1]]
    return data
    

def count_resets(data):
    count = 0
    for i in range(1, len(data)):
        if (data[i] < data[i-1]):
            count += 1
    return count
    
def get_state_names(model):
    if model == "wt":
        s = ["Voltage","h_gate","m_gate","j_gate","d_gate","f_gate","X_r","Ca_i","Na_i","K_i","Ca_JSR","Ca_NSR","X_s","b_gate","g_gate","X_s2","I_rel"]
    elif model == "bs":
        s = ["Voltage","d_gate","f_gate","X_r","Ca_i","Na_i","K_i","Ca_JSR","Ca_NSR","X_s","b_gate","g_gate","X_s2","I_rel","y_gate","z_gate"]
        for i in range(13):
            s += ["markov_" + str(i)]
    elif model == "wtm":
        s = ["Voltage","d_gate","f_gate","X_r","Ca_i","Na_i","K_i","Ca_JSR","Ca_NSR","X_s","b_gate","g_gate","X_s2","I_rel","y_gate","z_gate"]
        for i in range(9):
            s += ["markov_" + str(i)]
    elif model == "ttepi":
        s = ["Voltage", "Ca_i_total", "Ca_SR_total", "Na_i", "K_i", "m_gate", "h_gate", "j_gate", "X_r1_gate", "X_r2_gate", "X_s_gate", "r_gate", "s_gate", "d_gate", "f_gate", "F_Ca", "g_gate"]
    elif model == "xiaepi" or "xiaepi_bs":
        s = ["Voltage", "Ca_i_total", "Ca_SR_total", "Na_i", "K_i", "m_gate", "h_gate", "j_gate", "X_r1_gate", "X_r2_gate", "X_s_gate", "r_gate", "s_gate", "d_gate", "f_gate", "F_Ca", "g_gate", "mL_gate", "hL_gate"]
    else:
        raise NotImplementedError
        
    return s

def ind2name(index, model):      
    return get_state_names(model)[index]
    
def normalize(data):
    data -= np.min(data)
    peak = np.max(data)
    if peak != 0:
        data /= peak
    
def normalized(data):
    normdata = data[:] - np.min(data)
    upper = np.max(normdata)
    if upper == 0:
        return normdata
    else:
        return normdata / float(upper)
        
#def c0_from_bs_state(x):
#    return x[0]*
        
if __name__ == "__main__":
    dir_helper = os.path.dirname(os.path.realpath(__file__))
    p_dir = os.path.join(dir_helper, "..")
    sys.path.append(dir_helper)