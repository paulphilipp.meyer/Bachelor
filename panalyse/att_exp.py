#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

import matplotlib.pyplot as plt
import matplotlib.animation as animation


#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()

import pickle

#dir_data = '/home/paul/Schreibtisch/Michael Stauske'
dir_data = '/data.bmp/pmeyer/Bachelor/stauske_data/pickle_dicts'


#%%
#for itr in range(len(d['traces'])):
#    print(str(itr)+', ' + str(len(d['traces'][itr])))
#print(d['comment'])
#%%
name = '/BrS1/18-19Nov2014.pkl'
d = pickle.load( open( dir_data + name, "rb" ), encoding='latin1' )
dt = d['dt']
cls = []
unpaced = d['traces'][0]

cls.append('1666')
paced1 = np.vstack(d['traces'][1:21])
num_points1 = paced1.shape[1]
times1 = np.zeros(num_points1)
for i in range(num_points1):
    times1[i] = i*dt
   
cls.append('1250')
paced2 = np.vstack(d['traces'][21:41])
num_points2 = paced2.shape[1]
times2 = np.zeros(num_points2)
for i in range(num_points2):
    times2[i] = i*dt
    
cls.append('1000')
paced3 = np.vstack(d['traces'][41:])
num_points3 = paced3.shape[1]
times3 = np.zeros(num_points3)
for i in range(num_points3):
    times3[i] = i*dt


def update_line(num, times3, paced3, line):
    line.set_data(times3, paced3[num])
    return line,

fig1 = plt.figure()

l, = plt.plot([], [], 'r-')
plt.xlim(0, 1)
plt.ylim(0, 1)
plt.xlabel('x')
plt.title('test')
line_ani = animation.FuncAnimation(fig1, update_line, 25, fargs=(times3, paced3, l),
                                   interval=50, blit=True)
#
#for ts in range(10, 200, 10):
#    fig = plt.figure()
#    ax = fig.add_subplot(111)
#    ax.plot(paced3[:-1,ts],paced3[1:,ts], 'o')


