import numpy as np
import argparse
import sys

parser = argparse.ArgumentParser(description='Print given npy file.')
parser.add_argument('filename')
parser.add_argument('-c', '--column', help="column", type=int, default = -1)
args = parser.parse_args()

data = np.load(args.filename, mmap_mode='r')
print("array of shape" + str(data.shape))
print("column " + str(args.column))

sys.stdout.write(str(data[args.column,0]))
for i in data[args.column, 1:]:
	sys.stdout.write(", " + str(i))
sys.stdout.write("\n")
