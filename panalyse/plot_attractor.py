import matplotlib.pyplot as plt
import numpy as np
from helper import normalized, unwrap
from subprocess import call
#helper.initLatex()

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'

sep = "/"

name = "tim_meas_1"
name = 'timm_TT-BrS_1'


directory = data_dir + sep + name + sep

apd50 = np.load(directory + "apd50.npy")
apd90 = np.load(directory + "apd90.npy")
v_max = np.load(directory + "v_max.npy")
v_min = np.load(directory + "v_min.npy")
v_10 = np.load(directory + "v_10.npy")
v_50 = np.load(directory + "v_50.npy")
v_100 = np.load(directory + "v_100.npy")
v_200 = np.load(directory + "v_200.npy")
beats = np.arange(len(v_max))


meas =   ( apd50 ,  apd90 ,  v_max ,  v_min ,  v_10 ,  v_50 ,  v_100 ,  v_200 )
labels = ["apd50", "apd90", "v_max", "v_min", "v_10", "v_50", "v_100", "v_200"]

end = -1
meacut = []
for mea in meas:
    meacut.append(mea[-end:])


take = (0,1,5,6)
ltake = len(take)
per = 1
cperiod = 3
shift = 1

colors = plt.get_cmap('gist_rainbow')(np.linspace(1,0,cperiod))
c = [colors[q%cperiod] for q in range(len(meacut[im][shift::per]))]
c='r'
alpha = 1
size = 1

fig = plt.figure()
for i in range(ltake):
    im = take[i]
    ax = fig.add_subplot(221+i)
    #plt.plot(meas[i][:-1], meas[i][1:], 'o')
#    what = ax.scatter(meas[im][:-1:2], meas[im][1::2], c=beats[:-5:2])
#    what = ax.scatter(meas[im][:-y:y], meas[im][y::y])
#    what = ax.plot(meas[im][:end-shift:per], meas[im][shift:end:per])
    what = ax.scatter(meacut[im][:-shift:per], meacut[im][shift::per], c=c, alpha =alpha, s = size)
#    what = ax.plot(meacut[im][:-shift:per], meacut[im][shift::per])
#    plt.xlabel(labels[im]+"_n")
#    plt.ylabel(labels[im]+"_n+1")
#    plt.title("Xia->Bs @cl 1000ms")
    


#cbar = plt.colorbar(what, ax=fig.get_axes())
#cbar = plt.colorbar(what)
#cbar.set_label('n', rotation=0)
#plt.tight_layout()