#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

name = "test_na_da_1"
model = "xiaepi"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states


t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')

skip_first = 0
till = -1
every = 1
t = t[skip_first:till:every]
states = states[skip_first:till:every]

#skip_first = 1000000


ut = np.copy(t)
unwrap(ut)
#%%

run timeseries2currents
#%%
'''     Ionenkonzentrationen
'''
fig = plt.figure()
ax1 = fig.add_subplot(111)
till = -30000
ax1.plot(ut, normalized(Voltage), c='orange')
ax1.plot(ut[:till], normalized(Ca_i_free[:till]), c='g')
ax1.plot(ut[:till], normalized(K_i[:till]), c='r')
ax1.plot(ut[:till], normalized(Na_i[:till]), c='b')

#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.set_xlabel("time [ms]")
ax1.set_ylabel("")
fig.tight_layout()
#%%
'''     Alle Na-Stroeme
'''
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None
#ax.plot(ut)
#ax.plot(ut[skip_first:], (v[skip_first:]), label = "voltage")
#for i in range(0, states.shape[1]):
##for i in black:
#for i in (0,5, 6, 7):
ax1.plot(ut, Voltage, lw=3, c='r')
color_y_axis(ax1, 'r')
ax1.set_ylabel('membrane voltage [mV]')
ax1.set_xlabel("time [ms]")

INa_total = INa+INaL+IbNa+3*INaCa+3*INaK

ax2.plot(ut, INa, c='b', label='INa')
ax2.plot(ut, INaL, c='g', label='INaL')
ax2.plot(ut, IbNa, c='orange', label='IbNa')
ax2.plot(ut, 3*INaCa, c='black', label='INaCa')
ax2.plot(ut, 3*INaK, c='purple', label='INaK')

ax2.plot(ut, INa_total, c='magenta', label='INa total')

color_y_axis(ax2, 'b')
ax2.set_ylabel('currents')

#ax1.plot(ut, t)

plt.title(name)
#plt.xlim((-10,20))
#plt.ylim((-10,20))
plt.xlabel("time [ms]")
plt.legend()
fig.tight_layout()
#plt.show()
#%%
'''   visualize picking
'''
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None
#ax.plot(ut)
#ax.plot(ut[skip_first:], (v[skip_first:]), label = "voltage")
#for i in range(0, states.shape[1]):
##for i in black:
#for i in (0,5, 6, 7):
ax1.plot(ut, Voltage, lw=3, c='r')
color_y_axis(ax1, 'r')
ax1.set_ylabel('membrane voltage [mV]')
ax1.set_xlabel("time [ms]")

ax2.plot(ut, INa, c='b', label='INa')

color_y_axis(ax2, 'b')
ax2.set_ylabel('INa')


pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INa = np.ma.masked_where(np.ma.getmask(pick_ut), INa)

ax2.plot(ut, pick_INa, 'x')

plt.title(name)
#plt.xlim((-10,20))
#plt.ylim((-10,20))
plt.xlabel("time [ms]")
plt.legend()
fig.tight_layout()
#plt.show()
#%%
#np.ma.getmask(pick_t)
pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INa = np.ma.masked_where(np.ma.getmask(pick_ut), INa)
#pick_INa = INa * mask
fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1.plot(ut, Voltage, c='orange')
#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.plot(pick_v, pick_INa, 'x')
ax1.set_xlabel("membrane potential [mV]")
ax1.set_ylabel("INa")
fig.tight_layout()

#%%
#np.ma.getmask(pick_t)
pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INaL = np.ma.masked_where(np.ma.getmask(pick_ut), INaL)
#pick_INa = INa * mask
fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1.plot(ut, Voltage, c='orange')
#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.plot(pick_v, pick_INaL, 'x')
ax1.set_xlabel("membrane potential [mV]")
ax1.set_ylabel("INaL")
fig.tight_layout()

#%%
#np.ma.getmask(pick_t)
pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INaCa = np.ma.masked_where(np.ma.getmask(pick_ut), INaCa)
#pick_INa = INa * mask
fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1.plot(ut, Voltage, c='orange')
#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.plot(pick_v, 3*pick_INaCa, 'x')
ax1.set_xlabel("membrane potential [mV]")
ax1.set_ylabel("INaCa")
fig.tight_layout()

#%%
#np.ma.getmask(pick_t)
INa_total = INa+INaL+IbNa+3*INaCa+3*INaK
pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INa_total = np.ma.masked_where(np.ma.getmask(pick_ut), INa_total)
#pick_INa = INa * mask
fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1.plot(ut, Voltage, c='orange')
#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.plot(pick_v, pick_INa_total, 'x')
ax1.set_xlabel("membrane potential [mV]")
ax1.set_ylabel("INa total")
fig.tight_layout()

#%%
#np.ma.getmask(pick_t)
INaNaL = INa+INaL
pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INaNaL = np.ma.masked_where(np.ma.getmask(pick_ut), INaNaL)
#pick_INa = INa * mask
fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1.plot(ut, Voltage, c='orange')
#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.plot(pick_v, pick_INaNaL, 'x')
ax1.set_xlabel("membrane potential [mV]")
ax1.set_ylabel("INa + INaL")
fig.tight_layout()