import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

fig = plt.figure()
ax = fig.add_subplot(111)

#prefix = "pol_0"
#directory = dir_data + "/" + prefix
#t = np.load(directory+'/times.npy')
#states = np.load(directory+'/states.npy')
#dt = np.diff(t)
#ax.plot(t[1:], normalized(np.abs(np.diff(states[:,0])/dt)), label="dxdt[0]")
#ax.plot(t[1:], normalized(np.abs(np.diff(states[:,1])/dt)), label="dxdt[1]")
#ax.plot(t[1:], dt, label = "dt(abs 1e-6, rel 1e-6)")

prefix = "pol_1"
directory = dir_data + "/" + prefix
t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')
dt = np.diff(t)
ax.plot(t[1:], dt, label = "dt(abs 1e-8, rel 1e-8)")
ax.plot(t, states[:,0], label = "x1")
ax.plot(t, states[:,1], label = "x2")

plt.xlabel("x[0]")
plt.ylabel("x[1]")
plt.legend()

#plt.show(block=True)

