#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import helper
from subprocess import call
#helper.initLatex()

data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
#data_dir = '/scratch16/pmeyer/brugada'

name = "ein_xiaepi_bs_10"

print("load data")
directory = data_dir+'/'+name
times = np.load(directory + '/times.npy')
states = np.load(directory + '/states.npy')
voltages = states[:,0]
#laststate = states[-1]
#voltages = np.load(directory + '/voltages.npy')

#%%
# separate single beats
print("seperate beats")
cuts = []
for i in range(1, times.size):
    if times[i] < times[i-1]:
        cuts.append(i)
vsplit = np.split(voltages, cuts)
tsplit = np.split(times, cuts)

#%%
# determine measures
print("determine measures")
N_beats = len(tsplit)           # warning: extrasystoles are not considered
meas_v_max = np.empty(N_beats)
meas_v_min = np.empty(N_beats)
meas_apd90 = np.empty(N_beats)
meas_v_s   = np.empty(N_beats)  # stroboscopic potential
t_obs = 40                      # delay for stroboscopic potential

for i_beat in range(N_beats):
    print("beat {}".format(i_beat))       
    tb = tsplit[i_beat]
    vb = vsplit[i_beat]
        
    apd_marker = 1          # indicator for apd90: 1:before, 2:after first and 3:after sencond passing of the 90% border
    v_apd = .9 * vb[0]      # 90% border for apd90  
    meas_v_max[i_beat] = vb[0]
    meas_v_min[i_beat] = vb[0]
    
    for i in range(1, tb.size):
        if vb[i] > meas_v_max[i_beat]:
           meas_v_max[i_beat] = vb[i]
            
        elif vb[i] < meas_v_min[i_beat]:
            meas_v_min[i_beat] = vb[i]
        
        if tb[i-1] < t_obs and tb[i] >= t_obs:
            meas_v_s[i_beat] = vb[i-1] + (t_obs - tb[i-1]) * (vb[i] - vb[i-1]) / (tb[i]-tb[i-1]) # linear interpolation
        
        if apd_marker == 1:
            if vb[i] >= v_apd:
                apd_marker = 2
        elif apd_marker == 2:
            if vb[i] < v_apd:
                meas_apd90[i_beat] = tb[i-1] + (v_apd - vb[i-1]) * (tb[i] - tb[i-1]) / (vb[i] - vb[i-1]) #lin interpolation
                apd_marker = 3
    #end beatcycle
    if apd_marker != 3:
        meas_apd90[i_beat] = None
        
#%%
# plot measures
print("plot")
period = 1
# = 0: uebereinander plotten
# = n: modulo n plotten
#toZero = True
toZero = False
last = -1

elements = [meas_v_max, meas_v_min, meas_apd90, meas_v_s]
labels = ["v_max", "v_min", "apd90", "v_s"]
fig = plt.figure()
ax = fig.add_subplot(111)
marker = '.'
if period == 0:
    for i in (0,1,2,3):
        meas = elements[i]
        ax.plot((i, )*N_beats, meas, marker = marker, label=labels[i])
else:
    #last = N_beats%period
    offset = 0
    
    for i in (0,1,2,3):
        meas = elements[i]
        if toZero:
            offset = meas[last]
        ax.plot(range(N_beats)[::period], meas[::period]-offset, marker = marker, label=labels[i])
    
    #ax.plot(range(N_beats)[::period], meas_v_max[::period]-meas_v_max[last], label='v_max')
    #ax.plot(range(N_beats)[::period], meas_v_min[::period]-meas_v_min[last], label='v_min')
    #ax.plot(range(N_beats)[::period], meas_apd90[::period]-meas_apd90[last], label='apd90')
    #ax.plot(range(N_beats)[::period], meas_v_s[::period]-meas_v_s[last], label='v_s')
plt.title(name)
plt.legend()


#filename = "tmp.png"
#
#plt.savefig(filename)
#call(["eog", filename])
