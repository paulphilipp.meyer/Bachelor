
import matplotlib.pyplot as plt
import numpy as np

i_fig = 1
plt.close(i_fig)
fig = plt.figure(i_fig)
ax = fig.add_subplot(111)

prefix = 'einschwingen_wt_3'

dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"

skip_beats = 1


directory = dir_data + "/" + prefix + "/"

cl = np.load(directory+'cl.npy')
v_max = np.load(directory+'v_max.npy')
v_min = np.load(directory+'v_min.npy')
v_s = np.load(directory+'v_s.npy')
apd90 = np.load(directory+'apd90.npy')

icl = 0

lb = 0
rb = -1
ls = "-"
markersize = 2
plt.plot(v_s[icl,lb:rb], ls, label = "v_s", ms = markersize)
plt.plot(v_max[icl,lb:rb], ls, label = "v_max", ms = markersize)
plt.plot(v_min[icl,lb:rb], ls, label = "v_min", ms = markersize)
plt.plot(apd90[icl,lb:rb], ls, label = "apd90", ms = markersize)

handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]

plt.legend(handles, labels, loc = "upper right")
plt.title("cl: " + str(cl[icl]) + "ms")
plt.xlabel("# beat")
plt.ylabel("AP measures")
plt.show()
