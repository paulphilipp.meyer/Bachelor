#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

name = "tims_TT-BrS_0"
prefix = ""

directory = dir_data + "/" + name + "/"
#directory = dir_data + "\\" + name + "\\"

till = 3500
v = np.load(directory + prefix +'voltages.npy', mmap_mode='r')[:till]
t = np.load(directory + prefix +'times.npy', mmap_mode='r')[:till]


# # from states.dat
#t, v = np.loadtxt(directory + '/states.dat', usecols = (0, 1), unpack = True) # states


ut = np.copy(t)
unwrap(ut)

fig = plt.figure()
ax = fig.add_subplot(111)

#ax.plot(ut)
ax.plot(ut, v)
#ax.plot(t, v)

plt.xlabel("time [ms]")
plt.ylabel("membrane potential [mV]")
plt.show()
