#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

fig = plt.figure()
ax = fig.add_subplot(111)
plt.xlabel("time [ms]")

#prefix = "errcontrol_0"
#t = np.load(dir_data + "/" + prefix+'/times.npy')
#unwrap(t)
#states = np.load(dir_data + "/" + prefix+'/states.npy')
#dt = np.diff(t)
#ax.plot(t, normalized(states[:,0]), label = "V - "+prefix, color = "#ff9999")
#ax.plot(t[1:], dt, ",", label = "dt", color = "#ff6666")
#plt.legend()

prefix = "compare_xiaepi_0"
t = np.load(dir_data + "/" + prefix+'/times.npy')
unwrap(t)
states = np.load(dir_data + "/" + prefix+'/states.npy')
#dt = np.diff(t)
ax.plot(t, normalized(states[:,0]), label = "V - "+prefix, color = "#b3ff99")
#ax.plot(t[1:], dt, ",", label = "dt", color = "#66ff33")
plt.legend()

prefix = "compare_xiaepi_1"
t = np.load(dir_data + "/" + prefix+'/times.npy')
unwrap(t)
states = np.load(dir_data + "/" + prefix+'/states.npy')
#dt = np.diff(t)
ax.plot(t, normalized(states[:,0]), label = "V - "+prefix, color = "#99b3ff")
#ax.plot(t[1:], dt, ",", label = "dt", color = "#3366ff")
plt.legend()

#plt.xlim((35000,40000))
#plt.ylim((-0.01, 0.01))



#plt.show(block=True)

