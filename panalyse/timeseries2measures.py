#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

#data_dir = '../data'

dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"

name = 'ein_xiaepi_bs_1'

data = np.load(dir_data + '/' + name)

# separate single beats

cuts = []
for i in range(1, data.shape[1]):
    if data[0][i] < data[0][i-1]:
        cuts.append(i)
cycles = np.split(data, cuts, axis=1)


# determine measures

N_beats = len(cycles)           # warning: extrasystoles are not considered
meas_v_max = np.empty(N_beats)
meas_v_min = np.empty(N_beats)
meas_apd90 = np.empty(N_beats)
meas_v_s   = np.empty(N_beats)  # stroboscopic potential
t_obs = 40                      # delay for stroboscopic potential

for i_beat in range(N_beats):         
    beat = cycles[i_beat]
    apd_marker = 1          # indicator for apd90: 1:before, 2:after first and 3:after sencond passing of the 90% border
    v_apd = .9 * beat[1,0]      # 90% border for apd90  
    for i in range(1, beat.shape[1]):
        if beat[1,i] > meas_v_max[i_beat]:
           meas_v_max[i_beat] = beat[1,i]
            
        elif beat[1,i] < meas_v_min[i_beat]:
            meas_v_min[i_beat] = beat[1,i]
        
        if beat[0,i-1] < t_obs and beat[0, i] >= t_obs:
            meas_v_s[i_beat] = beat[1,i-1] + (t_obs - beat[0,i-1]) * (beat[1,i] - beat[1,i-1]) / (beat[0,i]-beat[0,i-1]) # linear interpolation
        
        if apd_marker == 1:
            if beat[1,i] >= v_apd:
                apd_marker = 2
        elif apd_marker == 2:
            if beat[1,i] < v_apd:
                meas_apd90[i_beat] = beat[0,i-1] + (v_apd - beat[1,i-1]) * (beat[0,i] - beat[0,i-1]) / (beat[1,i] - beat[1,i-1]) #lin interpolation
                apd_marker = 3
    #end beatcycle
    if apd_marker != 3:
        meas_apd90[i_beat] = None
        
        
# plot measures
        
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(range(N_beats), meas_v_max, label='v_max')
ax.plot(range(N_beats), meas_v_min, label='v_min')
ax.plot(range(N_beats), meas_apd90, label='apd90')
ax.plot(range(N_beats), meas_v_s, label='v_s')
plt.legend()
