#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/tim_like_exp.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean

fig = plt.figure(figsize=(figwidth, figheight))

axes=np.empty((3,2), dtype=object)
axes[0,0] = fig.add_subplot(3, 2, 1)
axes[1,0] = fig.add_subplot(3, 2, 3, sharex = axes[0,0], sharey = axes[0,0])
axes[2,0] = fig.add_subplot(3, 2, 5, sharex = axes[0,0], sharey = axes[0,0])

axes[0,1] = fig.add_subplot(3, 2, 2, sharex = axes[0,0], sharey = axes[0,0])
axes[1,1] = fig.add_subplot(3, 2, 4, sharex = axes[0,1], sharey = axes[0,0])
axes[2,1] = fig.add_subplot(3, 2, 6, sharex = axes[0,1], sharey = axes[0,0])


for ax in axes[:-1, :].flat:
    plt.setp(ax.get_xticklabels(), visible=False)
for ax in axes[:,1].flat:
    plt.setp(ax.get_yticklabels(), visible=False)
#for ax in axes:
#    ax.set_ylim((-95,42))

axes[0][0].set_ylim((-95,48))
axes[0][0].set_xlim((-50,700))
axes[0][1].set_xlim((-50,950))


axes[-1][0].set_xlabel("time [ms]", position=(1,0.1))
#axes[-1][1].set_xlabel("time [ms]")
axes[1][0].set_ylabel("membrane voltage [mV]")
#for ax in axes[1:]:
##    ax.axis('off')
#    ax.get_xaxis().set_ticks([])
#    ax.get_yaxis().set_ticks([])
fig.subplots_adjust(hspace=0.1, wspace=0.05)
plt.subplots_adjust(top=0.99, bottom=0.17, left = 0.11, right = 1.02)

bbox_props = dict(boxstyle="round", fc="w", ec="1", alpha=0.9)
boxx, boxy = (0.95, 0.85)
lw = 0.7
#%%
#name = "tims_exp_TT_3"
#directory = dir_data + "/" + name + "/"
#v = np.load(directory +'states.npy', mmap_mode='r').T[0]
#t = np.load(directory +'times.npy', mmap_mode='r')
#
#i06Hz = i08Hz = i10Hz = 0
#i_excite = 0
#i_beat = 0
#paced1 = paced2 = paced3 = [None,]* 20
#times1 = times2 = times3 = [None,]* 20
## find points where cl changes
#for i in range(1, len(t)):
#    if (t[i] < t[i-1]):
#        print("beat %d, time %f -> %f    excited from %d" % (i_beat, t[i-1], t[i], i_excite))
#        if ( (i08Hz==0) and (abs(t[i-1]-1250) < 1) ):
#            i08Hz = i_excite
#            i_beat = 0
#        if ( (i10Hz==0) and (abs(t[i-1]-1000) < 1) ):
#            i10Hz = i_excite
#            i_beat = 0
#            
#        if (i08Hz==0): # then still at 06Hz
#            paced1[i_beat] = v[i_excite:i-1]
#            times1[i_beat] = t[i_excite:i-1]
#        elif (i10Hz==0): # then at 08Hz
#            paced2[i_beat] = v[i_excite:i-1]
#            times2[i_beat] = t[i_excite:i-1]
#        else: #then at 10Hz
#            paced3[i_beat] = v[i_excite:i-1]
#            times3[i_beat] = t[i_excite:i-1]
#        i_excite = i
##        print('increase i_beat:')
#        i_beat += 1
#paced3[i_beat] = v[i_excite:i-1]
#times3[i_beat] = t[i_excite:i-1]
#        
#        
#num_points1 = i08Hz
#num_points2 = i10Hz - i08Hz
#num_points3 = len(t)- i10Hz
#
#name = "tims_exp_TT_5"
#directory = dir_data + "/" + name + "/"
#v = np.load(directory +'states.npy', mmap_mode='r').T[0]
#t = np.load(directory +'times.npy', mmap_mode='r')
#
#i06Hz = i08Hz = i10Hz = 0
#i_excite = 0
#i_beat = 0
#paced4 = paced5 = paced6 = [None,]* 20
#times4 = times5 = times6 = [None,]* 20
## find points where cl changes
#for i in range(1, len(t)):
#    if (t[i] < t[i-1]):
#        print("beat %d, time %f -> %f    excited from %d" % (i_beat, t[i-1], t[i], i_excite))
#        if ( (i08Hz==0) and (abs(t[i-1]-1250) < 1) ):
#            i08Hz = i_excite
#            i_beat = 0
#        if ( (i10Hz==0) and (abs(t[i-1]-1000) < 1) ):
#            i10Hz = i_excite
#            i_beat = 0
#            
#        if (i08Hz==0): # then still at 06Hz
#            paced4[i_beat] = v[i_excite:i-1]
#            times4[i_beat] = t[i_excite:i-1]
#        elif (i10Hz==0): # then at 08Hz
#            paced5[i_beat] = v[i_excite:i-1]
#            times5[i_beat] = t[i_excite:i-1]
#        else: #then at 10Hz
#            paced6[i_beat] = v[i_excite:i-1]
#            times6[i_beat] = t[i_excite:i-1]
#        i_excite = i
##        print('increase i_beat:')
#        i_beat += 1
#paced6[i_beat] = v[i_excite:i-1]
#times6[i_beat] = t[i_excite:i-1]
#        
#        
#num_points4 = i08Hz
#num_points5 = i10Hz - i08Hz
#num_points6 = len(t)- i10Hz
#
#
#
#cls = ['1666', '1250', '1000']

#%%
#fig = plt.figure()
#ax = plt.subplot(111)

#colors = plt.get_cmap('hot')(np.linspace(1,0,len(paced1)))

num = 0
i, j = (0, 0)
times = times1
data = paced1
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for num_beat, ap, color in zip(range(len(times)), data, colors):
    axes[i,j].plot(times[num_beat], ap, color=color, lw =lw)
axes[i,j].text(boxx, boxy, r'TT-WT', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
axes[i,j].text(boxx, boxy-0.3, r"$cl = "+str(cls[num])+'$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
num = 1
i, j = (1, 0)
times = times2
data = paced2
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for num_beat, ap, color in zip(range(len(times)), data, colors):
    axes[i,j].plot(times[num_beat], ap, color=color, lw =lw)
axes[i,j].text(boxx, boxy, r'TT-WT', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
axes[i,j].text(boxx, boxy-0.3, r"$cl = "+str(cls[num])+'$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
num = 2
i, j = (2, 0)
times = times3
data = paced3
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for num_beat, ap, color in zip(range(len(times)), data, colors):
    axes[i,j].plot(times[num_beat], ap, color=color, lw =lw)
axes[i,j].text(boxx, boxy, r'TT-WT', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
axes[i,j].text(boxx, boxy-0.3, r"$cl = "+str(cls[num])+'$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)

num = 0
i, j = (0, 1)
times = times4
data = paced4
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for num_beat, ap, color in zip(range(len(times)), data, colors):
    axes[i,j].plot(times[num_beat], ap, color=color, lw =lw)
axes[i,j].text(boxx, boxy, r'TT-BrS', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
axes[i,j].text(boxx, boxy-0.3, r"$cl = "+str(cls[num])+'$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
num = 1
i, j = (1, 1)
times = times5
data = paced5
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for num_beat, ap, color in zip(range(len(times)), data, colors):
    axes[i,j].plot(times[num_beat], ap, color=color, lw =lw)
axes[i,j].text(boxx, boxy, r'TT-BrS', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
axes[i,j].text(boxx, boxy-0.3, r"$cl = "+str(cls[num])+'$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
num = 1
num = 2
i, j = (2, 1)
times = times6
data = paced6
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for num_beat, ap, color in zip(range(len(times)), data, colors):
    axes[i,j].plot(times[num_beat], ap, color=color, lw =lw)
axes[i,j].text(boxx, boxy, r'TT-BrS', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
axes[i,j].text(boxx, boxy-0.3, r"$cl = "+str(cls[num])+'$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i,j].transAxes)
num = 1

# from https://stackoverflow.com/questions/8342549/matplotlib-add-colorbar-to-a-sequence-of-line-plots
plt.figure(2)
Z = [[0,0],[0,0]]
levels = np.arange(0.5,21.5, 1)
CS3 = plt.contourf(Z, levels, cmap=plt.get_cmap('plasma_r'))
plt.clf()
#cbar = fig.colorbar()
plt.close(2)
cbar = plt.colorbar(CS3, ax=fig.get_axes())
cbar.set_ticks([1, 5, 10, 15, 20])
cbar.set_label('action potential number', rotation=90)

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)