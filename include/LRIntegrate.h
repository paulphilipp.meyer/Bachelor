#ifndef LRINTEGRATE_H
#define LRINTEGRATE_H

#include "LRModel.h"

struct observer {
};

observer make_observer(std::string name);

struct store_voltage : public observer{
	std::vector< double >& m_p_voltages;
	std::vector< double >& m_p_times;

	store_voltage( std::vector< double >& vs, std::vector< double >& ts ):
		m_p_times( ts ), m_p_voltages( vs ) {}

	template<typename T>
	void operator()( T &x , const double t ) {
		m_p_voltages.push_back( x[0] );
		m_p_times.push_back( t );
	}
};

template<typename T>
struct store_state : public observer{
	std::vector< T >& m_p_states;
	std::vector< double >& m_p_times;

	store_state( std::vector< T >& states, std::vector< double >& times ):
		m_p_times( times ), m_p_states( states ) {}

	void operator()( T &x , const double t ) {
		m_p_states.push_back( x );
		m_p_times.push_back( t );
	}
};

struct store_measure_bs {
	static double m_v_max_so_far;
	static double m_v_min_so_far;
	static double m_v_apd90;
	static int m_apd90_marker;
	static double m_apd90;

	store_measure_bs();
	void operator()( const state_type_bs &x , const double t );
	void init( double voltage );
};

void write_state_bs( const state_type_bs &x , const double t );

//
// template<typename T>
// int save_state(T state, std::string filename) {
//
// 	std::ofstream state_writer (filename);
//
// 	for(int i=0; i<state.size(); i++) {
// 		state_writer << state[i] << "\n";
// }
//
// void load_state(state_type_bs save_here, std::string filename) {
// 	std::ifstream state_reader (filename);
// 	std::string line;
// 	if (state_reader.is_open()) {
// 		while ( getline (state_reader,line) )
// 	    {
// 	      cout << line << '\n';
// 	    }
// 	    myfile.close();
// 	  }
//
// 	  else cout << "Unable to open file";
//
// }

#endif
