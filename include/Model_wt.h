#ifndef MODEL_WT_H
#define MODEL_WT_H

#include "Cellmodel.h"

class Model_wt : public Cellmodel {
public:
	Model_wt();
	Model_wt(state_type &x);
	~Model_wt() {};
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t); 
};

#endif
