#ifndef MODEL_XIAEPI_BS_H
#define MODEL_XIAEPI_BS_H

#include "Cellmodel.h"

class Model_xiaepi_bs : public Cellmodel {
public:
	Model_xiaepi_bs(double sev);
	Model_xiaepi_bs() : Model_xiaepi_bs(1.0) {}
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t);
	static std::array<double, 74> intermediates;
	void print_intermediates();
	static double severity;
	static void set_severity(double sev);
	static std::array<double, 52> constants;
	void set_constants();
	static void rhs_fixed_potential(const state_type &x, state_type &dxdt, const double t);
	// calls rhs but sets dxdt[0] to 0
};


#endif
