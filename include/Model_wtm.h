#ifndef MODEL_WTM_H
#define MODEL_WTM_H

#include "Cellmodel.h"

class Model_wtm : public Cellmodel {
public:
	Model_wtm();
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t); 
};

#endif
