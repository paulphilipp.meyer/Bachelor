#ifndef CELLMODEL_H
#define CELLMODEL_H

#include <iostream>
#include <fstream>
#include <vector>
#include <boost/numeric/odeint.hpp>

// class Model_wt;
// class Model_bs;
// class Model_wtm;
// class Model_ttepi;
// class Model_ttepi_bs;

typedef std::vector<double> state_type;
typedef void (*rhs_type)(const state_type&,state_type&,double);

// https://stackoverflow.com/questions/236129/c-how-to-split-a-string - changed
state_type str2state(const std::string &text, char sep=',');

extern std::fstream output_writer;
extern double I_stim;
extern int DEBUG;

class Cellmodel {
public:
	Cellmodel() {};
	virtual ~Cellmodel() {};
	state_type state;
	virtual rhs_type get_rhs()=0;
	virtual unsigned int get_N_states()=0;
	static void write_state(const state_type& x, const double t);
	static void print_state(const state_type &x);
	static void print_state(const state_type &x, const double t);
	void print_state();
	virtual void print_intermediates();
	void init(std::string &str);
};

#include "Model_wt.h"
#include "Model_bs.h"
#include "Model_wtm.h"
#include "Model_ttepi.h"
#include "Model_xiaepi.h"
#include "Model_xiaepi_bs.h"

Cellmodel* make_model(std::string code);

// Observers ------------------------------------------------------
struct observer {
	virtual void operator()( state_type &x , const double t ) = 0;
};

struct store_voltage : public observer{
	std::vector< double >& m_p_voltages;
	std::vector< double >& m_p_times;

	store_voltage( std::vector< double >& vs, std::vector< double >& ts ):
		m_p_times( ts ), m_p_voltages( vs ) {};

	void operator()( state_type &x , const double t ) {
		m_p_voltages.push_back( x[0] );
		m_p_times.push_back( t );
	}
};

struct pushback_state : public observer{
	std::vector< double >& m_p_states;
	std::vector< double >& m_p_times;

	pushback_state( std::vector< double >& states, std::vector< double >& times ):
		m_p_times( times ), m_p_states( states ) {};

	void operator()( state_type &x , const double t ) {
		for (auto i=x.begin(); i!=x.end(); i++) {
			m_p_states.push_back( *i );
		}
		m_p_times.push_back( t );
	}
};

#endif
