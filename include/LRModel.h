#ifndef LRMODEL_H
#define LRMODEL_H

#include <iostream>
#include <fstream>
//#include <cmath>
//#include <stdlib.h>
#include <boost/numeric/odeint.hpp>
#include <boost/program_options.hpp>

typedef std::vector<double> state_type;
typedef void (*rhs_type)(const state_type&,state_type&,double);

extern std::fstream output_writer;
extern double I_stim;
extern int DEBUG;

// https://stackoverflow.com/questions/236129/c-how-to-split-a-string - changed
state_type str2state(const std::string &text, char sep=',');

// Cellmodels -------------------------------------------------------


class Model_wt : public Cellmodel {
public:
	Model_wt();
	Model_wt(state_type &x);
	~Model_wt() {};
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t); 
};

class Model_bs : public Cellmodel {
public:
	Model_bs();
	~Model_bs() {};
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t); 
};

class Model_wtm : public Cellmodel {
public:
	Model_wtm();
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t); 
};

class Model_ttepi : public Cellmodel {
public:
	Model_ttepi();
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t);
	static std::array<double, 7> intermediates;
	static std::array<double, 42> constants;
};


Cellmodel* make_model(std::string code);

// Observers ------------------------------------------------------
struct observer {
	virtual void operator()( state_type &x , const double t ) = 0;
};

struct store_voltage : public observer{
	std::vector< double >& m_p_voltages;
	std::vector< double >& m_p_times;

	store_voltage( std::vector< double >& vs, std::vector< double >& ts ):
		m_p_times( ts ), m_p_voltages( vs ) {};

	void operator()( state_type &x , const double t ) {
		m_p_voltages.push_back( x[0] );
		m_p_times.push_back( t );
	}
};

struct pushback_state : public observer{
	std::vector< double >& m_p_states;
	std::vector< double >& m_p_times;

	pushback_state( std::vector< double >& states, std::vector< double >& times ):
		m_p_times( times ), m_p_states( states ) {};

	void operator()( state_type &x , const double t ) {
		for (auto i=x.begin(); i!=x.end(); i++) {
			m_p_states.push_back( *i );
		}
		m_p_times.push_back( t );
	}
};


/*
class Buffered_save_state : public observer{
private:
	std::string m_filename;
	const unsigned int m_mem_size; // number of states in memory
	const unsigned int m_dim_state; // dimension of states (without time!)
	double *m_memory;
	unsigned int m_entries;
	double *m_write_marker;
public:
	double *get_mem() {
		return m_memory;
	}
	Buffered_save_state(unsigned int dim_state, unsigned int mem_size, const std::string filename)
		: m_filename(filename)
		, m_mem_size{ mem_size }
		, m_dim_state{ dim_state }
		, m_memory{ new double[mem_size * (dim_state+1)] }
		, m_entries{ 0 }
	{
	m_write_marker = m_memory;
	};
	~Buffered_save_state() {
		save_mem();
		delete[] m_memory;
	}
	void operator()( state_type &x , const double t );
	void save_mem();
	void print_mem();
};
*/

// not tested yet
class MemoryNpy {
private:
	std::string m_filename;
	double *m_data;
	unsigned int  m_capacity;
	unsigned int m_group_size;
	unsigned int m_elements;
public:
	MemoryNpy(std::string filename, unsigned int capacity, unsigned int group_size)
		: m_filename{ filename }
		, m_data{ new double[capacity] }
		, m_capacity{ capacity }
		, m_group_size{ group_size }
		, m_elements{ 0 }
	{}
	~MemoryNpy() {
		if (m_data)
			push_to_file();
		delete[] m_data;
	}
	void push(double x);
	void push_to_file();
};

#endif
