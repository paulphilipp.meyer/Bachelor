#ifndef HELPER_H
#define HELPER_H

#include <vector>
#include <fstream>
#include <stdlib.h>

std::vector<double> linspace(double start, double end, int num_in);

void minmax(const std::vector<double>& vec, double& v_max, size_t& index_v_max, double& v_min);

void apd5090(const std::vector<double>& beat_times, const std::vector<double>& beat_voltages, const size_t index_v_max, double& v_apd50, double& v_apd90, double& apd50, double& apd90 );


// void load_vector(std::vector<double> &vec, std::string filename);

/*
template<typename T>
void save_state(const T &state, const std::string filename) {
	std::ofstream file (filename, std::ios::trunc);
	assert (file.is_open());
	for (int i=0; i<state.size(); i++)
		file << boost::lexical_cast<std::string>(state[i]) << "\n";
	file.close();
}
*/
// void load_state(state_type_bs &state, const std::string filename);
// void load_state(state_type_wt &state, const std::string filename);


#endif
