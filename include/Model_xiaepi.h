#ifndef MODEL_XIAEPI_H
#define MODEL_XIAEPI_H

#include "Cellmodel.h"

class Model_xiaepi : public Cellmodel {
public:
	Model_xiaepi();
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t);
	static std::array<double, 74> intermediates;
	void print_intermediates();
	static std::array<double, 52> constants;
	void set_constants();
};


#endif
