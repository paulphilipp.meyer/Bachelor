#ifndef MODEL_BS_H
#define MODEL_BS_H

#include "Cellmodel.h"

class Model_bs : public Cellmodel {
public:
	Model_bs();
	~Model_bs() {};
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t); 
};

#endif
