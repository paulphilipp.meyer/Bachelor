#ifndef MODEL_TTEPI_H
#define MODEL_TTEPI_H

#include "Cellmodel.h"

class Model_ttepi : public Cellmodel {
public:
	Model_ttepi();
	unsigned int get_N_states();
	rhs_type get_rhs();
	static void rhs(const state_type &x, state_type &dxdt, const double t);
	static std::array<double, 72> intermediates;
	void print_intermediates();
	static std::array<double, 42> constants;
};

#endif
