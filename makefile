CC := g++

SRCDIR := src
BINDIR := bin
INCLUDEDIR := include
BUILDDIR := build

CNPY := -L/home/pmeyer/local/lib -lcnpy -Wl,-rpath=/home/pmeyer/local/lib 
HELPER := $(BUILDDIR)/helper.o
SHAMPINE := $(BUILDDIR)/shampine_gordon.o
BOOST_PO := -L /usr/bmp/boost/lib -l boost_program_options 

MODELS := $(BUILDDIR)/Cellmodel.o $(BUILDDIR)/Model_wt.o $(BUILDDIR)/Model_bs.o $(BUILDDIR)/Model_wtm.o $(BUILDDIR)/Model_ttepi.o $(BUILDDIR)/Model_xiaepi.o $(BUILDDIR)/Model_xiaepi_bs.o

#CFLAGS := -O2 -std=c++11 -Wno-deprecated -I $(INCLUDEDIR) -I /usr/bmp/boost/include -g
#CFLAGS := -std=c++11 -Wno-deprecated -I $(INCLUDEDIR) -I /usr/bmp/boost/include -g
CFLAGS := -std=c++11 -O2 -Wno-deprecated -I $(INCLUDEDIR) -I /usr/bmp/boost/include

.PHONY: clean all
all:
	@echo "yet to implement..."
clean:
	rm $(BUILDDIR)/*
	rm $(BINDIR)/*

#  OBJECTS #################################################################

$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp 
	$(CC) $(CFLAGS) -c -o $@ $<

#  TARGETS #################################################################

tim_sn : $(BUILDDIR)/tim_sn.o $(MODELS) $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

tim_simple : $(BUILDDIR)/tim_simple.o $(MODELS) $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

threshold : $(BUILDDIR)/threshold.o $(MODELS) $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

# for reproduction of sodium data
tim_1 : $(BUILDDIR)/tim_1.o $(MODELS) $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

# for reproduction of AP data
tim_2 : $(BUILDDIR)/tim_2.o $(MODELS) $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

tim_meas : $(BUILDDIR)/tim_meas.o $(MODELS) $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY)

bifn: $(BUILDDIR)/bifurcation_n.o $(MODELS) $(HELPER) 
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY)

bif_severity: $(BUILDDIR)/bif_severity.o $(MODELS)  $(HELPER)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

test_pol : $(BUILDDIR)/test_pol.o $(MODELS)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

# old stuff ################################################################

tim_s : $(BUILDDIR)/timeseries_states.o $(MODELS)
	$(CC) $(BUILDDIR)/timeseries_states.o $(MODELS) -o $(BINDIR)/tim_s $(CNPY)

bif : $(BUILDDIR)/bifurcation_vm.o $(MODELS) $(BUILDDIR)/helper.o
	$(CC) $(BUILDDIR)/bifurcation_vm.o $(MODELS) $(BUILDDIR)/helper.o -o $(BINDIR)/bif $(CNPY)

bif_fehler : $(BUILDDIR)/bifurcation_vm_fehler.o $(MODELS) $(BUILDDIR)/helper.o
	$(CC) $(BUILDDIR)/bifurcation_vm.o $(MODELS) $(BUILDDIR)/helper.o -o $(BINDIR)/bif $(CNPY)

testcnpy :  $(BUILDDIR)/testcnpy.
	$(CC) $(BUILDDIR)/testcnpy.o -o $(BINDIR)/testcnpcnpyy $(CNPY)

timeseries_v_wt : LRModel.o LRIntegrate.o timeseries_voltages_wt.o
	$(CC) LRModel.o timeseries_voltages_wt.o -o $(BINDIR)/timeseries_v_wt $(BOOST_PO) $(CNPY)

tim_dopri : $(BUILDDIR)/tim_runge_kutta_dopri.o $(MODELS)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

tim_cash : $(BUILDDIR)/tim_runge_kutta_cash.o $(MODELS)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

tim_errcontrol : $(BUILDDIR)/tim_errcontrol.o $(MODELS)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

test : $(BUILDDIR)/test.o $(MODELS)
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

test2 : $(SRCDIR)/testcnpy.cpp
	$(CC) -o $(BINDIR)/$@ $+ $(CNPY) 

testshampinegordon : $(BUILDDIR)/test_shampine_gordon.o $(BUILDDIR)/Model_shampine.o $(SHAMPINE)
	$(CC) $(BUILDDIR)/test.o $(MODELS) -o $(BINDIR)/test $(CNPY)
