#include <iostream>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <cmath>
#include <fstream>
#include <chrono>
#include <string>
#include <sstream>


using namespace std;
using namespace boost::numeric::odeint;
using namespace boost::numeric::odeint;


	double t = 0.0;	

	double cl = 343.0;

	const int N_total = 16+13;
	const int N_states = 16;
	const int N_markov = 13;
	

	typedef boost::array< double , N_total > state_type;

	//double pi = 3.0; //Not ideal?

	double pi = 3.14159265359;

	double data_F=96485; //// Faraday constant
	double data_R=8314;  //// Rey
	double data_Temp=310;  //// Absolute temperature K
	double l = 0.01;       // Length of the cell (cm)
	double a = 0.0011;     // Radius of the cell (cm)

	double vcell = 1000*pi*a*a*l;     //   3.801e-5 uL   // Cell volume (uL)
	double ageo = 2*pi*a*a+2*pi*a*l;  //   7.671e-5 cm^2    // Geometric membrane area (cm^2)
	double Acap = ageo*2;             //   1.534e-4 cm^2    // Capacitive membrane area (cm^2)
	double data_vmyo = vcell*0.68;    // Myoplasm volume (uL)
	double vmito = vcell*0.24;  // Mitochondria volume (uL)
	double data_vsr = vcell*0.06;    // SR volume (uL)
	double data_vnsr = vcell*0.0552;   // NSR volume (uL)
	//double data_vJSR=  vcell*0.0048;   // JSR volume (uL)
	double Volume_JSR = vcell*0.0048;
	double data_vss= vcell*0.02;  //  cell subspace volume (uL)

	double data_AF=Acap/data_F;

	double data_frt=data_F/data_Temp/data_R;

	double data_IKsCa_max=0.6;
	double data_IKsCa_Kd=38e-6;
	 
	double data_Is=80.0; ////// Stimulus current
	double data_fnsh=0.5; //// duration of Stimulus current
	double data_st=0; //// start time of timulus current



	double data_K_Relss=1;
	double data_kappa=0.125;
	double data_tau=4.75;
	double data_alpha_Rel=data_tau*data_kappa;

	double data_qn=9;//9
	double data_tautr=120;//100/12


	double data_GNa=16;                          // mS/cm^2
	double data_GNab=0.004; 
	double data_GNaL=65e-4;

	double data_KmCa=1.25e-4;




	double data_NCXmax=4.5;
	double data_ksat=0.27;
	double data_eta=0.35;
	double data_KmNai=12.3;
	double data_KmNao=87.5;
	double data_KmCai=0.0036;
	double data_KmCao=1.3;

	double data_gacai=1;         // Activity coefficient of Ca
	double data_gacao=0.341;     // Activity coefficient of Ca

	double data_kmca=6e-4;     // Half-saturation concentration of Ca channel (mM)
	double data_pca= 5.4e-4;     // Permiability of membrane to Ca (cm/s)

	double data_pna=6.75e-7;     // Permiability of membrane to Na (cm/s)
	double data_ganai=0.75;      // Activity coefficient of Na
	double data_ganao=0.75;      // Activity coefficient of Na
	double data_pk=1.93e-7;       // Permiability of membrane to K (cm/s)
	double data_gaki=0.75;       // Activity coefficient of K
	double data_gako=0.75;       // Activity coefficient of K


	double data_gcat = 0.05;


	double data_kmup = 0.00092;    // Half-saturation concentration of iup (mM)
	double data_iupbar = 0.00875;  // Max. current through iup channel (mM/ms)
	double data_nsrbar = 15;       // Max. [Ca] in NSR (mM)
	double data_ibarpca = 1.15; // Max. Ca current through sarcolemmal Ca pump (uA/uF)
	double data_kmpca = 0.5e-3; // Half-saturation concentration of sarcolemmal Ca pump (mM)
	double data_cmdnbar = 0.050;   // Max. [Ca] buffered in CMDN (mM)
	double data_trpnbar = 0.070;   // Max. [Ca] buffered in TRPN (mM)
	double data_kmcmdn = 0.00238;  // Equilibrium constant of buffering for CMDN (mM)
	double data_kmtrpn = 0.0005;   // Equilibrium constant of buffering for TRPN (mM)
	double data_trpnf = 40;   // forward  buffered in TRPN (mM)
	double data_trpnb = 0.02;   // backward  TRPN (mM)
	double data_cmdnf = 100;   // forward  buffered in TRPN (mM)
	double data_cmdnb = 0.238;   // backward  TRPN (mM)

	double data_csqnbar = 10;      // Max. [Ca] buffered in CSQN (mM)
	double data_kmcsqn = 0.8;      // Equilibrium constant of buffering for CSQN (mM)

	double data_csqnf = 100;  
	double data_csqnb = 80; 
	double data_gcab=0.003016;


	double data_taudiff=0.2;
	double data_KmCaMK=0.15;
	//double data_tautr=120;


	double data_iupmax=0.004375; 
	double data_Kmup=0.00092;
	double data_nsrmax=15.0;






	double data_c1 =0.00025;   // Scaling factor for inaca (uA/uF)
	double data_c2 = 0.0001;   // Half-saturation concentration of NaCa exhanger (mM)
	double data_gammas = 0.15;  // Position of energy barrier controlling voltage dependance of inaca




	double data_GKsmax = 0.433;

	double data_gkrmax =  0.02614;

	double data_prnak=0.01833;  


	double data_GK1max=0.75;
	double data_GKpmax= 0.00552;



	double data_kmnai = 10;    // Half-saturation concentration of NaK pump (mM)
	double data_kmko = 1.5;    // Half-saturation concentration of NaK pump (mM)
	double data_ibarnak = 2.25; // Max. current through Na-K pump (uA/uF)

	double In = 0.0;

	double data_ca_o = 1.8;
	double data_na_o = 140.0;
	double data_k_o = 4.5;
	

	int beat_counter = 0;
	double stim_st = 80.0;
	double stim_dur = 0.5;
	
	
	double G_to = 1.1;
	double dt = 0.001;

	
	double IcalFak = 0.5;
	

	

	double Cainb;
	double CaJSRnb;


double calcium_cai(double ca_t){
	
	double bmyo = data_cmdnbar+data_trpnbar-ca_t+data_kmtrpn+data_kmcmdn;
	double cmyo = data_kmcmdn*data_kmtrpn -ca_t*(data_kmtrpn+data_kmcmdn)+data_trpnbar*data_kmcmdn+data_cmdnbar*data_kmtrpn;
	double dmyo = -data_kmtrpn*data_kmcmdn*ca_t;

	return ( 2*pow(bmyo*bmyo-3*cmyo,0.5)/3)*cos(acos((9*bmyo*cmyo-2*bmyo*bmyo*bmyo-27*dmyo)/(2*pow(bmyo*bmyo-3*cmyo,1.5)))/3)-(bmyo/3);

	
	}

double calcium_JSR(double ca_t){
	
	double b_JSR=data_csqnbar+data_kmcsqn-ca_t;
	double c_JSR=ca_t*data_kmcsqn;
	
	return -b_JSR/2+sqrt(b_JSR*b_JSR+4*c_JSR)/2;
	
	
	}



double G_Na_markov = 23.5;

double **mat;

double C0;
void calc_odeint(state_type &x, state_type &dxdt, double t){
	
	IcalFak = 1.0;
	double ca_func = x[4];
	Cainb = calcium_cai(ca_func);
	

	ca_func = x[7];


	CaJSRnb = calcium_JSR(ca_func);



	double dss=1.0/(1.0+exp(-(x[0]+10)/6.24));
	double taud=dss*(1-exp(-(x[0]+10)/6.24))/(0.035*(x[0]+10));
	double dss1=1/(1+exp(-(x[0]+60)/0.024));
	dss=dss*dss1;
	double fss=1/(1+exp((x[0]+32)/8))+(0.6)/(1+exp((50-x[0])/20));

	double tauf=1/(0.0197*exp(-pow(0.0337*(x[0]+10),2.0))+0.02);
	
	double ibarca= data_pca*4*x[0]*data_F*data_frt*((data_gacai*Cainb*exp(2*x[0]*data_frt)-data_gacao*data_ca_o)/(exp(2*x[0]*data_frt)-1));

	double ibarna= data_pna*(x[0]*data_F*data_frt)*((data_ganai*x[5]*exp(x[0]*data_frt)-data_ganao*data_na_o)/(exp(x[0]*data_frt)-1));
	double ibark= data_pk*(x[0]*data_F*data_frt)*((data_gaki*x[6]*exp(x[0]*data_frt)-data_gako*data_k_o)/(exp(x[0]*data_frt)-1));

	double fca = 1/(1+(Cainb/data_kmca));

	double ilca   = x[1]*x[2]*fca*ibarca*IcalFak;
	double ilcana = x[1]*x[2]*fca*ibarna*IcalFak;
	double ilcak = x[1]*x[2]*fca*ibark*IcalFak;


	double ENa = log(data_na_o/x[5])/data_frt;       // Nernst potential of Na, mV
						

	


	double inab = data_GNab*(x[0]-ENa);


	
	//			 0       1	 2     3     4     5    6   7    8   9    10   11  12	
	//Vector: 	x[19], x[20], UIF, x[22], x[23], x[24], x[25], x[26], x[27], x[28], x[29], x[30], x[31]
		
	double alpha11 = 3.802/(0.1027*exp(-x[0]/17.0)+0.20*exp(-x[0]/150.0)); //xC3  ==>  xC2
	double alpha12 = 3.802/(0.1027*exp(-x[0]/15.0)+0.23*exp(-x[0]/150.0)); //xC2  ==>  xC1
	double alpha13 = 3.802/(0.1027*exp(-x[0]/12.0)+0.25*exp(-x[0]/150.0)); //xC1  ==>  xO

	//alpha12 = 3.802/(0.1027*exp(-x[0]/15.0)+0.23*exp(-x[0]/150)); //x[20]  ==>  UIF
	//beta12 = 0.20*exp(-(x[0]-5)/20.3); //UIF  ==>  x[20]

	double beta11 = 0.1917*exp(-x[0]/20.3); //xC2  ==>  xC3
	double beta12 = 0.20*exp(-(x[0]-5.0)/20.3); //xC1  ==>  xC2
	double beta13 = 0.22*exp(-(x[0]-10.0)/20.3); //xO  ==>  xC1


	double alpha3 = ((3.7933e-7)*exp(-x[0]/7.7))/2.5; //UIF  ==>  x[26]
	//alpha3 = ((3.7933*1e-7)*exp(-x[0]/7.7))/2.5; //x[20]  ==>  x[25]
	//alpha3 = ((3.7933*1e-7)*exp(-x[0]/7.7))/2.5; //x[19]  ==>  x[24]

	double beta3 = (0.0084+0.00002*x[0]); //x[26]  ==>  UIF
	//beta3 = (0.0084+0.00002*x[0]); //x[25]  ==>  x[20]
	//beta3 = (0.0084+0.00002*x[0]); //x[24]  ==>  x[19]

	double alpha2 = (9.178*exp(x[0]/29.68)); //x[27]  ==>  UIF
	double beta2 = (alpha13*alpha2*alpha3)/( beta13*beta3); //UIF  ==>  x[27]

	double alpha4 = alpha2/100.0; //UIF  ==>  UIM
	double beta4 = alpha3; //UIM  ==>  UIF
	double alpha5 = alpha2/(3.5e4); //UIM  ==>  x[23]
	double beta5 = alpha3/20.0; //x[23]  ==>  UIM

	double mUL = 1.0e-7; //background to burst   (U => L)
	double mLU = 9.5e-4; //burst to background   (L => U)
		
	//x[19]

	mat[0][0] = -alpha11 - alpha3; 
	mat[0][1] = beta11; //x[20]
	mat[0][5] = beta3; //x[24]

	//x[20]

	mat[1][1] = -beta11 -alpha12 -alpha3;
	mat[1][0] = alpha11; //x[19]
	mat[1][2] = beta12; //UIF
	mat[1][6] = beta3; //x[25]

	//UIF

	mat[2][2] = -beta12 - beta2 - alpha3 - alpha4;

	mat[2][1] = alpha12; //x[20]
	mat[2][3] = beta4; //x[22]
	mat[2][7] = beta3; //x[26]
	mat[2][8] = alpha2; //x[27]

	//x[22]

	mat[3][3] = -beta4 - alpha5;

	mat[3][2] = alpha4; //UIF
	mat[3][4] = beta5; //x[23]

		
	//x[23]	

	mat[4][4] = -beta5;	
	mat[4][3] = alpha5;		

	//x[24]


	mat[5][5] = -mUL - alpha11 - beta3;

	mat[5][0] = alpha3; //x[19]
	mat[5][6] = beta11; //x[25]
	mat[5][9] = mLU; //x[28]

	//x[25]

	mat[6][6] = -mUL - alpha12 - beta11 - beta3;

	mat[6][5] = alpha11; //x[24]
	mat[6][7] = beta12; //x[26]
	mat[6][1] = alpha3; //x[20]
	mat[6][10] = mLU; //x[29]

	//x[26]

	mat[7][7] = -mUL - alpha13 - beta12 - beta3;

	mat[7][6] = alpha12; //x[25]
	mat[7][8] = beta13; //x[27]
	mat[7][2] = alpha3; //UIF
	mat[7][11] = mLU; //x[30]

	//x[27]

	mat[8][8] = -mUL - alpha2 - beta13;

	mat[8][7] = alpha13; //x[26]
	mat[8][2] = beta2; //UIF
	mat[8][12] = mLU; //x[31]	
		

	//x[28]

	mat[9][9] = -mLU -alpha11;

	mat[9][10] = beta11; //x[29]
	mat[9][5] = mUL; //x[24]

	//x[29]

	mat[10][10] = -mLU - beta11 - alpha12;

	mat[10][9] = alpha11; //x[28]
	mat[10][11] = beta12; //x[30]
	mat[10][6] = mUL; //x[25]	


	//x[30]

	mat[11][11] = -mLU - alpha13 - beta12;

	mat[11][10] =  alpha12;//x[30]
	mat[11][12] = beta13; //x[31]
	mat[11][7] = mUL; //x[26]

	//x[31]

	mat[12][12] = -mLU - beta13;
	mat[12][11] = alpha13; //x[30]
	mat[12][8] = mUL; //x[27]	

	for(int i = 0; i < N_markov; i++){
			
		dxdt[N_states+i] = 0.0;
		for(int j = 0; j < N_markov; j++){
				
			dxdt[N_states+i] += mat[i][j] * x[N_states + j];
				
				}	
			}
			
	double sum = 0;
	for(int j = 1; j < N_markov; j++){
		
		sum += 	x[N_states + j];
		
		}	
	dxdt[N_states+0] = 0.0;
	x[N_states+0] = 1.0 - sum;
		
			
			
			
	double ina = G_Na_markov * (x[N_states+8]+x[N_states+12]) * (x[0] - ENa);



	//// Transient calcium channel


	double bss = 1/(1+exp(-(x[0]+14.0)/10.8));
	double taub = 3.7+6.1/(1+exp((x[0]+25.0)/4.5));
	double gss = 1/(1+exp((x[0]+60.0)/5.6));


	double taug;


	double aux=1-1./(1+exp(-x[0]/0.0024));
	taug = aux*(-0.875*x[0]+12.0)+12.0*(1-aux);

	double ECa = log(data_ca_o/Cainb)/2/data_frt;

	double icat = data_gcat*x[10]*x[10]*x[11]*(x[0]-ECa);



	double GK1 = data_GK1max*sqrt(data_k_o/5.4);
	double EK = log(data_k_o/x[6])/data_frt;

	double ak1 = 1.02/(1+exp(0.2385*(x[0]-EK-59.215)));
	double bk1 = (0.49124*exp(0.08032*(x[0]-EK+5.476))+exp(0.06175*(x[0]-EK-594.31)))/(1+exp(-0.5143*(x[0]-EK+4.753)));

	double gK1 = GK1*ak1/(ak1+bk1);
	double IK1 = gK1*(x[0]-EK);




	double ikp = data_GKpmax*(x[0]-EK)/(1+exp((7.488-x[0])/5.98)); // plato K 95





	double gks = data_GKsmax*(1+0.6/(1+pow(3.8e-5/Cainb,1.4)));
	double eks = log((data_k_o+data_prnak*data_na_o)/(x[6]+data_prnak*x[5]))/data_frt;

	double xss = 1/(1+exp(-(x[0]-1.5)/16.7));

	double tauxs = 1/(0.0000719*(x[0]+30)/(1-exp(-0.148*(x[0]+30)))+0.000131*(x[0]+30)/(exp(0.0687*(x[0]+30))-1));


	double iks = gks*x[9]*x[12]*(x[0]-eks);


	double gkr = data_gkrmax*sqrt(data_k_o/5.4);
	double ekr = log(data_k_o/x[6])/data_frt;
	double r_gate = 1/(1+exp((x[0]+9)/22.4));

	double ikr = gkr*x[3]*r_gate*(x[0]-ekr);
	double xrss = 1/(1+exp(-(x[0]+21.5)/7.5));
	double tauxr = 1/(0.00138*(x[0]+14.2)/(1-exp(-0.123*(x[0]+14.2)))+0.00061*(x[0]+38.9)/(exp(0.145*(x[0]+38.9))-1));




	double ipca = (data_ibarpca*Cainb)/(data_kmpca+Cainb);	 // sarcolema pump Ca SERCA
	double icab = data_gcab*(x[0]- log(data_ca_o/Cainb)/2/data_frt); // background Ca

	double ileak = data_iupbar/ data_nsrbar*x[8];




	double iup = data_iupbar*Cainb/(Cainb+data_kmup);
	double itr = (x[8]-CaJSRnb)/data_tautr;

	double inaca = data_c1*exp(( data_gammas-1)*x[0]* data_frt)*((exp(x[0]* data_frt)*pow(x[5],3)*data_ca_o- pow(data_na_o,3)*Cainb)/(1+ data_c2*exp(( data_gammas-1)*x[0]* data_frt)*(exp(x[0]* data_frt)*pow(x[5],3)*data_ca_o+ pow(data_na_o,3)*Cainb)));


	double sigma = (exp(data_na_o/67.3)-1)/7;

	double fnak = 1/(1+0.1245*exp((-0.1*x[0]*data_frt)) + 0.0365*sigma*exp(-x[0]*data_frt));

	double inak = data_ibarnak*fnak/(1+pow(data_kmnai/x[5],2))/(1+data_kmko/data_k_o);

	double Rel_ss=ilca*data_alpha_Rel/(1+pow(data_K_Relss/CaJSRnb,data_qn));
	double tau_Rel=data_tau/(1+0.0123/CaJSRnb);

	double R_to = exp(x[0]/100.0);
		
	double alpha_z = 10.0*exp((x[0]-40.0)/25.0)/(1.0 + exp((x[0]-40.0)/25.0));
	double beta_z =  10.0*exp(-(x[0]+90.0)/25.0)/(1.0 + exp(-(x[0]+90.0)/25.0));
			
	double alpha_y = 0.015/(1.0+exp((x[0]+60.0)/5.0));	
	double beta_y =  0.1*exp((x[0]+25.0)/5.0)/(1.0 + exp((x[0]+25.0)/5.0));

	
	double I_to = G_to * x[15] * x[15]*x[15]*x[14]*R_to*(x[0]-EK); 


	dxdt[14] = alpha_y*(1.0-x[14])-beta_y*x[14];
	dxdt[15] = alpha_z*(1.0-x[15])-beta_z*x[15];

	dxdt[1]=(dss-x[1])/taud;
	dxdt[2]=(fss-x[2])/tauf;
	dxdt[3]=(xrss-x[3])/tauxr;
	dxdt[9]=(xss-x[9])/tauxs;
	dxdt[12]=(xss-x[12])/(tauxs*4);


	dxdt[10]=(bss-x[10])/taub;
	dxdt[11]=(gss-x[11])/taug;

	double Rel = x[13];
	
	double caiont = ilca+icab+ipca-2*inaca+icat; 

	double naiont = ina+inab+3*inaca+ ilcana+3*inak;
	double kiont = ikr+iks+IK1+ikp+ ilcak-2*inak-In;

	kiont += I_to;


	dxdt[5]=-naiont*data_AF/(data_vmyo);
	dxdt[6]=-kiont*data_AF/(data_vmyo);

	dxdt[4] =-caiont*data_AF/(data_vmyo*2)+(ileak-iup)*data_vnsr/data_vmyo+(Rel)*Volume_JSR/data_vmyo;

	dxdt[8] = iup-itr*Volume_JSR/data_vnsr-ileak;//;

	dxdt[7] = itr-(Rel);
	dxdt[13]=-(Rel_ss + x[13])/tau_Rel;

	dxdt[0] = -(naiont+kiont+caiont);
	

	}



int wc = 0;
int wi = 2500;

fstream voltage_output;

void write_steady_state(const state_type &x , const double t){
	wc++;
	if(wc%wi == 0){
			
		voltage_output <<t <<"\t" <<x[0] <<"\t" <<x[1] <<"\t" <<x[2] <<"\t" <<x[3] <<"\t" <<x[4] <<"\t" <<x[5] <<endl;
		wc = 0;
}


}


void write_out(const state_type &x , const double t){
	wc++;
	if(wc%wi == 0){
			
		voltage_output <<t <<"\t" <<beat_counter*cl  <<"\t" <<x[0] <<endl;
		wc = 0;
}


}


double calc_C0(state_type &x){
	
	
	double C0 = -(x[0]*Acap/(data_vmyo*data_F) - (x[6] + x[5] + 2.0*x[4] + (2.0 * Volume_JSR/data_vmyo) * x[7] + (2.0 * data_vnsr/data_vmyo) * x[8]));
	
	
	return C0;
	return -(x[0]*Acap/(data_vmyo*data_F) - (x[6] + x[5] + 2.0*x[4] + (2.0 * Volume_JSR/data_vmyo) * x[7] + (2.0 * data_vnsr/data_vmyo) * x[8]));
	
	}


state_type set_in_conditions(){
	
	state_type x;
	
		
	x[0]=-89.9863526453;
	x[1]=0;
	x[2]=0.9998361223;
	x[3]=0.00010819;
	x[4]=0.0113541899;
	x[5]=12.6287781918;
	x[6]=143.9032192933;
	x[7]=7.1976768273;
	x[8]=1.1992257705;
	x[9]=0.0041593655;
	x[10]=0.000879064;
	x[11]=0.9952963692;
	x[12]=0.0041593655;
	x[13]=0.0;
	x[14]=0.9999848527;
	x[15]=0.0108622692;
	x[16]=0.2647239905;
	x[17]=0.0029986711;
	x[18]=1.26611666411852E-05;
	x[19]=3.10466004246949E-06;
	x[20]=4.35027112113673E-08;
	x[21]=0.7239492459;
	x[22]=0.0082005628;
	x[23]=3.46249012981194E-05;
	x[24]=2.33634992229991E-08;
	x[25]=7.62051838475029E-05;
	x[26]=8.63217135543962E-07;
	x[27]=3.64472645555614E-09;
	x[28]=2.45931569494852E-12;
	
	return x;
	}


int main(int argc, char **argv){
	
	
	
	state_type states = set_in_conditions();
	C0 = calc_C0(states);
	cout <<states[0] <<endl;
	cl = 200.0;

	mat = (double**)malloc(N_markov * sizeof(double*));

	for (int i=0;i<N_markov;i++){
			
		mat[i] = (double*)malloc(N_markov * sizeof(double));
			}
				

	for(int i = 0; i < N_markov; i++){
		for(int j = 0; j < N_markov; j++){

			mat[i][j] = 0.0;
				
			}
		}
	


	string filename = "output.dat";

	voltage_output.open(filename,ios::out);
	
	double abs_err = 1e-8;
	double rel_err = abs_err;

	runge_kutta_fehlberg78 < state_type > rk78;	
		
		
	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );
	bulirsch_stoer< state_type > stepper_bs( abs_err, rel_err , 1.0 , 1.0 );
		
	wi = 10;
	
	while(beat_counter < 1000){
		
		
		In = data_Is;
		integrate_const(rk78, calc_odeint  , states , 0.0 , stim_dur , dt ,write_out);		
		In = 0.0;

		integrate_adaptive( stepper_c, calc_odeint  , states , stim_dur , cl , dt,write_out);
			
		
		beat_counter++;
		

		}
		

	
	return 0;
	
	}
	
