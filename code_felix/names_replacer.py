#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  names_replacer.py
#  
#  Copyright 2015 Felix Koeth <felix.koeth@stud.uni-goettingen.de>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys

def mode1(input_file, output_file, names_list):
        source_reader = open(input_file, "r")
        source_writer = open(output_file, "w")
        
        for line in source_reader:
                for changer in names_list:
                        if changer[0] in line:
                                line = line.replace(changer[0], changer[1])
                source_writer.write(line)
        
def mode2(input_file, output_file, names_list):
        source_reader = open(input_file, "r")
        source_writer = open(output_file, "w")
        
        for line in source_reader:
                for changer in names_list:
                        if changer[1] in line:
                                line = line.replace(changer[1], changer[0])
                source_writer.write(line)
        

def main():
        
        
        input_file = ""
        output_file = ""
        names_file = ""

        mode = 0
        usage = "Arguments: -i INPUT_FILE -o OUTPUT_FILE -n NAMES_FILE -d direction (1: txt to arr, 2: arr to txt)"

        for i in range(len(sys.argv)):
                if sys.argv[i] == "-?" or sys.argv[i] == "-h":
                        print(usage)
                        return 0
                if sys.argv[i] == "-i":
                        input_file = sys.argv[i+1]
                if sys.argv[i] == "-o":
                        output_file = sys.argv[i+1]
                if sys.argv[i] == "-n":
                        names_file = sys.argv[i+1]
                if sys.argv[i] == "-d":
                        mode = int(sys.argv[i+1])
        
        
        if input_file == "" or names_file == "" or output_file == "" or mode == 0:
                print("Parameter missing:")
                print(usage)
                return 0
        if input_file == output_file:
                print("Error: Same output and input file")
                print(usage)
                return 0        
                
        names_reader = open(names_file, "r")
        
        names_list = []
        
        for i in names_reader:
                if i[0] != "#":
                        x = i.split(",")
                        x[1] = x[1].replace('\n', "")
                        names_list.append(x)
                
        
        if mode == 1:
                mode1(input_file, output_file, names_list)
        if mode == 2:
                mode2(input_file, output_file, names_list)

        print("All done!")

        return 0

if __name__ == '__main__':
        main()
