#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  names_replacer.py
#  
#  Copyright 2015 Felix Koeth <felix.koeth@stud.uni-goettingen.de>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
import os
def mode1(input_file, output_file, names_list):
        source_reader = open(input_file, "r")
        source_writer = open(output_file, "w")
        
        for line in source_reader:
                for changer in names_list:
                        if changer[0] in line:
                                line = line.replace(changer[0], changer[1])
                source_writer.write(line)
        
def mode2(input_file, output_file, names_list):
        source_reader = open(input_file, "r")
        source_writer = open(output_file, "w")
        
        for line in source_reader:
                for changer in names_list:
                        if changer[1] in line:
                                line = line.replace(changer[1], changer[0])
                source_writer.write(line)
        

def main():
        
        
        input_file = ""
        output_file = ""


        
        usage = "Arguments: -i INPUT_FILE -o OUTPUT_FILE"

        for i in range(len(sys.argv)):
                if sys.argv[i] == "-?" or sys.argv[i] == "-h":
                        print(usage)
                        return 0
                if sys.argv[i] == "-i":
                        input_file = sys.argv[i+1]
                if sys.argv[i] == "-o":
                        output_file = sys.argv[i+1]

        if input_file == "" or output_file == "":
                print("Parameter missing:")
                print(usage)
                return 0
        if input_file == output_file:
                print("Error: Same output and input file")
                print(usage)
                return 0

        names_list = []
        
        
        f = open(input_file, "r")
        
        n_states = 0
        n_constants = 0
        n_variables = 0
   
   
        for i in f:
                if i[0] != "#" and i != "\n":
                        i.replace("\t", "")
                        i.replace("\n", "")
                        x = i.split(",")
                        
                        x[1] = x[1].replace('\n', "")
                        if x[1][0] != "(":
                                y = x[1].split("[")
                                n = int(y[1].replace("]",""))
                                #print x
                                if "dxdt[" in i and n > n_states:
                                        n_states = n
                                if "variables[" in i and n > n_variables:
                                        n_variables = n
                                if "constants[" in i and n > n_constants:
                                        n_constants = n
                        
                        x.append(len(x[0]))

                        if x[0] in [j for i in names_list for j in i]:
                                print("Error " + x[0] + " is duplicated")
                        if x[1] in [j for i in names_list for j in i]:
                                print("Error " + x[1] + " is duplicated. THIS IS BAD!")
                                return 0
                        names_list.append(x)
                        
                        
        f.close()
        

        
        names_list.sort(key=lambda x: x[2])
        names_list.reverse()
        f = open(output_file, "w")
        
        for i in names_list:
                s = i[0] + "," + i[1] + "\n"
                
                f.write(s)
        
        print("Number of States = " + str(n_states+1))
        print("Number of Variables = " + str(n_variables+1))
        print("Number of Constants = " + str(n_constants+1))

        return 0

if __name__ == '__main__':
        main()

