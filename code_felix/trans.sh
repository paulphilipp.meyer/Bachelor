#!bin/bash


python sort_names_by_length.py -i arr_new_matlab.csv -o array_names_sorted_odeint.csv
python names_replacer.py -i markov_attractor.cpp -o TRANSLATED_markov_attractor.cpp -n array_names_sorted_odeint.csv -d 1

g++ TRANSLATED_markov_attractor.cpp -o markov_attractor -std=c++11  -O2

