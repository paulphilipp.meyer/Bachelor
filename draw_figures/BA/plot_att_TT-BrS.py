#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False
filename = r'../../figures/att_TT-BrS_cl1000.png'

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure(figsize=(figwidth, figheight))

ax = fig.add_subplot(111)

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"

name = "tim_meas_3"

directory = data_dir + sep + name + sep
apd50 = np.load(directory + "apd50.npy")
apd90 = np.load(directory + "apd90.npy")
v_max = np.load(directory + "v_max.npy")
v_min = np.load(directory + "v_min.npy")
v_10 = np.load(directory + "v_10.npy")
v_50 = np.load(directory + "v_50.npy")
v_100 = np.load(directory + "v_100.npy")
v_200 = np.load(directory + "v_200.npy")
beats = np.arange(len(v_max))
meas =   ( apd50 ,  apd90 ,  v_max ,  v_min ,  v_10 ,  v_50 ,  v_100 ,  v_200 )
labels = ["apd50", "apd90", "V_{max}", "V_{min}", "V_{10}", "V_{50}", "V_{100}", "V_{200}"]

size = 5
im = 6
what = ax.scatter(meas[im][:-1], meas[im][1:], c=beats[:-1], s=size)
ax.set_xlabel('$'+labels[im]+"^{n}$")
ax.set_ylabel('$'+labels[im]+"^{n+1}$")
#    plt.title("Xia->Bs @cl 1000ms")
    
cbar = plt.colorbar(what)
cbar.set_label('n', rotation=0)
plt.tight_layout()

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
    
#%%
#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = True
filename = r'../../figures/att_TT-BrS_cl400.png'

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure(figsize=(figwidth, figheight))

ax = fig.add_subplot(111)

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"

name = "tim_meas_1"

directory = data_dir + sep + name + sep
apd50 = np.load(directory + "apd50.npy")
apd90 = np.load(directory + "apd90.npy")
v_max = np.load(directory + "v_max.npy")
v_min = np.load(directory + "v_min.npy")
v_10 = np.load(directory + "v_10.npy")
v_50 = np.load(directory + "v_50.npy")
v_100 = np.load(directory + "v_100.npy")
v_200 = np.load(directory + "v_200.npy")
beats = np.arange(len(v_max))
meas =   ( apd50 ,  apd90 ,  v_max ,  v_min ,  v_10 ,  v_50 ,  v_100 ,  v_200 )
labels = ["apd50", "apd90", "V_{max}", "V_{min}", "V_{10}", "V_{50}", "V_{100}", "V_{200}"]

size = 5
im = 6
what = ax.scatter(meas[im][:-1], meas[im][1:], c=beats[:-1], s=size)
ax.set_xlabel('$'+labels[im]+"^{n}$")
ax.set_ylabel('$'+labels[im]+"^{n+1}$")
#    plt.title("Xia->Bs @cl 1000ms")
    
cbar = plt.colorbar(what)
cbar.set_label('n', rotation=0)
plt.tight_layout()

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
    
    #%%
#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False
filename = r'../../figures/att_TT.png'

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure(figsize=(figwidth, figheight))

ax = fig.add_subplot(111)

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"

name = "timm_TT-BrS_0"

directory = data_dir + sep + name + sep
apd50 = np.load(directory + "apd50.npy")
apd90 = np.load(directory + "apd90.npy")
v_max = np.load(directory + "v_max.npy")
v_min = np.load(directory + "v_min.npy")
v_10 = np.load(directory + "v_10.npy")
v_50 = np.load(directory + "v_50.npy")
v_100 = np.load(directory + "v_100.npy")
v_200 = np.load(directory + "v_200.npy")
beats = np.arange(len(v_max))
meas =   ( apd50 ,  apd90 ,  v_max ,  v_min ,  v_10 ,  v_50 ,  v_100 ,  v_200 )
labels = ["apd50", "apd90", "V_{max}", "V_{min}", "V_{10}", "V_{50}", "V_{100}", "V_{200}"]

size = 3
im = 6
start=0
what = ax.scatter(meas[im][start:-1], meas[im][start+1:], c=range(1,len(meas[im])-start), s=size, cmap = 'hot_r', edgecolors='none')

ax.set_xlabel('$'+labels[im]+"^{n}$")
ax.set_ylabel('$'+labels[im]+"^{n+1}$")
#    plt.title("Xia->Bs @cl 1000ms")
    
cbar = plt.colorbar(what)
cbar.set_label('n', rotation=0)
plt.tight_layout()

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    plt.close(fig)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
    
        #%%
#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False
filename = r'../../figures/att_TT-BrS_cl1000.png'

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure(figsize=(figwidth, figheight))

ax = fig.add_subplot(111)

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"

name = "timm_TT-BrS_1"

directory = data_dir + sep + name + sep
apd50 = np.load(directory + "apd50.npy")
apd90 = np.load(directory + "apd90.npy")
v_max = np.load(directory + "v_max.npy")
v_min = np.load(directory + "v_min.npy")
v_10 = np.load(directory + "v_10.npy")
v_50 = np.load(directory + "v_50.npy")
v_100 = np.load(directory + "v_100.npy")
v_200 = np.load(directory + "v_200.npy")
beats = np.arange(len(v_max))
meas =   ( apd50 ,  apd90 ,  v_max ,  v_min ,  v_10 ,  v_50 ,  v_100 ,  v_200 )
labels = ["apd50", "apd90", "V_{max}", "V_{min}", "V_{10}", "V_{50}", "V_{100}", "V_{200}"]

size = 1
im = 6
start=0
what = ax.scatter(meas[im][start:-1], meas[im][start+1:], c=range(1,len(meas[im])-start), s=size, cmap = 'hot_r', edgecolors='none')

ax.set_xlabel('$'+labels[im]+"^{n}$")
ax.set_ylabel('$'+labels[im]+"^{n+1}$")
#    plt.title("Xia->Bs @cl 1000ms")
    
cbar = plt.colorbar(what)
cbar.set_label('n', rotation=0)
plt.tight_layout()

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
    
        #%%
#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False
filename = r'../../figures/att_TT-BrS.png'

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure(figsize=(figwidth, figheight))

ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122, sharey = ax1)

plt.setp(ax2.get_yticklabels(), visible=False)

ax1.set_xlabel('$V_{100}^{n}$', position=(1,0.1))
ax1.set_ylabel('$V_{100}^{n+1}$')

ax1.set_ylim((-90, 25))

fig.subplots_adjust(wspace=0.1)
fig.subplots_adjust(top=0.98, bottom=0.17, left = 0.12, right = 1)

colormap = 'inferno_r'
size = 3
textbox_props = dict(boxstyle="round", fc="w", ec="none", alpha=0.9)
boxx, boxy = (0.05, 0.95)

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"

name = "timm_TT-BrS_0"
ax = ax1
directory = data_dir + sep + name + sep
v_100 = np.load(directory + "v_100.npy")
beats = np.arange(len(v_100))
start=0
what = ax.scatter(v_100[start:-1], v_100[start+1:], c=range(1,len(v_100)-start), s=size, cmap = colormap, edgecolors='none')
ax.text(boxx, boxy, r'$cl = \SI{400}{ms}$', ha="left", va="top",
        bbox=textbox_props, transform=ax.transAxes)

name = "timm_TT-BrS_1"
ax = ax2
directory = data_dir + sep + name + sep
v_100 = np.load(directory + "v_100.npy")
beats = np.arange(len(v_100))
start=0
what = ax.scatter(v_100[start:-1], v_100[start+1:], c=range(1,len(v_100)-start), s=size, cmap = colormap, edgecolors='none')
ax.text(boxx, boxy, r'$cl = \SI{1000}{ms}$', ha="left", va="top",
        bbox=textbox_props, transform=ax.transAxes)

# from https://stackoverflow.com/questions/8342549/matplotlib-add-colorbar-to-a-sequence-of-line-plots
plt.figure(2)
Z = [[0,0],[0,0]]
levels = np.arange(0.5,5005.5, 1)
CS3 = plt.contourf(Z, levels, cmap=plt.get_cmap(colormap))
plt.clf()
#cbar = fig.colorbar()
plt.close(2)
cbar = plt.colorbar(CS3, ax=fig.get_axes())
cbar.set_ticks([1, 1000, 2000, 3000, 4000, 5000])
cbar.set_label('Cycle number $n$')
 
#cbar = plt.colorbar(what, ax=fig.get_axes())
#cbar.set_label('n', rotation=0)

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    plt.close(fig)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)