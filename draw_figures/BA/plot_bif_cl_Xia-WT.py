#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/bif_cl_TT-WT.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.6

fig = plt.figure(figsize=(figwidth, figheight))
fig.clear()
ax = fig.add_subplot(111)
#ax.grid()
#ax2 = fig.add_subplot(212)
#ax2.set_ylabel("apd90")
#ax2.set_xlim((100,300))
ax.set_xlim((200,1300))
ax.set_ylim((-1,311))

plt.ylabel("$APD_{90}$ [ms]")
plt.xlabel("Cycle length [ms]")

#fig.subplots_adjust(hspace=0.25)
plt.subplots_adjust(bottom=0.22, top=0.98, left=0.1, right=0.99)

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
#sep = "\\"
markersize = 20
lw = 1
i = 0
marker = "."
markersize =  1.5

## 100 - 400
#prefix = 'bif_xiaepi_1'
#skip_beats = 40
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
##for darray in (v_s, apd90):
##    normalize(darray)
##for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix, marker = marker, s=markersize)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "r",marker = marker, s=markersize)
#for row in apd90.T[::]: ax2.scatter(cl, row, color = "r",marker = marker, s=markersize) 
#
## 100 - 400
#prefix = 'bif_xiaepi_2'
#skip_beats = 40
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
##for darray in (v_s, apd90):
##    normalize(darray)
##for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix, marker = marker, s=markersize)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "r",marker = marker, s=markersize)
#for row in apd90.T[::]: ax2.scatter(cl, row, color = "r",marker = marker, s=markersize)
# 350 - 1300
prefix = 'bif_xiaepi_7'
skip_beats = 40
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, color = "r",marker = marker, s=markersize)
#for row in apd90.T[::]: ax2.scatter(cl, row, color = "r",marker = marker, s=markersize)



# 100 - 400
prefix = 'bif_xiaepi_9'
skip_beats = 40
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')[10:]
v_s = np.load(directory + 'v_s.npy')[10:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[10:,skip_beats:]
cl = cl[:v_s.shape[0]]
for i in range(apd90.shape[0]):
    for j in range(apd90.shape[1]):
        if apd90[i,j] == 0:
            apd90[i,j]=cl[i]
#for darray in (v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, color = "r",marker = marker, s=markersize)
#for row in apd90.T[::]: ax2.scatter(cl, row, color = "r",marker = marker, s=markersize)

# 100 - 400
prefix = 'bif_xiaepi_8'
skip_beats = 40
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for i in range(apd90.shape[0]):
    for j in range(apd90.shape[1]):
        if apd90[i,j] == 0:
            apd90[i,j]=cl[i]
#for darray in (v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, color = "b",marker = marker, s=markersize)
#for row in apd90.T[::]: ax2.scatter(cl, row, color = "b",marker = marker, s=markersize)

# 350 - 1300
prefix = 'bif_xiaepi_5'
skip_beats = 40
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, color = "b",marker = marker, s=markersize)
#for row in apd90.T[::]: ax2.scatter(cl, row, color = "b",marker = marker, s=markersize)




ax.plot(np.arange(100,400, 10), np.arange(100,400, 10), color='black', lw=1)
#ax2.plot(np.arange(100,400, 10), np.arange(100,400, 10), color='black', lw=1)

# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
#plt.legend(handles, labels, loc='lower right')
#plt.tight_layout()
# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)