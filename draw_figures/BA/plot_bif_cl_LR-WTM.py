#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/bif_cl_LR-WTM.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.5

fig = plt.figure(1, figsize=(figwidth, figheight))
fig2 = plt.figure(2, figsize=(figwidth, figheight))
fig.clear()
ax = fig.add_subplot(111)
ax2 = fig2.add_subplot(111)
#ax.grid()
#ax2 = fig.add_subplot(212)
#ax2.set_ylabel("apd90")
#ax2.set_xlim((100,300))

#ax.set_xlim((100,999))
#ax.set_ylim((45,120))

ax.set_ylabel("apd90 [ms]")
ax2.set_ylabel("$V_{50}$ [mV]")
ax.set_xlabel("Cycle length [ms]")
ax2.set_xlabel("Cycle length [ms]")

#fig.subplots_adjust(hspace=0.25)
fig.subplots_adjust(bottom=0.18, top=0.98, left=0.1, right=0.99)
fig2.subplots_adjust(bottom=0.27, top=0.98, left=0.11, right=0.99)

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
#sep = "\\"
markersize = 20
lw = 1
i = 0
marker = "."
markersize =  1.5
alpha = 0.2



prefix = 'bif_wtm_2'
skip_beats = 80
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for i in range(apd90.shape[0]):
    for j in range(apd90.shape[1]):
        if apd90[i,j] == 0:
            apd90[i,j]=cl[i]
#for darray in (v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, color = "r",marker = marker, s=markersize, alpha = alpha)
for row in v_s.T[::]: ax2.scatter(cl, row, color = "r",marker = marker, s=markersize, alpha = alpha)

prefix = 'bif_wtm_0'
skip_beats = 80
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for i in range(apd90.shape[0]):
    for j in range(apd90.shape[1]):
        if apd90[i,j] == 0:
            apd90[i,j]=cl[i]
#for darray in (v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, color = '#0D0887',marker = marker, s=markersize, alpha = alpha)
for row in v_s.T[::]: ax2.scatter(cl, row, color = '#0D0887',marker = marker, s=markersize, alpha = alpha)

#ax.plot(np.arange(100,400, 10), np.arange(100,400, 10), color='black', lw=1)
#ax2.plot(np.arange(100,400, 10), np.arange(100,400, 10), color='black', lw=1)

# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
#plt.legend(handles, labels, loc='lower right')
#plt.tight_layout()
# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)






