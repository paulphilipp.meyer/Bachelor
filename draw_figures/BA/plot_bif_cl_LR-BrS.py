#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False
filename = r'../../figures/bif_cl_LR-BrS.png'

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*0.9
fig = plt.figure(figsize=(figwidth, figheight))
fig.clear()

fig.subplots_adjust(hspace=0.25)
fig.subplots_adjust(bottom=0.15, top=0.98, left=0.1, right=0.97)


ax = fig.add_subplot(211)
plt.ylabel("$APD_{90}$")
ax2 = fig.add_subplot(212)
plt.ylabel("$APD_{90}$")
plt.xlabel("Cycle length [ms]")

ax.set_ylim((-1,201))
ax.set_xlim((150,1000))

ax2.set_ylim((10,126))
ax2.set_xlim((200, 275))


marker = "."
markersize = 2
alpha = 0.5

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

#names = ['beef_xiaepi_16', 'beef_xiaepi_17']
#colors = ["r", "b"]
#skip_beats = 20
#marker = "."
#markersize = 2
#for i in range(len(names)):
#    name = names[i]
#    directory = dir_data + "/" + name + "/"
#    cl = np.load(directory + 'cl.npy')[:,skip_beats:]
#    apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#    for row in apd90.T[::]: ax.scatter(cl, row, color = colors[i], label = "$APD90$", marker = marker, s=markersize)
    




prefix = 'bif_bs_8'
skip_beats = 20
color = 'r'
directory = dir_data + "/" + prefix + "/"
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_max, v_min, v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, marker = marker, s=markersize, edgecolors='none', facecolors=color, alpha=alpha)
for row in apd90.T[::]: ax2.scatter(cl, row, marker = marker, s=markersize, edgecolors='none', facecolors=color, alpha=alpha)

prefix = 'bif_bs_6'
skip_beats = 20
color = '#0D0887'
directory = dir_data + "/" + prefix + "/"
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
#for darray in (v_max, v_min, v_s, apd90):
#    normalize(darray)
for row in apd90.T[::]: ax.scatter(cl, row, marker = marker, s=markersize, edgecolors='none', facecolors=color, alpha=alpha)
for row in apd90.T[::]: ax2.scatter(cl, row, marker = marker, s=markersize, edgecolors='none', facecolors=color, alpha=alpha)

#ax.plot(np.arange(100,400, 10), np.arange(100,400, 10), color='black', lw=1)
#plt.legend()

# remove multiple labels
#handles, labels = plt.gca().get_legend_handles_labels()
#labels, ids = np.unique(labels, return_index=True)
#handles = [handles[i] for i in ids]
#plt.legend(handles, labels, loc='upper center')
#plt.tight_layout()


#plt.tight_layout(0)

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    plt.close()
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
