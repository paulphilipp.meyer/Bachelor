#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, normalize
plt.ion()
fig = plt.figure()

ax = fig.add_subplot(111)
plt.ylabel("APD90")
#plt.xlim((900,1100))

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)


plt.ylabel("Membrane potential (t=50ms) [mV]")
#plt.xlim((900,1100))
plt.xlabel("Excitation cycle length [ms]")

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
#sep = "\\"
markersize = 1.5
marker = "."
lw = 1

## 100 - 400
#prefix = 'beef_xiaepi_bs_11'
#skip_beats = 20
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
##for darray in (v_max, v_min, v_s, apd90):
##    normalize(darray)
#for row in v_s.T: ax2.scatter(cl, row, color = "orange", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "orange", marker = marker, s=markersize, lw = lw)
#
## 300 - 525
#prefix = 'beef_xiaepi_bs_1'
#skip_beats = 20
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "r", marker = marker, s=markersize, lw = lw)
#
## 300 - 600
#prefix = 'beef_xiaepi_bs_6'
#skip_beats = 20
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "g", marker = marker, s=markersize, lw = lw)
#
## 350 - 400
#prefix = 'beef_xiaepi_bs_13'
#skip_beats = 50
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "r", marker = marker, s=markersize, lw = lw)
#
## 400 - 450
#prefix = 'beef_xiaepi_bs_14'
#skip_beats = 50
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "b", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "b", marker = marker, s=markersize, lw = lw)
#
## 400 - 700
#prefix = 'beef_xiaepi_bs_10'
#skip_beats = 20
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "orange", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "orange", marker = marker, s=markersize, lw = lw)
#
## 525 - 700
#prefix = 'beef_xiaepi_bs_12'
#skip_beats = 20
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "r", marker = marker, s=markersize, lw = lw)
#
## 600 - 900
#prefix = 'beef_xiaepi_bs_7'
#skip_beats = 20
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "r", marker = marker, s=markersize, lw = lw)
#
## 500 - 800
#prefix = 'beef_xiaepi_bs_8'
#skip_beats = 30
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "g", marker = marker, s=markersize, lw = lw)
#
## 800 - 1100
#prefix = 'beef_xiaepi_bs_9'
#skip_beats = 30
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "g", marker = marker, s=markersize, lw = lw)
#
## 990 - 1010
#prefix = 'beef_xiaepi_bs_23'
#skip_beats = 30
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "b", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "b", marker = marker, s=markersize, lw = lw)
#
## 1100 - 1148
#prefix = 'beef_xiaepi_bs_16'
#skip_beats = 30
#skip_cl = 6
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')[skip_cl:]
#v_s = np.load(directory + 'v_s.npy')[skip_cl:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[skip_cl:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "b", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "b", marker = marker, s=markersize, lw = lw)
#
## 1140 - 1160
#prefix = 'beef_xiaepi_bs_25'
#skip_beats = 0
#skip_cl = 0
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')[skip_cl:]
#v_s = np.load(directory + 'v_s.npy')[skip_cl:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[skip_cl:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "g", marker = marker, s=markersize, lw = lw)
#
## 1140 - 1160
#prefix = 'beef_xiaepi_bs_22'
#skip_beats = 0
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "b", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "b", marker = marker, s=markersize, lw = lw)
#
## 1148 - 1200
#prefix = 'beef_xiaepi_bs_15'
#skip_beats = 30
#skip_cl = 9
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')[skip_cl:]
#v_s = np.load(directory + 'v_s.npy')[skip_cl:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[skip_cl:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "g", marker = marker, s=markersize, lw = lw)




## 1200 - 1700
#prefix = 'bif_xiaepi_bs_1'
#skip_beats = 0
#directory = dir_data + sep + prefix + sep
#cl = np.load(directory + 'cl.npy')
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#cl = cl[:v_s.shape[0]]
#for row in v_s.T: ax2.scatter(cl, row, color = "b", label = prefix , marker = marker, s=markersize, lw = lw)
#for row in apd90.T[::]: ax.scatter(cl, row, color = "b", marker = marker, s=markersize, lw = lw)

# 1200 - 1700
prefix = 'bif_xiaepi_bs_2'
skip_beats = 0
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = "r", label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = "r", marker = marker, s=markersize, lw = lw)

# 1200 - 2000
prefix = 'bif_xiaepi_bs_3'
skip_beats = 0
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = "g", label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = "g", marker = marker, s=markersize, lw = lw)

# 
prefix = 'bif_xiaepi_bs_5'
skip_beats = 0
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = "b", label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = "b", marker = marker, s=markersize, lw = lw)

# 
prefix = 'bif_xiaepi_bs_8'
skip_beats = 0
directory = dir_data + sep + prefix + sep
cl = np.load(directory + 'cl.npy')
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
cl = cl[:v_s.shape[0]]
for row in v_s.T: ax2.scatter(cl, row, color = "magenta", label = prefix , marker = marker, s=markersize, lw = lw)
for row in apd90.T[::]: ax.scatter(cl, row, color = "magenta", marker = marker, s=markersize, lw = lw)


#ax2.set_xlim((1500,1600))
#ax2.set_ylim((-50,-20))

# remove multiple labels
#handles, labels = plt.gca().get_legend_handles_labels()
#labels, ids = np.unique(labels, return_index=True)
#handles = [handles[i] for i in ids]
#plt.legend(handles, labels, loc='lower right')

plt.title("Xia -> Brugada")
#
#from subprocess import call
#filename = "tmp.png"
#plt.savefig(filename)
#call(["eog", filename])
