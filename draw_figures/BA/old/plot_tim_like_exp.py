#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/tim_like_exp.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.5

fig, axes = plt.subplots(3, sharex=True, sharey=True, figsize=(figwidth, figheight))
fig2, ax2 = plt.subplots(1, sharex=True, sharey=True, figsize=(figwidth, figheight))

plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
for ax in axes:
    ax.set_ylim((-95,42))
    ax.set_xlim((-15, 400))

axes[-1].set_xlabel("Time [ms]")
axes[1].set_ylabel("Voltage [mV]")
fig.subplots_adjust(bottom=0.2)
    
ax2.set_ylim((-95,42))
ax2.set_xlim((-15, 400))
ax2.set_xlabel("Time [ms]")
ax2.set_ylabel("Voltage [mV]")
fig2.subplots_adjust(bottom=0.27, right=0.97, top=0.99)


#for ax in axes[1:]:
##    ax.axis('off')
#    ax.get_xaxis().set_ticks([])
#    ax.get_yaxis().set_ticks([])
#fig.subplots_adjust(hspace=0.1, wspace=0.05)


bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
boxx, boxy = (0.9, 0.7)
cls = ['1666', '1250', '1000']
for i, ax in zip(range(len(axes)), axes):
    ax.text(boxx, boxy, r"$cl = "+str(cls[i])+'$', ha="center", va="center",
        bbox=bbox_props, transform=axes[i].transAxes)


lw = 0.7
colormap = 'inferno'
base_colors = ['#007f0a', '#0a007f', '#7f0a00']
labels = ['initial state A', 'initial state B', 'initial state C']

#ax2.annotate('A', xy=(343, -57), xytext=(380, -50), color = base_colors[0],
#            arrowprops=dict(facecolor=base_colors[0], ec='none', shrink=0.05, width = 2, headwidth=5),
#            )
#ax2.annotate('B', xy=(328, -52), xytext=(369, -12), color = base_colors[1],
#            arrowprops=dict(facecolor=base_colors[1], ec='none', shrink=0.05, width = 2, headwidth=5),
#            )
#ax2.annotate('C', xy=(305, -40), xytext=(328, 12), color = base_colors[2],
#            arrowprops=dict(facecolor=base_colors[2], ec='none', shrink=0.05, width = 2, headwidth=5),
#            )

fig2.canvas.draw()

#%%
name = "tims_exp_TT_11"
itim = 0
directory = dir_data + "/" + name + "/"
v = np.load(directory +'states.npy', mmap_mode='r').T[0]
t = np.load(directory +'times.npy', mmap_mode='r')

i06Hz = i08Hz = i10Hz = 0
i_excite = 0
i_beat = 0
paced1 = paced2 = paced3 = [None,]* 20
times1 = times2 = times3 = [None,]* 20
# find points where cl changes
for i in range(1, len(t)):
    if (t[i] < t[i-1]):
        print("%s: beat %d, time %f" % (name, i_beat, t[i-1]))
        if ( (i08Hz==0) and (abs(t[i-1]-1250) < 1) ):
            i08Hz = i_excite
            i_beat = 0
        if ( (i10Hz==0) and (abs(t[i-1]-1000) < 1) ):
            i10Hz = i_excite
            i_beat = 0
            
        if (i08Hz==0): # then still at 06Hz
            paced1[i_beat] = v[i_excite:i-1]
            times1[i_beat] = t[i_excite:i-1]
        elif (i10Hz==0): # then at 08Hz
            paced2[i_beat] = v[i_excite:i-1]
            times2[i_beat] = t[i_excite:i-1]
        else: #then at 10Hz
            paced3[i_beat] = v[i_excite:i-1]
            times3[i_beat] = t[i_excite:i-1]
        i_excite = i
#        print('increase i_beat:')
        i_beat += 1
paced3[i_beat] = v[i_excite:i-1]
times3[i_beat] = t[i_excite:i-1]
        
num_points1 = i08Hz
num_points2 = i10Hz - i08Hz
num_points3 = len(t)- i10Hz

num = 0
for i, ap in zip(range(num_points1), paced1):
    axes[num].plot(times1[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw, label = labels[itim])
num = 1
for i, ap in zip(range(num_points2), paced2):
    axes[num].plot(times2[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw)
num = 2
for i, ap in zip(range(num_points3), paced3):
    axes[num].plot(times3[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw)
    
#%%
name = "tims_exp_TT_12"
itim = 1
directory = dir_data + "/" + name + "/"
v = np.load(directory +'states.npy', mmap_mode='r').T[0]
t = np.load(directory +'times.npy', mmap_mode='r')

i06Hz = i08Hz = i10Hz = 0
i_excite = 0
i_beat = 0
paced1 = paced2 = paced3 = [None,]* 20
times1 = times2 = times3 = [None,]* 20
# find points where cl changes
for i in range(1, len(t)):
    if (t[i] < t[i-1]):
        print("%s: beat %d, time %f" % (name, i_beat, t[i-1]))
        if ( (i08Hz==0) and (abs(t[i-1]-1250) < 1) ):
            i08Hz = i_excite
            i_beat = 0
        if ( (i10Hz==0) and (abs(t[i-1]-1000) < 1) ):
            i10Hz = i_excite
            i_beat = 0
            
        if (i08Hz==0): # then still at 06Hz
            paced1[i_beat] = v[i_excite:i-1]
            times1[i_beat] = t[i_excite:i-1]
        elif (i10Hz==0): # then at 08Hz
            paced2[i_beat] = v[i_excite:i-1]
            times2[i_beat] = t[i_excite:i-1]
        else: #then at 10Hz
            paced3[i_beat] = v[i_excite:i-1]
            times3[i_beat] = t[i_excite:i-1]
        i_excite = i
#        print('increase i_beat:')
        i_beat += 1
paced3[i_beat] = v[i_excite:i-1]
times3[i_beat] = t[i_excite:i-1]
        
num_points1 = i08Hz
num_points2 = i10Hz - i08Hz
num_points3 = len(t)- i10Hz

num = 0
for i, ap in zip(range(num_points1), paced1):
    axes[num].plot(times1[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw, label = labels[itim])
num = 1
for i, ap in zip(range(num_points2), paced2):
    axes[num].plot(times2[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw)
num = 2
for i, ap in zip(range(num_points3), paced3):
    axes[num].plot(times3[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw)
    
#%%
name = "tims_exp_TT_13"
itim = 2
directory = dir_data + "/" + name + "/"
v = np.load(directory +'states.npy', mmap_mode='r').T[0]
t = np.load(directory +'times.npy', mmap_mode='r')

i06Hz = i08Hz = i10Hz = 0
i_excite = 0
i_beat = 0
paced1 = paced2 = paced3 = [None,]* 20
times1 = times2 = times3 = [None,]* 20
# find points where cl changes
for i in range(1, len(t)):
    if (t[i] < t[i-1]):
        print("%s: beat %d, time %f" % (name, i_beat, t[i-1]))
        if ( (i08Hz==0) and (abs(t[i-1]-1250) < 1) ):
            i08Hz = i_excite
            i_beat = 0
        if ( (i10Hz==0) and (abs(t[i-1]-1000) < 1) ):
            i10Hz = i_excite
            i_beat = 0
            
        if (i08Hz==0): # then still at 06Hz
            paced1[i_beat] = v[i_excite:i-1]
            times1[i_beat] = t[i_excite:i-1]
        elif (i10Hz==0): # then at 08Hz
            paced2[i_beat] = v[i_excite:i-1]
            times2[i_beat] = t[i_excite:i-1]
        else: #then at 10Hz
            paced3[i_beat] = v[i_excite:i-1]
            times3[i_beat] = t[i_excite:i-1]
        i_excite = i
#        print('increase i_beat:')
        i_beat += 1
paced3[i_beat] = v[i_excite:i-1]
times3[i_beat] = t[i_excite:i-1]
        
num_points1 = i08Hz
num_points2 = i10Hz - i08Hz
num_points3 = len(t)- i10Hz

num = 0
for i, ap in zip(range(num_points1), paced1):
    axes[num].plot(times1[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw, label = labels[itim])
num = 1
for i, ap in zip(range(num_points2), paced2):
    axes[num].plot(times2[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw)
num = 2
for i, ap in zip(range(num_points3), paced3):
    axes[num].plot(times3[i], ap, color=base_colors[itim], lw =lw)
    ax2.plot(times1[i], ap, color=base_colors[itim], lw =lw)

# remove multiple labels
handles, labels = plt.gca().get_legend_handles_labels()
labels, ids = np.unique(labels, return_index=True)
handles = [handles[i] for i in ids]
plt.legend(handles, labels, loc = (0.1,0.1))

# save / display plot
if not interactive:
    print("saving...")
    fig2.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)