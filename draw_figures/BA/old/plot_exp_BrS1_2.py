#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/tim_exp_BrS1_2.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8

fig, axes = plt.subplots(3, sharex=True, sharey=True, figsize=(figwidth, figheight))

plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
for ax in axes:
    ax.set_ylim((-70,42))
    
axes[-1].set_xlabel("time [ms]")
axes[1].set_ylabel("membrane voltage [mV]")
#for ax in axes[1:]:
##    ax.axis('off')
#    ax.get_xaxis().set_ticks([])
#    ax.get_yaxis().set_ticks([])
#fig.subplots_adjust(hspace=0.1, wspace=0.05)
plt.subplots_adjust(bottom=0.18)

import pickle

#dir_data = '/home/paul/Schreibtisch/Michael Stauske'
dir_data = '/data.bmp/pmeyer/Bachelor/stauske_data/pickle_dicts'

name = '/BrS1/03-20Nov2014.pkl'
#name = '/BrS1/04-20Nov2014.pkl'

filepath = dir_data + name

d = pickle.load( open( filepath, "rb" ), encoding='latin1' )
#%%
for itr in range(len(d['traces'])):
    print(str(itr)+', ' + str(len(d['traces'][itr])))
print(d['comment'])
#%%
dt = d['dt']
cls = ['1666', '1250', '1000']
unpaced = d['traces'][0]

paced1 = np.vstack(d['traces'][1:21])
num_points1 = paced1.shape[1]
times1 = np.zeros(num_points1)
for i in range(num_points1):
    times1[i] = i*dt
   
paced2 = np.vstack(d['traces'][21:41])
num_points2 = paced2.shape[1]
times2 = np.zeros(num_points2)
for i in range(num_points2):
    times2[i] = i*dt
    
paced3 = np.vstack(d['traces'][41:])
num_points3 = paced3.shape[1]
times3 = np.zeros(num_points3)
for i in range(num_points3):
    times3[i] = i*dt
    

#fig = plt.figure()
#ax = plt.subplot(111)
bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
boxx, boxy = (0.9, 0.7)
lw = 0.7
#colors = plt.get_cmap('hot')(np.linspace(1,0,len(paced1)))
num = 0
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(paced1)))
for i, ap, color in zip(range(num_points1), paced1, colors):
    axes[num].plot(times1, ap, color=color, lw =lw)
axes[num].text(boxx, boxy, r"$cl = "+str(cls[num])+'$', ha="center", va="center",
        bbox=bbox_props, transform=axes[num].transAxes)

num = 1
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(paced1)))
for i, ap, color in zip(range(num_points2), paced2, colors):
    axes[num].plot(times2, ap, color=color, lw =lw)
axes[num].text(boxx, boxy, r"$cl = "+str(cls[num])+'$', ha="center", va="center",
        bbox=bbox_props, transform=axes[num].transAxes)

num = 2
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(paced1)))
for i, ap, color in zip(range(num_points3), paced3, colors):
    axes[num].plot(times3, ap, color=color, lw =lw)
axes[num].text(boxx, boxy, r"$cl = "+str(cls[num])+'$', ha="center", va="center",
        bbox=bbox_props, transform=axes[num].transAxes)
    


# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)