#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper

interactive = False
final = True
filename = r'../../figures/tim_LR-BrS.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.45

fig, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(figwidth, figheight))
for axgroup in axes:
    for ax in axgroup:
        ax.axis('off')
#        ax.get_xaxis().set_ticks([])
#        ax.get_yaxis().set_ticks([])


fig.subplots_adjust(hspace=0.1, wspace=0.1)
fig.subplots_adjust(bottom=0, right=1, left=0, top=1)

#ax1.set_yticks((0.5,), minor=False)
#ax_cl1.set_yticks((0.5,), minor=False)
#ax_cl2.set_yticks((0.5,), minor=False)
#ax_cl3.set_yticks((0.5,), minor=False)
#ax_cl1.set_yticklabels((150,), fontdict=None, minor=False)
#ax_cl2.set_yticklabels((150,), fontdict=None, minor=False)
#ax_cl3.set_yticklabels((1000,), fontdict=None, minor=False)



bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
textbox_props = dict(boxstyle="round", fc="w", ec="none", alpha=0.9)

anchor = (60,-60)
axes[1,1].plot((anchor[0], anchor[0], anchor[0]+50), (anchor[1]+20, anchor[1], anchor[1]), c='black')
axes[1,1].text(anchor[0]+25, anchor[1]-18, r'$\SI{50}{ms}$', ha="center", va="center")
axes[1,1].text(anchor[0]-25, anchor[1]+10, r'$\SI{20}{mV}$', ha="center", va="center")

labs = ['A', 'B', 'C', 'D']
cls = [215, 300, 260, 800]
for iax in range(len(axes.flat)):
    ax = axes.flat[iax]
    ax.text(0.95, 0.9, labs[iax], ha="right", va="top",
            bbox=bbox_props, transform=ax.transAxes)
    ax.text(0.95, 0.55, r'$cl = \SI{'+str(cls[iax])+'}{ms}$', ha="right", va="top",
        transform=ax.transAxes, bbox=textbox_props)


axes[0,0].set_xlabel('Time [ms]')
axes[0,0].set_ylabel('Voltage [mV]')
axes[0, 0].set_xlim((-10,210))
lw = 1
#%%
i,j = (0, 0)
cl = 215
name = "spot_LR-BrS_1"
directory = dir_data + "/" + name + "/"
begin = 0
till = 1000
if final: till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
data = helper.sep_beats(t, v)
numbeats = data.shape[1]
print('beats for cl {0}: {1}'.format(cl, numbeats))
#ut = np.copy(t)
#helper.unwrap(ut)
#ut -= cl
colors = plt.get_cmap('plasma')(np.linspace(1,0,numbeats))
for ibeat, color in zip(range(numbeats), colors):
    axes[i, j].plot(data[0,ibeat,:], data[1,ibeat,:], color=color, lw =lw)

#axes[i, j].plot(t, v)
#axes[i, j].text(0.85, 0.8, r"$cl = "+str(cl)+'$', ha="center", va="center",
#        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (1,0)
cl = 260
name = "spot_LR-BrS_2"
directory = dir_data + "/" + name + "/"
begin = 0
till = 1000
if final: till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
data = helper.sep_beats(t, v)
numbeats = data.shape[1]
print('beats for cl {0}: {1}'.format(cl, numbeats))
#ut = np.copy(t)
#helper.unwrap(ut)
#ut -= cl
colors = plt.get_cmap('plasma')(np.linspace(1,0,numbeats))
for ibeat, color in zip(range(numbeats), colors):
    axes[i, j].plot(data[0,ibeat,:], data[1,ibeat,:], color=color, lw =lw)

#axes[i, j].plot(t, v)
#axes[i, j].plot(ut-cl, v, '--')
#axes[i, j].text(0.85, 0.8, r"$cl = "+str(cl)+'$', ha="center", va="center",
#        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (0,1)
cl = 300
name = "spot_LR-BrS_3"
directory = dir_data + "/" + name + "/"
begin = 0
till = 1000
if final: till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
data = helper.sep_beats(t, v)
numbeats = data.shape[1]
print('beats for cl {0}: {1}'.format(cl, numbeats))
#ut = np.copy(t)
#helper.unwrap(ut)
#ut -= cl
colors = plt.get_cmap('plasma')(np.linspace(1,0,numbeats))
for ibeat, color in zip(range(numbeats), colors):
    axes[i, j].plot(data[0,ibeat,:], data[1,ibeat,:], color=color, lw =lw)

#axes[i, j].plot(t, v)
#axes[i, j].text(0.85, 0.8, r"$cl = "+str(cl)+'$', ha="center", va="center",
#        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (1,1)
cl = 800
name = "spot_LR-BrS_4"
directory = dir_data + "/" + name + "/"
begin = 0
till = 1000
if final: till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
data = helper.sep_beats(t, v)
numbeats = data.shape[1]
print('beats for cl {0}: {1}'.format(cl, numbeats))
#ut = np.copy(t)
#helper.unwrap(ut)
#ut -= cl
colors = plt.get_cmap('plasma')(np.linspace(1,0,numbeats))
for ibeat, color in zip(range(numbeats), colors):
    axes[i, j].plot(data[0,ibeat,:], data[1,ibeat,:], color=color, lw =lw)

#axes[i, j].plot(t, v)
#axes[i, j].text(0.85, 0.8, r"$cl = "+str(cl)+'$', ha="center", va="center",
#        bbox=bbox_props, transform=axes[i, j].transAxes)

# from https://stackoverflow.com/questions/8342549/matplotlib-add-colorbar-to-a-sequence-of-line-plots
plt.figure(2)
Z = [[0,0],[0,0]]
levels = np.arange(0.5,1001.5, 1)
CS3 = plt.contourf(Z, levels, cmap=plt.get_cmap('plasma_r'))
plt.clf()
plt.close(2)
cbar = plt.colorbar(CS3, ax=fig.get_axes(), shrink=0.9)
cbar.set_ticks([1, 250, 500, 750, 1000])
cbar.set_label('Cycle number $n$', rotation=90)


#plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)