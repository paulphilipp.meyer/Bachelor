#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper

interactive = False
filename = '../../figures/tim_LR-WT.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.6

fig, axes = plt.subplots(3, 2, sharex=True, sharey=True, figsize=(figwidth, figheight))
for axgroup in axes:
    for ax in axgroup:
        ax.axis('off')
#        ax.get_xaxis().set_ticks([])
#        ax.get_yaxis().set_ticks([])


fig.subplots_adjust(hspace=0.1, wspace=0.05)
plt.subplots_adjust(bottom=0, right=1, left=0, top=1)

#ax1.set_yticks((0.5,), minor=False)
#ax_cl1.set_yticks((0.5,), minor=False)
#ax_cl2.set_yticks((0.5,), minor=False)
#ax_cl3.set_yticks((0.5,), minor=False)
#ax_cl1.set_yticklabels((150,), fontdict=None, minor=False)
#ax_cl2.set_yticklabels((150,), fontdict=None, minor=False)
#ax_cl3.set_yticklabels((1000,), fontdict=None, minor=False)

axes[1,1].set_xlim((-20, 400))

bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
anchor = (230,-25)
axes[2,1].plot((anchor[0], anchor[0], anchor[0]+100), (anchor[1]+50, anchor[1], anchor[1]), c='black')
axes[2,1].text(anchor[0]+50, anchor[1]-20, r'$\SI{100}{ms}$', ha="center", va="center")
axes[2,1].text(anchor[0]-40, anchor[1]+20, r'$\SI{50}{mV}$', ha="center", va="center")
color = '#0f55ce'
color = '#0D0887'
xcl = 0.95
ycl = 0.9
#%%
i,j = (0, 0)
cl = 150
name = "tim_LR-WT_0"
directory = dir_data + "/" + name + "/"
begin = 0
till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
ut = np.copy(t)
helper.unwrap(ut)
ut -= cl

axes[i, j].plot(ut, v, color=color)
axes[i, j].text(xcl, ycl, r"$cl = \SI{"+str(cl)+'}{ms}$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (1,0)
cl = 200
name = "tim_LR-WT_15"
directory = dir_data + "/" + name + "/"
begin = 0
till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
ut = np.copy(t)
helper.unwrap(ut)
ut -= cl

axes[i, j].plot(ut, v, color=color)
axes[i, j].plot(ut-cl, v, '--', color='#e56800')
axes[i, j].text(xcl, ycl, r"$cl = \SI{"+str(cl)+'}{ms}$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (2,0)
cl = 300
name = "tim_LR-WT_7"
directory = dir_data + "/" + name + "/"
begin = 0
till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
ut = np.copy(t)
helper.unwrap(ut)
ut -= cl

axes[i, j].plot(ut, v, color=color)
axes[i, j].text(xcl, ycl, r"$cl = \SI{"+str(cl)+'}{ms}$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (0,1)
cl = 500
name = "tim_LR-WT_10"
directory = dir_data + "/" + name + "/"
begin = 0
till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
ut = np.copy(t)
helper.unwrap(ut)
ut -= cl

axes[i, j].plot(ut, v, color=color)
axes[i, j].text(xcl, ycl, r"$cl = \SI{"+str(cl)+'}{ms}$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (1,1)
cl = 700
name = "tim_LR-WT_12"
directory = dir_data + "/" + name + "/"
begin = 0
till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
ut = np.copy(t)
helper.unwrap(ut)
ut -= cl

axes[i, j].plot(ut, v, color=color)
axes[i, j].text(xcl, ycl, r"$cl = \SI{"+str(cl)+'}{ms}$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i, j].transAxes)
#%%
i,j = (2,1)
cl = 1000
name = "tim_LR-WT_14"
directory = dir_data + "/" + name + "/"
begin = 0
till = -1
v = np.load(directory +'voltages.npy', mmap_mode='r')[begin:till]
t = np.load(directory +'times.npy', mmap_mode='r')[begin:till]
ut = np.copy(t)
helper.unwrap(ut)
ut -= cl

axes[i, j].plot(ut, v, color=color)
axes[i, j].text(xcl, ycl, r"$cl = \SI{"+str(cl)+'}{ms}$', ha="right", va="top",
        bbox=bbox_props, transform=axes[i, j].transAxes)
#plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)