#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/bif_sev_cl1000.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8

fig = plt.figure(1, figsize=(figwidth, figheight))
fig.clear()
ax = fig.add_subplot(111)
ax.set_ylabel("apd90")
#ax2 = fig.add_subplot(212)
#ax2.set_ylabel("apd90")
#ax2.set_xlim((100,300))
ax.set_xlim((0, 1))
ax.set_ylim((0,520))

plt.ylabel("$APD_{90}$ [ms]")
plt.xlabel("Severity")

#fig.subplots_adjust(hspace=0.25)
plt.subplots_adjust(bottom=0.18, top=0.98, left=0.1, right=0.98)

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

sep = "/"
#sep = "\\"

lw = 0
skip_beats = 50
markersize = 5
marker = '.'
alpha = 0.6

name = 'bif_sev_15'
color = "b"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color, s = markersize, lw=lw, alpha=alpha)
#
name = 'bif_sev_16'
color = "r"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color, s = markersize, lw=lw, alpha=alpha)
#
name = 'bif_sev_17'
color = "b"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color, s = markersize, lw=lw, alpha=alpha)
#
name = 'bif_sev_18'
color = "r"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color, s = markersize, lw=lw, alpha=alpha)
#
name = 'bif_sev_19'
color = "b"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color, s = markersize, lw=lw, alpha=alpha)
#
name = 'bif_sev_20'
color = "r"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color, s = markersize, lw=lw, alpha=alpha)
#
#name = 'bif_sev_2'
#color = "r"
#directory = dir_data + sep + name  + sep
#sev = np.load(directory + 'sev.npy')
##v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
##v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#sev = sev[:v_s.shape[0]]
##for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
##for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
##for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
#for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,s = markersize, lw=lw, alpha=alpha)
##
##
#name = 'bif_sev_3'
#color = "magenta"
#directory = dir_data + sep + name + sep
#sev = np.load(directory + 'sev.npy')
##v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
##v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#sev = sev[:v_s.shape[0]]
##for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
##for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
##for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
#for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,s = markersize, lw=lw, alpha=alpha)
##
#name = 'bif_sev_12'
#color = "b"
#directory = dir_data + sep + name + sep
#sev = np.load(directory + 'sev.npy')
##v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
##v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#sev = sev[:v_s.shape[0]]
##for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
##for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
##for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
#for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,s = markersize, lw=lw)
#
#name = 'bif_sev_14'
#color = "b"
#directory = dir_data + sep + name + sep
#sev = np.load(directory + 'sev.npy')
##v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
##v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
#v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
#apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
#sev = sev[:v_s.shape[0]]
##for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
##for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
##for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
#for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,s = markersize, lw=lw)

name = 'bif_sev_23'
color = "r"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,s = markersize, lw=lw, alpha=alpha)

name = 'bif_sev_24'
color = "b"
directory = dir_data + sep + name + sep
sev = np.load(directory + 'sev.npy')
#v_max = np.load(directory + 'v_max.npy')[:,skip_beats:]
#v_min = np.load(directory + 'v_min.npy')[:,skip_beats:]
v_s = np.load(directory + 'v_s.npy')[:,skip_beats:skip_beats+20]
apd90 = np.load(directory + 'apd90.npy')[:,skip_beats:]
sev = sev[:v_s.shape[0]]
#for row in v_max.T: ax.scatter(sev, row, color = "r", label = "$v_{max}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_min.T: ax.scatter(sev, row, color = "g", label = "$v_{min}$", s=markersize, lw=lw, alpha=alpha)
#for row in v_s.T: ax2.scatter(sev, row, marker = marker, color = color, label = name, s = markersize, lw=lw, alpha=alpha)
for row in apd90.T[::]: ax.scatter(sev, row, marker = marker, color = color,s = markersize, lw=lw, alpha=alpha)
# remove multiple labels
#handles, labels = plt.gca().get_legend_handles_labels()
#labels, ids = np.unique(labels, return_index=True)
#handles = [handles[i] for i in ids]
#plt.legend(handles, labels, loc='lower right')
#plt.tight_layout()
# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)