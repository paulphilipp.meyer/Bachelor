#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper

interactive = False
filename = '../../figures/timesteps.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure()

name = "errcontrol_1"
model = "wt"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states

t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')

ut = np.copy(t)
helper.unwrap(ut)

skip = 1
till = 330000
every = 1
t = t[skip:till:every]
states = states[skip:till:every]
ut = ut[skip:till:every]

dt = np.diff(ut)


ax1 = fig.add_subplot(221)
ax2 = ax1.twinx()

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas' 

def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    ax.tick_params(axis='y', colors=color)
#    [t.set_color(color) for t in ax.yaxis.get_ticklines()]
    return None

base_colors = ['#007f0a', '#0a007f', '#7f0a00']
color_dt = base_colors[2]
color_v = base_colors[1]
color_y_axis(ax1, color_v)
color_y_axis(ax2, color_dt)

ax1.set_xlabel("Time [ms]")
ax2.set_ylabel("Time step [ms]", color=color_dt)
ax1.set_ylabel("Voltage [mV]", color=color_v)

ax1.set_ylim((-90,140))

bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
boxx, boxy = (1.2, 1.1)
# values ###################################################################

#figtmp = plt.figure()
#axtmp = figtmp.add_subplot(111)
#axtmp.semilogy(ut[1:], dt, c=color_dt, zorder=1)


ax2.semilogy(ut[1:], dt, c=color_dt, zorder=1)
#ax2.plot(ut[1:], dt, c=color_dt, zorder=1)

ax1.plot(ut, states[:, 0],visible=True, lw=1.5, c= color_v, zorder=5)    
ax1.text(boxx, boxy, r'A', ha="center", va="center",
        bbox=bbox_props, transform=ax1.transAxes)
ax2.minorticks_off()

#%%
#
##!/usr/bin/env python
## -*- coding: utf-8 -*-
##import sys
##sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
#import matplotlib.pyplot as plt
#import numpy as np
#import helper
#
#interactive = True
#
##dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
##dir_data = "../data"
##dir_data = "/scratch/paulphilipp.meyer/brugada"
#dir_data = "/scratch16/pmeyer/brugada"
##dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"
#
#helper.initLatex()
#if interactive:
#    plt.ion()
#else:
#    plt.ioff()
#figwidth_pt = 418.25555
#inches_per_pt = 1.0/72.27
#golden_mean = (np.sqrt(5.0)-1.0)/2.0 
#figwidth = figwidth_pt*inches_per_pt
#figheight = figwidth*golden_mean*.8


name = "pol_1"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states

t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')

ut = np.copy(t)
helper.unwrap(ut)

skip = 200
till = 450
every = 1
t = t[skip:till:every]
states = states[skip:till:every]
ut = ut[skip:till:every]

dt = np.diff(ut)


#fig = plt.figure()
ax1 = fig.add_subplot(222)
ax2 = ax1.twinx()

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas' 

def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    ax.tick_params(axis='y', colors=color)
#    [t.set_color(color) for t in ax.yaxis.get_ticklines()]
    return None

#color_dt = 'r'
#color_v = 'b'
color_y_axis(ax1, color_v)
color_y_axis(ax2, color_dt)

ax1.set_xlabel("Time [ms]")
ax2.set_ylabel("Time step [ms]", color=color_dt)
ax1.set_ylabel("System variables", color=color_v)

ax1.set_ylim((-0.25, 2))
ax2.set_ylim((0, 0.225))
#ax2.set_yticks((0.01, 0.02, 0.04, 0.09, 0.1, 1))
#ax2.set_yticklabels(["0.01", "0.02", "0.04", "0.09", "0.1", "1"])

# values ###################################################################

ax1.plot(ut, helper.normalized(states[:, 0]),visible=True, lw=1, c= color_v, zorder=5)   
ax1.plot(ut, helper.normalized(states[:, 1]),visible=True, lw=1, c= color_v, zorder=5)   
ax1.text(boxx, boxy, r'B', ha="center", va="center",
        bbox=bbox_props, transform=ax1.transAxes) 

#ax2.semilogy(ut[1:], dt, c=color_dt, zorder=1)
ax2.plot(ut[1:], dt, c=color_dt, zorder=1)
#ax2.minorticks_off()
#%%

##!/usr/bin/env python
## -*- coding: utf-8 -*-
##import sys
##sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
#import matplotlib.pyplot as plt
#import numpy as np
#import helper
#
#interactive = True
#
##dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
##dir_data = "../data"
##dir_data = "/scratch/paulphilipp.meyer/brugada"
#dir_data = "/scratch16/pmeyer/brugada"
##dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"
#
#helper.initLatex()
#if interactive:
#    plt.ion()
#else:
#    plt.ioff()
#figwidth_pt = 418.25555
#inches_per_pt = 1.0/72.27
#golden_mean = (np.sqrt(5.0)-1.0)/2.0 
#figwidth = figwidth_pt*inches_per_pt
#figheight = figwidth*golden_mean*.8


name = "einschwingen_wt_2"
model = "wt"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states

t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')

ut = np.copy(t)
helper.unwrap(ut)

skip = 83000
till = 103000
every = 1
t = t[skip:till:every]
states = states[skip:till:every]
ut = ut[skip:till:every]

dt = np.diff(ut)

#fig = plt.figure()
ax1 = fig.add_subplot(223)
ax2 = ax1.twinx()

ax1.set_zorder(ax2.get_zorder()+1) # put ax in front of ax2
ax1.patch.set_visible(False) # hide the 'canvas' 

def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    ax.tick_params(axis='y', colors=color)
#    [t.set_color(color) for t in ax.yaxis.get_ticklines()]
    return None

#color_dt = 'r'
#color_v = 'b'
color_y_axis(ax1, color_v)
color_y_axis(ax2, color_dt)

ax1.set_xlabel("Time [ms]")
ax2.set_ylabel("Time step [ms]", color=color_dt)
ax1.set_ylabel("Voltage [mV]", color=color_v)

ax1.set_ylim((-95,140))

# values ###################################################################

ax2.semilogy(ut[1:], dt, c=color_dt, zorder=1)
ax1.plot(ut, states[:, 0],visible=True, lw=1.5, c= color_v, zorder=5)  
ax1.text(boxx, boxy, r'C', ha="center", va="center",
        bbox=bbox_props, transform=ax1.transAxes)  

ax2.minorticks_off()
#plt.tight_layout()
#%%
fig.subplots_adjust(hspace=0.4, wspace=0.6)
fig.subplots_adjust(bottom=0.1, right=0.915, left=0.1, top=0.93)
fig.show()
#%%
# save / display plot
plt.savefig(filename)
#os.system("exo-open "+filename+" &")
if not interactive:
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
    plt.close()