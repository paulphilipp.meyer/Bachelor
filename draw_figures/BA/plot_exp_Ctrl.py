#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import sys
#sys.path.append("/teutates/home/pmeyer/brugada_code/panalyse")
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

interactive = False
filename = r'../../figures/tim_exp_Ctrl.png'

#dir_data = "/home/paul/Uni/6 Bachelorarbeit/data"
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"
#dir_data = r"C:\Users\Sab\Uni\Module\6 Bachelorarbeit\data"

helper.initLatex()
if interactive:
    plt.ion()
else:
    plt.ioff()
figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*0.8

#fig, axes = plt.subplots(3, 2, sharex=True, sharey=True, figsize=(figwidth, figheight))
plt.close(1)
fig = plt.figure(1, figsize=(figwidth, figheight))
fig.clf()


axes=np.empty((3,2), dtype=object)
axes[0,0] = fig.add_subplot(3, 2, 1)
axes[1,0] = fig.add_subplot(3, 2, 3, sharex = axes[0,0], sharey = axes[0,0])
axes[2,0] = fig.add_subplot(3, 2, 5, sharex = axes[0,0], sharey = axes[0,0])

axes[0,1] = fig.add_subplot(3, 2, 2, sharex = axes[0,0], sharey = axes[0,0])
axes[1,1] = fig.add_subplot(3, 2, 4, sharex = axes[0,1], sharey = axes[0,0])
axes[2,1] = fig.add_subplot(3, 2, 6, sharex = axes[0,1], sharey = axes[0,0])

#plt.setp([a.get_xticklabels() for a in axes[:,-1]], visible=False)
axes[0][0].set_ylim((-70,48))
axes[0][0].set_xlim((-50,700))
axes[0][1].set_xlim((-50,950))


for ax in axes[:-1, :].flat:
    plt.setp(ax.get_xticklabels(), visible=False)
for ax in axes[:,1].flat:
    plt.setp(ax.get_yticklabels(), visible=False)
    
#    ax.axis('off')
#    ax.get_xaxis().set_ticks([])
#    ax.get_yaxis().set_ticks([])
fig.subplots_adjust(hspace=0.1, wspace=0.05)
plt.subplots_adjust(top=0.99, bottom=0.18, left = 0.11, right = 1.02)

axes[-1][0].set_xlabel("Time [ms]", position=(1,0.1))
axes[1][0].set_ylabel("Voltage [mV]")


bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
wbox_props = dict(boxstyle="round", fc="w", ec='1', alpha=0.9)
boxx, boxy = (0.97, 0.9)
cls = ['1666', '1666', '1250','1250', '1000', '1000']
letters = ['A','D','B','E','C','F']
for i, ax in zip(range(len(axes.flat)), axes.flat):
    ax.text(boxx, boxy, letters[i], ha="right", va="top",
            bbox=bbox_props, transform=ax.transAxes)
    ax.text(boxx, boxy-0.35, r"$cl=\SI{"+str(cls[i])+r'}{ms}$', ha="right", va="top",
            bbox=wbox_props, transform=ax.transAxes)
    
# from https://stackoverflow.com/questions/8342549/matplotlib-add-colorbar-to-a-sequence-of-line-plots
plt.figure(2)
Z = [[0,0],[0,0]]
levels = np.arange(0.5,21.5, 1)
CS3 = plt.contourf(Z, levels, cmap=plt.get_cmap('plasma_r'))
plt.clf()
#cbar = fig.colorbar()
plt.close(2)
cbar = plt.colorbar(CS3, ax=fig.get_axes())
cbar.set_ticks([1, 5, 10, 15, 20])
cbar.set_label('Cycle number $n$', rotation=90)

import pickle

#dir_data = '/home/paul/Schreibtisch/Michael Stauske'
dir_data = '/data.bmp/pmeyer/Bachelor/stauske_data/pickle_dicts'

#%%
#for itr in range(len(d['traces'])):
#    print(str(itr)+', ' + str(len(d['traces'][itr])))
#print(d['comment'])
#%%
name = '/Ctrl2/20150316-07.pkl'
d = pickle.load( open( dir_data + name, "rb" ), encoding='latin1' )
dt = d['dt']
cls = []
unpaced = d['traces'][0]

cls.append('1666')
paced1 = np.vstack(d['traces'][1:21])
num_points1 = paced1.shape[1]
times1 = np.zeros(num_points1)
for i in range(num_points1):
    times1[i] = i*dt
   
cls.append('1250')
paced2 = np.vstack(d['traces'][21:41])
num_points2 = paced2.shape[1]
times2 = np.zeros(num_points2)
for i in range(num_points2):
    times2[i] = i*dt
    
cls.append('1000')
paced3 = np.vstack(d['traces'][41:])
num_points3 = paced3.shape[1]
times3 = np.zeros(num_points3)
for i in range(num_points3):
    times3[i] = i*dt

#%%
name = '/Ctrl2/20150316-04.pkl'

d = pickle.load( open( dir_data + name, "rb" ), encoding='latin1' )
dt = d['dt']
unpaced = d['traces'][0]


cls.append('1666')
paced4 = np.vstack(d['traces'][1:21])
num_points4 = paced4.shape[1]
times4 = np.zeros(num_points4)
for i in range(num_points4):
    times4[i] = i*dt

cls.append('1250')
paced5 = np.vstack(d['traces'][21:41])
num_points5 = paced5.shape[1]
times5 = np.zeros(num_points5)
for i in range(num_points5):
    times5[i] = i*dt

cls.append('1000') 
paced6 = np.vstack(d['traces'][41:])
num_points6 = paced6.shape[1]
times6 = np.zeros(num_points6)
for i in range(num_points3):
    times6[i] = i*dt


#fig = plt.figure()
#ax = plt.subplot(111)

lw = 0.7
#colors = plt.get_cmap('hot')(np.linspace(1,0,len(paced1)))

num = 0
i, j = (0, 0)
times = times1
data = paced1
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for ap, color in zip(data, colors):
    axes[i,j].plot(times, ap, color=color, lw =lw)

num = 1
i, j = (1, 0)
times = times2
data = paced2
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for ap, color in zip(data, colors):
    axes[i,j].plot(times, ap, color=color, lw =lw)

num = 2
i, j = (2, 0)
times = times3
data = paced3
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for ap, color in zip(data, colors):
    axes[i,j].plot(times, ap, color=color, lw =lw)


num = 3
i, j = (0, 1)
times = times4
data = paced4
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for ap, color in zip(data, colors):
    axes[i,j].plot(times, ap, color=color, lw =lw)

num = 4
i, j = (1, 1)
times = times5
data = paced5
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for ap, color in zip(data, colors):
    axes[i,j].plot(times, ap, color=color, lw =lw)

num = 5
i, j = (2, 1)
times = times6
data = paced6
colors = plt.get_cmap('plasma')(np.linspace(1,0,len(data)))
for ap, color in zip(data, colors):
    axes[i,j].plot(times, ap, color=color, lw =lw)


#for tim in (paced1, paced2, paced3, paced4, paced5, paced6):
#    print(tim.shape)

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)