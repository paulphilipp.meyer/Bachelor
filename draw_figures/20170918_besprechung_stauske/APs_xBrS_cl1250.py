#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import normalized, unwrap
from subprocess import call
#helper.initLatex()

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"
name = "tim_meas_5
directory = data_dir + sep + name + sep

fig = plt.figure()
ax = fig.add_subplot(111)


tload = np.load(directory + "1_times.npy", mmap_mode='r')
vload = np.load(directory + "1_voltages.npy", mmap_mode='r')
every = 10
till = -1
t = tload[:till:every]
voltages = vload[:till:every]

#ut = t.copy()
#unwrap(ut)

fig = plt.figure()
ax = fig.add_subplot(111)
plt.title('xBrS - cl1250 (tim_meas_5)')
ax.set_xlabel('time [ms]')
ax.set_ylabel('membrane potential [mV]')
plt.plot(t, voltages)