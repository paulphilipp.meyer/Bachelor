#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from helper import normalized, unwrap
from subprocess import call
#helper.initLatex()

#data_dir = "/home/paul/Uni/6 Bachelorarbeit/data"
data_dir = '/scratch16/pmeyer/brugada'
sep = "/"

#fig = plt.figure(figsize=(8.27,11.69))
fig = plt.figure(figsize=(11.69,8.27)) # A4 landscape


name = "tim_meas_3"
directory = data_dir + sep + name + sep
tload = np.load(directory + "times.npy", mmap_mode='r')
vload = np.load(directory + "voltages.npy", mmap_mode='r')
every = 10
till = 10000000
t = tload[:till:every]
voltages = vload[:till:every]
#ut = t.copy()
#unwrap(ut)
ax = fig.add_subplot(221)
plt.title('Xia-BrS - cl1000 (tim_meas_3)')
ax.set_xlabel('time [ms]')
ax.set_ylabel('membrane potential [mV]')
plt.plot(t, voltages)

#%%
name = "tim_meas_5"
directory = data_dir + sep + name + sep


tload = np.load(directory + "1_times.npy", mmap_mode='r')
vload = np.load(directory + "1_voltages.npy", mmap_mode='r')
every = 10
till = -1
t = tload[:till:every]
voltages = vload[:till:every]
#ut = t.copy()
#unwrap(ut)
ax = fig.add_subplot(222)
#fig = plt.figure()
plt.title('Xia-BrS - cl1250 (tim_meas_5)')
ax.set_xlabel('time [ms]')
ax.set_ylabel('membrane potential [mV]')
plt.plot(t, voltages)

plt.tight_layout()