#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, ind2name, normalized

#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

name = "test_na_da_1"
model = "xiaepi"

directory = dir_data + "/" + name
#data = np.loadtxt(directory +'/states.dat', unpack = True) # states


t = np.load(directory+'/times.npy')
states = np.load(directory+'/states.npy')

skip_first = 0
till = -1
every = 1
t = t[skip_first:till:every]
states = states[skip_first:till:every]

#skip_first = 1000000


ut = np.copy(t)
unwrap(ut)
#%%
from helper import get_state_names
#model = "xiaepi"

state_names = get_state_names(model)
assert len(state_names) == states.shape[1]

d = dict(zip(state_names, states))

if model == "xiaepi":

    assert state_names == ["Voltage", "Ca_i_total", "Ca_SR_total", "Na_i", "K_i", "m_gate", "h_gate", "j_gate", "X_r1_gate", "X_r2_gate", "X_s_gate", "r_gate", "s_gate", "d_gate", "f_gate", "F_Ca", "g_gate", "mL_gate", "hL_gate"]
    I_stim = 0
    Ko          = 5.4
    Cao         = 2.0
    Nao         = 140.0
    Vc          = 0.016404
    Vsr         = 0.001094
    Bufc        = 0.15
    Kbufc       = 0.001
    Bufsr       = 10.0
    Kbufsr      = 0.3
    taufca      = 2.0
    taug        = 2.0
    Vmaxup      = 0.000425
    Kup         = 0.00025
    R           = 8314.472
    F           = 96485.3415
    T           = 310.0
    RTONF       = 26.7137606597
    CAPACITANCE = 0.185
    Gkr         = 0.096
    pKNa        = 0.03
    GK1         = 5.405
    GNa         = 14.838
    GbNa        = 0.00029
    KmK         = 1.0
    KmNa        = 40.0
    knak        = 1.362
    GCaL        = 0.000175
    GbCa        = 0.000592
    knaca       = 1000
    KmNai       = 12.3 ## 87.5 <- tentusscher
    KmCa        = 1.38
    ksat        = 0.27 ## 0.1 <- tentusscher
    n           = 0.35
    GpCa        = 0.825
    KpCa        = 0.0005
    GpK         = 0.0146
    FONRT       = 0.0374338908227
    inverseVcF2 = 0.000315906749849
    inverseVcF  = 0.000631813499697
    Kupsquare   = 6.25e-08
    eta         = 0.35
    KmNao       = 87.5
    KmCai       = 0.0036
    KmCao       = 1.3
    KmCaact     = 1.25e-4
    if model=="xiaepi":
        	GNaL    = 0.0065
        	numax   = 5.0
        	Gto     = 0.294
        	Gks     = 0.245
    elif model=="xiaendo":
        	GNaL    = 0.0065
        	numax   = 3.9
        	Gto     = 0.053
        	Gks     = 0.225
    elif model=="xiamcell":
        	GNaL    = 0.0095
        	numax   = 4.3
        	Gks     = 0.062
        	Gto     = 0.238
    Nao_cubed   = Nao*Nao*Nao
    KmNao_cubed = KmNao*KmNao*KmNao
    KmNai_cubed = KmNai*KmNai*KmNai

    Voltage     = states[:,0]
    Ca_i_total  = states[:,1]
    Ca_SR_total = states[:,2]
    Na_i        = states[:,3]
    K_i         = states[:,4]
    m_gate      = states[:,5]
    h_gate      = states[:,6]
    j_gate      = states[:,7]
    X_r1_gate   = states[:,8]
    X_r2_gate   = states[:,9]
    X_s_gate    = states[:,10]
    r_gate      = states[:,11]
    s_gate      = states[:,12]
    d_gate      = states[:,13]
    f_gate      = states[:,14]
    F_Ca        = states[:,15]
    g_gate      = states[:,16]
    mL_gate     = states[:,17]
    hL_gate     = states[:,18]

    bc = Bufc - Ca_i_total + Kbufc
    cc = Kbufc * Ca_i_total
    Ca_i_free = ( np.sqrt(bc*bc+4*cc) - bc) / 2
    bjsr = Bufsr - Ca_SR_total + Kbufsr
    cjsr = Kbufsr * Ca_SR_total
    Ca_SR_free = ( np.sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2
    Ek = RTONF * np.log( Ko / K_i )
    Ena = RTONF * np.log( Nao / Na_i )
    Eks = RTONF * np.log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i))
    Eca = 0.5 * RTONF * np.log( Cao / Ca_i_free )
    Ak1 = 0.1 / ( 1.0 + np.exp( 0.06*(Voltage-Ek-200) ) )
    Bk1 = ( 3. * np.exp( 0.0002*(Voltage-Ek+100) ) + np.exp(0.1*(Voltage-Ek-10)))  /  (1. + np.exp(-0.5*(Voltage-Ek)))
    rec_iK1 = Ak1 / ( Ak1+Bk1 )
    rec_iNaK = 1./( 1.+0.1245*np.exp(-0.1*Voltage*FONRT)+0.0353*np.exp(-Voltage*FONRT) )
    rec_ipK = 1. / (1.+np.exp( (25-Voltage)/5.98) )
    
    # currents ################################################# 
    INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena)
    ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( np.exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( np.exp(2*Voltage*FONRT)-1.)
    Ito = Gto*r_gate*s_gate*(Voltage-Ek)
    IKr = Gkr*np.sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek)
    IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks)
    IK1 = GK1*rec_iK1*(Voltage-Ek)
    # INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*np.exp((n-1)*Voltage*FONRT)))*(np.exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-np.exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5)
    INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK
    IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free)
    IpK = GpK*rec_ipK*(Voltage-Ek)
    IbNa = GbNa * (Voltage-Ena)
    IbCa = GbCa * (Voltage-Eca)
    INaL = GNaL * mL_gate*mL_gate*mL_gate * hL_gate * ( Voltage-Ena )
    Na_i_cubed = Na_i*Na_i*Na_i
    INaCa = 1 / ( 1 + KmCaact*KmCaact/(2.25*Ca_i_free*Ca_i_free)) *        \
    		numax*( Na_i_cubed*Cao*np.exp(eta*Voltage*FONRT) - 1.5*Nao_cubed*Ca_i_free*np.exp((eta-1)*Voltage*FONRT )) / ( \
    			( 1+ksat*np.exp((eta-1)*Voltage*FONRT) ) * (            \
    				KmCao*Na_i_cubed +                               \
    				1.5*KmNao_cubed*Ca_i_free +                      \
    				KmNai_cubed*Cao*(1+1.5*Ca_i_free/KmCai) +        \
    				KmCai*Nao_cubed*(1+Na_i_cubed/KmNai_cubed) +     \
    				Na_i_cubed*Cao +                                 \
    				1.5*Nao_cubed*Ca_i_free                          \
    			)                                                    \
    		)
    
    sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK + INaL - I_stim
    Ileak = 0.00008 * (Ca_SR_free - Ca_i_free)
    SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) )
    CaSRsquare = Ca_SR_free * Ca_SR_free
    A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232
    Irel = A * d_gate * g_gate
    
    currents =       [ INa ,  INaL,   ICaL ,  Ito ,  IKr ,  IKs ,  IK1 ,  INaCa ,  INaK ,  IpCa ,  IpK ,  IbNa ,  IbCa]
    currents_names = ["INa", "INaL", "ICaL", "Ito", "IKr", "IKs", "IK1", "INaCa", "INaK", "IpCa", "IpK", "IbNa", "IbCa"]

#%%
#'''     Ionenkonzentrationen
#'''
#fig = plt.figure()
#ax1 = fig.add_subplot(221)
#till = -30000
#ax1.plot(ut, normalized(Voltage), c='orange')
##ax1.plot(ut[:till], normalized(Ca_i_free[:till]), c='g', label = "Ca^{2+}")
#ax1.plot(ut[:till], (Ca_i_free[:till]), c='g', label = "Ca^{2+}")
#ax1.plot(ut[:till], (K_i[:till]), c='r')
#ax1.plot(ut[:till], (Na_i[:till]), c='b')
#
##ax1.plot(ut, pick_v, 'x', c='r')
##ax1.plot(ut, INa, c='orange')
##ax1.plot(ut, pick_INa, 'x', c='r')
#ax1.set_xlabel("time [ms]")
#ax1.set_ylabel("")
##plt.legend()
#fig.tight_layout()
#%%
#'''     Alle Na-Stroeme
#'''
#fig = plt.figure()
#ax1 = fig.add_subplot(111)
#ax2 = ax1.twinx()
#def color_y_axis(ax, color):
#    """Color your axes."""
#    for t in ax.get_yticklabels():
#        t.set_color(color)
#    return None
##ax.plot(ut)
##ax.plot(ut[skip_first:], (v[skip_first:]), label = "voltage")
##for i in range(0, states.shape[1]):
###for i in black:
##for i in (0,5, 6, 7):
#ax1.plot(ut, Voltage, lw=3, c='r')
#color_y_axis(ax1, 'r')
#ax1.set_ylabel('membrane voltage [mV]')
#ax1.set_xlabel("time [ms]")
#
#INa_total = INa+INaL+IbNa+3*INaCa+3*INaK
#
#ax2.plot(ut, INa, c='b', label='INa')
#ax2.plot(ut, INaL, c='g', label='INaL')
#ax2.plot(ut, IbNa, c='orange', label='IbNa')
#ax2.plot(ut, 3*INaCa, c='black', label='INaCa')
#ax2.plot(ut, 3*INaK, c='purple', label='INaK')
#
#ax2.plot(ut, INa_total, c='magenta', label='INa total')
#
#color_y_axis(ax2, 'b')
#ax2.set_ylabel('currents')
#
##ax1.plot(ut, t)
#
#plt.title(name)
##plt.xlim((-10,20))
##plt.ylim((-10,20))
#plt.xlabel("time [ms]")
#plt.legend()
#fig.tight_layout()
##plt.show()
#%%
'''   visualize picking
'''
fig = plt.figure(figsize=(11.69,8.27))
ax1 = fig.add_subplot(221)
ax2 = ax1.twinx()
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None
#ax.plot(ut)
#ax.plot(ut[skip_first:], (v[skip_first:]), label = "voltage")
#for i in range(0, states.shape[1]):
##for i in black:
#for i in (0,5, 6, 7):
ax1.plot(ut, Voltage, lw=3, c='r')
color_y_axis(ax1, 'r')
ax1.set_ylabel('membrane voltage [mV]')
ax1.set_xlabel("time [ms]")

ax2.plot(ut, INa+INaL, c='b')

color_y_axis(ax2, 'b')
ax2.set_ylabel('INa+INaL')


pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INa = np.ma.masked_where(np.ma.getmask(pick_ut), INa)

ax2.plot(ut, pick_INa, 'x')

plt.title('Na-Currents - Xia-WT')
#plt.xlim((-10,20))
#plt.ylim((-10,20))
plt.xlabel("time [ms]")
#%%
ax1 = fig.add_subplot(223)
#np.ma.getmask(pick_t)
INaNaL = INa+INaL
pick_ut = np.ma.masked_where(t<2999.99, ut)
pick_v = np.ma.masked_where(np.ma.getmask(pick_ut), Voltage)
pick_INaNaL = np.ma.masked_where(np.ma.getmask(pick_ut), INaNaL)
#pick_INa = INa * mask

#ax1.plot(ut, Voltage, c='orange')
#ax1.plot(ut, pick_v, 'x', c='r')
#ax1.plot(ut, INa, c='orange')
#ax1.plot(ut, pick_INa, 'x', c='r')
ax1.plot(pick_v, pick_INaNaL, 'x')
ax1.set_xlabel("membrane potential [mV]")
ax1.set_ylabel("INa + INaL")
fig.tight_layout()
