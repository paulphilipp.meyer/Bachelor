#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False # switch for reading data from plot interactively

filename = r'../figures/bif_wt.png'

prefix = 'bif_wt_3'
#prefix = 'bif_wt_6'

#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = "/scratch16/pmeyer/brugada"

# begin imports ############################################

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import subprocess
import helper
# end imports ############################################



skip_beats = 50
stepwidth = 1
clstepwidth = 1
directory = dir_data + "/" + prefix + "/"

cl = np.load(directory + 'cl.npy')[::clstepwidth]
v_max = np.load(directory + 'v_max.npy')[::clstepwidth,skip_beats::stepwidth]
v_min = np.load(directory + 'v_min.npy')[::clstepwidth,skip_beats::stepwidth]
v_s = np.load(directory + 'v_s.npy')[::clstepwidth,skip_beats::stepwidth]
apd90 = np.load(directory + 'apd90.npy')[::clstepwidth,skip_beats::stepwidth]

# manipulate data #############################################################

for darray in (v_max, v_min, v_s, apd90):
    helper.normalize(darray)



# plotting ####################################################################
helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

figwidth_pt = 418.25555
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean*.8
fig = plt.figure(figsize=(figwidth, figheight))
ax = fig.add_subplot(111)

plt.xlabel(r"Excitation period length [ms]")
plt.ylabel(r"Normalized AP-measure")
xmin, xmax = (150, 350)
plt.xlim((xmin, xmax))
ymin, ymax = (0, 1)
plt.ylim((ymin, ymax))

markersize = 1.5
#for row in v_max.T: ax.scatter(cl, row, color = "r", label = "v_max", s=markersize, edgecolors='none')
#for row in v_min.T: ax.scatter(cl, row, color = "g", label = "v_min", s=markersize, edgecolors='none')
#for row in v_s.T: ax.scatter(cl, row, color = "b", label = "v_s", s=markersize, edgecolors='none')
#for row in apd90.T: ax.scatter(cl, row, color = "orange",label = "APD90", s=markersize, edgecolors='none')

plt.plot(np.tile(cl, (v_max.shape[1], 1)).T, v_max, '.', ms = markersize, mew = 0, color = "r")
plt.plot(np.tile(cl, (v_min.shape[1], 1)).T, v_min, '.', ms = markersize, mew = 0, color = "g")
plt.plot(np.tile(cl, (v_s.shape[1], 1)).T, v_s, '.', ms = markersize, mew = 0, color = "b")
plt.plot(np.tile(cl, (apd90.shape[1], 1)).T, apd90, '.', ms = markersize, mew = 0, color = "orange")


# remove multiple labels
v_max_line = mpl.lines.Line2D([], [], color='r', label = "$v_{max}$")
v_min_line = mpl.lines.Line2D([], [], color='g', label = "$v_{min}$")
v_s_line = mpl.lines.Line2D([], [], color='b', label = "$v_{s}$")
apd90_line = mpl.lines.Line2D([], [], color='orange', label = "$APD90$")
leg = plt.legend(handles=[v_max_line, v_min_line, v_s_line, apd90_line], loc = (.765,.3), handletextpad=0.1, labelspacing=0.2)
leg.get_frame().set_linewidth(.2)

#handles, labels = plt.gca().get_legend_handles_labels()
#labels, ids = np.unique(labels, return_index=True)
#handles = [handles[i] for i in ids]
#plt.legend(handles, labels)

plt.grid()
plt.tight_layout(0)

# save / display plot
if not interactive:
    print("saving...")
    plt.savefig(filename, rasterized = True, dpi = 400)
    print("opening...")
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)
