#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False # switch for reading data from plot interactively

filename = r'../images/ap_measures.pdf'


import matplotlib as mpl
pgf_with_pdflatex = {
    "pgf.texsystem": "pdflatex",
    "pgf.preamble": [
         r"\usepackage[utf8x]{inputenc}",
         r"\usepackage[T1]{fontenc}",
         r"\usepackage{cmbright}",
         ]
}
mpl.rcParams.update(pgf_with_pdflatex)

import matplotlib.pyplot as plt
if not interactive:
    plt.ioff()


import numpy as np
from helper import unwrap, normalize, initLatex
import subprocess

initLatex()
#dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"
dir_data = '/scratch16/pmeyer/brugada'

prefix = "tim_s_wt_0"

directory = dir_data + "/" + prefix
data = np.loadtxt(directory +'/states.dat', unpack = True) # states

#data = data[:,28800:]

t = data[0]
v = data[1]

skip_first = 0

ut = np.copy(t)
unwrap(ut)

figwidth_pt = 418.25555*0.5
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean
fig = plt.figure(figsize=(figwidth, figheight))

ax = fig.add_subplot(111)
#ax.plot(ut)
ax.plot(ut[skip_first:]-250, (v[skip_first:]), label = "voltage", lw=1.5)

black =(7,14,21,28)
#for i in range(2, data.shape[0]):
#for i in black:
for i in ():
    ax.plot(ut[skip_first:], (np.abs((data[i, skip_first:]))), label = ind2name(i-1, model = "wt"))



# labels / ticks/ legend
plt.xlabel(r"Time [ms]")
plt.ylabel("Voltage [mV]")
plt.grid()
if not interactive:
    plt.xticks((-50, 0, 50, 100, 150), (-50, 0, "", 100, 150))
    plt.yticks((-100, -60, -20, 20, 60), (-100, -60, -20, "", 60))
xmin, xmax = (-30, 130)
plt.xlim((xmin, xmax))
ymin, ymax = (-100, 60)
plt.ylim((ymin, ymax))
xticksmargin = 8
yticksmargin = 3

# annotations

# apd90
plt.plot([0.1, 101.8], [-79.7,]*2, 'k-', lw=1)
#plt.text(5.7, -88, r"$APD90$")
plt.text(5.7, -76, r"$APD90$")

# v_max
v_max = 37.985
plt.plot([-50, 4.4], [v_max,]*2, 'k--', lw=1)
plt.text(xmin-yticksmargin, v_max, r"$V_{max}$", va="center", ha = "right")

#v_s
t_s = 50
v_s = 6.766
plt.plot([t_s,]*2, [-100, v_s], 'k--', lw=1)
plt.plot([-50, t_s], [v_s]*2, 'k--', lw=1)
plt.text(t_s, ymin-xticksmargin, r"$t_s$", ha = "center", va = "top")
plt.text(xmin-yticksmargin, v_s, r"$V_s$", va="center", ha = "right")


plt.tight_layout(0)


# save / display plot
plt.savefig(filename)
#os.system("exo-open "+filename+" &")
if not interactive:
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)

