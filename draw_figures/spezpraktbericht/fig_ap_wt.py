#!/usr/bin/env python
# -*- coding: utf-8 -*-

interactive = False # switch for reading data from plot interactively

filename = r'../images/ap_wt.pdf'

prefix = "tim_s_wt_0"

dir_data = "../data"
#dir_data = "/scratch/paulphilipp.meyer/brugada"

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import helper
import subprocess

helper.initLatex()

if interactive:
    plt.ion()
else:
    plt.ioff()

directory = dir_data + "/" + prefix
data = np.loadtxt(directory +'/states.dat', unpack = True) # states

#data = data[:,28800:]

t = data[0]
v = data[1]

skip_first = 0

ut = np.copy(t)
helper.unwrap(ut)

figwidth_pt = 418.25555*0.5
inches_per_pt = 1.0/72.27
golden_mean = (np.sqrt(5.0)-1.0)/2.0 
figwidth = figwidth_pt*inches_per_pt
figheight = figwidth*golden_mean
fig = plt.figure(figsize=(figwidth, figheight))

ax = fig.add_subplot(111)
#ax.plot(ut)
ax.plot(ut[skip_first:]-250, (v[skip_first:]), label = "voltage", lw=1)

black =(7,14,21,28)
#for i in range(2, data.shape[0]):
#for i in black:
#for i in ():
#    ax.plot(ut[skip_first:], (np.abs((data[i, skip_first:]))), label = ind2name(i-1, model = "wt"))



# labels / ticks/ legend
plt.xlabel(r"Time [ms]")
plt.ylabel(r"Voltage [mV]")
plt.grid()
#if not interactive:
#    plt.xticks((-50, 0, 50, 100, 150), (-50, 0, "", 100, 150))
#    plt.yticks((-100, -80, -60, -40, -20, 0, 20, 40, 60), (-100, -80, -60, -40, -20, "", 20, "", 60))
xmin, xmax = (-50, 450)
plt.xlim((xmin, xmax))
ymin, ymax = (-100, 60)
plt.ylim((ymin, ymax))
xticksmargin = 3
yticksmargin = 2


plt.tight_layout(0)


# save / display plot
plt.savefig(filename)
if not interactive:
    subprocess.Popen(["exo-open "+filename+" &"],shell=True)

########################################################
########################################################
########################################################
########################################################
########################################################
"""

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
from helper import unwrap, ind2name, normalized
#dir_data = "../data"
dir_data = "/scratch/paulphilipp.meyer/brugada"

prefix = "tim_s_wt_0"

directory = dir_data + "/" + prefix
data = np.loadtxt(directory +'/states.dat', unpack = True) # states

#data = data[:,28800:]


t = data[0]
v = data[1]

#skip_first = 1000000
skip_first = 0

ut = np.copy(t)
unwrap(ut)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


fig = plt.figure()
ax = fig.add_subplot(111)
#ax.plot(ut)
ax.plot(ut[skip_first:]-200, (v[skip_first:]), label = "voltage")

blue = (8,15,22,29)
black =(7,14,21,28)
#for i in range(2, data.shape[0]):
#for i in black:
for i in ():
    ax.plot(ut[skip_first:], (np.abs((data[i, skip_first:]))), label = ind2name(i-1, model = "wt"))

#for i, x in enumerate(data[:,.1]):
#    plt.plot(x, y)
#    plt.text(x[-1], y[-1], 'sample {i}'.format(i=i))
#plt.title("cl: "+prefix)
plt.xlabel("time [ms]")
plt.ylabel("membrane potential [mV]")
plt.grid()
plt.xticks(np.arange(0,500,100))
plt.xlim((0, 500))
plt.ylim((-100,60))
#plt.legend()

# annotations

# apd90
plt.plot([50.1, 151.8], [-79.7,]*2, 'k-', lw=2)

# v_max
plt.plot([0, 54.4], [37.985,]*2, 'k-', lw=2)

plt.show()
"""
