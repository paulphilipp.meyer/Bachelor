#include "LRModel.h"
#include "LRIntegrate.h"
#include <limits.h>
#include "cnpy.h"

using namespace std;
using namespace boost::numeric::odeint;
namespace po = boost::program_options;


int main(int argc, char* argv[]){

	// handle commandline options
	po::options_description desc("Allowed options");
	desc.add_options()
    	("help", "produce help message")
		("cl", po::value<double>(), "(250) stimulation cycle length")
    	("N_beats", po::value<int>(), "(5) number of simulated beats")
    	("skip_beats", po::value<int>(), "(0) number of skipped beats")
		("dir_data", po::value<std::string>(), "() directory for data")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);


	std::string dir_data;

	unsigned int skip_beats = 0;
	int N_beats = 5;
	double stim_dur = 0.5;
	double cl = 250;
	double dt = 0.01;


	if (vm.count("help")) {
	    std::cout << desc << "\n";
	    return 1;
	}
	if (vm.count("dir_data")) {
		dir_data = vm["dir_data"].as< std::string >();
	} else {
		std::cerr << "#######################################\n"
			         "#  no data directory given! aborted.  #\n"
			         "#######################################\n";
		return 1;
	}
	if (vm.count("N_beats")) {
		N_beats = vm["N_beats"].as< int >();
	}
	if (vm.count("cl")) {
		cl = vm["cl"].as< double >();
	}
	if (vm.count("skip_beats")) {
		skip_beats = vm["skip_beats"].as< int >();
	}


	state_type_wt state;
	init_sim(state);
	runge_kutta_fehlberg78 < state_type_wt > rk78;

	double abs_err = 1e-8;
	double rel_err = abs_err;

	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );

	std::vector< double > voltages;
	std::vector< double > times;
	store_voltage observer = store_voltage(voltages, times);

	int curr_beat = 0;
	while(curr_beat < skip_beats){

		std::cout << "Skipping " << skip_beats << " beats...\n";
		I_stim = 80.0;
		integrate_const(rk78, calc_odeint_wt  , state , 0.0 , stim_dur , dt);
		I_stim = 0.0;
		integrate_adaptive( stepper_c, calc_odeint_wt  , state , stim_dur , cl , dt);

		curr_beat++;
	} // end skip_beat loop

	curr_beat = 0;
	while(curr_beat < N_beats){
		curr_beat++;

		std::cout << "\tAnalysing beat number " << curr_beat << "\n";
		I_stim = 80.0;
		integrate_const(rk78, calc_odeint_wt  , state , 0.0 , stim_dur , dt, observer );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, calc_odeint_wt  , state , stim_dur , cl , dt, observer );


	} // end beat loop


// .npy output
	assert(UINT_MAX > times.size());
	unsigned int integration_steps = times.size();
	const unsigned int shape_t[] = {integration_steps, };
	cnpy::npy_save(dir_data + "/times.npy", &times[0], shape_t, 1, "w");
	cnpy::npy_save(dir_data + "/voltages.npy", &voltages[0], shape_t, 1, "w");



} //end main
