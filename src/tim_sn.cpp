/* options:
 * 1		2	3		4			5			6		7			8	9		10
 * model	cl	N_beats	skip_beats	dir_data	DEBUG	init_state	dt	abserr	relerr*/
#include "Cellmodel.h"
#include <limits.h>
#include "cnpy.h"

using namespace boost::numeric::odeint;

int DEBUG;

int main(int argc, char* argv[]){

// parse args
	if (argc-1 < 7) {
		std::cerr << "##########################\n";
		std::cerr << "# too few arguments: " << argc-1 << "\n";
		std::cerr << "##########################\n";
		return 1;
	}
	std::string model = argv[1];
	double cl = std::stod(argv[2]);
	unsigned int N_beats = std::stoi(argv[3]);
	unsigned int skip_beats = std::stoi(argv[4]);
	std::string dir_data = argv[5];
	DEBUG = std::stoi(argv[6]);
	std::string init_state_str="None";
	if (argc-1==7)
		init_state_str = argv[7];
	double dt = std::stod(argv[8]);
	double abs_err = std::stod(argv[9]);
	double rel_err = std::stod(argv[10]);
	
// initialize simulation 
	double stim_dur = 0.5;

	Cellmodel* sim = make_model( model );
	if (init_state_str.compare("None")!=0) {
		if (DEBUG >2) {
			std::clog << "Custom init_state_detected:\n";
			std::clog << init_state_str << "\n";
		}
		sim->init(init_state_str);
	}

	if (DEBUG > 2) {
		std::clog << "initialized with:\n";
		sim->print_state();
	}

	runge_kutta_fehlberg78 < state_type > rk78;
	auto stepper = make_controlled( abs_err, rel_err, rk78 );

	rhs_type rhs = sim->get_rhs();

	std::vector< double > states;
	unsigned int states_shape[2];
	states_shape[1] = sim->get_N_states();
	std::vector< double > times;
	unsigned int times_shape[2];
	pushback_state save_s = pushback_state(states, times);
	std::vector<double> skipped_first_states;
//	unsigned int skipped_first_states_shape[2];
//	skipped_first_states_shape[1] = sim->get_N_states();

	unsigned int integration_steps;
//	for (unsigned int i=0; i<N_beats; i++)
//		saviour(sim->state, 0);
//	I_stim = 80.0;
//	integrate_const( rk78, rhs, sim->state, 0.0, stim_dur, dt, save_s );
//	I_stim = 0.0;
//	integrate_adaptive( stepper_c, rhs, sim->state, stim_dur, cl, dt, save_s );



//	unsigned int integration_steps = times.size();
//	const unsigned int shape_t[] = {integration_steps, };
//	cnpy::npy_save(dir_data + "/times.npy", &times[0], shape_t, 1, "w");
//cnpy::npy_save(dir_data + "/voltages.npy", &voltages[0], shape_t, 1, "w");


// skip loop
	if (DEBUG)
		std::clog << "Skipping beats...\n";
	unsigned int curr_beat = 0;
	while(curr_beat < skip_beats){
		curr_beat++;
		skipped_first_states.insert(skipped_first_states.end(), sim->state.begin(), sim->state.end());
		if (DEBUG > 1)
			std::clog << "\33[2K\rSkipping beat " << curr_beat << "/" << skip_beats << "..."<< std::flush;
		I_stim = 80.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.0, stim_dur, dt );
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs, sim->state, stim_dur, cl, dt );
		if (std::isnan(sim->state[0])) {
			std::cout << "NaN occured during skipping of beats.\n";
			std::cout << "beat: " << curr_beat << "\n";
			std::cout << "aborting\n";
			return 1;
		}
	}
//	if (skip_beats>0) {
//		skipped_first_states_shape[0] = curr_beat;
//		cnpy::npy_save(dir_data + "/skipped_first_states.npy", &skipped_first_states[0], times_shape, 1, "a");
//		//if (DEBUG > 1)
//		//	std::clog << "saved "<<curr_beat<<" skipped first states\n";
//	}
	if (DEBUG > 1)
		std::clog << "\n";
	
// analyse loop
	if (DEBUG)
		std::clog << "Analysing beats...\n";	
	curr_beat = 0;
	while(curr_beat < N_beats){
		curr_beat++;
		
		times.clear();
		states.clear();
		
		if (DEBUG > 1)
			std::clog << "\33[2K\rAnalysing beat " << curr_beat << "/" << N_beats << "..." << std::flush;
		I_stim = 80.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.0, stim_dur, dt, save_s );
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs, sim->state, stim_dur, cl, dt, save_s );
		
		if (DEBUG > 1)
			std::clog << "saving...";
		integration_steps = times.size();
		times_shape[0] = states_shape[0] = integration_steps;
		cnpy::npy_save(dir_data + "/times.npy", &times[0], times_shape, 1, "a");
		cnpy::npy_save(dir_data + "/states.npy", &states[0], states_shape, 2, "a");
		if (std::isnan(sim->state[0])) {
			std::cout << "NaN occured during analyse beats.\n";
			std::cout << "beat: " << curr_beat << "\n";
			std::cout << "aborting\n";
			return 1;
		}
	}
	if (DEBUG > 1)
		std::clog << "\n";
	if (DEBUG > 1) {
		std::clog.precision(17);
		std::clog << "last state encountered:\n";
		auto iter = states.end()-sim->get_N_states();
		while (iter!=states.end()) {
			std::clog << double(*(iter++)) << ", ";
		}
	}
	delete sim;
	return 0;
}
