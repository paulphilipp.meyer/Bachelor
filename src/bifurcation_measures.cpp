#include "LRModel.h"
#include "LRIntegrate.h"
#include "helper.h"
#include "include/cnpy.h"

//using namespace std;
using namespace boost::numeric::odeint;
namespace po = boost::program_options;

/* Outdated at the moment, uses observer directly determining AP measures


Change everything _bs to _wt to simulate WildType, _bs for BrugadaSyndrome
 * */

int main(){

	unsigned int N_beats = 20;

	double dt = 0.01;
	double stim_dur = 0.5;
	double t_s = 50;	// delay for stroboscopic potential

	double cl_min = 150;
	double cl_max = 300;
	unsigned int cl_steps = 20;

	std::vector< double > cls = linspace( cl_min, cl_max, cl_steps );

	std::cout << "Simulating " << N_beats << " heartbeats per " << cl_steps
			<< " pacing frequencies..\n";


	state_type_bs states;
	runge_kutta_fehlberg78 < state_type_bs > rk78;

	double abs_err = 1e-8;
	double rel_err = abs_err;

	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );
	store_measure_bs observer = store_measure_bs();

	std::vector< double > v_max( N_beats*cl_steps );
	std::vector< double > v_min( N_beats*cl_steps );
	std::vector< double > v_s( N_beats*cl_steps );
	std::vector< double > apd90( N_beats*cl_steps );

	init_sim(states);

	int curr_beat, index;
	double cl;
	for ( unsigned int i_cl=0; i_cl<cl_steps; ++i_cl ) {

		cl = cls[i_cl];
		std::cout << "Cycle length: " << cl << "\n";
		curr_beat = 0;
		while(curr_beat < N_beats){
			index =  i_cl*N_beats + curr_beat;
			std::cout << "\tBeat number " << curr_beat << "\t index: " << index << "\n";
			observer.init(states[0]);

			I_stim = 80.0;
			integrate_const(rk78, calc_odeint_bs  , states , 0.0 , stim_dur , dt, observer );

			I_stim = 0.0;
			integrate_adaptive( stepper_c, calc_odeint_bs  , states , stim_dur , t_s , dt, observer );

			v_s[index] = states[0];
			integrate_adaptive( stepper_c, calc_odeint_bs  , states , t_s , cl , dt, observer );

			v_max[index] = observer.m_v_max_so_far;
			v_min[index] = observer.m_v_min_so_far;
			apd90[index] = observer.m_apd90;

			curr_beat++;

		}
	}

	// saving to .npy files
	const unsigned int cl_shape[] = {cl_steps, };
	cnpy::npy_save("../data/cl.npy", &cls[0], cl_shape, 1, "w");

	const unsigned int meas_shape[] = { cl_steps, N_beats };
	cnpy::npy_save("../data/v_max.npy", &v_max[0], meas_shape, 2, "w");
	cnpy::npy_save("../data/v_min.npy", &v_min[0], meas_shape, 2, "w");
	cnpy::npy_save("../data/v_s.npy", &cls[0], meas_shape, 2, "w");
	cnpy::npy_save("../data/apd90.npy", &apd90[0], meas_shape, 2, "w");


}
