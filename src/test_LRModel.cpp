#include "LRModel.h"
#include <limits.h>
#include <iostream>

using namespace boost::numeric::odeint;


int main(int argc, char* argv[]){

	unsigned int skip_beats = 0;
	int N_beats = 5;
	double stim_dur = 0.5;
	double cl = 250;
	double dt = 0.01;


	std::string filename;
	output_writer.open("tmp.dat", std::ios::out);
	model_wt sim;
	std::cout << "cout\n";
	output_writer << "output\n";
	runge_kutta_fehlberg78 < state_type > rk78;
	double abs_err = 1e-8;
	double rel_err = abs_err;
	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );

	std::cout << "state:\n";
	for(auto const& value: sim.state) {
			std::cout <<"\t" <<value;
	}
	std::cout << "\ndxdt:\n";
	std::vector<double> differential;
	differential = sim.state;
	sim.rhs(sim.state, differential, 0);
	for(auto const& value: differential) {
			std::cout <<"\t" <<value;
	}
	
	
	/*

	// skip loop
	int curr_beat = 0;
	while(curr_beat < skip_beats){
		curr_beat++;

		std::clog << "Skipping " << skip_beats << " beats...\n";
		I_stim = 80.0;
		integrate_const( rk78, sim.rhs, sim.state, 0.0, stim_dur, dt );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, sim.rhs, sim.state, stim_dur, cl, dt );
	}
	
	// analyse loop
	curr_beat = 0;
	while(curr_beat < N_beats){
		curr_beat++;

		std::cout << "\tAnalysing beat number " << curr_beat << "\n";
		I_stim = 80.0;
		integrate_const(rk78, sim.rhs, sim.state, 0.0, stim_dur, dt, sim.write_state );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, sim.rhs, sim.state, stim_dur, cl, dt, sim.write_state );
	}
*/
}
