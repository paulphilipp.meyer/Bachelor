/* args:
 * 1		2			3		4			5		6			7			8			9			10		11
 * model	cl_start	cl_stop	cl_steps	N_beats	skip_beats	init_skip	init_each	dir_data	debug	init_state
 */
#include "LRModel.h"
#include "helper.h"
#include "cnpy.h"
#include <cstdio>
#include <limits.h>

using namespace boost::numeric::odeint;

int main(int argc, char* argv[]){

// parse args
	if (argc-1 < 11) {
		throw std::invalid_argument("to few arguments given");	
	}
	std::string model = argv[1];
	double cl_start = std::stod(argv[2]);
	double cl_stop = std::stod(argv[3]);
	unsigned int cl_steps = std::stod(argv[4]);
	unsigned int N_beats = std::stoi(argv[5]);
	unsigned int skip_beats = std::stoi(argv[6]);
	unsigned int init_skip_beats = std::stoi(argv[7]);
	bool init_each = (argv[8] != "0");
	std::string dir_data = argv[9];
	int debug = std::stoi(argv[10]);
	std::string init_state_str = "None";
	if (argc-1==11) {
		init_state_str = argv[11];
	//std::clog << "\t" << init_state_str << "\n";
	}
	
// initialize simulation 
	if (debug)
		std::clog << "initialize...\n";
	double stim_dur = 0.5;
	double dt = 0.01;
	double t_s = 50;	// delay for stroboscopic potential

	std::vector< double > cls = linspace( cl_start, cl_stop, cl_steps);

	if (debug > 2) {
		std::clog << "cls: ";
		for (auto i:cls)
			std::clog << i << " ";
		std::clog << "\n";
	}

	runge_kutta_fehlberg78 < state_type > rk78;
	double abs_err = 1e-8;
	double rel_err = abs_err;
	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );

	Cellmodel* sim = make_model( model );
	if (init_state_str.compare("None")!=0) {
		if (debug > 2) std::clog << "Custom init_state detected\n";
		// more debug options in src/timeseries/states
		sim->init(init_state_str);
	}
	state_type init_state = sim->state;

	rhs_type rhs = (*sim).get_rhs();
	
	std::vector< double > beat_voltages;
	std::vector< double > beat_times;
	std::vector< double > last_states;
	std::vector< double > first_states;
	store_voltage observer = store_voltage( beat_voltages, beat_times );

	std::vector< double > v_max( N_beats*cl_steps );
	std::vector< double > v_min( N_beats*cl_steps );
	std::vector< double > v_s( N_beats*cl_steps );
	std::vector< double > apd90( N_beats*cl_steps );


	int curr_beat, index, apd90_marker;
	unsigned int integration_steps;
	double cl, v_apd90;

	// initial transient beats
	if (!init_each) { 
		if (debug==1)
			std::clog << "initial transient beats\n";
		curr_beat = 0;
		while(curr_beat < init_skip_beats) {
			curr_beat++;
			cl = cls[0];
			if (debug > 1)
				std::clog << "\rInitial beat: " << curr_beat << "/" << init_skip_beats << "...";

			I_stim = 80.0;
			integrate_const(rk78, rhs , sim->state , 0.0 , stim_dur , dt);
			I_stim = 0.0;
			integrate_adaptive( stepper_c, rhs , sim->state , stim_dur , cl , dt);
		}
		if (debug >1)
			std::clog << "\n";
		if (debug==1)
			std::clog << "done\n";
	} // end inital transient beats

	// pacing frequencies loop
	for ( unsigned int i_cl=0; i_cl<cl_steps; ++i_cl ) {
		cl = cls[i_cl];
		if (debug == 1)
			std::clog << "Cycle length: " << cl << " (" <<i_cl+1 <<"/" <<cl_steps <<")\n";
		
		if (init_each)
			std::copy( init_state.begin(), init_state.end(), sim->state.begin() );
		
		// skip beats
		if (debug==1)
			std::clog << "Skipping beats...\n";
		curr_beat = 0;
		while(curr_beat < skip_beats) {
			curr_beat++;

			if (debug > 1)
				std::clog << "\33[2K\rcl "<<cl << " ("<<i_cl+1<<"/"<<cl_steps
					<<") skipping beat " << curr_beat << "/" << skip_beats << "...";
			
			I_stim = 80.0;
			integrate_const(rk78, rhs , sim->state , 0.0 , stim_dur , dt);
			I_stim = 0.0;
			integrate_adaptive( stepper_c, rhs  , sim->state , stim_dur , cl , dt);
		}

		// data beats
		if (debug==1)
			std::clog << "Analysing beats...\n";
		// save first state
		for (double entry : sim->state)
			first_states.push_back(entry);
		curr_beat = 0;
		while(curr_beat < N_beats) {
			curr_beat++;
			if (debug > 1)
				std::clog << "\33[2K\rcl "<<cl<<" ("<<i_cl+1<<"/"<<cl_steps
					<<") analysing beat " << curr_beat << "/" << N_beats << "...";

			beat_voltages.clear();
			beat_times.clear();
			index = i_cl*N_beats + curr_beat;

			I_stim = 80.0;
			integrate_const(rk78, rhs , sim->state , 0.0 , stim_dur , dt, observer );
			I_stim = 0.0;
			integrate_adaptive( stepper_c, rhs , sim->state , stim_dur , t_s , dt, observer );

			// calc v_s
			v_s[index] = sim->state[0];
			integrate_adaptive( stepper_c, rhs , sim->state , t_s , cl , dt, observer );

			assert(UINT_MAX > beat_times.size());
			integration_steps = beat_times.size();
			
			// determine measures
			apd90_marker = 0;
			v_max[index] = v_min[index] = beat_voltages[0];
			v_apd90 = .9 * beat_voltages[0];
			for (int i=0; i<integration_steps; ++i) {

				//calc v_max & v_min
				if (v_max[index] < beat_voltages[i]) v_max[index] = beat_voltages[i];
				if (v_min[index] > beat_voltages[i]) v_min[index] = beat_voltages[i];

				//calc apd90
				switch (apd90_marker) {
					case 0:
					if (beat_voltages[i] > v_apd90) apd90_marker = 1;
					break;
				case 1:
					if (beat_voltages[i] < v_apd90) {
						apd90[index] = beat_times[i];
						apd90_marker = 2;
					}
				}
			} // end determine measures loop

		} // end beat loop

		// save last states
		for (double entry : sim->state)
			last_states.push_back(entry);

	} // end cl loop
	if (debug > 1)
		std::clog << "\n";

	// saving to .npy files
	if (debug)
		std::clog << "Saving results...\n";
	const unsigned int cl_shape[] = {cl_steps, };
	cnpy::npy_save(dir_data + "/cl.npy", &cls[0], cl_shape, 1, "w");

	const unsigned int last_states_shape[] = { cl_steps, sim->get_N_states() };
	//std::clog << "size of last_states: " << last_states.size() << "\n should be " << last_states_shape[0] <<"x"<<last_states_shape[1]<<"\n";
	assert( last_states.size() == last_states_shape[0] * last_states_shape[1] );
	cnpy::npy_save(dir_data + "/last_states.npy", &last_states[0], last_states_shape, 2, "w");
	cnpy::npy_save(dir_data + "/first_states.npy", &first_states[0], last_states_shape, 2, "w");

	const unsigned int meas_shape[] = { cl_steps, N_beats };
	cnpy::npy_save(dir_data + "/v_max.npy", &v_max[0], meas_shape, 2, "w");
	cnpy::npy_save(dir_data + "/v_min.npy", &v_min[0], meas_shape, 2, "w");
	cnpy::npy_save(dir_data + "/v_s.npy", &v_s[0], meas_shape, 2, "w");
	cnpy::npy_save(dir_data + "/apd90.npy", &apd90[0], meas_shape, 2, "w");
	if (debug)
		std::clog << "done\n";
		
}
