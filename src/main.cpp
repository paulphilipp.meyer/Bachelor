#include "LRModel.h"

using namespace std;
using namespace boost::numeric::odeint;

/* Change everything _bs to _wt to simulate WildType, _bs for BrugadaSyndrome
 * */

int main(){

	state_type_bs states;
	init_sim(states);
	runge_kutta_fehlberg78 < state_type_bs > rk78;



	double abs_err = 1e-8;
	double rel_err = abs_err;


	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );


	output_writer.open("../data/output.dat", ios::out);


	int curr_beat = 0;
	int N_beats = 100;
	double stim_dur = 0.5;
	double cl = 300.0;
	double dt = 0.01;


	while(curr_beat < N_beats){


		I_stim = 80.0;
		integrate_const(rk78, calc_odeint_bs  , states , 0.0 , stim_dur , dt,write_voltage_bs );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, calc_odeint_bs  , states , stim_dur , cl , dt,write_voltage_bs);

		curr_beat++;


		}



	}
