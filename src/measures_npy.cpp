#include "LRModel.h"
#include "LRIntegrate.h"

using namespace std;
using namespace boost::numeric::odeint;
namespace po = boost::program_options;

/* Change everything _bs to _wt to simulate WildType, _bs for BrugadaSyndrome
 * */

int main(int argc, char* argv[]){

	// // handle commandline options
	// po::options_description desc("Allowed options");
	// desc.add_options()
    // 	("help", "produce help message")
    // 	("N_beats", po::value<int>(), "number of simulated beats")
	// 	("cl", po::value<double>(), "stimulation cycle length")
	// ;
	//
	// po::variables_map vm;
	// po::store(po::parse_command_line(argc, argv, desc), vm);
	// po::notify(vm);

	output_writer.open("../data/output.dat", ios::out);


	int curr_beat = 0;
	int N_beats = 10;
	double stim_dur = 0.5;
	double cl = 300.0;
	double dt = 0.01;
	//
	// if (vm.count("help")) {
	//     cout << desc << "\n";
	//     return 1;
	// }
	// if (vm.count("N_beats")) {
	// 	N_beats = vm["N_beats"].as< int >();
	// }
	// if (vm.count("cl")) {
	// 	cl = vm["cl"].as< double >();
	// }

	cout << "N_beats: " << N_beats << "\n";
	cout << "cl: " << cl << "\n";

	state_type_bs states;
	init_sim(states);
	runge_kutta_fehlberg78 < state_type_bs > rk78;

	double abs_err = 1e-8;
	double rel_err = abs_err;

	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );

	std::vector< double > voltages;
	std::vector< double > times;

	std::vector< double > v_max;
	std::vector< double > v_min;
	std::vector< double > v_s;
	std::vector< double > apd90;

	while(curr_beat < N_beats){
		store_measure_bs::v_max_so_far = store_measure_bs::v_min_so_far = states[0];
		store_measure_bs::v_apd = .9 * states[0];
		store_measure_bs::apd_marker = 0;

		I_stim = 80.0;
		integrate_const(rk78, calc_odeint_bs  , states , 0.0 , stim_dur , dt, store_voltage_bs(voltages, times) );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, calc_odeint_bs  , states , stim_dur , cl , dt, store_voltage_bs(voltages, times) );



		curr_beat++;

	}

	for(auto const& voltage: voltages) {
      	output_writer << voltage << '\n';
	}
}
