/* options:
 * 1		2	3		4			5			(6)
 * model	cl	N_beats	skip_beats	dir_data	debug  */

#include "LRModel.h"
#include <limits.h>
#include "cnpy.h"
using namespace boost::numeric::odeint;


int main(int argc, char* argv[]){

// parse args
	if (argc-1 <= 5) {
		std::cerr << "##########################\n";
		std::cerr << "# too few arguments: " << argc-1 << "\n";
		std::cerr << "##########################\n";
		return 1;
	}
	std::string model = argv[1];
	double cl = std::stod(argv[2]);
	unsigned int N_beats = std::stoi(argv[3]);
	unsigned int skip_beats = std::stoi(argv[4]);
	std::string dir_data = argv[5];
	int debug = 0;
	if (argc == 7)
		debug = std::stoi(argv[6]);
	
// initialize simulation 
	double stim_dur = 0.5;
	double dt = 0.01;

	Cellmodel* sim = make_model( model );
/*	if (model=="wt") {
		sim = new Model_wt;
	} else if (model=="bs") {
		sim = new Model_bs;
	} else {
		std::cerr << "##########################\n";
		std::cerr << "# unrecognised model: " << model << "\n";
		std::cerr << "##########################\n";
		return 1;
	}
*/	
	runge_kutta_fehlberg78 < state_type > rk78;
	double abs_err = 1e-8;
	double rel_err = abs_err;
	auto stepper_c = make_controlled( abs_err , rel_err , rk78 );

	std::string filename = dir_data + "/states.dat";
	output_writer.open(filename, std::ios::out);
	
	rhs_type rhs = (*sim).get_rhs();
	
// skip loop
	if (debug)
		std::clog << "Skipping beats...\n";
	int curr_beat = 0;
	while(curr_beat < skip_beats){
		curr_beat++;

		if (debug > 1)
			std::clog << "\33[2K\rSkipping beat " << curr_beat << "/" << skip_beats << "..."<< std::flush;
		I_stim = 80.0;
		integrate_const( rk78, rhs, sim->state, 0.0, stim_dur, dt );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, rhs, sim->state, stim_dur, cl, dt );
	}
	if (debug > 1)
		std::clog << "\33[2K\r";

// analyse loop
	if (debug)
		std::clog << "Analysing beats...\n";	
	curr_beat = 0;
	while(curr_beat < N_beats){
		curr_beat++;
		
		if (debug > 1)
			std::clog << "\33[2K\rAnalysing beat " << curr_beat << "/" << N_beats << "..." << std::flush;
		I_stim = 80.0;
		integrate_const(rk78, rhs, sim->state, 0.0, stim_dur, dt, Cellmodel::write_state );
		I_stim = 0.0;
		integrate_adaptive( stepper_c, rhs, sim->state, stim_dur, cl, dt, Cellmodel::write_state );
	}
	if (debug > 1)
		std::clog << "\33[2K\r";
	delete sim;
}
