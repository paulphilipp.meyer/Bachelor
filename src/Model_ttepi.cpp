#include "Model_ttepi.h"	
#define MODEL TTEPI


#define		Ko			constants[0]
#define		Cao			constants[1]
#define		Nao			constants[2]
#define		Vc			constants[3]
#define		Vsr			constants[4]
#define		Bufc		constants[5]
#define		Kbufc		constants[6]
#define		Bufsr		constants[7]
#define		Kbufsr		constants[8]
#define		taufca		constants[9]
#define		taug		constants[10]
#define		Vmaxup		constants[11]
#define		Kup			constants[12]
#define		R			constants[13]
#define		F			constants[14]
#define		T			constants[15]
#define		RTONF		constants[16]
#define		CAPACITANCE	constants[17]
#define		Gkr			constants[18]
#define		pKNa		constants[20]
#define		Gks			constants[20]
#define		GK1			constants[21]
#define		Gto			constants[22]
#define		GNa			constants[23]
#define		GbNa		constants[24]
#define		KmK			constants[25]
#define		KmNa		constants[26]
#define		knak		constants[27]
#define		GCaL		constants[28]
#define		GbCa		constants[29]
#define		knaca		constants[30]
#define		KmNai		constants[31]
#define		KmCa		constants[32]
#define		ksat		constants[33]
#define		n			constants[34]
#define		GpCa		constants[35]
#define		KpCa		constants[36]
#define		GpK			constants[37]
#define		FONRT		constants[38]
#define		inverseVcF2	constants[39]
#define		inverseVcF	constants[40]
#define		Kupsquare	constants[41]
//	("Ko" , 5.4),
// -> #define		Ko		constants[0]
// kyyjpkxxxi#define		elDJdEde$h+h

#define		bc			intermediates[0]
#define		cc			intermediates[1]
#define		Ca_i_free	intermediates[2]
#define		bjsr		intermediates[3]
#define		cjsr 		intermediates[4]
#define		Ca_SR_free	intermediates[5]
#define		Ek			intermediates[6]
#define		Ena			intermediates[7]
#define		Eks			intermediates[8]
#define		Eca			intermediates[9]
#define		Ak1			intermediates[10]
#define		Bk1			intermediates[11]
#define		rec_iK1		intermediates[12]
#define		rec_iNaK	intermediates[13]
#define		rec_ipK		intermediates[14]
#define		AM			intermediates[15]
#define		BM			intermediates[16]
#define		TAU_M		intermediates[17]
#define		M_INF		intermediates[18]
#define		AH			intermediates[19]
#define		BH			intermediates[20]
#define		TAU_H		intermediates[21]
#define		H_INF		intermediates[22]
#define		AJ			intermediates[23]
#define		BJ			intermediates[24]
#define		TAU_J		intermediates[25]
#define		J_INF		intermediates[26]
#define		Xr1_INF		intermediates[27]
#define		axr1		intermediates[28]
#define		bxr1		intermediates[29]
#define		TAU_Xr1		intermediates[30]
#define		Xr2_INF		intermediates[31]
#define		axr2		intermediates[32]
#define		bxr2		intermediates[33]
#define		TAU_Xr2		intermediates[34]
#define		Xs_INF		intermediates[35]
#define		Axs			intermediates[36]
#define		Bxs			intermediates[37]
#define		TAU_Xs		intermediates[38]
#define		R_INF		intermediates[39]
#define		S_INF		intermediates[40]
#define		TAU_R		intermediates[41]
#define		TAU_S		intermediates[42]
#define		D_INF		intermediates[43]
#define		Ad			intermediates[44]
#define		Bd			intermediates[45]
#define		Cd			intermediates[46]
#define		TAU_D		intermediates[47]
#define		F_INF		intermediates[48]
#define		TAU_F		intermediates[49]
#define		FCa_INF		intermediates[50]
#define		G_INF		intermediates[51]
#define		INa			intermediates[52]
#define		ICaL		intermediates[53]
#define		Ito			intermediates[54]
#define		IKr			intermediates[55]
#define		IKs			intermediates[56]
#define		IK1			intermediates[57]
#define		INaCa		intermediates[58]
#define		INaK		intermediates[59]
#define		IpCa		intermediates[60]
#define		IpK			intermediates[61]
#define		IbNa		intermediates[62]
#define		IbCa		intermediates[63]
#define		sItot		intermediates[64]
#define		Ileak		intermediates[65]
#define		SERCA		intermediates[66]
#define		CaSRsquare	intermediates[67]
#define		A			intermediates[68]
#define		Irel		intermediates[69]
#define		CaCurrent	intermediates[70]
#define		CaSRCurrent	intermediates[71]
// i#define		k$vbbbykjja			ph+

#define		Voltage		x[0]
#define		Ca_i_total	x[1]
#define		Ca_SR_total	x[2]
#define		Na_i		x[3]
#define		K_i			x[4]
#define		m_gate		x[5]
#define		h_gate		x[6]
#define		j_gate		x[7]
#define		X_r1_gate	x[8]
#define		X_r2_gate	x[9]
#define		X_s_gate	x[10]
#define		r_gate		x[11]
#define		s_gate		x[12]
#define		d_gate		x[13]
#define		f_gate		x[14]
#define		F_Ca		x[15]
#define		g_gate		x[16]

#define		Voltage_rate		dxdt[0]
#define		Ca_i_total_rate		dxdt[1]
#define		Ca_SR_total_rate	dxdt[2]
#define		Na_i_rate			dxdt[3]
#define		K_i_rate			dxdt[4]
#define		m_gate_rate			dxdt[5]
#define		h_gate_rate			dxdt[6]
#define		j_gate_rate			dxdt[7]
#define		X_r1_gate_rate		dxdt[8]
#define		X_r2_gate_rate		dxdt[9]
#define		X_s_gate_rate		dxdt[10]
#define		r_gate_rate			dxdt[11]
#define		s_gate_rate			dxdt[12]
#define		d_gate_rate			dxdt[13]
#define		f_gate_rate			dxdt[14]
#define		F_Ca_rate			dxdt[15]
#define		g_gate_rate			dxdt[16]

Model_ttepi::Model_ttepi() {
	state.push_back( -86.2 ); // Voltage
	state.push_back( 0.0097 ); // Ca_i_total
	state.push_back( 0.27692 ); // Ca_SR_total
	state.push_back( 11.6 ); // Na_i
	state.push_back( 138.3 ); // K_i
	state.push_back( 3.1436e-5 ); // m_gate
	state.push_back( 0.75 ); // h_gate
	state.push_back( 0.75 ); // j_gate
	state.push_back( 1e-20 ); // X_r1_gate
	state.push_back( 1-1e-20 ); // X_r2_gate
	state.push_back( 01e-20 ); // X_s_gate
	state.push_back( 01e-20 ); // r_gate
	state.push_back( 1-1e-20 ); // s_gate
	state.push_back( 01e-20 ); // d_gate
	state.push_back( 1-1e-20 ); // f_gate
	state.push_back( 1-1e-20 ); // F_Ca
	state.push_back( 1-1e-20 ); // g_gate
}

unsigned int Model_ttepi::get_N_states() {
	return 17;
}

rhs_type Model_ttepi::get_rhs() {
	return Model_ttepi::rhs;
}

void Model_ttepi::rhs(const state_type &x, state_type &dxdt, const double t) {
	bc = Bufc - Ca_i_total + Kbufc;
	cc = Kbufc * Ca_i_total;
	Ca_i_free = ( sqrt(bc*bc+4*cc) - bc) / 2;
	bjsr = Bufsr - Ca_SR_total + Kbufsr;
	cjsr = Kbufsr * Ca_SR_total;
	Ca_SR_free = ( sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2;

	Ek = RTONF * log( Ko / K_i );
	Ena = RTONF * log( Nao / Na_i );
	Eks = RTONF * log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i));
	Eca = 0.5 * RTONF * log( Cao / Ca_i_free );
	Ak1 = 0.1 / ( 1.0 + exp( 0.06*(Voltage-Ek-200) ) );
	Bk1 = ( 3. * exp( 0.0002*(Voltage-Ek+100) ) + exp(0.1*(Voltage-Ek-10)))  /  (1. + exp(-0.5*(Voltage-Ek)));
	rec_iK1 = Ak1 / ( Ak1+Bk1 );
	rec_iNaK = 1./( 1.+0.1245*exp(-0.1*Voltage*FONRT)+0.0353*exp(-Voltage*FONRT) );
	rec_ipK = 1. / (1.+exp( (25-Voltage)/5.98) );
	 
	// steady state values and time constants ####################
	AM = 1./(1.+exp((-60.-Voltage)/5.));
	BM = 0.1/(1.+exp((Voltage+35.)/5.))+0.10/(1.+exp((Voltage-50.)/200.));
	TAU_M = AM * BM;
	M_INF = 1./((1.+exp((-56.86-Voltage)/9.03))*(1.+exp((-56.86-Voltage)/9.03)));
	if (Voltage>=-40.) {
		AH = 0;
		BH = (0.77/(0.13*(1.+exp(-(Voltage+10.66)/11.1))));
		TAU_H = 1.0/(AH+BH);
	} else {
		AH = (0.057*exp(-(Voltage+80.)/6.8));
		BH=(2.7*exp(0.079*Voltage)+(3.1e5)*exp(0.3485*Voltage));
		TAU_H=1.0/(AH+BH);
	}
	H_INF = 1./((1.+exp((Voltage+71.55)/7.43))*(1.+exp((Voltage+71.55)/7.43)));


	if (Voltage>=-40.) {
		AJ = 0.;
		BJ = (0.6*exp((0.057)*Voltage)/(1.+exp(-0.1*(Voltage+32.))));
		TAU_J = 1.0/(AJ+BJ);
	} else {
		AJ = (((-2.5428e4)*exp(0.2444*Voltage)-(6.948e-6)* exp(-0.04391*Voltage))*(Voltage+37.78)/ (1.+exp(0.311*(Voltage+79.23))));
		BJ = (0.02424*exp(-0.01052*Voltage)/(1.+exp(-0.1378*(Voltage+40.14))));
		TAU_J = 1.0/(AJ+BJ);
	}
	J_INF = H_INF;

	Xr1_INF = 1./(1.+exp((-26.-Voltage)/7.));
	axr1 = 450./(1.+exp((-45.-Voltage)/10.));
	bxr1 = 6./(1.+exp((Voltage-(-30.))/11.5));
	TAU_Xr1 = axr1*bxr1;
	Xr2_INF = 1./(1.+exp((Voltage-(-88.))/24.));
	axr2 = 3./(1.+exp((-60.-Voltage)/20.));
	bxr2 = 1.12/(1.+exp((Voltage-60.)/20.));
	TAU_Xr2 = axr2*bxr2;

	Xs_INF = 1./(1.+exp((-5.-Voltage)/14.));
	Axs = 1100./(sqrt(1.+exp((-10.-Voltage)/6)));
	Bxs = 1./(1.+exp((Voltage-60.)/20.));
	TAU_Xs = Axs*Bxs;
	#if MODEL==TTEPI
		R_INF=1./(1.+exp((20-Voltage)/6.));
		S_INF=1./(1.+exp((Voltage+20)/5.));
		TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8;
		TAU_S=85.*exp(-(Voltage+45.)*(Voltage+45.)/320.)+5./(1.+exp((Voltage-20.)/5.))+3.;
	#elif MODEL==TTENDO
		R_INF=1./(1.+exp((20-Voltage)/6.));
		S_INF=1./(1.+exp((Voltage+28)/5.));
		TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8;
		TAU_S=1000.*exp(-(Voltage+67)*(Voltage+67)/1000.)+8.;
	#elif MODEL==TTMCELL
		R_INF=1./(1.+exp((20-Voltage)/6.));
		S_INF=1./(1.+exp((Voltage+20)/5.));
		TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8;
		TAU_S=85.*exp(-(Voltage+45.)*(Voltage+45.)/320.)+5./(1.+exp((Voltage-20.)/5.))+3.;
	#endif
	D_INF = 1./(1.+exp((-5-Voltage)/7.5));
	Ad = 1.4/(1.+exp((-35-Voltage)/13))+0.25;
	Bd = 1.4/(1.+exp((Voltage+5)/5));
	Cd = 1./(1.+exp((50-Voltage)/20));
	TAU_D = Ad*Bd+Cd;
	F_INF = 1./(1.+exp((Voltage+20)/7));
	TAU_F = 1125*exp(-(Voltage+27)*(Voltage+27)/300)+80+165/(1.+exp((25-Voltage)/10));
		 
	FCa_INF = (1./(1.+pow((Ca_i_free/0.000325),8)) + 0.1/(1.+exp((Ca_i_free-0.0005)/0.0001))+ 0.20/(1.+exp((Ca_i_free-0.00075)/0.0008))+ 0.23 )/1.46;
	if(Ca_i_free<0.00035) {
		G_INF = 1./(1.+pow((Ca_i_free/0.00035),6));
	} else {
		G_INF = 1./(1.+pow((Ca_i_free/0.00035),16));
	}
	
	// currents ################################################# 
	INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena);
	ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( exp(2*Voltage*FONRT)-1.);
	Ito = Gto*r_gate*s_gate*(Voltage-Ek);
	IKr = Gkr*sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek);
	IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks);
	IK1 = GK1*rec_iK1*(Voltage-Ek);
	INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*exp((n-1)*Voltage*FONRT)))*(exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5);
	INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK;
	IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free);
	IpK = GpK*rec_ipK*(Voltage-Ek);
	IbNa = GbNa*(Voltage-Ena);
	IbCa = GbCa*(Voltage-Eca);

	sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK - I_stim;

	Ileak = 0.00008 * (Ca_SR_free - Ca_i_free);
	SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) );
	CaSRsquare = Ca_SR_free * Ca_SR_free;
	A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232;
	Irel = A * d_gate * g_gate;

	// voltage rate ################################################
	Voltage_rate = - sItot;

	// concentration rates #########################################
	CaCurrent = - ( ICaL + IbCa + IpCa - 2*INaCa ) * inverseVcF2 * CAPACITANCE;
	CaSRCurrent = SERCA - Irel - Ileak;
	Ca_i_total_rate = CaCurrent - CaSRCurrent;
	Ca_SR_total_rate = Vc/Vsr * CaSRCurrent;
	Na_i_rate = - ( INa + IbNa + 3*INaK + 3*INaCa ) * inverseVcF * CAPACITANCE;
	K_i_rate = - ( -I_stim + IK1 + Ito + IKr + IKs - 2*INaK + IpK) * inverseVcF * CAPACITANCE ;

	// gating rates ################################################
	m_gate_rate = ( M_INF - m_gate ) / TAU_M;
	h_gate_rate = ( H_INF - h_gate ) / TAU_H;
	j_gate_rate = ( J_INF - j_gate ) / TAU_J;
	X_r1_gate_rate = ( Xr1_INF - X_r1_gate ) / TAU_Xr1;
	X_r2_gate_rate = ( Xr2_INF - X_r2_gate ) / TAU_Xr2;
	X_s_gate_rate = ( Xs_INF - X_s_gate ) / TAU_Xs;
	r_gate_rate = ( R_INF - r_gate ) / TAU_R;
	s_gate_rate = ( S_INF - s_gate ) / TAU_S;
	d_gate_rate = ( D_INF - d_gate ) / TAU_D;
	f_gate_rate = ( F_INF - f_gate ) / TAU_F;

	if (FCa_INF > F_Ca && Voltage > -60) {
		F_Ca_rate = 0;
	} else {
		F_Ca_rate = ( FCa_INF - F_Ca ) / taufca;
	}
	if (G_INF > g_gate && Voltage > -60) {
		g_gate_rate = 0;
	} else {
		g_gate_rate = ( G_INF - g_gate ) / taug;
	}

//	std::clog << t << "------------------------------------------------------\n";
//	std::clog << std::fixed;
//	
//	std::clog << std::setw(9) << Voltage << " ";
//	std::clog << std::setw(9) << Ca_i_total << " ";
//	std::clog << std::setw(9) << Ca_SR_total << " ";
//	std::clog << std::setw(9) << Na_i << " ";
//	std::clog << std::setw(9) << K_i << " ";
//	std::clog << std::setw(9) << m_gate << " ";
//	std::clog << std::setw(9) << h_gate << " ";
//	std::clog << std::setw(9) << j_gate << " ";
//	std::clog << std::setw(9) << X_r1_gate << " ";
//	std::clog << std::setw(9) << X_r2_gate << " ";
//	std::clog << std::setw(9) << X_s_gate << " ";
//	std::clog << std::setw(9) << r_gate << " ";
//	std::clog << std::setw(9) << s_gate << " ";
//	std::clog << std::setw(9) << d_gate << " ";
//	std::clog << std::setw(9) << f_gate << " ";
//	std::clog << std::setw(9) << F_Ca << " ";
//	std::clog << std::setw(9) << g_gate << " ";
//	std::clog << "\n";

//	std::clog << std::setw(9) << Voltage_rate << " ";
//	std::clog << std::setw(9) << Ca_i_total_rate << " ";
//	std::clog << std::setw(9) << Ca_SR_total_rate << " ";
//	std::clog << std::setw(9) << Na_i_rate << " ";
//	std::clog << std::setw(9) << K_i_rate << " ";
//	std::clog << std::setw(9) << m_gate_rate << " ";
//	std::clog << std::setw(9) << h_gate_rate << " ";
//	std::clog << std::setw(9) << j_gate_rate << " ";
//	std::clog << std::setw(9) << X_r1_gate_rate << " ";
//	std::clog << std::setw(9) << X_r2_gate_rate << " ";
//	std::clog << std::setw(9) << X_s_gate_rate << " ";
//	std::clog << std::setw(9) << r_gate_rate << " ";
//	std::clog << std::setw(9) << s_gate_rate << " ";
//	std::clog << std::setw(9) << d_gate_rate << " ";
//	std::clog << std::setw(9) << f_gate_rate << " ";
//	std::clog << std::setw(9) << F_Ca_rate << " ";
//	std::clog << std::setw(9) << g_gate_rate << " ";
//	std::clog << t << "\n";
//	std::clog << M_INF << " " << TAU_M << "\n";
//	std::clog << FCa_INF << "inf | tau " << taufca << "\n"; 

//	Voltage_rate = 0;
//	Ca_i_total_rate = 0;
//	Ca_SR_total_rate = 0;
//	Na_i_rate = 0;
//	K_i_rate = 0;
//	m_gate_rate = 0;
//	h_gate_rate = 0;
//	j_gate_rate = 0;
//	X_r1_gate_rate = 0;
//	X_r2_gate_rate = 0;
//	X_s_gate_rate = 0;
//	r_gate_rate = 0;
//	s_gate_rate = 0;
//	d_gate_rate = 0;
//	f_gate_rate = 0;
//	F_Ca_rate = 0;
//	g_gate_rate = 0;
}

std::array<double, 72> Model_ttepi::intermediates; 

void Model_ttepi::print_intermediates() {
	for (auto item : intermediates)
		std::clog << item << "\n";
}

std::array<double, 42> Model_ttepi::constants = {{
	5.4, // Ko [0]
	2.0, // Cao [1]
	140.0, // Nao [2]
	0.016404, // Vc [3]
	0.001094, // Vsr [4]
	0.15, // Bufc [5]
	0.001, // Kbufc [6]
	10.0, // Bufsr [7]
	0.3, // Kbufsr [8]
	2.0, // taufca [9]
	2.0, // taug [10]
	0.000425, // Vmaxup [11]
	0.00025, // Kup [12]
	8314.472, // R [13]
	96485.3415, // F [14]
	310.0, // T [15]
	26.7137606597, // RTONF [16]
	0.185, // CAPACITANCE [17]
	0.096, // Gkr [18]
	0.03, // pKNa [19]
	0.245, // Gks [20]
	5.405, // GK1 [21]
	0.294, // Gto [22]
	14.838, // GNa [23]
	0.00029, // GbNa [24]
	1.0, // KmK [25]
	40.0, // KmNa [26]
	1.362, // knak [27]
	0.000175, // GCaL [28]
	0.000592, // GbCa [29]
	1000, // knaca [30]
	87.5, // KmNai [31]
	1.38, // KmCa [32]
	0.1, // ksat [33]
	0.35, // n [34]
	0.825, // GpCa [35]
	0.0005, // KpCa [36]
	0.0146, // GpK [37]
	0.0374338908227, // FONRT [38]
	0.000315906749849, // inverseVcF2 [39]
	0.000631813499697, // inverseVcF [40]
	6.25e-08 // Kupsquare [41]
}};
