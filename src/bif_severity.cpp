/* args:
 * 1		2			3			4			5		6			7			8			9			
 * cl	sev_start	sev_stop	sev_steps	N_beats	skip_beats	init_skip	init_each	dir_data	
 *
 * 10		11			12	13
 * debug	init_state	dt	save_voltages
 */
#include "Cellmodel.h"
#include "helper.h"
#include "cnpy.h"
#include <limits.h>

using namespace boost::numeric::odeint;

int main(int argc, char* argv[]){

// parse args
	double cl = std::stod(argv[1]);
	double sev_start = std::stod(argv[2]);
	double sev_stop = std::stod(argv[3]);
	unsigned int sev_steps = std::stod(argv[4]);
	unsigned int N_beats = std::stoi(argv[5]);
	unsigned int skip_beats = std::stoi(argv[6]);
	unsigned int init_skip_beats = std::stoi(argv[7]);
	bool init_each = std::stoi(argv[8]);
	std::cerr << "init_each: " << init_each << "\n";
	std::string dir_data = argv[9];
	int debug = std::stoi(argv[10]);
	std::string init_state_str = argv[11];
	double dt = std::stod(argv[12]);
	bool save_voltages = std::stoi(argv[13]);

// initialize simulation 
	if (debug)
		std::cout << "initialize...\n";
	double stim_dur = 0.5;
	double t_s = 50;	// delay for stroboscopic potential

	std::vector< double > sevs = linspace( sev_start, sev_stop, sev_steps);

	if (init_each)
		std::cerr << "arg: " << argv[8]<< "comp: "<< (argv[8]!="0") <<"inits each\n";
	if (debug > 2) {
		std::clog << "cevs: ";
		for (auto i:sevs)
			std::clog << i << " ";
		std::clog << "\n";
	}
	
	const unsigned int sev_shape[] = {sev_steps, };
	cnpy::npy_save(dir_data + "/sev.npy", &sevs[0], sev_shape, 1, "w");

	runge_kutta_fehlberg78 < state_type > rk78;
	double abs_err = 1e-8;
	double rel_err = abs_err;
	auto stepper = make_controlled( abs_err , rel_err , rk78 );

	Model_xiaepi_bs* sim = new Model_xiaepi_bs(sev_start);
	if (init_state_str.compare("None")!=0) {
		if (debug > 2) std::clog << "Custom init_state detected\n";
		// more debug options in src/timeseries/states
		sim->init(init_state_str);
	}
	state_type init_state = sim->state;

	rhs_type rhs = (*sim).get_rhs();
	
	const unsigned int append_single_state_shape[] = { 1, sim->get_N_states()};
	const unsigned int append_meas_shape[] = { 1, N_beats };
	
	std::vector< double > beat_voltages;
	std::vector< double > beat_times;
	std::vector< double > last_states;
	std::vector< double > first_states;
	store_voltage observer = store_voltage( beat_voltages, beat_times );

	std::vector< double > v_max( N_beats );
	std::vector< double > v_min( N_beats );
	std::vector< double > v_s( N_beats );
	std::vector< double > apd50( N_beats );
	std::vector< double > apd90( N_beats );

	int curr_index, apd90_marker;
	unsigned int integration_steps;
	double sev, v_apd50, v_apd90;
	size_t index_v_max;

	// initial transient beats
	if (!init_each) { 
		if (debug==1)
			std::clog << "initial transient beats\n";
		curr_index = 0;
		while(curr_index < init_skip_beats) {
			sev = sevs[0];
			if (debug > 1)
				std::clog << "\rInitial beat: " << curr_index+1 << "/" << init_skip_beats << "...";

			I_stim = 80.0;
			integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt);
			I_stim = 0.0;
			integrate_adaptive( stepper, rhs , sim->state , stim_dur , cl , dt);
			curr_index++;
		}
		if (debug >1)
			std::clog << "\n";
		if (debug==1)
			std::clog << "done\n";
	} // end inital transient beats

	// severity loop
	for ( unsigned int i_sev=0; i_sev<sev_steps; ++i_sev ) {
		sev = sevs[i_sev];
		if (debug == 1)
			std::clog << "Severity: " << sev << " (" <<i_sev+1 <<"/" <<sev_steps <<")\n";
		sim->set_severity(sev);
		if (init_each)
			std::copy( init_state.begin(), init_state.end(), sim->state.begin() );
		
		// skip beats
		curr_index = 0;
		while(curr_index < skip_beats) {

			if (debug > 1)
				std::clog << "\33[2K\rseverity: "<<sev << " ("<<i_sev+1<<"/"<<sev_steps
					<<") skipping beat " << curr_index+1 << "/" << skip_beats << "...";
			
			I_stim = 80.0;
			integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt);
			I_stim = 0.0;
			integrate_adaptive( stepper, rhs  , sim->state , stim_dur , cl , dt);
			curr_index++;
		}

		// data beats
		if (debug==1)
			std::clog << "Analysing beats...\n";
		
		cnpy::npy_save(dir_data + "/first_states.npy", &sim->state[0], append_single_state_shape, 2, "a");
		
		curr_index = 0;
		while(curr_index < N_beats) {
			if (debug > 1)
				std::clog << "\33[2K\rseverity: "<<sev<<" ("<<i_sev+1<<"/"<<sev_steps
					<<") analysing beat " << curr_index << "/" << N_beats << "...";

			beat_voltages.clear();
			beat_times.clear();

			I_stim = 80.0;
			integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt, observer );
			I_stim = 0.0;
			integrate_adaptive( stepper, rhs , sim->state , stim_dur , t_s , dt, observer );
			v_s[curr_index] = sim->state[0];
			integrate_adaptive( stepper, rhs , sim->state , t_s , cl , dt, observer );

			assert(UINT_MAX > beat_times.size());
			integration_steps = beat_times.size();
		
			if (save_voltages) {
				cnpy::npy_save(dir_data + "/timeseries/sevn" + std::to_string(i_sev) + "_times.npy", &beat_times[0], &integration_steps, 1, "a");
				cnpy::npy_save(dir_data + "/timeseries/sevn" + std::to_string(i_sev) + "_voltages.npy", &beat_voltages[0], &integration_steps, 1, "a");
			}

			if (std::isnan(sim->state[0])) {
				std::cout << "NaN occured at severity " << sev << ".\n";
				std::cout << "beat: " << curr_index << "\n";
				std::cout << "aborting\n";
				return 1;
			}
			
			// determine v_min v_max
			minmax( beat_voltages, v_max[curr_index], index_v_max, v_min[curr_index]);

			// determine apds
			apd5090( beat_times, beat_voltages, index_v_max, v_apd50, v_apd90, apd50[curr_index], apd90[curr_index] );

			curr_index++;
		} // end beat loop

		cnpy::npy_save(dir_data + "/last_states.npy", &sim->state[0], append_single_state_shape, 2, "a");

		cnpy::npy_save(dir_data + "/v_max.npy", &v_max[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/v_min.npy", &v_min[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/v_s.npy", &v_s[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/apd50.npy", &apd50[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/apd90.npy", &apd90[0], append_meas_shape, 2, "a");

	} // end cl loop
	if (debug > 1)
		std::clog << "\n";

	if (debug)
		std::clog << "done\n";
		
}
