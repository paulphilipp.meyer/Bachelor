#include "Cellmodel.h"

double I_stim = 0.0;
std::fstream output_writer;

state_type str2state(const std::string &text, char sep/* =',' */) {
  state_type tokens;
  std::size_t start = 0, end = 0;
  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(std::stod(text.substr(start, end - start)));
    start = end + 1;
  }
  tokens.push_back(std::stod(text.substr(start)));
  return tokens;
}

void Cellmodel::write_state(const state_type &x, const double t){

		output_writer <<t <<"\t";
		for(auto const& value: x) {
			output_writer <<"\t" <<value;
		}
		output_writer <<"\n";
}

void Cellmodel::print_state(const state_type &x) {
	for(auto const& value: x) {
		std::clog <<value<< "\t";
	}
	std::clog <<"\n";
}

void Cellmodel::print_state(const state_type &x, const double t){
	std::clog <<"t" << t <<"\t";
	Cellmodel::print_state(x);
}

void Cellmodel::print_state() {
	Cellmodel::print_state(state);
}

void Cellmodel::print_intermediates() {
	std::clog << "no intermediates in this model.\n";
}

void Cellmodel::init(std::string &str) {
	state = str2state(str); 
}

Cellmodel* make_model(std::string code) {
	if (code == "wt") {
		return new Model_wt();
	} else if (code == "bs") {
		return new Model_bs();
	} else if (code == "wtm") {
		return new Model_wtm();
	} else if (code == "ttepi") {
		return new Model_ttepi();
	} else if (code == "xiaepi") {
		return new Model_xiaepi();
	} else if (code == "xiaepi_bs") {
		return new Model_xiaepi_bs();
	} else {
		std::cerr << "unrecognised model: " << code << "\n";
		throw std::invalid_argument("model: " + code);
	}
}
