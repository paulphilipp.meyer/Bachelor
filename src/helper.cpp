#include "helper.h"

std::vector<double> linspace(double start, double end, int num_in)
{

  std::vector<double> linspaced;

  if (num_in == 0) { return linspaced; }
  if (num_in == 1)
    {
      linspaced.push_back(start);
      return linspaced;
    }

  double num = static_cast<double>(num_in);

  double delta = (end - start) / (num - 1);

	for(int i=0; i < num-1; ++i)
		linspaced.push_back(start + delta * i);
	linspaced.push_back(end); // I want to ensure that start and end
								  // are exactly the same as the input
  return linspaced;
}


void minmax(const std::vector<double>& vec, double& v_max, size_t& index_v_max, double& v_min) {
	if (vec.size() == 0)
			throw std::invalid_argument("minmax called with empty vector");
	v_max = v_min = vec[0];
	index_v_max = 0;
	for (int i=1; i<vec.size(); ++i) {
		if ( vec[i] < v_min ) {
			v_min = vec[i];
		} else if ( vec[i] > v_max ) {
			v_max = vec[i];
			index_v_max = i;
		}
	}
}

void apd5090(const std::vector<double>& beat_times, const std::vector<double>& beat_voltages, const size_t index_v_max, double& v_apd50, double& v_apd90, double& apd50, double& apd90 ) {
	
	v_apd50 = .5 * (beat_voltages[0] + beat_voltages[index_v_max]);
	v_apd90 = .9 * beat_voltages[0] + .1 * beat_voltages[index_v_max];
	apd50 = apd90 = 0;

	for (int i=index_v_max; i<beat_voltages.size(); ++i) {		
		if ( apd50 == 0 ) {    // apd50 needs to be determined next
			if ( beat_voltages[i] < v_apd50 ) 
				apd50 = beat_times[i];
		} else {               // apd90 needs to be determined next
			if ( beat_voltages[i] < v_apd90 ) {
				apd90 = beat_times[i];
				break;
			}
		}
	}
}
/*
void load_state(state_type_bs &state, const std::string filename) {
	std::ifstream file (filename);
	assert (file.is_open());
	std::string line;
	for (int i=0; i<state.size(); ++i) {
		std::getline(file, line);
		state[i] = std::strtod(line.c_str(), NULL);
	}
}

void load_state(state_type_wt &state, const std::string filename) {
	std::ifstream file (filename);
	assert (file.is_open());
	std::string line;
	for (int i=0; i<state.size(); ++i) {
		std::getline(file, line);
		state[i] = std::strtod(line.c_str(), NULL);
	}
}
*/

// void load_vector(std::vector<double> &vec, std::string filename) {
//     vec.clear();
//     string line;
//     ifstream myfile (filename);
//     if (myfile.is_open()) {
//         while ( getline (myfile,line) ) {
//           vec.push_back(std::stod(line));
//         }
//         myfile.close();
//     }
//
//   else cout << "Unable to open file";
