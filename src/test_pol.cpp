/* options:
 * 1		2		3			4		5			6	7		8
 * mu		T		dir_data	DEBUG	init_state	dt	abserr	relerr*/
#include "Cellmodel.h"
#include <limits.h>
#include "cnpy.h"

using namespace boost::numeric::odeint;

void vanDerPol( const state_type &x, state_type &dxdt, const double t);

int DEBUG;
double mu;

int main(int argc, char* argv[]){

// parse args
	if (argc-1 < 1) {
		std::cerr << "##########################\n";
		std::cerr << "# too few arguments: " << argc-1 << "\n";
		std::cerr << "##########################\n";
		return 1;
	}
	mu = std::stod(argv[1]);
	double T = std::stod(argv[2]);
	std::string dir_data = argv[3];
	DEBUG = std::stoi(argv[4]);
	std::string init_state_str="None";
	init_state_str = argv[5];
	double dt = std::stod(argv[6]);
	double abs_err = std::stod(argv[7]);
	double rel_err = std::stod(argv[8]);
// initialize simulation 
	state_type state = {2,0};

	if (init_state_str.compare("None")!=0) {
		if (DEBUG >2) {
			std::clog << "Custom init_state_detected:\n";
			std::clog << init_state_str << "\n";
		}
		std::size_t start = 0, end = 0, i = 0;
		while ((end = init_state_str.find(',', start)) != std::string::npos) {
			state[i] = std::stod(init_state_str.substr(start, end - start));
			start = end + 1;
			i++;
		}
		state[i] = std::stod(init_state_str.substr(start));
	}

	runge_kutta_fehlberg78 < state_type > rk78;
	auto stepper = make_controlled( abs_err, rel_err, rk78 );

	rhs_type rhs = vanDerPol;

	std::vector< double > states;
	std::vector< double > times;
	pushback_state save_s = pushback_state(states, times);

//	integrate_const( rk78, rhs, sim->state, 0.0, stim_dur, dt, save_s );

	integrate_adaptive( stepper, rhs, state, 0.0, T, dt, save_s );



	unsigned int integration_steps = times.size();
	const unsigned int shape_t[] = {integration_steps, };
	const unsigned int shape_s[] = {integration_steps, 2};
	std::cerr << integration_steps <<'\n';
	std::cerr << states.size() << '\n';
	std::cerr << "saving to " << dir_data;
	cnpy::npy_save(dir_data + "/times.npy", &times[0], shape_t, 1, "w");
	cnpy::npy_save(dir_data + "/states.npy", &states[0], shape_s, 2, "w");

	return 0;
}

void vanDerPol( const state_type &x, state_type &dxdt, const double t) {
	dxdt[0] = x[1];
	dxdt[1] = mu * (1 - x[0]*x[0]) * x[1] - x[0];
}
