#include "LRIntegrate.h"

void store_measure_bs::operator()( const state_type_bs &x , const double t ) {

	// v_max
	if (x[0] > m_v_max_so_far) m_v_max_so_far = x[0];

	// v_min
	if (x[0] < m_v_min_so_far) m_v_min_so_far = x[0];

	// apd90
	switch (m_apd90_marker) {
		case 0:
			if (x[0] > m_v_apd90) m_apd90_marker = 1;
			break;
		case 1:
			if (x[0] < m_v_apd90) {
				m_apd90 = t;
				m_apd90_marker = 2;
			}
	}
}

void store_measure_bs::init( double voltage ) {

	m_v_max_so_far = m_v_min_so_far = voltage;
	m_v_apd90 = .9 * voltage;
	m_apd90_marker = 0;
	m_apd90 = 0; // When this stays 0, something went wrong
}

double store_measure_bs::m_v_min_so_far;
double store_measure_bs::m_v_max_so_far;
double store_measure_bs::m_v_apd90;
int store_measure_bs::m_apd90_marker;
double store_measure_bs::m_apd90;


void write_state_bs( const state_type_bs &x , const double t ){

		output_writer <<t <<"\t";
		for(auto const& value: x) {
			output_writer <<"\t" <<value;
		}
		output_writer <<"\n";
}

void write_state_wt( const state_type_wt &x , const double t ){

		output_writer <<t <<"\t";
		for(auto const& value: x) {
			output_writer <<"\t" <<value;
		}
		output_writer <<"\n";
}
