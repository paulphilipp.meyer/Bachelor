/* args:
 * 1		2			3		4			5		6			7			8			9		
 * model	cl_start	cl_stop	cl_steps	N_beats	skip_beats	init_skip	init_each	dir_data	
 *
 * 10		11			12
 * debug	init_state	dt
 */
#include "Cellmodel.h"
#include "helper.h"
#include "cnpy.h"
#include <limits.h>

using namespace boost::numeric::odeint;

int main(int argc, char* argv[]){

// parse args
	if (argc-1 < 11) {
		throw std::invalid_argument("to few arguments given");	
	}
	std::string model = argv[1];
	double cl_start = std::stod(argv[2]);
	double cl_stop = std::stod(argv[3]);
	unsigned int cl_steps = std::stod(argv[4]);
	unsigned int N_beats = std::stoi(argv[5]);
	unsigned int skip_beats = std::stoi(argv[6]);
	unsigned int init_skip_beats = std::stoi(argv[7]);
	bool init_each = std::stoi(argv[8]);
	std::cerr << "init_each: " << init_each << "\n";
	std::string dir_data = argv[9];
	int debug = std::stoi(argv[10]);
	std::string init_state_str = argv[11];
	double dt = std::stod(argv[12]);
	
// initialize simulation 
	if (debug)
		std::clog << "initialize...\n";
	double stim_dur = 0.5;
	double t_s = 50;	// delay for stroboscopic potential

	std::vector< double > cls = linspace( cl_start, cl_stop, cl_steps);

	if (init_each)
		std::cerr << "arg: " << argv[8]<< "comp: "<< (argv[8]!="0") <<"inits each\n";
	if (debug > 2) {
		std::clog << "cls: ";
		for (auto i:cls)
			std::clog << i << " ";
		std::clog << "\n";
	}
	
	const unsigned int cl_shape[] = {cl_steps, };
	cnpy::npy_save(dir_data + "/cl.npy", &cls[0], cl_shape, 1, "w");

	runge_kutta_fehlberg78 < state_type > rk78;
	double abs_err = 1e-8;
	double rel_err = abs_err;
	auto stepper = make_controlled( abs_err , rel_err , rk78 );

	Cellmodel* sim = make_model( model );
	if (init_state_str.compare("None")!=0) {
		if (debug > 2) std::clog << "Custom init_state detected\n";
		// more debug options in src/timeseries/states
		sim->init(init_state_str);
	}
	state_type init_state = sim->state;

	rhs_type rhs = (*sim).get_rhs();
	
	const unsigned int append_single_state_shape[] = { 1, sim->get_N_states()};
	const unsigned int append_meas_shape[] = { 1, N_beats };
	
	std::vector< double > beat_voltages;
	std::vector< double > beat_times;
	std::vector< double > last_states;
	std::vector< double > first_states;
	store_voltage observer = store_voltage( beat_voltages, beat_times );

	std::vector< double > v_max( N_beats );
	std::vector< double > v_min( N_beats );
	std::vector< double > v_s( N_beats );
	std::vector< double > apd50( N_beats );
	std::vector< double > apd90( N_beats );


	int curr_index;
	unsigned int integration_steps;
	double cl, v_apd50, v_apd90;
	size_t index_v_max;

	// initial transient beats
	if (!init_each) { 
		if (debug==1)
			std::clog << "initial transient beats\n";
		curr_index = 0;
		while(curr_index < init_skip_beats) {
			cl = cls[0];
			if (debug > 1)
				std::clog << "\rInitial beat: " << curr_index+1 << "/" << init_skip_beats << "...";

			I_stim = 80.0;
			integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt);
			I_stim = 0.0;
			integrate_adaptive( stepper, rhs , sim->state , stim_dur , cl , dt);
			curr_index++;
		}
		if (debug >1)
			std::clog << "\n";
		if (debug==1)
			std::clog << "done\n";
	} // end inital transient beats

	// pacing frequencies loop
	for ( unsigned int i_cl=0; i_cl<cl_steps; ++i_cl ) {
		cl = cls[i_cl];
		if (debug == 1)
			std::clog << "Cycle length: " << cl << " (" <<i_cl+1 <<"/" <<cl_steps <<")\n";
		
		if (init_each)
			std::copy( init_state.begin(), init_state.end(), sim->state.begin() );
		
		// skip beats
		curr_index = 0;
		while(curr_index < skip_beats) {

			if (debug > 1)
				std::clog << "\33[2K\rcl "<<cl << " ("<<i_cl+1<<"/"<<cl_steps
					<<") skipping beat " << curr_index+1 << "/" << skip_beats << "...";
			
			I_stim = 80.0;
			integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt);
			I_stim = 0.0;
			integrate_adaptive( stepper, rhs  , sim->state , stim_dur , cl , dt);
			curr_index++;
		}

		// data beats
		if (debug==1)
			std::clog << "Analysing beats...\n";
		
		cnpy::npy_save(dir_data + "/first_states.npy", &sim->state[0], append_single_state_shape, 2, "a");
		
		curr_index = 0;
		while(curr_index < N_beats) {
			if (debug > 1)
				std::clog << "\33[2K\rcl "<<cl<<" ("<<i_cl+1<<"/"<<cl_steps
					<<") analysing beat " << curr_index+1 << "/" << N_beats << "...";

			beat_voltages.clear();
			beat_times.clear();

			I_stim = 80.0;
			integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt, observer );
			I_stim = 0.0;
			integrate_adaptive( stepper, rhs , sim->state , stim_dur , t_s , dt, observer );
			v_s[curr_index] = sim->state[0];
			integrate_adaptive( stepper, rhs , sim->state , t_s , cl , dt, observer );

			assert(UINT_MAX > beat_times.size());
			integration_steps = beat_times.size();
			
			if (std::isnan(sim->state[0])) {
				std::cout << "NaN occured at cl.\n";
				std::cout << "beat: " << curr_index+1 << "\n";
				std::cout << "aborting\n";
				return 1;
			}

			// determine v_min v_max
			minmax( beat_voltages, v_max[curr_index], index_v_max, v_min[curr_index]);

			// determine apds
			apd5090( beat_times, beat_voltages, index_v_max, v_apd50, v_apd90, apd50[curr_index], apd90[curr_index] );

			curr_index++;
		} // end beat loop

		cnpy::npy_save(dir_data + "/last_states.npy", &sim->state[0], append_single_state_shape, 2, "a");

		cnpy::npy_save(dir_data + "/v_max.npy", &v_max[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/v_min.npy", &v_min[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/v_s.npy", &v_s[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/apd50.npy", &apd50[0], append_meas_shape, 2, "a");
		cnpy::npy_save(dir_data + "/apd90.npy", &apd90[0], append_meas_shape, 2, "a");

	} // end cl loop
	if (debug > 1)
		std::clog << "\n";

	if (debug)
		std::clog << "done\n";
		
}
