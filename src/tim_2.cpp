/* try to reproduce Na-data 
 * 
 * options:
 * 1         2      3           4   5       6       7      
 * dir_data  DEBUG  init_state  dt  abserr  relerr  severity*/
#include "Cellmodel.h"
#include <limits.h>
#include "cnpy.h"

using namespace boost::numeric::odeint;

int DEBUG;

int main(int argc, char* argv[]){

// parse args
	std::string dir_data = argv[1];
	DEBUG = std::stoi(argv[2]);
	std::string init_state_str = argv[3];
	double dt = std::stod(argv[4]);
	double abs_err = std::stod(argv[5]);
	double rel_err = std::stod(argv[6]);
	double severity = std::stod(argv[7]);
	
// initialize simulation 
	Model_xiaepi_bs* sim = new Model_xiaepi_bs(severity);
	
	if (init_state_str.compare("None")!=0) {
		sim->init(init_state_str);
	}
	if (DEBUG > 2) {
		std::clog << "initialized with:\n";
		sim->print_state();
	}

	runge_kutta_fehlberg78 < state_type > rk78;
	auto stepper = make_controlled( abs_err, rel_err, rk78 );

	rhs_type rhs = sim->get_rhs();

	std::vector< double > states;
	unsigned int states_shape[2];
	states_shape[1] = sim->get_N_states();
	std::vector< double > times;
	unsigned int times_shape[1];
	pushback_state save_s = pushback_state(states, times);
	unsigned int integration_steps;

// simulate
	// 0.6 Hz
	std::clog << "Frequency: 0.6 Hz\n";
	for(int i=0; i<20; i++) {
		std::clog << "\33[2K\rAnalysing beat " << i+1 << "/20..."<< std::flush;
		I_stim = 80.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.0, 0.5, dt, save_s );
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.5, 1666.6666666666667, dt, save_s );
	}
	integration_steps = times.size();
	times_shape[0] = states_shape[0] = integration_steps;
	cnpy::npy_save(dir_data + "/times.npy", &times[0], times_shape, 1, "a");
	cnpy::npy_save(dir_data + "/states.npy", &states[0], states_shape, 2, "a");
	times.clear();
	states.clear();

	// 0.8 Hz
	std::clog << "Frequency: 0.8 Hz\n";
	for(int i=0; i<20; i++) {
		std::clog << "\33[2K\rAnalysing beat " << i+1 << "/20..."<< std::flush;
		I_stim = 80.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.0, 0.5, dt, save_s );
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.5, 1250.0, dt, save_s );
	}
	integration_steps = times.size();
	times_shape[0] = states_shape[0] = integration_steps;
	cnpy::npy_save(dir_data + "/times.npy", &times[0], times_shape, 1, "a");
	cnpy::npy_save(dir_data + "/states.npy", &states[0], states_shape, 2, "a");
	times.clear();
	states.clear();

	// 1.0 Hz
	std::clog << "Frequency: 1.0 Hz\n";
	for(int i=0; i<20; i++) {
		std::clog << "\33[2K\rAnalysing beat " << i+1 << "/20..."<< std::flush;
		I_stim = 80.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.0, 0.5, dt, save_s );
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs, sim->state, 0.5, 1000.0, dt, save_s );
	}
	integration_steps = times.size();
	times_shape[0] = states_shape[0] = integration_steps;
	cnpy::npy_save(dir_data + "/times.npy", &times[0], times_shape, 1, "a");
	cnpy::npy_save(dir_data + "/states.npy", &states[0], states_shape, 2, "a");
	times.clear();
	states.clear();

	delete sim;
	return 0;
}
