/* args:
 * 1      2   3        4           5         6      7           8   9                
 * model  cl  N_beats  skip_beats  dir_data	 debug  init_state  dt  save_buffer_size 
 *
 * 10	
 * save_voltages
 */

#include "Cellmodel.h"
#include "helper.h"
#include "cnpy.h"
#include <limits.h>

using namespace boost::numeric::odeint;

int main(int argc, char* argv[]){

// parse args
	std::string model = argv[1];
	double cl = std::stod(argv[2]);
	unsigned int N_beats = std::stoi(argv[3]);
	unsigned int skip_beats = std::stoi(argv[4]);
	std::string dir_data = argv[5];
	int debug = std::stoi(argv[6]);
	std::string init_state_str = argv[7];
	double dt = std::stod(argv[8]);
	unsigned int save_buffer_size = std::stoi(argv[9]);
	unsigned int save_voltages = std::stoi(argv[10]);

// initialize simulation 
	if (debug)
		std::clog << "initialize...\n";
	double stim_dur = 0.5;

	runge_kutta_fehlberg78 < state_type > rk78;
	double abs_err = 1e-8;
	double rel_err = abs_err;
	auto stepper = make_controlled( abs_err , rel_err , rk78 );

	Cellmodel* sim = make_model( model );
	if (init_state_str.compare("None")!=0) {
		if (debug > 2) std::clog << "Custom init_state detected\n";
		// more debug options in src/timeseries/states
		sim->init(init_state_str);
	}
	
	rhs_type rhs = (*sim).get_rhs();
	std::vector< double > beat_voltages;
	std::vector< double > beat_times;
	store_voltage observer = store_voltage( beat_voltages, beat_times );
	
	const unsigned int append_single_state_shape[] = { 1, sim->get_N_states()};
	unsigned int append_meas_shape[] = { save_buffer_size, };
	
	
	std::vector< double > last_states;

	std::vector< double > meas_v_max;
	std::vector< double > meas_v_min;
	std::vector< double > meas_v_10;
	std::vector< double > meas_v_50;
	std::vector< double > meas_v_100;
	std::vector< double > meas_v_200;
	std::vector< double > meas_apd50;
	std::vector< double > meas_apd90;


	unsigned int curr_beat, integration_steps;
	size_t index_v_max;
	double voltage, v_max, v_min, v_apd50, v_apd90, apd50, apd90;

	// skip beats
	curr_beat = 0;
	while( curr_beat < skip_beats) {
		curr_beat++;
		if (debug == 2)
			std::clog << "\33[2K\rskipping beat " << curr_beat << "/" << skip_beats << "...";
		else if (debug > 2)
			std::clog << "skipping beat " << curr_beat << "/" << N_beats << "...\n";
		
		I_stim = 80.0;
		integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt);
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs  , sim->state , stim_dur , cl , dt);
	}

	// data beats
	curr_beat = 0;
	while ( curr_beat < N_beats ) {
		curr_beat++;
		if (debug == 2)
			std::clog << "\33[2K\ranalysing beat " << curr_beat << "/" << N_beats << "...";
		else if (debug > 2)
			std::clog << "analysing beat " << curr_beat << "/" << N_beats << "...\n";

		cnpy::npy_save(dir_data + "/first_states.npy", &sim->state[0], append_single_state_shape, 2, "a");

		beat_voltages.clear();
		beat_times.clear();

		I_stim = 80.0;
		integrate_adaptive( stepper, rhs , sim->state , 0.0 , stim_dur , dt, observer );
		I_stim = 0.0;
		integrate_adaptive( stepper, rhs , sim->state , stim_dur , 10.0 , dt, observer );
		meas_v_10.push_back(sim->state[0]);
		integrate_adaptive( stepper, rhs , sim->state , 10.0, 50.0, dt, observer );
		meas_v_50.push_back(sim->state[0]);
		integrate_adaptive( stepper, rhs , sim->state , 50.0, 100.0, dt, observer );
		meas_v_100.push_back(sim->state[0]);
		if (cl > 200) {
			integrate_adaptive( stepper, rhs , sim->state , 100.0, 200.0, dt, observer );
			meas_v_200.push_back(sim->state[0]);
			integrate_adaptive( stepper, rhs , sim->state , 200.0, cl, dt, observer );
		} else {
			integrate_adaptive( stepper, rhs , sim->state , 100.0, cl, dt, observer );
		}

		integration_steps = beat_times.size();
		
		if (std::isnan(sim->state[0])) {
			std::cout << "NaN occured at cl.\n";
			std::cout << "beat: " << curr_beat << "\n";
			std::cout << "aborting\n";
			return 1;
		}
		cnpy::npy_save(dir_data + "/last_states.npy", &sim->state[0], append_single_state_shape, 2, "a");
		if (save_voltages) {
			cnpy::npy_save(dir_data + "/times.npy", &beat_times[0], &integration_steps, 1, "a");
			cnpy::npy_save(dir_data + "/voltages.npy", &beat_voltages[0], &integration_steps, 1, "a");
		}

		// determine v_min v_max
		if ( debug > 2 )
			std::clog << "determine v_min v_max\n";
		minmax( beat_voltages, v_max, index_v_max, v_min);
		meas_v_max.push_back(v_max);
		meas_v_min.push_back(v_min);

		// determine apds
		if ( debug > 2 )
			std::clog << "determine apds\n";
		apd5090( beat_times, beat_voltages, index_v_max, v_apd50, v_apd90, apd50, apd90 );
		meas_apd50.push_back(apd50);
		meas_apd90.push_back(apd90);

		if (curr_beat % save_buffer_size == 0 ) {
			if ( debug > 2 )
				std::clog << "saving\n";
			cnpy::npy_save(dir_data + "/v_max.npy", &meas_v_max[0], append_meas_shape, 1, "a");
			meas_v_max.clear();
			cnpy::npy_save(dir_data + "/v_min.npy", &meas_v_min[0], append_meas_shape, 1, "a");
			meas_v_min.clear();
			cnpy::npy_save(dir_data + "/v_10.npy", &meas_v_10[0], append_meas_shape, 1, "a");
			meas_v_10.clear();
			cnpy::npy_save(dir_data + "/v_50.npy", &meas_v_50[0], append_meas_shape, 1, "a");
			meas_v_50.clear();
			cnpy::npy_save(dir_data + "/v_100.npy", &meas_v_100[0], append_meas_shape, 1, "a");
			meas_v_100.clear();
			if ( cl > 200 ) {
				cnpy::npy_save(dir_data + "/v_200.npy", &meas_v_200[0], append_meas_shape, 1, "a");
				meas_v_200.clear();
			}
			cnpy::npy_save(dir_data + "/apd50.npy", &meas_apd50[0], append_meas_shape, 1, "a");
			meas_apd50.clear();
			cnpy::npy_save(dir_data + "/apd90.npy", &meas_apd90[0], append_meas_shape, 1, "a");
			meas_apd90.clear();
		}

	} // end beat loop
	if ( debug > 2 )
		std::clog << "saving last meas\n";
	
	append_meas_shape[0] = meas_v_max.size();

	cnpy::npy_save(dir_data + "/v_max.npy", &meas_v_max[0], append_meas_shape, 1, "a");
	meas_v_max.clear();
	cnpy::npy_save(dir_data + "/v_min.npy", &meas_v_min[0], append_meas_shape, 1, "a");
	meas_v_min.clear();
	cnpy::npy_save(dir_data + "/v_10.npy", &meas_v_10[0], append_meas_shape, 1, "a");
	meas_v_10.clear();
	cnpy::npy_save(dir_data + "/v_50.npy", &meas_v_50[0], append_meas_shape, 1, "a");
	meas_v_50.clear();
	cnpy::npy_save(dir_data + "/v_100.npy", &meas_v_100[0], append_meas_shape, 1, "a");
	meas_v_100.clear();
	if ( cl > 200 ) {
		cnpy::npy_save(dir_data + "/v_200.npy", &meas_v_200[0], append_meas_shape, 1, "a");
		meas_v_200.clear();
	}
	cnpy::npy_save(dir_data + "/apd50.npy", &meas_apd50[0], append_meas_shape, 1, "a");
	meas_apd50.clear();
	cnpy::npy_save(dir_data + "/apd90.npy", &meas_apd90[0], append_meas_shape, 1, "a");
	meas_apd90.clear();

	if (debug > 1)
		std::clog << "\n";
}
