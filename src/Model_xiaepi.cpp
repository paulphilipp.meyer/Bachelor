#include "Model_xiaepi.h"	
#define MODEL XIAEPI

// j_constants
#define		Ko			Model_xiaepi::constants[0]
#define		Cao			Model_xiaepi::constants[1]
#define		Nao			Model_xiaepi::constants[2]
#define		Vc			Model_xiaepi::constants[3]
#define		Vsr			Model_xiaepi::constants[4]
#define		Bufc		Model_xiaepi::constants[5]
#define		Kbufc		Model_xiaepi::constants[6]
#define		Bufsr		Model_xiaepi::constants[7]
#define		Kbufsr		Model_xiaepi::constants[8]
#define		taufca		Model_xiaepi::constants[9]
#define		taug		Model_xiaepi::constants[10]
#define		Vmaxup		Model_xiaepi::constants[11]
#define		Kup			Model_xiaepi::constants[12]
#define		R			Model_xiaepi::constants[13]
#define		F			Model_xiaepi::constants[14]
#define		T			Model_xiaepi::constants[15]
#define		RTONF		Model_xiaepi::constants[16]
#define		CAPACITANCE	Model_xiaepi::constants[17]
#define		Gkr			Model_xiaepi::constants[18]
#define		pKNa		Model_xiaepi::constants[20]
#define		Gks			Model_xiaepi::constants[20]
#define		GK1			Model_xiaepi::constants[21]
#define		Gto			Model_xiaepi::constants[22]
#define		GNa			Model_xiaepi::constants[23]
#define		GbNa		Model_xiaepi::constants[24]
#define		KmK			Model_xiaepi::constants[25]
#define		KmNa		Model_xiaepi::constants[26]
#define		knak		Model_xiaepi::constants[27]
#define		GCaL		Model_xiaepi::constants[28]
#define		GbCa		Model_xiaepi::constants[29]
#define		knaca		Model_xiaepi::constants[30]
#define		KmNai		Model_xiaepi::constants[31]
#define		KmCa		Model_xiaepi::constants[32]
#define		ksat		Model_xiaepi::constants[33]
#define		n			Model_xiaepi::constants[34]
#define		GpCa		Model_xiaepi::constants[35]
#define		KpCa		Model_xiaepi::constants[36]
#define		GpK			Model_xiaepi::constants[37]
#define		FONRT		Model_xiaepi::constants[38]
#define		inverseVcF2	Model_xiaepi::constants[39]
#define		inverseVcF	Model_xiaepi::constants[40]
#define		Kupsquare	Model_xiaepi::constants[41]
#define		GNaL		Model_xiaepi::constants[42]
#define     numax       Model_xiaepi::constants[43]
#define     eta         Model_xiaepi::constants[44]
#define     KmNao       Model_xiaepi::constants[45]
#define     KmCai       Model_xiaepi::constants[46]
#define     KmCao       Model_xiaepi::constants[47]
#define     KmCaact     Model_xiaepi::constants[48]
#define     Nao_cubed   Model_xiaepi::constants[49]
#define     KmNao_cubed Model_xiaepi::constants[50]
#define     KmNai_cubed Model_xiaepi::constants[51]
// GNaL  ->  #define		GNaL		Model_xiaepi::constants[42]
// i#define     $$1000a 25|d$kly$kjjph+
//
//	("Ko" , 5.4),
// -> #define		Ko		Model_xiaepi::constants[0]
// kyyjpkxxxi#define		elDJdEde$h+h

// j_intermediates
#define		bc			intermediates[0]
#define		cc			intermediates[1]
#define		Ca_i_free	intermediates[2]
#define		bjsr		intermediates[3]
#define		cjsr 		intermediates[4]
#define		Ca_SR_free	intermediates[5]
#define		Ek			intermediates[6]
#define		Ena			intermediates[7]
#define		Eks			intermediates[8]
#define		Eca			intermediates[9]
#define		Ak1			intermediates[10]
#define		Bk1			intermediates[11]
#define		rec_iK1		intermediates[12]
#define		rec_iNaK	intermediates[13]
#define		rec_ipK		intermediates[14]
#define		AM			intermediates[15]
#define		BM			intermediates[16]
#define		TAU_M		intermediates[17]
#define		M_INF		intermediates[18]
#define		AH			intermediates[19]
#define		BH			intermediates[20]
#define		TAU_H		intermediates[21]
#define		H_INF		intermediates[22]
#define		AJ			intermediates[23]
#define		BJ			intermediates[24]
#define		TAU_J		intermediates[25]
#define		J_INF		intermediates[26]
#define		Xr1_INF		intermediates[27]
#define		axr1		intermediates[28]
#define		bxr1		intermediates[29]
#define		TAU_Xr1		intermediates[30]
#define		Xr2_INF		intermediates[31]
#define		axr2		intermediates[32]
#define		bxr2		intermediates[33]
#define		TAU_Xr2		intermediates[34]
#define		Xs_INF		intermediates[35]
#define		Axs			intermediates[36]
#define		Bxs			intermediates[37]
#define		TAU_Xs		intermediates[38]
#define		R_INF		intermediates[39]
#define		S_INF		intermediates[40]
#define		TAU_R		intermediates[41]
#define		TAU_S		intermediates[42]
#define		D_INF		intermediates[43]
#define		Ad			intermediates[44]
#define		Bd			intermediates[45]
#define		Cd			intermediates[46]
#define		TAU_D		intermediates[47]
#define		F_INF		intermediates[48]
#define		TAU_F		intermediates[49]
#define		FCa_INF		intermediates[50]
#define		G_INF		intermediates[51]
#define		INa			intermediates[52]
#define		ICaL		intermediates[53]
#define		Ito			intermediates[54]
#define		IKr			intermediates[55]
#define		IKs			intermediates[56]
#define		IK1			intermediates[57]
#define		INaCa		intermediates[58]
#define		INaK		intermediates[59]
#define		IpCa		intermediates[60]
#define		IpK			intermediates[61]
#define		IbNa		intermediates[62]
#define		IbCa		intermediates[63]
#define		sItot		intermediates[64]
#define		Ileak		intermediates[65]
#define		SERCA		intermediates[66]
#define		CaSRsquare	intermediates[67]
#define		A			intermediates[68]
#define		Irel		intermediates[69]
#define		CaCurrent	intermediates[70]
#define		CaSRCurrent	intermediates[71]
#define     INaL        intermediates[72]
#define     Na_i_cubed  intermediates[73]
// i#define		k$vbbbykjja			ph+

#define		Voltage		x[0]
#define		Ca_i_total	x[1]
#define		Ca_SR_total	x[2]
#define		Na_i		x[3]
#define		K_i			x[4]
#define		m_gate		x[5]
#define		h_gate		x[6]
#define		j_gate		x[7]
#define		X_r1_gate	x[8]
#define		X_r2_gate	x[9]
#define		X_s_gate	x[10]
#define		r_gate		x[11]
#define		s_gate		x[12]
#define		d_gate		x[13]
#define		f_gate		x[14]
#define		F_Ca		x[15]
#define		g_gate		x[16]
#define     mL_gate     x[17]
#define		hL_gate     x[18]

#define		Voltage_rate		dxdt[0]
#define		Ca_i_total_rate		dxdt[1]
#define		Ca_SR_total_rate	dxdt[2]
#define		Na_i_rate			dxdt[3]
#define		K_i_rate			dxdt[4]
#define		m_gate_rate			dxdt[5]
#define		h_gate_rate			dxdt[6]
#define		j_gate_rate			dxdt[7]
#define		X_r1_gate_rate		dxdt[8]
#define		X_r2_gate_rate		dxdt[9]
#define		X_s_gate_rate		dxdt[10]
#define		r_gate_rate			dxdt[11]
#define		s_gate_rate			dxdt[12]
#define		d_gate_rate			dxdt[13]
#define		f_gate_rate			dxdt[14]
#define		F_Ca_rate			dxdt[15]
#define		g_gate_rate			dxdt[16]
#define     mL_gate_rate        dxdt[17]
#define		hL_gate_rate        dxdt[18]


Model_xiaepi::Model_xiaepi() {
	set_constants();
	state.clear();

	state.push_back( -86.2 ); // Voltage
	state.push_back( 0.0097 ); // Ca_i_total
	state.push_back( 0.27692 ); // Ca_SR_total
	state.push_back( 11.6 ); // Na_i
	state.push_back( 138.3 ); // K_i
	state.push_back( 3.1436e-5 ); // m_gate
	state.push_back( 0.75 ); // h_gate
	state.push_back( 0.75 ); // j_gate
	state.push_back( 1e-20 ); // X_r1_gate
	state.push_back( 1-1e-20 ); // X_r2_gate
	state.push_back( 01e-20 ); // X_s_gate
	state.push_back( 01e-20 ); // r_gate
	state.push_back( 1-1e-20 ); // s_gate
	state.push_back( 01e-20 ); // d_gate
	state.push_back( 1-1e-20 ); // f_gate
	state.push_back( 1-1e-20 ); // F_Ca
	state.push_back( 1-1e-20 ); // g_gate
	state.push_back( 1e-20 ); // mL_gate
	state.push_back( 1e-20 ); // hL_gate
}

unsigned int Model_xiaepi::get_N_states() {
	return 19;
}

rhs_type Model_xiaepi::get_rhs() {
	return Model_xiaepi::rhs;
}

void Model_xiaepi::rhs(const state_type &x, state_type &dxdt, const double t) {
	bc = Bufc - Ca_i_total + Kbufc;
	cc = Kbufc * Ca_i_total;
	Ca_i_free = ( sqrt(bc*bc+4*cc) - bc) / 2;
	bjsr = Bufsr - Ca_SR_total + Kbufsr;
	cjsr = Kbufsr * Ca_SR_total;
	Ca_SR_free = ( sqrt(bjsr*bjsr+4*cjsr) - bjsr) / 2;

	Ek = RTONF * log( Ko / K_i );
	Ena = RTONF * log( Nao / Na_i );
	Eks = RTONF * log( (Ko + pKNa * Nao )/( K_i + pKNa * Na_i));
	Eca = 0.5 * RTONF * log( Cao / Ca_i_free );
	Ak1 = 0.1 / ( 1.0 + exp( 0.06*(Voltage-Ek-200) ) );
	Bk1 = ( 3. * exp( 0.0002*(Voltage-Ek+100) ) + exp(0.1*(Voltage-Ek-10)))  /  (1. + exp(-0.5*(Voltage-Ek)));
	rec_iK1 = Ak1 / ( Ak1+Bk1 );
	rec_iNaK = 1./( 1.+0.1245*exp(-0.1*Voltage*FONRT)+0.0353*exp(-Voltage*FONRT) );
	rec_ipK = 1. / (1.+exp( (25-Voltage)/5.98) );
	 
	// steady state values and time constants ####################
	AM = 1./(1.+exp((-60.-Voltage)/5.));
	BM = 0.1/(1.+exp((Voltage+35.)/5.))+0.10/(1.+exp((Voltage-50.)/200.));
	TAU_M = AM * BM;
	M_INF = 1./((1.+exp((-56.86-Voltage)/9.03))*(1.+exp((-56.86-Voltage)/9.03)));
	if (Voltage>=-40.) {
		AH = 0;
		BH = (0.77/(0.13*(1.+exp(-(Voltage+10.66)/11.1))));
		TAU_H = 1.0/(AH+BH);
	} else {
		AH = (0.057*exp(-(Voltage+80.)/6.8));
		BH=(2.7*exp(0.079*Voltage)+(3.1e5)*exp(0.3485*Voltage));
		TAU_H=1.0/(AH+BH);
	}
	H_INF = 1./((1.+exp((Voltage+71.55)/7.43))*(1.+exp((Voltage+71.55)/7.43)));


	if (Voltage>=-40.) {
		AJ = 0.;
		BJ = (0.6*exp((0.057)*Voltage)/(1.+exp(-0.1*(Voltage+32.))));
		TAU_J = 1.0/(AJ+BJ);
	} else {
		AJ = (((-2.5428e4)*exp(0.2444*Voltage)-(6.948e-6)* exp(-0.04391*Voltage))*(Voltage+37.78)/ (1.+exp(0.311*(Voltage+79.23))));
		BJ = (0.02424*exp(-0.01052*Voltage)/(1.+exp(-0.1378*(Voltage+40.14))));
		TAU_J = 1.0/(AJ+BJ);
	}
	J_INF = H_INF;

	Xr1_INF = 1./(1.+exp((-26.-Voltage)/7.));
	axr1 = 450./(1.+exp((-45.-Voltage)/10.));
	bxr1 = 6./(1.+exp((Voltage-(-30.))/11.5));
	TAU_Xr1 = axr1*bxr1;
	Xr2_INF = 1./(1.+exp((Voltage-(-88.))/24.));
	axr2 = 3./(1.+exp((-60.-Voltage)/20.));
	bxr2 = 1.12/(1.+exp((Voltage-60.)/20.));
	TAU_Xr2 = axr2*bxr2;

	Xs_INF = 1./(1.+exp((-5.-Voltage)/14.));
	Axs = 1100./(sqrt(1.+exp((-10.-Voltage)/6)));
	Bxs = 1./(1.+exp((Voltage-60.)/20.));
	TAU_Xs = Axs*Bxs;
	#if MODEL==XIAEPI
		R_INF=1./(1.+exp((20-Voltage)/6.));
		S_INF=1./(1.+exp((Voltage+20)/5.));
		TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8;
		TAU_S=85.*exp(-(Voltage+45.)*(Voltage+45.)/320.)+5./(1.+exp((Voltage-20.)/5.))+3.;
	#elif MODEL==XIAENDO
		R_INF=1./(1.+exp((20-Voltage)/6.));
		S_INF=1./(1.+exp((Voltage+28)/5.));
		TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8;
		TAU_S=1000.*exp(-(Voltage+67)*(Voltage+67)/1000.)+8.;
	#elif MODEL==XIAMCELL
		R_INF=1./(1.+exp((20-Voltage)/6.));
		S_INF=1./(1.+exp((Voltage+20)/5.));
		TAU_R=9.5*exp(-(Voltage+40.)*(Voltage+40.)/1800.)+0.8;
		TAU_S=85.*exp(-(Voltage+45.)*(Voltage+45.)/320.)+5./(1.+exp((Voltage-20.)/5.))+3.;
	#endif
	D_INF = 1./(1.+exp((-5-Voltage)/7.5));
	Ad = 1.4/(1.+exp((-35-Voltage)/13))+0.25;
	Bd = 1.4/(1.+exp((Voltage+5)/5));
	Cd = 1./(1.+exp((50-Voltage)/20));
	TAU_D = Ad*Bd+Cd;
	F_INF = 1./(1.+exp((Voltage+20)/7));
	TAU_F = 1125*exp(-(Voltage+27)*(Voltage+27)/300)+80+165/(1.+exp((25-Voltage)/10));
		 
	FCa_INF = (1./(1.+pow((Ca_i_free/0.000325),8)) + 0.1/(1.+exp((Ca_i_free-0.0005)/0.0001))+ 0.20/(1.+exp((Ca_i_free-0.00075)/0.0008))+ 0.23 )/1.46;
	if(Ca_i_free<0.00035) {
		G_INF = 1./(1.+pow((Ca_i_free/0.00035),6));
	} else {
		G_INF = 1./(1.+pow((Ca_i_free/0.00035),16));
	}
	
	// currents ################################################# 
	INa = GNa * m_gate*m_gate*m_gate * h_gate * j_gate * (Voltage-Ena);
	ICaL = GCaL*d_gate*f_gate*F_Ca*4*Voltage*(F*FONRT) * ( exp(2*Voltage*FONRT)*Ca_i_free-0.341*Cao )/( exp(2*Voltage*FONRT)-1.);
	Ito = Gto*r_gate*s_gate*(Voltage-Ek);
	IKr = Gkr*sqrt(Ko/5.4)*X_r1_gate*X_r2_gate*(Voltage-Ek);
	IKs = Gks*X_s_gate*X_s_gate*(Voltage-Eks);
	IK1 = GK1*rec_iK1*(Voltage-Ek);
	//INaCa = knaca*(1./(KmNai*KmNai*KmNai+Nao*Nao*Nao))*(1./(KmCa+Cao))*(1./(1+ksat*exp((n-1)*Voltage*FONRT)))*(exp(n*Voltage*FONRT)*Na_i*Na_i*Na_i*Cao-exp((n-1)*Voltage*FONRT)*Nao*Nao*Nao*Ca_i_free*2.5);
	INaK = knak*(Ko/(Ko+KmK))*(Na_i/(Na_i+KmNa))*rec_iNaK;
	IpCa = GpCa*Ca_i_free/(KpCa+Ca_i_free);
	IpK = GpK*rec_ipK*(Voltage-Ek);
	IbNa = GbNa * (Voltage-Ena);
	IbCa = GbCa * (Voltage-Eca);
	INaL = GNaL * mL_gate*mL_gate*mL_gate * hL_gate * ( Voltage-Ena );
	Na_i_cubed = Na_i*Na_i*Na_i;
	INaCa = 1 / ( 1 + KmCaact*KmCaact/(2.25*Ca_i_free*Ca_i_free)) *
			numax*( Na_i_cubed*Cao*exp(eta*Voltage*FONRT) - 1.5*Nao_cubed*Ca_i_free*exp((eta-1)*Voltage*FONRT )) / (
				( 1+ksat*exp((eta-1)*Voltage*FONRT) ) * (
					KmCao*Na_i_cubed +
					1.5*KmNao_cubed*Ca_i_free +
					KmNai_cubed*Cao*(1+1.5*Ca_i_free/KmCai) + 
					KmCai*Nao_cubed*(1+Na_i_cubed/KmNai_cubed) +
					Na_i_cubed*Cao +
					1.5*Nao_cubed*Ca_i_free
				)
			);


	sItot = IKr + IKs + IK1 + Ito + INa + IbNa + ICaL + IbCa + INaK + INaCa + IpCa + IpK + INaL - I_stim;

	Ileak = 0.00008 * (Ca_SR_free - Ca_i_free);
	SERCA = Vmaxup / (1. + Kupsquare/(Ca_i_free*Ca_i_free) );
	CaSRsquare = Ca_SR_free * Ca_SR_free;
	A = 0.016464*CaSRsquare/(0.0625+CaSRsquare) + 0.008232;
	Irel = A * d_gate * g_gate;

	// voltage rate ################################################
	Voltage_rate = - sItot;

	// concentration rates #########################################
	CaCurrent = - ( ICaL + IbCa + IpCa - 2*INaCa ) * inverseVcF2 * CAPACITANCE;
	CaSRCurrent = SERCA - Irel - Ileak;
	Ca_i_total_rate = CaCurrent - CaSRCurrent;
	Ca_SR_total_rate = Vc/Vsr * CaSRCurrent;
	Na_i_rate = - ( INa + INaL + IbNa + 3*INaK + 3*INaCa ) * inverseVcF * CAPACITANCE;
	K_i_rate = - ( -I_stim + IK1 + Ito + IKr + IKs - 2*INaK + IpK) * inverseVcF * CAPACITANCE ;

	// gating rates ################################################
	m_gate_rate = ( M_INF - m_gate ) / TAU_M;
	h_gate_rate = ( H_INF - h_gate ) / TAU_H;
	j_gate_rate = ( J_INF - j_gate ) / TAU_J;
	X_r1_gate_rate = ( Xr1_INF - X_r1_gate ) / TAU_Xr1;
	X_r2_gate_rate = ( Xr2_INF - X_r2_gate ) / TAU_Xr2;
	X_s_gate_rate = ( Xs_INF - X_s_gate ) / TAU_Xs;
	r_gate_rate = ( R_INF - r_gate ) / TAU_R;
	s_gate_rate = ( S_INF - s_gate ) / TAU_S;
	d_gate_rate = ( D_INF - d_gate ) / TAU_D;
	f_gate_rate = ( F_INF - f_gate ) / TAU_F;

	if (FCa_INF > F_Ca && Voltage > -60) {
		F_Ca_rate = 0;
	} else {
		F_Ca_rate = ( FCa_INF - F_Ca ) / taufca;
	}
	if (G_INF > g_gate && Voltage > -60) {
		g_gate_rate = 0;
	} else {
		g_gate_rate = ( G_INF - g_gate ) / taug;
	}
	// mL_gate_rate
	//             (                         AmL                                  ) * (1-mL_gate) -
	mL_gate_rate = (0.32 * (Voltage + 47.13) / ( 1 - exp( -0.1*(Voltage+47.13) ) )) * (1-mL_gate) -
	//  (       BmL                 ) * mL_gate
		(0.08 * exp( Voltage/-11.0 )) * mL_gate;
	
	// hL_gate_rate
	//             ( (     HL_INF                )-hL_gate) / TAU_HL;
	hL_gate_rate = ( (1/(1+exp((Voltage+91)/6.1)))-hL_gate) /  600;

//	std::clog << t << "------------------------------------------------------\n";
//	//std::clog << std::fixed;
//	
//	std::clog << std::setw(9) << Voltage << " ";
//	std::clog << std::setw(9) << Ca_i_total << " ";
//	std::clog << std::setw(9) << Ca_SR_total << " ";
//	std::clog << std::setw(9) << Na_i << " ";
//	std::clog << std::setw(9) << K_i << " ";
//	std::clog << std::setw(9) << m_gate << " ";
//	std::clog << std::setw(9) << h_gate << " ";
//	std::clog << std::setw(9) << j_gate << " ";
//	std::clog << std::setw(9) << X_r1_gate << " ";
//	std::clog << std::setw(9) << X_r2_gate << " ";
//	std::clog << std::setw(9) << X_s_gate << " ";
//	std::clog << std::setw(9) << r_gate << " ";
//	std::clog << std::setw(9) << s_gate << " ";
//	std::clog << std::setw(9) << d_gate << " ";
//	std::clog << std::setw(9) << f_gate << " ";
//	std::clog << std::setw(9) << F_Ca << " ";
//	std::clog << std::setw(9) << g_gate << " ";
//	std::clog << std::setw(9) << mL_gate << " ";
//	std::clog << std::setw(9) << hL_gate << " ";
//	std::clog << "\n";
//
//	std::clog << std::setw(9) << Voltage_rate << " ";
//	std::clog << std::setw(9) << Ca_i_total_rate << " ";
//	std::clog << std::setw(9) << Ca_SR_total_rate << " ";
//	std::clog << std::setw(9) << Na_i_rate << " ";
//	std::clog << std::setw(9) << K_i_rate << " ";
//	std::clog << std::setw(9) << m_gate_rate << " ";
//	std::clog << std::setw(9) << h_gate_rate << " ";
//	std::clog << std::setw(9) << j_gate_rate << " ";
//	std::clog << std::setw(9) << X_r1_gate_rate << " ";
//	std::clog << std::setw(9) << X_r2_gate_rate << " ";
//	std::clog << std::setw(9) << X_s_gate_rate << " ";
//	std::clog << std::setw(9) << r_gate_rate << " ";
//	std::clog << std::setw(9) << s_gate_rate << " ";
//	std::clog << std::setw(9) << d_gate_rate << " ";
//	std::clog << std::setw(9) << f_gate_rate << " ";
//	std::clog << std::setw(9) << F_Ca_rate << " ";
//	std::clog << std::setw(9) << g_gate_rate << " ";
//	std::clog << std::setw(9) << mL_gate_rate << " ";
//	std::clog << std::setw(9) << hL_gate_rate << " ";
//	std::clog << t << "\n";
//
//
//	std::clog << INa << " ";
//	std::clog << IbNa << " ";
//	std::clog << ICaL << " ";
//	std::clog << INaCa << " ";
//	std::clog << IpCa << " ";
//	std::clog << IbCa << " ";
//	std::clog << IKr << " ";
//	std::clog << IKs << " ";
//	std::clog << IK1 << " ";
//	std::clog << INaK << " ";
//	std::clog << Ito << " ";
//	std::clog << IpK << " ";
//	std::clog << INaL << " ";
//	std::clog << "\n";
//	
//	std::clog << Voltage-Ek << "\n";
//	std::clog << Ek << "\n";
//	std::clog << Ko << "\n";
//	std::clog << Cao << "\n";
//	std::clog << K_i<< "(K_i)\n";
//	if (std::isnan(Voltage_rate))
//		assert(1==0);
}

std::array<double, 74> Model_xiaepi::intermediates; 

void Model_xiaepi::print_intermediates() {
	for (auto item : intermediates)
		std::clog << item << "\n";
}

std::array<double, 52> Model_xiaepi::constants;

void Model_xiaepi::set_constants() {
	Ko			= 5.4;
	Cao			= 2.0;
	Nao			= 140.0;
	Vc			= 0.016404;
	Vsr			= 0.001094;
	Bufc		= 0.15;
	Kbufc		= 0.001;
	Bufsr		= 10.0;
	Kbufsr		= 0.3;
	taufca		= 2.0;
	taug		= 2.0;
	Vmaxup		= 0.000425;
	Kup			= 0.00025;
	R			= 8314.472;
	F			= 96485.3415;
	T			= 310.0;
	RTONF		= 26.7137606597;
	CAPACITANCE = 0.185;
	Gkr			= 0.096;
	pKNa		= 0.03;
	GK1			= 5.405;
	GNa			= 14.838;
	GbNa		= 0.00029;
	KmK			= 1.0;
	KmNa		= 40.0;
	knak		= 1.362;
	GCaL		= 0.000175;
	GbCa		= 0.000592;
	knaca		= 1000;
	KmNai		= 12.3; // 87.5; <- tentusscher
	KmCa		= 1.38;
	ksat		= 0.27; // 0.1; <- tentusscher
	n			= 0.35;
	GpCa		= 0.825;
	KpCa		= 0.0005;
	GpK			= 0.0146;
	FONRT		= 0.0374338908227;
	inverseVcF2 = 0.000315906749849;
	inverseVcF	= 0.000631813499697;
	Kupsquare	= 6.25e-08;
	eta         = 0.35;
	KmNao       = 87.5;
	KmCai       = 0.0036;
	KmCao       = 1.3;
	KmCaact     = 1.25e-4;
	#if MODEL==XIAEPI
		GNaL	= 0.0065;
		numax   = 5;
		Gto		= 0.294;
		Gks		= 0.245;
	#elif MODEL==XIAENDO
		GNaL	= 0.0065;
		numax   = 3.9;
		Gto		= 0.053;
		Gks		= 0.225;
	#elif MODEL==XIAMCELL
		GNaL	= 0.0095;
		numax   = 4.3;
		Gks		= 0.062;
		Gto		= 0.238;
	#endif
	Nao_cubed   = Nao*Nao*Nao;
	KmNao_cubed = KmNao*KmNao*KmNao;
	KmNai_cubed = KmNai*KmNai*KmNai;
}
