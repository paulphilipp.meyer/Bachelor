#include "Model_ttepi.h"

Model_ttepi::Model_ttepi() {
	state.push_back( -86.2 ); // Voltage
	state.push_back( 0.0097 ); // Ca_i_total
	state.push_back( 0.27692 ); // Ca_SR_total
	state.push_back( 11.6 ); // Na_i
	state.push_back( 138.3 ); // K_i
	state.push_back( 3.1436e-5 ); // m_gate
	state.push_back( 0.75 ); // h_gate
	state.push_back( 0.75 ); // j_gate
	state.push_back( 0 ); // X_r1_gate
	state.push_back( 1 ); // X_r2_gate
	state.push_back( 0 ); // X_s_gate
	state.push_back( 0 ); // r_gate
	state.push_back( 1 ); // s_gate
	state.push_back( 0 ); // d_gate
	state.push_back( 1 ); // f_gate
	state.push_back( 1 ); // F_Ca
	state.push_back( 1 ); // g_gate
}

unsigned int Model_ttepi::get_N_states() {
	return 17;
}

rhs_type Model_ttepi::get_rhs() {
	return Model_ttepi::rhs;
}

void Model_ttepi::rhs(const state_type &x, state_type &dxdt, const double t) {
	// model: ttepi
	// intermediate results:
	intermediates[0] = constants[5] + constants[6] - x[1];
	intermediates[1] = -1.0L/2.0L*intermediates[0] + (1.0L/2.0L)*sqrt(4*constants[6]*x[1] + pow(intermediates[0], 2));
	intermediates[2] = -1.0L/2.0L*intermediates[3] + (1.0L/2.0L)*sqrt(4*constants[8]*x[2] + pow(intermediates[3], 2));
	intermediates[3] = constants[7] + constants[8] - x[2];
	intermediates[4] = constants[16]*log(constants[0]/x[4]);
	intermediates[5] = constants[16]*log(constants[2]/x[3]);
	intermediates[6] = 0.1/(6.14421235332821e-6*exp(-0.06*intermediates[4] + 0.06*x[0]) + 1.0);

	// rates:
	enthält fehler in stepfca
	
	dxdt[0] = -I_stim - 0.430331482911935*sqrt(constants[0])*constants[18]*x[8]*x[9]*(-intermediates[4] + x[0]) - 1.0*constants[0]*constants[27]*x[3]/((constants[0] + constants[25])*(constants[26] + x[3])*(0.1245*exp(-0.1*constants[38]*x[0]) + 1.0 + 0.0353*exp(-constants[38]*x[0]))) - 4*constants[14]*constants[28]*constants[38]*x[0]*x[13]*x[14]*x[15]*(-0.341*constants[1] + intermediates[1]*exp(2*constants[38]*x[0]))/(exp(2*constants[38]*x[0]) - 1.0) - constants[20]*pow(x[10], 2)*(-constants[16]*log((constants[0] + constants[19]*constants[2])/(constants[19]*x[3] + x[4])) + x[0]) - constants[21]*intermediates[6]*(-intermediates[4] + x[0])/(intermediates[6] + (0.367879441171442*exp(-0.1*intermediates[4] + 0.1*x[0]) + 3.06060402008027*exp(-0.0002*intermediates[4] + 0.0002*x[0]))/(exp(-intermediates[4] - 0.5*x[0]) + 1.0)) - constants[22]*x[11]*x[12]*(-intermediates[4] + x[0]) - constants[23]*pow(x[5], 3)*x[6]*x[7]*(-intermediates[5] + x[0]) - constants[24]*(-intermediates[5] + x[0]) - constants[29]*(-0.5*constants[16]*log(constants[1]/intermediates[1]) + x[0]) - 1.0*constants[30]*(constants[1]*pow(x[3], 3)*exp(constants[34]*constants[38]*x[0]) - 2.5*pow(constants[2], 3)*intermediates[1]*exp(constants[38]*x[0]*(constants[34] - 1)))/((constants[1] + constants[32])*(pow(constants[2], 3) + pow(constants[31], 3))*(constants[33]*exp(constants[38]*x[0]*(constants[34] - 1)) + 1)) - constants[35]*intermediates[1]/(constants[36] + intermediates[1]) - 1.0*constants[37]*(-intermediates[4] + x[0])/(65.4052157419383*exp(-0.167224080267559*x[0]) + 1.0);
	dxdt[1] = -constants[11]/(constants[41]/pow(intermediates[1], 2) + 1.0) + constants[17]*constants[39]*(-4*constants[14]*constants[28]*constants[38]*x[0]*x[13]*x[14]*x[15]*(-0.341*constants[1] + intermediates[1]*exp(2*constants[38]*x[0]))/(exp(2*constants[38]*x[0]) - 1.0) - constants[29]*(-0.5*constants[16]*log(constants[1]/intermediates[1]) + x[0]) + 2.0*constants[30]*(constants[1]*pow(x[3], 3)*exp(constants[34]*constants[38]*x[0]) - 2.5*pow(constants[2], 3)*intermediates[1]*exp(constants[38]*x[0]*(constants[34] - 1)))/((constants[1] + constants[32])*(pow(constants[2], 3) + pow(constants[31], 3))*(constants[33]*exp(constants[38]*x[0]*(constants[34] - 1)) + 1)) - constants[35]*intermediates[1]/(constants[36] + intermediates[1])) - 8.0e-5*intermediates[1] + 8.0e-5*intermediates[2] + x[13]*x[16]*(0.016464*pow(intermediates[2], 2)/(pow(intermediates[2], 2) + 0.0625) + 0.008232);
	dxdt[2] = constants[3]*(constants[11]/(constants[41]/pow(intermediates[1], 2) + 1.0) + 8.0e-5*intermediates[1] - 8.0e-5*intermediates[2] - x[13]*x[16]*(0.016464*pow(intermediates[2], 2)/(pow(intermediates[2], 2) + 0.0625) + 0.008232))/constants[4];
	dxdt[3] = constants[17]*constants[40]*(-3.0*constants[0]*constants[27]*x[3]/((constants[0] + constants[25])*(constants[26] + x[3])*(0.1245*exp(-0.1*constants[38]*x[0]) + 1.0 + 0.0353*exp(-constants[38]*x[0]))) - constants[23]*pow(x[5], 3)*x[6]*x[7]*(-intermediates[5] + x[0]) - constants[24]*(-intermediates[5] + x[0]) - 3.0*constants[30]*(constants[1]*pow(x[3], 3)*exp(constants[34]*constants[38]*x[0]) - 2.5*pow(constants[2], 3)*intermediates[1]*exp(constants[38]*x[0]*(constants[34] - 1)))/((constants[1] + constants[32])*(pow(constants[2], 3) + pow(constants[31], 3))*(constants[33]*exp(constants[38]*x[0]*(constants[34] - 1)) + 1)));
	dxdt[4] = constants[17]*constants[40]*(-I_stim - 0.430331482911935*sqrt(constants[0])*constants[18]*x[8]*x[9]*(-intermediates[4] + x[0]) + 2.0*constants[0]*constants[27]*x[3]/((constants[0] + constants[25])*(constants[26] + x[3])*(0.1245*exp(-0.1*constants[38]*x[0]) + 1.0 + 0.0353*exp(-constants[38]*x[0]))) - constants[20]*pow(x[10], 2)*(-constants[16]*log((constants[0] + constants[19]*constants[2])/(constants[19]*x[3] + x[4])) + x[0]) - constants[21]*intermediates[6]*(-intermediates[4] + x[0])/(intermediates[6] + (0.367879441171442*exp(-0.1*intermediates[4] + 0.1*x[0]) + 3.06060402008027*exp(-0.0002*intermediates[4] + 0.0002*x[0]))/(exp(-intermediates[4] - 0.5*x[0]) + 1.0)) - constants[22]*x[11]*x[12]*(-intermediates[4] + x[0]) - 1.0*constants[37]*(-intermediates[4] + x[0])/(65.4052157419383*exp(-0.167224080267559*x[0]) + 1.0));
	dxdt[5] = 1.0*(-x[5] + 1.0/pow(0.00184221158116513*exp(-0.110741971207087*x[0]) + 1.0, 2))*(6.14421235332821e-6*exp(-0.2*x[0]) + 1.0)/(0.1/(1096.63315842846*exp(0.2*x[0]) + 1.0) + 0.1/(0.778800783071405*exp(0.005*x[0]) + 1.0));
	dxdt[6] = (-x[6] + 1.0/pow(15212.5932856544*exp(0.134589502018843*x[0]) + 1.0, 2))/((1 - 1.0/(exp((-41.6666666666667)*(x[0] + 40)) + 1))*(0.0646209624466736*exp(-0.0900900900900901*x[0]) + 0.168831168831169) + 1.0/((exp((-41.6666666666667)*(x[0] + 40)) + 1)*(4.43126792958051e-7*exp(-0.147058823529412*x[0]) + 2.7*exp(0.079*x[0]) + 310000.0*exp(0.3485*x[0]))));
	dxdt[7] = (-x[7] + 1.0/pow(15212.5932856544*exp(0.134589502018843*x[0]) + 1.0, 2))/(1.66666666666667*(1 - 1.0/(exp((-41.6666666666667)*(x[0] + 40)) + 1))*(0.0407622039783662*exp(-0.1*x[0]) + 1.0)*exp(-0.057*x[0]) + 1.0/(((x[0] + 37.78)*(-6.948e-6*exp(-0.04391*x[0]) - 25428.0*exp(0.2444*x[0]))/(50262745825.954*exp(0.311*x[0]) + 1.0) + 0.02424*exp(-0.01052*x[0])/(0.00396086833990426*exp(-0.1378*x[0]) + 1.0))*(exp((-41.6666666666667)*(x[0] + 40)) + 1)));
	dxdt[8] = 0.00037037037037037*(-x[8] + 1.0/(0.0243728440732796*exp(-0.142857142857143*x[0]) + 1.0))*(0.0111089965382423*exp(-0.1*x[0]) + 1.0)*(13.5813245225782*exp(0.0869565217391304*x[0]) + 1.0);
	dxdt[9] = 0.297619047619048*(-x[9] + 1.0/(39.1212839981532*exp(0.0416666666666667*x[0]) + 1.0))*(0.0497870683678639*exp(-0.05*x[0]) + 1.0)*(0.0497870683678639*exp(0.05*x[0]) + 1.0);
	dxdt[10] = 0.000909090909090909*sqrt(1.0 + 0.188875602837562*exp(-1.0L/6.0L*x[0]))*(-x[10] + 1.0/(0.69967253737513*exp(-0.0714285714285714*x[0]) + 1.0))*(0.0497870683678639*exp(0.05*x[0]) + 1.0);
	dxdt[11] = (-x[11] + 1.0/(28.0316248945261*exp(-0.166666666666667*x[0]) + 1.0))/(9.5*exp(0.000555555555555556*(-x[0] - 40.0)*(x[0] + 40.0)) + 0.8);
	dxdt[12] = (-x[12] + 1.0/(54.5981500331442*exp(0.2*x[0]) + 1.0))/(85.0*exp(0.003125*(-x[0] - 45.0)*(x[0] + 45.0)) + 3.0 + 5.0/(0.0183156388887342*exp(0.2*x[0]) + 1.0));
	dxdt[13] = (-x[13] + 1.0/(0.513417119032592*exp(-0.133333333333333*x[0]) + 1.0))/(1.4*(0.25 + 1.4/(exp(-1.0L/13.0L*x[0] - 35.0L/13.0L) + 1.0))/(exp((1.0L/5.0L)*x[0] + 1) + 1.0) + 1.0/(exp(-1.0L/20.0L*x[0] + 5.0L/2.0L) + 1.0));
	dxdt[14] = (-x[14] + 1.0/(exp((1.0L/7.0L)*x[0] + 20.0L/7.0L) + 1.0))/(1125*exp((1.0L/300.0L)*(-x[0] - 27)*(x[0] + 27)) + 80 + 165/(exp(-1.0L/10.0L*x[0] + 5.0L/2.0L) + 1.0));
	dxdt[15] = 1.0*(-x[15] + 0.157534246575342 + 0.0684931506849315/(0.00673794699908547*exp(10000.0*intermediates[1]) + 1.0) + 0.136986301369863/(0.391605626676799*exp(1250.0*intermediates[1]) + 1.0) + 0.684931506849315/(8.03402376701711e+27*pow(intermediates[1], 8) + 1.0))/(constants[9]*(exp(41.6666666666667*(x[0] + 60)) + 1)*(exp(5000*(-x[14] + 1.0/(exp((1.0L/7.0L)*x[0] + 20.0L/7.0L) + 1.0))) + 1));
	dxdt[16] = 1.0*(-x[16] + 1.0*(1 - 1.0/(exp((-41.6666666666667)*(intermediates[1] - 0.00035)) + 1))/(1.97201988740492e+55*pow(intermediates[1], 16) + 1.0) + 1.0/((5.43991024148102e+20*pow(intermediates[1], 6) + 1.0)*(exp((-41.6666666666667)*(intermediates[1] - 0.00035)) + 1)))/(constants[10]*(exp(41.6666666666667*(x[0] + 60)) + 1)*(exp(5000*(-x[16] + 1.0*(1 - 1.0/(exp((-41.6666666666667)*(intermediates[1] - 0.00035)) + 1))/(1.97201988740492e+55*pow(intermediates[1], 16) + 1.0) + 1.0/((5.43991024148102e+20*pow(intermediates[1], 6) + 1.0)*(exp((-41.6666666666667)*(intermediates[1] - 0.00035)) + 1)))) + 1));
}

std::array<double, 7> Model_ttepi::intermediates;

std::array<double, 42> Model_ttepi::constants = {{
	5.4, // Ko [0]
	2.0, // Cao [1]
	140.0, // Nao [2]
	0.016404, // Vc [3]
	0.001094, // Vsr [4]
	0.15, // Bufc [5]
	0.001, // Kbufc [6]
	10.0, // Bufsr [7]
	0.3, // Kbufsr [8]
	2.0, // taufca [9]
	2.0, // taug [10]
	0.000425, // Vmaxup [11]
	0.00025, // Kup [12]
	8314.472, // R [13]
	96485.3415, // F [14]
	310.0, // T [15]
	26.7137606597, // RTONF [16]
	0.185, // CAPACITANCE [17]
	0.096, // Gkr [18]
	0.03, // pKNa [19]
	0.245, // Gks [20]
	5.405, // GK1 [21]
	0.294, // Gto [22]
	14.838, // GNa [23]
	0.00029, // GbNa [24]
	1.0, // KmK [25]
	40.0, // KmNa [26]
	1.362, // knak [27]
	0.000175, // GCaL [28]
	0.000592, // GbCa [29]
	1000, // knaca [30]
	87.5, // KmNai [31]
	1.38, // KmCa [32]
	0.1, // ksat [33]
	0.35, // n [34]
	0.825, // GpCa [35]
	0.0005, // KpCa [36]
	0.0146, // GpK [37]
	0.0374338908227, // FONRT [38]
	0.000315906749849, // inverseVcF2 [39]
	0.000631813499697, // inverseVcF [40]
	6.25e-08 // Kupsquare [41]
}};
