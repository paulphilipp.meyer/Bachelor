/* options:
 * 1		2	3			4		5			6	7		8
 * model	T	dir_data	DEBUG	init_state	dt	abserr	relerr*/
#include "Cellmodel.h"
#include <limits.h>
#include "cnpy.h"

using namespace boost::numeric::odeint;

int DEBUG;

int main(int argc, char* argv[]){

// parse args
	std::string model = argv[1];
	double T = std::stod(argv[2]);
	std::string dir_data = argv[3];
	DEBUG = std::stoi(argv[4]);
	std::string init_state_str = argv[5];
	double dt = std::stod(argv[6]);
	double abs_err = std::stod(argv[7]);
	double rel_err = std::stod(argv[8]);
	
// initialize simulation 
	Cellmodel* sim = make_model( model );
	if (init_state_str.compare("None")!=0) {
		sim->init(init_state_str);
	}
	if (DEBUG > 2) {
		std::clog << "initialized with:\n";
		sim->print_state();
	}

	runge_kutta_fehlberg78 < state_type > rk78;
	auto stepper = make_controlled( abs_err, rel_err, rk78 );

	rhs_type rhs = sim->get_rhs();

	std::vector< double > states;
	unsigned int states_shape[2];
	states_shape[1] = sim->get_N_states();
	std::vector< double > times;
	unsigned int times_shape[1];
	pushback_state save_s = pushback_state(states, times);
	unsigned int integration_steps;

// simulate
	I_stim = 80.0;
	integrate_adaptive( stepper, rhs, sim->state, 0.0, 0.5, dt, save_s );
	I_stim = 0.0;
	integrate_adaptive( stepper, rhs, sim->state, 0.5, 280.0, dt, save_s );
	integrate_adaptive( rk78, rhs, sim->state, 280.0, T, 0.02, save_s );

	// save block
	integration_steps = times.size();
	times_shape[0] = states_shape[0] = integration_steps;
	cnpy::npy_save(dir_data + "/times.npy", &times[0], times_shape, 1, "a");
	cnpy::npy_save(dir_data + "/states.npy", &states[0], states_shape, 2, "a");
	times.clear();
	states.clear();

	delete sim;
	return 0;
}
